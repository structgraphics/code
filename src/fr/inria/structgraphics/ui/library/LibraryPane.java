package fr.inria.structgraphics.ui.library;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.json.JsonArray;
import javax.json.JsonObject;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.MarkFactory;
import fr.inria.structgraphics.graphics.SimpleMark;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.persistence.JsonDescription;
import fr.inria.structgraphics.persistence.JsonObjectReader;
import fr.inria.structgraphics.persistence.JsonObjectWriter;
import fr.inria.structgraphics.ui.DisplayPreferences;
import fr.inria.structgraphics.ui.viscanvas.Canvas;
import fr.inria.structgraphics.ui.viscanvas.CanvasFrame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.transform.Transform;
import javafx.stage.WindowEvent;

public class LibraryPane extends BorderPane {

	public final String DRAG_CODE= "LiBRArY";
	
	private ArrayList<Mark> libraryComponents = new ArrayList<>();
	
	private VisFrame fakeframe;
	
	private CanvasFrame canvasFrame;
	
	private VBox items;
    private Canvas canvas;
    	
	public LibraryPane(CanvasFrame canvasFrame) {
		items = new VBox(0);
		
		this.canvasFrame = canvasFrame;

		canvas = new Canvas();
		canvas.getChildren().add(items);
		
		canvas.setStyle("-fx-background-color: #FFFFFF;");
		items.setStyle("-fx-background-color: #FFFFFF;");
		
		fakeframe =  MarkFactory.createVisFrame(DisplayPreferences.CANVAS_WIDTH, DisplayPreferences.CANVAS_HEIGHT);
		
		canvasFrame.getCanvas().setOnDragOver(new EventHandler<DragEvent>() { 
		    public void handle(DragEvent event) {
		    	Dragboard db = event.getDragboard();
		    	
		    	//TODO: I need to fix that so that it only activate when
		    	SimpleMark mark = canvasFrame.getDragged();
		        if (mark != null && db.hasString() && db.getString().equals(DRAG_CODE)) {
		            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);		           		            
		        }
	            event.consume();
		    }
		});
				
		canvasFrame.getCanvas().setOnDragDropped(new EventHandler <DragEvent>() {
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasString() && db.getString().equals(DRAG_CODE)) {
                	addMarkToCanvas(canvasFrame.getDragged(), event.getX(), event.getY());
                	
                    success = true;
                }
                event.setDropCompleted(success);
                
                event.consume();
            }
        });
		
		//load();
	}
	
	public ArrayList<Mark> getComponents() {
		return libraryComponents;
	}
	
	private void addMarkToCanvas(SimpleMark mark, double x, double y) {
		SimpleMark copy = mark.createCopy(canvasFrame.getVisFrame(), 0, 0);
		copy.initProperties();
		copy.getCoords().x.set(x);
		copy.getCoords().y.set(copy.getRootHeight() - y);
		copy.update();
		
		canvasFrame.getInspector().update();
	}
	
	public Canvas getCanvas() {
		return canvas;
	}
		
	public void addToLibrary(SimpleMark mark) {
		SimpleMark copy = mark.createCopy(fakeframe, 0, 0);
		copy.initProperties();
		copy.update();
		libraryComponents.add(mark);
		addToList(copy);
		
		save();
	}
	
	private void addToList(SimpleMark mark) {		
		Group group = mark.getGroup();
	
		//double w = group.getBoundsInLocal().getWidth();
		double scalex = Math.min(1, ( CanvasFrame.LIB_WIDTH - 60) / mark.width());
		double scaley = Math.min(1, 100/mark.height());
		double scale = Math.min(scalex, scaley);
			
		SnapshotParameters params = new SnapshotParameters();
		params.setTransform(Transform.scale(scale, scale));
		
		ImageView imageView = new ImageView();
		imageView.setImage(group.snapshot(params, null));
		
		HBox hbox = new HBox(0);
		hbox.setAlignment(Pos.CENTER);
		//hbox.setPadding(new Insets(10, 10, 10, 10));
		hbox.setStyle("-fx-padding: 8;" + "-fx-border-style: solid inside;"
		        + "-fx-border-width: 2;" + "-fx-border-insets: 5;"
		        + "-fx-border-radius: 5;" + "-fx-border-color: #aaaadd;");
		
		hbox.getChildren().add(imageView);
		
		items.getChildren().add(hbox);
		
        hbox.setOnDragDetected(
        		new EventHandler<MouseEvent>() { 
        			public void handle(MouseEvent event) {   		
		                Dragboard db = imageView.startDragAndDrop(TransferMode.COPY);
		                ClipboardContent content = new ClipboardContent();
		                content.putString(DRAG_CODE);
		                db.setContent(content);
		                
		                canvasFrame.setDraggable(mark);
		                event.consume();
        			}
        });
        
        hbox.setOnMousePressed(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) { 
				
				if(event.isSecondaryButtonDown()) {
					ContextMenu contextMenu = new ContextMenu();
				  
			        MenuItem item = new MenuItem("Remove from Library");
			        item.setOnAction(new EventHandler<ActionEvent>() {
			            @Override
			            public void handle(ActionEvent event) {			     
			            	libraryComponents.remove(items.getChildren().indexOf(hbox));
			            	items.getChildren().remove(hbox);
			            	save();
			            }
			        });
			        
			        contextMenu.getItems().add(item);
			        
			        contextMenu.setOnHiding(new EventHandler<WindowEvent>() {
						@Override
						public void handle(WindowEvent event) {
							canvasFrame.getSplitPane().setContextMenu(null);
						}
					});
			        
			        canvasFrame.getSplitPane().setContextMenu(contextMenu);
			}
		}
      });
	}

	public void clear() {
		items.getChildren().clear();
	}
	
	public void load() { 
		File libfile = new File("./sessions/library.json");
		if(libfile == null) return;

		JsonArray visArray;
		JsonDescription descr = new JsonDescription(libfile);
		visArray = descr.isLibrary() ? descr.getLibraryArray(false) : descr.getVisArray(false);		
		load(visArray);
	}
	
	public void load(JsonArray visArray) { 		
		if(visArray != null && !visArray.isEmpty())
			libraryComponents = JsonObjectReader.readChildrenFromJasonArray(fakeframe, visArray);
				
		for(Mark mark:libraryComponents) {
			addToList((SimpleMark)mark);
		}
	}
	
	public void save() {
		if(libraryComponents.isEmpty()) return;
		
		File libfile = new File("./sessions/library.json");
		if(libfile == null) return;
		else {
			JsonObject visualizations = 
					JsonObjectWriter.saveToJason(libraryComponents, true);
			try {
				FileWriter writer = new FileWriter(libfile, false);
				writer.write(JsonObjectWriter.fromObjectToReadableString(visualizations, 0).toString());
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
