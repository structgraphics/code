package fr.inria.structgraphics.ui.inspector;

import javafx.util.StringConverter;

public class GeneralStringConverter extends StringConverter {

	@Override
	public String toString(Object object) {
		return object.toString();
	}

	@Override
	public Object fromString(String string) {
		return string;
	}

}
