package fr.inria.structgraphics.ui.inspector;

import java.util.Collection;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.ui.inspector.PropertiesTab.Type;
import javafx.beans.property.Property;
import javafx.geometry.Pos;
import javafx.scene.Node;


public class BasePropertiesPane extends PropertiesPane {
	        
    protected IndividualPropertiesGrid basegrid;
    protected TitledIndividualPropertiesPane sharedpane;
    
    protected TitledTablePane tabularpane;
    protected TabularPropertiesGrid fulltabularpane;

    static PropertyDetail activeDetail;    
    
    protected Container node;
    
    public BasePropertiesPane(InspectorView inspector, String name, Container node) {
    	this(inspector, name, node, Type.SELF);

    }
    
    public BasePropertiesPane(InspectorView inspector, String name, Container node, Type type) {
    	super(inspector, name);
    	this.node = node;
    	
        getStyleClass().add("detail-pane");
        setManaged(false);
        setVisible(false);
        setExpanded(false);
        setMaxWidth(Double.MAX_VALUE);
        setId("title-label");
        setAlignment(Pos.CENTER_LEFT);
        
        if(type == Type.SELF || type == Type.GROUP || type == Type.PUBLIC) {
            basegrid = new IndividualPropertiesGrid(node, type);
            addPane(basegrid);
        } else if(type == Type.CHILDREN) {
            sharedpane = new TitledIndividualPropertiesPane("shared", new IndividualPropertiesGrid(node, type));
            addPane(sharedpane);
        }
	}

	protected void addToPane(final Node... nodes) {
        if(basegrid != null) basegrid.addToPane(nodes);
        else if(sharedpane != null) sharedpane.addToPane(nodes);
    }
    
    @Override
    protected void clearPane() {
    	if(basegrid != null) basegrid.clearPane();
    	if(sharedpane != null) sharedpane.clearPane();
        if(tabularpane != null) tabularpane.clearPane();
        if(fulltabularpane != null) fulltabularpane.clearPane();
    }

    @Override
    public void addDetails(final Collection<Property> properties) {
    	if(basegrid != null) basegrid.addDetails(properties);
    	else if(sharedpane != null) sharedpane.addDetails(properties);
    }
    
    
    @Override
    public void refresh() {
    	if(basegrid != null) basegrid.refresh();
    	if(sharedpane != null) sharedpane.refresh();
    	if(tabularpane != null) tabularpane.refresh();
    	if(fulltabularpane != null) fulltabularpane.refresh();
    }

    public void update() {
    	refresh();
    }

}
