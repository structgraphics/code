package fr.inria.structgraphics.ui.inspector;

import java.util.Map;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnections;

public class FlowConnectionsTable extends PropertiesTable {

	private FlowConnections flowConnections = null;
		
	public FlowConnectionsTable(String name, FlowConnections flowConnections, Container node) {
		super(name, node);
		
		this.full = false;
		this.flowConnections = flowConnections;
		
		props = flowConnections.getVariableList();
		addEntries(props);
	}
	
	@Override
	protected void addEntries(Map<PropertyName, FlexibleListProperty>  tabproperties) {
		for(PropertyName name: tabproperties.keySet()) {
			FlexibleListProperty properties = tabproperties.get(name);

			PropertyDetailColumn column = new FlowPropertyDetailColumn(this, properties, name, node);
    		column.add(properties);
    		add(column);	
		}
	}
	
	@Override
	public boolean isNetwork() {
		return true;
	}
	
	@Override
	public int getColumnsNum() { // TO Change
		return 3;
	}
	
	@Override
	public int getRowsNum() {
		if(flowConnections.isEmpty()) return 0;
		else return flowConnections.size() + 1;
	}
}
