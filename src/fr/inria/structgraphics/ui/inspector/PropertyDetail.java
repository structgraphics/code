package fr.inria.structgraphics.ui.inspector;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import javafx.util.StringConverter;

import java.util.ArrayList;
import java.util.List;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.graphics.VisGroup;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.ColoringSchemeProperty;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.LineTypeProperty;
import fr.inria.structgraphics.types.OpacityProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.RotationProperty;
import fr.inria.structgraphics.types.ShapeProperty;
import fr.inria.structgraphics.types.Shareable;
import fr.inria.structgraphics.types.StrokeWidthProperty;
import fr.inria.structgraphics.types.AlignmentProperty.XSticky;
import fr.inria.structgraphics.types.AlignmentProperty.YSticky;
import fr.inria.structgraphics.types.DistributionProperty.Constraint;
import fr.inria.structgraphics.ui.Draggable;
import fr.inria.structgraphics.ui.inspector.detailmodel.SimpleSerializer;
import fr.inria.structgraphics.ui.inspector.util.TooltipCreator;

public class PropertyDetail implements Draggable {

	public enum EditionType {
		COMBO, SLIDER, COLOR_PICKER, TEXT, NONE
	};

    private List<Property> properties;    
	public Label label;
	private List<SimpleSerializer> serializers = new ArrayList<>();
	
	public HBox valueLabel = new HBox(4);
	private Node graphic_unlock, graphic_lock;
	public Label lockIcon = null;

    private double min, max;
    
    private EditionType editionType = EditionType.TEXT;
    
    private Object[] validItems; // TODO: Not sure if used for any type of data. Let's see.
    
    private Node[] fields;
    private Label tmplabel;
    private int tmpindex;
	
    private boolean tabular = false;
    private boolean publicPane = false;
    
    private PropertyName id = null;
    private Container node;
	private Mark temporaryFocus = null;

	public PropertyDetail(Property property, boolean tabular, boolean publicPane, PropertyName id, Container node) {
		this.tabular = tabular;
		this.publicPane = publicPane;
		
		this.node = node;
		this.id = id;
		label = new Label(tabular ? "" : property.getName() + ":");
		label.setContentDisplay(ContentDisplay.LEFT);
			
		if(property instanceof FlexibleListProperty) {
			properties = ((FlexibleListProperty)property).flatten();
						
           	for(Property prop: properties) {
           		valueLabel.getChildren().add(new Label());
           		serializers.add(new SimpleSerializer(prop));
           	}			
		} else {
			valueLabel.getChildren().add(new Label());
			properties = new ArrayList<>();
			properties.add(property);
			serializers.add(new SimpleSerializer(property));
		}
		
        // TODO
        editionType = serializers.get(0).getEditionType();

        // TODO
        Object val = properties.get(0).getValue();
        if(val instanceof RefX) {
        	setValidItems(RefX.values());
        } else if(val instanceof RefY) {
        	setValidItems(RefY.values());
        } else if(val instanceof YSticky) {
        	setValidItems(YSticky.values());
        } else if(val instanceof XSticky) {
        	setValidItems(XSticky.values());
        }  else if(val instanceof Constraint) {
        	setValidItems(Constraint.values());
        } else if(val instanceof ShapeProperty.Type) {
        	setValidItems(ShapeProperty.Type.values());
        } else if(val instanceof LineTypeProperty.Type) {
        	setValidItems(LineTypeProperty.Type.values());
        } else if(val instanceof ColoringSchemeProperty.Scheme) {
        	setValidItems(ColoringSchemeProperty.Scheme.values());
        } else if(properties.get(0) instanceof OpacityProperty) {
        	setMinMax(0, 1);
        } else if(properties.get(0) instanceof RotationProperty) {
        	setMinMax(0, 360);
        } else if(properties.get(0) instanceof StrokeWidthProperty) {
        	setMinMax(0, 8);
        }
         
        updateLabels();
        initInteractors();
        
        if(property instanceof Shareable) {
			label.setDisable(!((Shareable)property).getActiveProperty().getValue());
			valueLabel.setDisable(!((Shareable)property).getActiveProperty().getValue());
        	
        	((Shareable)property).getActiveProperty().addListener(new InvalidationListener() {
				@Override
				public void invalidated(Observable observable) {
					label.setDisable(!((Shareable)property).getActiveProperty().getValue());
					valueLabel.setDisable(!((Shareable)property).getActiveProperty().getValue());
				}
			});
        }  
        
        if(publicPane) {
        //	label.setStyle("-fx-text-fill: #FF8C00;");
        }
        
        
	}
	
	public PropertyDetail(Property property, boolean tabular, PropertyName id, Container node) {	
		this(property, tabular, false, id, node);
	}
    	
	public PropertyDetail(Property property, PropertyName id, Container node) {	
		this(property, false, false, id, node);
	}
	
	
	void updateLabel(Label label, Property property) {
		final Node graphic;
      
        if(editionType == EditionType.COLOR_PICKER) {
        	graphic = new Rectangle(20, 10);
        	((Rectangle)graphic).setFill((Color)property.getValue());
        	
        	property.addListener(new ChangeListener<Color>() {
				@Override
				public void changed(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
					((Rectangle)graphic).setFill(newValue);
				}
			});
        } else if(property instanceof ShapeProperty) {
        	graphic = new ImageView(DisplayUtils.getIcon(property.getValue().toString()));
        	property.addListener(new ChangeListener<ShapeProperty.Type>() {
				@Override
				public void changed(ObservableValue<? extends ShapeProperty.Type> observable, ShapeProperty.Type oldValue, ShapeProperty.Type newValue) {
					((ImageView)graphic).setImage(DisplayUtils.getIcon(property.getValue().toString()));
				}
			});
        } else if(property instanceof LineTypeProperty) {
        	graphic = new ImageView(DisplayUtils.getIcon(property.getValue().toString()));
        	property.addListener(new ChangeListener<LineTypeProperty.Type>() {
				@Override
				public void changed(ObservableValue<? extends LineTypeProperty.Type> observable, LineTypeProperty.Type oldValue, LineTypeProperty.Type newValue) {
					((ImageView)graphic).setImage(DisplayUtils.getIcon(property.getValue().toString()));
					
					Object owner = property.getBean();
					// TODO: Need to update the inspector!!!!!
					if(owner instanceof VisCollection) {
						((VisFrame)((VisCollection)owner).getRoot()).getInspector().updateTreeViewLabel((VisCollection)owner);
					}					
				}
			});
        } 
        else graphic = null;
        
        /// TODO: This has changed recently. It's hopefully correct!!!!
        StringConverter converter = null;
        if(property.getValue() instanceof Color)
        	converter = new ColorStringConverter();
        else if(property.getValue() instanceof RefX)
        	converter = new RefXStringConverter();
        else if(property.getValue() instanceof RefY)
        	converter = new RefYStringConverter();
        else if(property.getValue() instanceof YSticky)
        	converter = new XAlignmentStringConverter();
        else if(property.getValue() instanceof XSticky)
        	converter = new YAlignmentStringConverter();
        else if(property.getValue() instanceof Constraint)
        	converter = new DistributionStringConverter();
        else if(property instanceof FlexibleListProperty) // TODO: Here, I need to take care of multiple value labels
        	converter = new ListStringConverter();
        else if(property instanceof ShapeProperty)
        	converter = new ShapeStringConverter();
        else if(property instanceof LineTypeProperty)
        	converter = new LineTypeStringConverter();
        else if(property instanceof StrokeWidthProperty || property instanceof OpacityProperty)
        	converter = new DoubleStringConverter(true);
        else if(property instanceof ColoringSchemeProperty)
        	converter = new ColoringStringConverter();
        else if(property instanceof StringProperty)
        	converter = null;
        else converter = new DoubleStringConverter();
        
        if(graphic == null) {
        	// TODO: This needs to change completely!!!
            if(property instanceof FlexibleListProperty) label.textProperty().bind(new StringBinding() {
    			@Override
    			protected String computeValue() {
    				FlexibleListProperty list = (FlexibleListProperty)property;
    				return list.getValueAsString();
    			}
    		});
            else if(converter!= null) Bindings.bindBidirectional(label.textProperty(), property, converter); // TODO: This to change as well
            else label.textProperty().bind(property); 
        } else { // TODO: ... 
        	label.setGraphic(graphic);
        	label.setGraphicTextGap(1.5);
        }
                
        // Change that for tabular data
        if(tabular) label.setContentDisplay(ContentDisplay.RIGHT);  // TODO: ...   
	}
		
	void updateLabels() {
		for(int i = 0; i < properties.size(); ++i) {
			Node child = valueLabel.getChildren().get(i);
			if(child instanceof Label) updateLabel((Label)valueLabel.getChildren().get(i), properties.get(i));
		}
	}
	
	private void initInteractors() {
		fields = new Node[properties.size()];
		
		for(int i = 0; i < properties.size(); ++i) {
			initInteractor(i, (Label)valueLabel.getChildren().get(i), properties.get(i), serializers.get(i));
		}
	}
	
	// TODO: .....
	public void createLockIcon(boolean lock, boolean top) {
		lockIcon = new Label(" ");
		graphic_unlock = new Label("\u29BB"); //ImageView(DisplayUtils.getIcon("unlinked"));
		graphic_lock = top ? new Label("\u27F8") : new Label("\u27F9"); //ImageView(DisplayUtils.getIcon("linked"));
		lockIcon.setGraphic(lock ? graphic_lock : graphic_unlock);
		lockIcon.setGraphicTextGap(top ? 4.5 : 0);
	}
	
	public void createExpandIcon(boolean expand) {
		lockIcon = new Label();
		graphic_unlock = new Label("-");
		graphic_lock = new Label("+");
		lockIcon.setGraphic(expand ? graphic_lock : graphic_unlock);
		lockIcon.setGraphicTextGap(1.5);
	}
	

	public void switchLock(boolean lock) {
		lockIcon.setGraphic(lock ? graphic_lock : graphic_unlock);
	}
	
	private void addGroupSharingInteractor(Label label, Property property, Mark mark) {
		TooltipCreator.createTooltip(label, property.getName() +  ": Press Shift to Change Visibility"); 
		
		BooleanProperty publicProp = ((Shareable)property).getPublicProperty();
		
		decorateLabel(label, publicProp.get());
				
		label.setOnMouseMoved(new EventHandler<MouseEvent>(){
		      @Override  
		      public void handle(MouseEvent event) {
		    	 label.getScene().getWindow().requestFocus();
		    	 label.requestFocus();
		      }
		}); 
		label.setOnMouseExited(new EventHandler<MouseEvent>(){
		      @Override  
		      public void handle(MouseEvent event) {
		    	  label.getParent().requestFocus();
		    	  //label.setStyle("-fx-border-color: none;");
		      }
		}); 
        label.setOnKeyPressed(new EventHandler<KeyEvent>() {
					@Override
					public void handle(KeyEvent event) {
						if(event.isShiftDown()) {
							publicProp.set(!publicProp.get());
							
							Object owner = property.getBean();
							
							// TODO: Need to update the inspector!!!!!
							if(owner instanceof Mark) {
								VisBody virtualGroup = ((Mark)owner).getRootVirtualGroup();
								
								if(virtualGroup instanceof VisCollection) {
									((VisFrame)virtualGroup.getContainer()).getInspector().updateCollectionViews((VisCollection)virtualGroup);
								}
							}
							
							//label.setStyle(publicProp.get() ? "-fx-border-color: orange;" : "-fx-border-color: none;");
						}
					}
		});
        
        publicProp.addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				//label.setStyle(publicProp.get() ? "-fx-border-color: #FF8C00;" : "-fx-border-color: none;");
				
				decorateLabel(label, publicProp.get());
				//VisGroup topGroup = ((VisGroup)mark.getContainer()).getTopGroup();
				//ArrayList<Property> bindingsgroup = topGroup.getChildPropertyStructure().getBindingsOf(property);
				
			}
		});
        
	}
	
	private void decorateLabel(Label label, boolean flag) {
		if(!label.getText().isEmpty()) label.setUnderline(flag);
		else label.setStyle(flag ? "-fx-border-color: darkgrey; -fx-border-width:1.5px;" : "-fx-border-color: none;");

	}
	
	
	private void initInteractor(int num, Label label, Property property, SimpleSerializer serializer) {				
		Object bean = property.getBean();
		if(!publicPane && bean instanceof Mark && property instanceof Shareable && !(node instanceof VisCollection) &&
				(bean instanceof VisGroup || ((Mark)bean).getContainer() instanceof VisGroup)){
			addGroupSharingInteractor(label, property, (Mark)bean);
		}
			
        final Control control;
        switch (editionType) {
			case COMBO: {
	        	control = new ComboBox<Object>();
	        	control.getStyleClass().add("detail-field");
	        	((ComboBox<Object>)control).getSelectionModel().selectedItemProperty().addListener((o, oldValue, newValue) -> {
	        		if (newValue != null && !newValue.equals(property.getValue())) {
	        			serializer.setValue(newValue.toString());
	        			//updateLabel();
	        			recover();
	        		}
	        	});
	
	        	control.setOnMouseClicked(ev -> {
	        		if (ev.isSecondaryButtonDown()) {
	        			recover();
	        		}
	        	});
	     	
	        	break;
			}
	        case SLIDER: {
	        	control = new Slider();
	        	control.getStyleClass().add("detail-field");
	        	((Slider)control).valueProperty().addListener((o, oldValue, newValue) -> {
	        		serializer.setValue(newValue.toString());
	        		//updateLabel();
	        	});
		        	
	        	break;
	        }
	        case COLOR_PICKER: {
	        	control = new ColorPicker();
	        	control.getStyleClass().add("detail-field");
	        	((ColorPicker)control).valueProperty().addListener((o, oldValue, newValue) -> {
	        		serializer.setValue(newValue.toString());
	        		///updateLabels();
	        	});
	        	
	        	break;
	        }
	        default: {	        	
	        	control = new TextField(); // TODO: Enable a drag-based text-field modifier....
	        		        	
	        	control.setPrefWidth(50);
	        	control.getStyleClass().add("detail-field");
	      	
	        	((TextField)control).setOnAction(ev -> {
	        		if (serializer != null) {
	        			serializer.setValue(((TextField)control).getText());
	        		}
	
	        		//updateLabel();
	        		recover();
	        	});
	        }
		}
        
    	PropertyDetail.this.fields[num] = control;
    	
    	 	
    	label.setOnMouseClicked(event -> { // TODO: ... 
    		
    		Object container = property.getBean();
    		if(container != null && container instanceof Mark && !((Mark)container).isHighlighted()) {
       			temporaryFocus  = ((Mark)container);
       			temporaryFocus.setHighlight(true, true);
    		}
    		
            if (BasePropertiesPane.activeDetail != null) {
                BasePropertiesPane.activeDetail.recover();
            }
            
            switch (editionType) {
	            case COMBO: {
	            	((ComboBox<Object>)control).setItems(FXCollections.observableArrayList(validItems));
	            	((ComboBox<Object>)control).getSelectionModel().select(property.getValue());
	            	break;
	            }
	            case SLIDER: {
	            	((Slider)control).setMax(max);
	            	((Slider)control).setMin(min);
	            	((Slider)control).setValue((double)property.getValue());
	            	break;
	            }
	            case COLOR_PICKER: {
	            	((ColorPicker)control).setValue((Color)property.getValue());
	            	break;
	            }
	            default: {
	            	((TextField)control).setText(property.getValue() +"");
	            	break;
	            }	
            }
            
          //  final HBox group = (HBox) label.getParent(); // TODO: Needs to change or not? Possibly not...
           // group.getChildren().clear();
           // group.getChildren().add(PropertyDetail.this.field);
            
            valueLabel.getChildren().remove(num);
            valueLabel.getChildren().add(num, PropertyDetail.this.fields[num]);
            
            PropertyDetail.this.fields[num].requestFocus();
            PropertyDetail.this.tmplabel = label;
            PropertyDetail.this.tmpindex = num;
            BasePropertiesPane.activeDetail = PropertyDetail.this;
            
            
           // addTooltip(control, property.getName());
            
                    
        });
	}
	
	public void recover() {
		if(tmplabel != null) {
			valueLabel.getChildren().remove(tmpindex);
			valueLabel.getChildren().add(tmpindex, tmplabel);
			tmplabel = null;
		}
        
		if(temporaryFocus != null && temporaryFocus.isHighlighted()) {
			temporaryFocus.setHighlight(false, true);
			temporaryFocus = null;
		}
		
        BasePropertiesPane.activeDetail = null;
	}
	
    public void setValidItems(final Object[] validItems) {
        this.validItems = validItems;
    }
        
    public void setMinMax(final double min, final double max) {
        this.min = min;
        this.max = max;
    }
    
    public void refresh() {        
    	updateLabels(); 
        
    	serializers.clear();
       	for(Property prop: properties) {
       		serializers.add(new SimpleSerializer(prop));
       	}		
        
    }

	@Override
	public Node getNode() {
		return label;
	}
	
	@Override
	public Type getType() {
		if(!(node instanceof VisBody) || !getGroup().getChildPropertyStructure().getProperties().containsKey(new PropertyName(properties.get(0).getName())))
			return Type.Value;
		return Type.Column;
	}

	
	@Override
	public int getColumnsNum() {
		return properties.size();
	}

	@Override
	public int getRowsNum() {
		if(getType() == Type.Value) return 2;
		else return ((VisBody)node).getComponents().size() + 1;
	}

	@Override
	public Object getPropertiesContent() { // TODO!!!! This need to change to deal with multiple columns!!!!!
		return FlexibleListProperty.createList(properties.get(0).getBean(), properties);
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		updateLabels();
	}

	@Override
	public PropertyName getID() {
		return id;
	}

	@Override
	public VisBody getTopGroup() {
		return node.getRootVirtualGroup();
	}

	@Override
	public VisBody getGroup() {
		return node.getVirtualGroup();
	}
	
	@Override
	public Container getContainer() {
		return node;
	}

	@Override
	public boolean isInWide() {
		return true;
	}
	
	@Override
	public boolean isNetwork() {
		return false;
	}

}
