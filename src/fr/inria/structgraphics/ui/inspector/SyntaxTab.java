package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.graphics.Container;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

public class SyntaxTab extends Tab {
	
	public static final String TAB_NAME = "Syntax";
	private InspectorView inspector;
	
	private Container activeNode = null;
	        
    private VBox vbox;
    private TextArea textArea;

	public SyntaxTab(InspectorView inspector) {
		super(TAB_NAME);
			
		this.inspector = inspector;
		
       // ScrollPane scrollPane = new ScrollPane();
       // scrollPane.setFitToWidth(true);
        
        textArea = new TextArea();
        textArea.prefRowCountProperty().set(100);
        textArea.setEditable(false);
        
 //       vbox = new VBox();
 //       vbox.getChildren().add(textArea);
 //       vbox.setFillWidth(true);
        //scrollPane.setContent(textArea);
        
        setContent(textArea);
        setClosable(false);
	}
	

	public void setActiveNode(Container node) { // TODO: ...
		if(activeNode == node) return;
		else clean();
				
	}
		
	
    private void createPane(Container node) {

    }
	
	
	private void clean() {
		vbox.getChildren().clear();
	}
	
	public void cleanAll() {
		vbox.getChildren().clear();
	}	    
    
}
