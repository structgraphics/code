package fr.inria.structgraphics.ui.inspector;

import java.util.Collection;

import javafx.beans.property.Property;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;

public abstract class PropertiesPane extends TitledPane {
    
    protected InspectorView inspector;
    private VBox box;
    
    public PropertiesPane(InspectorView inspector, String name) {
    	super();

    	this.inspector = inspector;
    	setText(name);    	
    	
    	box = new VBox();
    	setContent(box);
    }
    
    public void addPane(Node node) {
    	box.getChildren().add(node);
    }
    
    public void addGap() {
    	box.getChildren().add(new Label("             "));
    }
    
    public abstract void addDetails(final Collection<Property> details);
    
    protected abstract void clearPane();    
    public abstract void refresh();
    public abstract void update();
    
}
