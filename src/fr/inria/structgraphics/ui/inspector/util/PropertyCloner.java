package fr.inria.structgraphics.ui.inspector.util;

import java.util.ArrayList;
import java.util.List;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.AlignmentXProperty;
import fr.inria.structgraphics.types.AlignmentYProperty;
import fr.inria.structgraphics.types.ColorProperty;
import fr.inria.structgraphics.types.ColoringSchemeProperty;
import fr.inria.structgraphics.types.ComponentRefXProperty;
import fr.inria.structgraphics.types.ComponentRefYProperty;
import fr.inria.structgraphics.types.ContainerRefXProperty;
import fr.inria.structgraphics.types.ContainerRefYProperty;
import fr.inria.structgraphics.types.DeltaXProperty;
import fr.inria.structgraphics.types.DeltaYProperty;
import fr.inria.structgraphics.types.DistributionProperty;
import fr.inria.structgraphics.types.DistributionXProperty;
import fr.inria.structgraphics.types.DistributionYProperty;
import fr.inria.structgraphics.types.DoubleListProperty;
import fr.inria.structgraphics.types.FillColorProperty;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.FontSizeProperty;
import fr.inria.structgraphics.types.HeightProperty;
import fr.inria.structgraphics.types.LineTypeProperty;
import fr.inria.structgraphics.types.OpacityProperty;
import fr.inria.structgraphics.types.RotationProperty;
import fr.inria.structgraphics.types.ShapeProperty;
import fr.inria.structgraphics.types.Shareable;
import fr.inria.structgraphics.types.StrokeColorProperty;
import fr.inria.structgraphics.types.StrokeWidthProperty;
import fr.inria.structgraphics.types.TextProperty;
import fr.inria.structgraphics.types.WidthProperty;
import fr.inria.structgraphics.types.XProperty;
import fr.inria.structgraphics.types.YProperty;
import fr.inria.structgraphics.types.AlignmentProperty.XSticky;
import fr.inria.structgraphics.types.AlignmentProperty.YSticky;
import fr.inria.structgraphics.types.ColoringSchemeProperty.Scheme;
import javafx.beans.property.Property;
import javafx.scene.paint.Color;

public class PropertyCloner {

	public static Property replicate(Property property) {
		
		Property prop;
		
		if(property instanceof WidthProperty) prop = new WidthProperty(property.getBean(), (double)property.getValue());
		else if(property instanceof HeightProperty) prop = new HeightProperty(property.getBean(), (double)property.getValue());
		else if(property instanceof XProperty) prop = new XProperty(property.getBean(), (double)property.getValue());
		else if(property instanceof YProperty) prop = new YProperty(property.getBean(), (double)property.getValue());
		else if(property instanceof DeltaXProperty) prop = new DeltaXProperty(property.getBean(), (double)property.getValue());
		else if(property instanceof DeltaYProperty) prop = new DeltaYProperty(property.getBean(), (double)property.getValue());
		else if(property instanceof RotationProperty) prop = new RotationProperty(property.getBean(), (double)property.getValue());
		else if(property instanceof StrokeColorProperty) prop = new StrokeColorProperty(property.getBean(), (Color) property.getValue());
		else if(property instanceof FillColorProperty) prop = new FillColorProperty(property.getBean(), (Color) property.getValue());
		else if(property instanceof ContainerRefXProperty) prop = new ContainerRefXProperty(property.getBean(), (RefX) property.getValue());
		else if(property instanceof ContainerRefYProperty) prop = new ContainerRefYProperty(property.getBean(), (RefY) property.getValue());
		else if(property instanceof ComponentRefXProperty) prop = new ComponentRefXProperty(property.getBean(), (RefX) property.getValue());
		else if(property instanceof ComponentRefYProperty) prop = new ComponentRefYProperty(property.getBean(), (RefY) property.getValue());
		else if(property instanceof StrokeWidthProperty) prop = new StrokeWidthProperty(property.getBean(), (double)property.getValue());
		else if(property instanceof AlignmentXProperty) prop = new AlignmentXProperty(property.getBean(), (YSticky) property.getValue());
		else if(property instanceof AlignmentYProperty) prop = new AlignmentYProperty(property.getBean(), (XSticky) property.getValue());
		else if(property instanceof ShapeProperty) prop = new ShapeProperty(property.getBean(), (ShapeProperty.Type) property.getValue());
		else if(property instanceof LineTypeProperty) prop = new LineTypeProperty(property.getBean(), (LineTypeProperty.Type) property.getValue());
		else if(property instanceof DistributionXProperty) prop = new DistributionXProperty(property.getBean(), (DistributionProperty.Constraint) property.getValue());
		else if(property instanceof DistributionYProperty) prop = new DistributionYProperty(property.getBean(), (DistributionProperty.Constraint) property.getValue());
		else if(property instanceof TextProperty) prop = new TextProperty(property.getBean(), (String)property.getValue());
		else if(property instanceof FontSizeProperty) prop = new FontSizeProperty(property.getBean(), (double)property.getValue());
		else if(property instanceof OpacityProperty) prop = new OpacityProperty(property.getBean(), (double)property.getValue());
		else if(property instanceof ColoringSchemeProperty) prop = new ColoringSchemeProperty(property.getBean(), (Scheme)property.getValue());
		else if(property instanceof FlexibleListProperty) {
			List<Property> list = new ArrayList<>();
			for(Property prop_: (FlexibleListProperty)property) 
				list.add(PropertyCloner.replicate(prop_));
			
			return FlexibleListProperty.createList(property.getBean(), list);
		}		
		else return property;
		
		if(property instanceof Shareable) {
			((Shareable)prop).getActiveProperty().bind(((Shareable) property).getActiveProperty());
			((Shareable)prop).getPublicProperty().bind(((Shareable) property).getPublicProperty());
			((Shareable)prop).getHiddenProperty().bind(((Shareable) property).getHiddenProperty());
		}
		
		return prop;
	}
}
