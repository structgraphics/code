package fr.inria.structgraphics.ui.inspector.util;

import java.lang.reflect.Field;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.control.Tooltip;
import javafx.util.Duration;

public class TooltipCreator {

	private final static Duration duration = new Duration(250);
	
	public static Tooltip createTooltip(Node node, String text) {
		Tooltip t = new Tooltip(text);
		Tooltip.install(node, t); 
		
		return t;
	}
	
	
	// See: https://stackoverflow.com/questions/26854301/how-to-control-the-javafx-tooltips-delay
	public static void hackTooltipStartTiming() {
		Tooltip tooltip = new Tooltip();
		
	    try {
	        Field fieldBehavior = tooltip.getClass().getDeclaredField("BEHAVIOR");
	        fieldBehavior.setAccessible(true);
	        Object objBehavior = fieldBehavior.get(tooltip);

	        Field fieldTimer = objBehavior.getClass().getDeclaredField("activationTimer");
	        fieldTimer.setAccessible(true);
	        Timeline objTimer = (Timeline) fieldTimer.get(objBehavior);

	        objTimer.getKeyFrames().clear();
	        objTimer.getKeyFrames().add(new KeyFrame(duration));
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}
