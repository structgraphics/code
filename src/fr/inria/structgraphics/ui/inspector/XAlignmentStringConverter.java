package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.types.AlignmentProperty.YSticky;

public class XAlignmentStringConverter extends GeneralStringConverter {

	@Override
	public Object fromString(String string) {
		if(string.equalsIgnoreCase("no")) return YSticky.No;
		else return YSticky.Yes;
	}

}
