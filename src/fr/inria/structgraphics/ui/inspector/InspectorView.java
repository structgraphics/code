package fr.inria.structgraphics.ui.inspector;

import java.util.ArrayList;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.ui.Draggable;
import fr.inria.structgraphics.ui.spreadsheet.DataView;
import fr.inria.structgraphics.ui.tools.SelectTool;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

public class InspectorView extends BorderPane {

    public static final String STYLESHEETS = InspectorView.class.getResource("inspector.css").toExternalForm();
	
	private SplitPane splitPane;
	private VisualizationTreeView treeView;
	private TabPane tabPane;
	private PropertiesTab propertiesTab;
	private SyntaxTab syntaxTab;

	private VisFrame visframe;
	
	private Draggable dragged = null;
	
	private DataView dataView;
	
	public InspectorView() {
		buildUI();
	}	
	
	private void buildUI() {
        // main splitpane
        splitPane = new SplitPane();
        splitPane.setId("splitpane");
		
        treeView = new VisualizationTreeView(this);
        treeView.setId("treeview");
        
        StackPane treeViewStackPane = new StackPane(treeView);
        treeViewStackPane.setStyle(" -fx-padding: 0");
        treeView.setMaxHeight(Double.MAX_VALUE);
        
        tabPane = new TabPane();
        propertiesTab = new PropertiesTab(this);
        tabPane.getTabs().add(propertiesTab);
        
        
        // TODO: I do not care about syntax for now...
     //   syntaxTab = new SyntaxTab(this);
     //   tabPane.getTabs().add(syntaxTab);
           
        splitPane.setDividerPosition(0, 0.3);
        splitPane.getItems().addAll(treeViewStackPane, tabPane);
        
        this.setCenter(splitPane);
	}
	
	public void setVisualizationFrame(VisFrame visframe) {
		this.visframe = visframe;
		visframe.setInspector(this);
		
		// TODO: No handling syntax for now 
		// syntaxTab.setSyntax(visframe.getSyntax());
	}
	
	public void update() {
		propertiesTab.cleanAll();
		treeView.update(visframe);
	}
	
	public void updateOrder() {
		propertiesTab.cleanAll();
		treeView.updateOrder();
		dataView.update();
	}
		
	public VisFrame getVisualizationFrame() {
		return visframe;
	}
	
	
	public void refresh() {
	//	propertiesTab.cleanAll();
	//	treeView.refreshProperties(visframe);
	}
	
	public PropertiesTab getPropertiesTable() {
		return propertiesTab;
	}

	public void setView(ArrayList<Mark> marks) {
		treeView.setActiveNodes(marks);
	}
	
	public void setView(Mark mark) {
		treeView.setActiveNode(mark);
	}
	
	public void updateTreeViewLabel(Mark mark) {
		treeView.updateLabel(mark);
	}
	
	public void selectView(Container node) {
	//	treeView.selectView(node);
	//	propertiesTab.setActiveNode(node);
	}
	
	public void generateView(Container node) {
		propertiesTab.publishNode(node);
	}

	public void updateCollectionViews(VisCollection collection) {
		propertiesTab.publishNode(collection);
		for(Mark mark:collection.getComponents()) {
			if(mark instanceof VisCollection)
				updateCollectionViews((VisCollection)mark);
		}
	}
	
	public void setDraggable(Draggable draggable) {
		dragged = draggable;
	}
	
	public Draggable getDragged() {
		return dragged;
	}

	public void setSelector(SelectTool select) {
		treeView.setSelector(select);
	}

	public void setDataView(DataView dataView) {
		this.dataView = dataView;
		
	}
	
}
