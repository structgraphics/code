package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;

public class RefXStringConverter extends GeneralStringConverter {

	@Override
	public Object fromString(String string) {
		if(string.equalsIgnoreCase("right")) return RefX.Right;
		else if(string.equalsIgnoreCase("center")) return RefX.Center;
		else return RefX.Left;
	}

}
