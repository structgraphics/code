package fr.inria.structgraphics.ui.inspector;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.Property;
import javafx.util.StringConverter;

public class ListStringConverter extends StringConverter<List<Property>> {

	@Override
	public String toString(List<Property> list) {
		StringBuffer buffer = new StringBuffer("[ ");
		
		if(list.size() == 1) buffer.append(list.get(0).getValue());
		else if(list.size() > 1) buffer.append(list.get(0).getValue() + "... ");
		
		buffer.append(" ]");
		
		return buffer.toString();
	}

	@Override
	public List<Property> fromString(String string) {
		return new ArrayList<>(); // TODO: This is not useful. Not sure if it will be eventually used...
	}

}
