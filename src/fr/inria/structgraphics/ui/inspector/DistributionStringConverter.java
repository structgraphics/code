package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.types.DistributionProperty.Constraint;

public class DistributionStringConverter extends GeneralStringConverter {

	@Override
	public Object fromString(String string) {
		if(string.equalsIgnoreCase("none")) return Constraint.None;
		else if(string.equalsIgnoreCase("spacing")) return Constraint.Spacing;
		else return Constraint.Distance;
	}
}
