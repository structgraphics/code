package fr.inria.structgraphics.ui.inspector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.graphics.VisGroup;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.Shareable;
import fr.inria.structgraphics.ui.viscanvas.groupings.CollectionPropertyStructure;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnections;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;

public class PropertiesTab extends Tab {
	
	public enum Type {
		SELF, CHILDREN, TABLE, GROUP, PUBLIC, CONNECTION_TABLE
	}
	
	public static final String TAB_NAME = "Properties Structure";
	private InspectorView inspector;
	
	private Container activeNode = null;
	
    private Hashtable<NodeEntry, PropertiesPane> propertiesPanes = new Hashtable<NodeEntry, PropertiesPane>();
    
    private VBox vbox;

	public PropertiesTab(InspectorView inspector) {
		super(TAB_NAME);
			
		this.inspector = inspector;
		
        ScrollPane scrollPane = new ScrollPane();
        //scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setFitToWidth(true);
        vbox = new VBox();
        vbox.setFillWidth(true);
        scrollPane.setContent(vbox);
        getStyleClass().add("all-details-pane");
        
        setContent(scrollPane);
        setClosable(false);
	}
	

	public void setActiveNode(Container node) {
		if(activeNode == node) return;
		else clean();

		PropertiesPane pane = getPane(node, Type.SELF);
        pane.setExpanded(true);
        pane.setManaged(true);
        pane.setVisible(true);        
        
        //////////
        if(node instanceof VisCollection) { // TODO: Deal with Groups!!!!!
        	pane = getPane(node, Type.CHILDREN);
            pane.setExpanded(true);
            pane.setManaged(true);
            pane.setVisible(true);
            
        	pane = getPane(node, Type.TABLE);
            pane.setExpanded(true);
            pane.setManaged(true);
            pane.setVisible(true);
            
            // For Top collections show the connections if available
            if(node instanceof LineConnectedCollection) {
            	FlowConnections connections = ((LineConnectedCollection) node).getFlowConnections();
            	if(connections != null && !connections.isEmpty()) {
            		pane = getPane(node, Type.CONNECTION_TABLE);
                    pane.setExpanded(true);
                    pane.setManaged(true);
                    pane.setVisible(true);
            	}
            }
        } 
        else if(node instanceof VisGroup && !(((VisGroup) node).getContainer() instanceof VisGroup)) {
        	pane = getPane(node, Type.GROUP);
            pane.setExpanded(true);
            pane.setManaged(true);
            pane.setVisible(true);
            
        	pane = getPane(node, Type.PUBLIC);
        	pane.setExpanded(true);
        	pane.setManaged(true);
        	pane.setVisible(true);  
        }
	}
	
	
	public void publishNode(Container node) {
		if(node instanceof VisGroup && !(((VisGroup) node).getContainer() instanceof VisGroup)) {
        	createPane(node, Type.PUBLIC);
        }
		
        createPane(node, Type.SELF);
  
        if(node instanceof VisCollection) {
        	createPane(node, Type.CHILDREN);
        	createPane(node, Type.TABLE);
        	
        	if(node.hasConnections()) createPane(node, Type.CONNECTION_TABLE);
        }
        else if (node instanceof VisGroup && !(((VisGroup) node).getContainer() instanceof VisGroup)) {
        	createPane(node, Type.GROUP);
        }
	}
	
	
    private void createPane(Container node, Type type) {
    	String paneName = "";
    	
    	if(type == Type.SELF)
    		paneName = "Self Properties";
    	else if(type == Type.CHILDREN)
    		paneName = "Children Properties";
    	else if(type == Type.GROUP)
    		paneName = "Children Properties";
    	else if(type == Type.TABLE)
    		paneName = "Tabular Structure";
    	else if(type == Type.PUBLIC)
    		paneName = "Public (Visible to Collections)";
    	else if(type == Type.CONNECTION_TABLE)
    		paneName = "Flow Connections";
    	
        NodeEntry nodeEntry = new NodeEntry(node, paneName);
        
        if(type == Type.SELF) {
        	BasePropertiesPane pane = new BasePropertiesPane(inspector,paneName, node);
        	propertiesPanes.put(nodeEntry, pane);
        	Collection<Property> properties = node.getSelfProperties().values();
        
        	pane.addDetails(properties);  
        }
        else if(type == Type.CHILDREN) { // TODO: Deal with Groups!!!!!
        	CollectionPropertiesPane pane = new CollectionPropertiesPane(inspector, paneName, (VisCollection)node, type);
        	propertiesPanes.put(nodeEntry, pane);
        	pane.addTabDetails("variable"); 
        }
        else if(type == Type.TABLE) {
        	CollectionPropertiesPane pane = new CollectionPropertiesPane(inspector, paneName, (VisCollection)node, type);
        	propertiesPanes.put(nodeEntry, pane);
        	pane.addExtendedTable("full tabular structure"); 
        } else if(type == Type.GROUP) {
        	GroupPropertiesPane pane = new GroupPropertiesPane(inspector, paneName, (VisGroup)node, type);
        	propertiesPanes.put(nodeEntry, pane);
        	pane.addTabDetails();
        } else if(type == Type.PUBLIC) {
        	BasePropertiesPane pane = new BasePropertiesPane(inspector, paneName, node, type);
        	propertiesPanes.put(nodeEntry, pane);
        	
    		ArrayList<Property> publicProperties = new ArrayList<>();
    		ArrayList<Property> allProperties = new ArrayList<>();
        	((VisGroup)node).splitProperties(publicProperties, allProperties);
        	pane.addDetails(publicProperties);  
        	
        	addVisibilityListeners(allProperties, pane, (VisGroup)node);
        } else if(type == Type.CONNECTION_TABLE) {   	
        	// TODO:...
        	FlowConnectionsPane pane = new FlowConnectionsPane(inspector, paneName, (LineConnectedCollection)node, type);
        	propertiesPanes.put(nodeEntry, pane);
        	
        	Collection<Property> properties = new ArrayList<>();
        	properties.add(((LineConnectedCollection)node).coloringScheme);
        	properties.add(((LineConnectedCollection)node).flowPaint);
        	properties.add(((LineConnectedCollection)node).flowOpacity);
        	pane.addDetails(properties);
        	
        	pane.addExtendedTable("structure"); 
        }
    }
    
    private void addVisibilityListeners(ArrayList<Property> properties, BasePropertiesPane pane, VisGroup group) {
    	ChangeListener<Boolean> listener = new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				pane.clearPane();
	    		ArrayList<Property> publicProperties = new ArrayList<>();
	    		ArrayList<Property> allProperties = new ArrayList<>();
	        	group.splitProperties(publicProperties, allProperties);
	        	pane.addDetails(publicProperties);  
			}
		};
    	
    	for(Property property:properties) {
   			((Shareable)property).getPublicProperty().addListener(listener);
    	}
    }
	
    public void updateStructures() {    	
    	for(PropertiesPane pane:propertiesPanes.values())
    		pane.update();
    }
 
    
	private void clean() {
		vbox.getChildren().clear();
		//propertiesPanes.clear();
	}
	
	public void cleanAll() {
		vbox.getChildren().clear();
		propertiesPanes.clear();
	}
	
    private PropertiesPane getPane(Container node, Type type) {
    	String paneName = "";
    	
    	if(type == Type.SELF)
    		paneName = "Self Properties";
    	else if(type == Type.CHILDREN)
    		paneName = "Children Properties";
    	else if(type == Type.TABLE)
    		paneName = "Tabular Structure";
    	else if(type == Type.GROUP)
    		paneName = "Children Properties";
    	else if(type == Type.PUBLIC)
    		paneName = "Public (Visible to Collections)";
    	else if(type == Type.CONNECTION_TABLE)
    		paneName = "Flow Connections";
    	
        NodeEntry nodeEntry = new NodeEntry(node, paneName);
        PropertiesPane pane = propertiesPanes.get(nodeEntry);
       
        vbox.getChildren().add(pane);
        pane.refresh();
        
        return pane;
    }
    
       
    class NodeEntry {
    	private Container node;
    	private String name;
    	
    	public NodeEntry(Container node, String name) {
    		this.node = node;
    		this.name = name;
    	}
    	
    	public Container getNode() {
    		return node;
    	}
    	
    	public String getName() {
    		return name;
    	}
    	
    	@Override
    	public boolean equals(Object obj) {
    		if(obj instanceof NodeEntry) {
    			NodeEntry entry = (NodeEntry)obj;
    			    			
    			return entry.node == this.node && entry.name.equals(this.name);
    		}
    		else return false;
    	}

		@Override
		public int hashCode() {
			return name.hashCode() + 31*node.hashCode();
		}
    	
  
    }




    
}
