package fr.inria.structgraphics.ui.inspector;

import java.util.Collection;
import java.util.SortedSet;

import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.ui.DragManager;
import fr.inria.structgraphics.ui.Draggable;
import fr.inria.structgraphics.ui.inspector.PropertiesTab.Type;
import fr.inria.structgraphics.ui.viscanvas.groupings.CollectionPropertyStructure;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.event.EventHandler;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;

public class CollectionPropertiesPane extends BasePropertiesPane {
	        	
	private CollectionPropertyStructure structure;
	
    public CollectionPropertiesPane(InspectorView inspector, String name, VisCollection node, Type type) {
    	super(inspector, name, node, type);
    	this.structure = node.getChildPropertyStructure();
    	
    	structure.getFlagProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				inspector.getPropertiesTable().updateStructures();
			}
		});
    }
       
    // This adds the extended (detailed) tabular form
    public void addExtendedTable(String name) { // TODO: This needs work !!!
		PropertiesTable table = new PropertiesTable(name, structure, true, node);
		fulltabularpane = new TabularPropertiesGrid(false);
		fulltabularpane.addPropertiesTable(table);
		
		addPane(fulltabularpane);
    }
    
	public void addTabDetails(String name) {
		addDetails(structure.getMerged(), structure.getCommon());
				
		PropertiesTable table = new PropertiesTable(name, structure, node);
		//tabularpane = new TabularPropertiesPane(name);
		tabularpane = new TitledTablePane(name, new TabularPropertiesGrid(true));
		tabularpane.addPropertiesTable(table);
		addPane(tabularpane);
				
		sharedpane.setOnDragOver(new EventHandler<DragEvent>() {
			
		    public void handle(DragEvent event) {

		    	Dragboard db = event.getDragboard();
		  			    	
		    	//TODO: I need to fix that so that it only activate when
		        if (inspector.getDragged().getPropertiesContent() instanceof FlexibleListProperty && db.hasString() && !(inspector.getDragged() instanceof PropertyDetail)) {
		            /* allow for both copying and moving, whatever user chooses */
		            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		           		            
		           // spreadsheetView.addEventHandler(MouseEvent.MOUSE_MOVED, eventHandler);		        
		        }
	            event.consume();
		    }
		});
		
		tabularpane.setOnDragOver(new EventHandler<DragEvent>() { 
		    public void handle(DragEvent event) {
		    	Dragboard db = event.getDragboard();
		    	
		    	//TODO: I need to fix that so that it only activate when
		    	Draggable dragged = inspector.getDragged();
		    	if(dragged == null) {
		    		event.consume();
		    		return;
		    	}
		    	
		    	Object content = dragged.getPropertiesContent();
		        if ((content instanceof ObjectProperty || content instanceof DoubleProperty || content instanceof FlexibleListProperty) && (inspector.getDragged() instanceof PropertyDetail) && db.hasString()  && inspector.getDragged().getID() != null) {
		            /* allow for both copying and moving, whatever user chooses */
		            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		           		            
		           // spreadsheetView.addEventHandler(MouseEvent.MOUSE_MOVED, eventHandler);		        
		        }
	            event.consume();
		    }
		});
		
		
		sharedpane.setOnDragDropped(new EventHandler <DragEvent>() {
            public void handle(DragEvent event) {
                /* data dropped */
                /* if there is a string data on dragboard, read it and use it */
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (event.getGestureSource() != sharedpane && db.hasString() && db.hasString() && db.getString().equals(DragManager.DRAG_CODE)) {
                	dragEnded(inspector.getDragged());
                    success = true;
                }
                /* let the source know whether the string was successfully 
                 * transferred and used */
                event.setDropCompleted(success);
                
                event.consume();
            }
        });
		
		
		tabularpane.setOnDragDropped(new EventHandler <DragEvent>() {
            public void handle(DragEvent event) {
                /* data dropped */
                /* if there is a string data on dragboard, read it and use it */
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasString() && db.getString().equals(DragManager.DRAG_CODE)) {
                	dragEnded(inspector.getDragged());
                    success = true;
                }
                /* let the source know whether the string was successfully 
                 * transferred and used */
                event.setDropCompleted(success);
                
                event.consume();
            }
        });
	}
	
 
	private void addDetails(Collection<Property> properties, SortedSet<PropertyName> common) {
		sharedpane.addDetails(properties, common);
	}

	
	@Override
	public void update() {
		if(sharedpane != null) sharedpane.clearPane();
		
		if(tabularpane !=null) {
			addDetails(structure.getMerged(), structure.getCommon());
			tabularpane.getTable().refresh();
			tabularpane.refreshGrid();
		} else if(fulltabularpane !=null) {
			fulltabularpane.getTable().refresh();
			fulltabularpane.refreshGrid();
		}
		
		refresh();
	}

	
	protected void dragEnded(Draggable dragged) {		
		//Object content = dragged.getPropertiesContent();
		if(dragged instanceof PropertyDetail) { // Need to move to the simple properties space		
			structure.moveToVariable(dragged.getID());			
		} else { // TODO: add it to the table
			structure.moveToCommon(dragged.getID());
			//inspector.getVisualizationFrame().update();
		}			

		// TODO...
		// inspector.getPropertiesTable().updateStructures();
	}
	
}

