package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.ui.DragManager;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;


public class TabularPropertiesGrid extends GridPane {
	
    static final String DETAIL_LABEL_STYLE = "detail-label";
        
    private PropertiesTable table;
        
    public TabularPropertiesGrid(boolean nested) {
       if(nested) getStyleClass().add("nested-detail-pane");
      //  getStyleClass().add("detail-grid");
    	        
        setOnMousePressed(event -> {
           if(BasePropertiesPane.activeDetail != null) BasePropertiesPane.activeDetail.recover();
        });
        
        setHgap(10);
        setVgap(2);
        setSnapToPixel(true);        
    }
    
    protected void addPropertiesTable(PropertiesTable table) {
    	this.table = table;
  
    	table.setHandle(this);
    	DragManager.getSingleton().observe(table);

    	refreshGrid();
    }
    
    public PropertiesTable getTable() {
    	return table;
    }
    
    
    public void refreshGrid() {
    	getChildren().clear();
    	
    	int j = 1;
    	for(PropertyDetailColumn column: table) {
    		int i = 0;

    		final Label label = new Label(column.getName()); // I probably need to add some listeners here!!!
    		column.setHandle(label);
    		DragManager.getSingleton().observe(column);
    		
    		label.getStyleClass().add("detail-label");
    		GridPane.setHalignment(label, HPos.CENTER);
       		GridPane.setValignment(label, VPos.TOP);
       		add(label, j, i++);
    		
    		for(PropertyDetail detail: column) {
    			detail.valueLabel.getStyleClass().add("detail-value");
    			final Group group = new Group(detail.valueLabel);
    	        GridPane.setHalignment(group, HPos.RIGHT);
    			add(group, j, i++);
    		}
    		j++;
    	}
    }
    
    
    protected void clearPane() {
        getChildren().clear();
        //details.clear();
        // TODO....
    }
    
    public void refresh() {
    	/*
        for (int i = 0; i < details.size(); i++) {
            details.get(i).refresh();           
        }*/
    }


}
