package fr.inria.structgraphics.ui.inspector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.GroupLayout.SequentialGroup;

import org.matheclipse.core.builtin.function.Set;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.ShapeMark;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.types.ShapeProperty;
import fr.inria.structgraphics.types.ShapeProperty.Type;
import fr.inria.structgraphics.ui.tools.SelectTool;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;

public class VisualizationTreeView extends TreeView<VisualizationNode> {

	private InspectorView inspector;
	private Map<Container, TreeItem<VisualizationNode>> map;
	private SelectTool selectTool;
	
	public VisualizationTreeView(InspectorView inspector) {
		this.inspector = inspector;
		map = new HashMap<>();
		 
		getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		setCellFactory(node -> new TreeCell<VisualizationNode>() {
			 @Override public void updateItem(final VisualizationNode item, final boolean empty) {
				 super.updateItem(item, empty);

				 TreeItem<VisualizationNode> treeItem = getTreeItem();
				 setGraphic(treeItem == null ? null : treeItem.getGraphic());
				 setText(item == null ? null : item.toString());
				 setOpacity(1);
			 }
		 });
		 
		 getSelectionModel().getSelectedIndices().addListener(new ListChangeListener<Integer>() {
			@Override
			public void onChanged(Change<? extends Integer> c) { // TODO: ...
				ObservableList<TreeItem<VisualizationNode>> selected = getSelectionModel().getSelectedItems();			
//				TreeItem<VisualizationNode> selected = getSelectionModel().getSelectedItem();
				
				if(selected !=null && !selected.isEmpty()) {
					TreeItem<VisualizationNode> last = selected.get(selected.size() - 1);
					if(last != null) inspector.getPropertiesTable().setActiveNode(last.getValue().getNode());
					
					if(selectTool != null && selected.get(0).getValue() != null) {
						selectTool.setActive(true);
						Container node = selected.get(0).getValue().getNode();
						if(node != null && node instanceof Mark) selectTool.select((Mark)node, this, selected.size() < 2);
						for(int i = 1; i < selected.size(); ++i) {
							TreeItem<VisualizationNode> item = selected.get(i);
							if(item.getValue().getNode() instanceof Mark) selectTool.addSelection((Mark)item.getValue().getNode(), this);
						}	
					}
				} else if(selectTool != null) selectTool.select(null, this);
			}
		 });
		 
	}

	public void updateLabel(Container container) {
		TreeItem<VisualizationNode> item = map.get(container);
		
		VisualizationNode node = new VisualizationNode(container);
		item.setValue(node);		
		item.setGraphic(new ImageView(DisplayUtils.getIcon(node)));
	}
	
	public void update(Container container) { 
		map.clear();
		
		setRoot(parseSubTree(container));		
		refreshPropertiesTabs(container);
	}
		
	public void updateOrder() {
		SelectTool tmp = selectTool;
		selectTool = null; // To not propagate back the chances to the selectTool
		
		update(inspector.getVisualizationFrame());
		getSelectionModel().clearSelection();
		
		for(Mark mark: tmp.getSelected()) {
			getSelectionModel().select(map.get(mark));
		}	
		selectTool = tmp;
	}
	
	public void setActiveNodes(ArrayList<Mark> marks) {
		SelectTool tmp = selectTool;
		selectTool = null; // To not propagate back the chances to the selectTool
		
		getSelectionModel().clearSelection();
		for(Mark mark: marks) {
			getSelectionModel().select(map.get(mark));
		}
		
		selectTool = tmp;
	}
		
	public void setActiveNode(Mark mark) {
		SelectTool tmp = selectTool;
		selectTool = null; // To not propagate back the chances to the selectTool
		
		getSelectionModel().clearSelection();		
		getSelectionModel().select(map.get(mark));
		
		selectTool = tmp;
	}
	
	private void refreshPropertiesTabs(Container parent) {
		for(Mark mark:parent.getComponents()) {
			refreshPropertiesTabs(mark);
		}
		
		inspector.generateView(parent);
	}

	private TreeItem<VisualizationNode> parseSubTree(Container parent) {		
		VisualizationNode node = new VisualizationNode(parent);
		TreeItem<VisualizationNode> item = new TreeItem<VisualizationNode>(node, new ImageView(DisplayUtils.getIcon(node)));
		
		map.put(parent, item);
		
		for(Mark mark:parent.getComponents()) {
			item.getChildren().add(parseSubTree(mark));
		}
		
		if(parent instanceof ShapeMark) {
			ShapeMark mark = (ShapeMark)parent;
			mark.getShapeProperty().addListener(new ChangeListener<ShapeProperty.Type>() {
				@Override
				public void changed(ObservableValue<? extends Type> observable, Type oldValue, Type newValue) {
					item.graphicProperty().set(new ImageView(DisplayUtils.getIcon(item.getValue())));
					refresh();
				}
			});
		}
		
		if(parent instanceof VisFrame) item.setExpanded(true);
//		item.setExpanded(true);
		
		return item;
	}

	public void setSelector(SelectTool selector) {
		selectTool = selector;
		
		ContextMenu contextMenu = new ContextMenu();
		selectTool.addItems(contextMenu);
		this.setContextMenu(contextMenu);
	}
	
}
