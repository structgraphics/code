package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;

public class FlowPropertyDetailColumn extends PropertyDetailColumn {
	
	private boolean isID = false;
	
	public FlowPropertyDetailColumn(FlowConnectionsTable table, FlexibleListProperty properties, PropertyName col, Container node) {
		super(table, properties, col, node);
		
		isID = col.isID(); 
	}
					
	@Override
	public int getColumnsNum() {
		return isID ? 2 : 1;
	}

}
