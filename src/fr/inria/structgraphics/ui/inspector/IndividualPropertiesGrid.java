package fr.inria.structgraphics.ui.inspector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.graphics.VisGroup;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.HeightProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.Shareable;
import fr.inria.structgraphics.types.WidthProperty;
import fr.inria.structgraphics.ui.DragManager;
import fr.inria.structgraphics.ui.inspector.PropertiesTab.Type;
import fr.inria.structgraphics.ui.viscanvas.groupings.DefaultSharingStrategy;
import fr.inria.structgraphics.ui.viscanvas.groupings.GroupPropertyStructure;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;


public class IndividualPropertiesGrid extends GridPane {
	
    private static final int LABEL_COLUMN = 0;
    private static final int VALUE_COLUMN = 1;
    private static final int LOCK_COLUMN = 2;
	
    static final String DETAIL_LABEL_STYLE = "detail-label";
        
    private List<PropertyDetail> details = new ArrayList<>();
    
    private Container node;
    
    private Type type;
    
    public IndividualPropertiesGrid(Container node, Type type) {
    	super();
    	
    	this.node = node;
    	this.type = type;
    	
        getStyleClass().add("detail-pane");
        
        getStyleClass().add("detail-grid");
        setOnMousePressed(event -> {
           if(BasePropertiesPane.activeDetail != null) BasePropertiesPane.activeDetail.recover();
        });
        
        setHgap(4);
        setVgap(2);
        setSnapToPixel(true);
 //       final ColumnConstraints colInfo = new ColumnConstraints(120);
        
        final ColumnConstraints colInfo = new ColumnConstraints(10);
        getColumnConstraints().addAll(colInfo, new ColumnConstraints());        
    }
   
    private void updateColumnWidth() {
    	double w = 20;
    	
    	for(PropertyDetail detail: details) {
        	Text theText = new Text(detail.label.getText());
        	theText.setFont(detail.label.getFont());
        	double width = theText.getBoundsInLocal().getWidth();
        	w = Math.max(w, 20 + width);
    	}
    	
    	getColumnConstraints().remove(0);
    	getColumnConstraints().add(0, new ColumnConstraints(w));
    }
    
    
    private ChangeListener<Boolean> widthListener = null, heightListener = null;
    
    protected void addDetail(Property property, int row, PropertyName id, GroupPropertyStructure structure) {
    	
    	PropertyDetail detail;
		detail = new PropertyDetail(property, false, type == Type.PUBLIC, id, node);
 		
		// TODO: The source of the problem is here!!!
        DragManager.getSingleton().observe(detail);

        final HBox labelgroup = new HBox(8, detail.label);
        labelgroup.setAlignment(Pos.TOP_RIGHT);
        GridPane.setConstraints(labelgroup, LABEL_COLUMN, row);
        GridPane.setHalignment(labelgroup, HPos.RIGHT);
        GridPane.setValignment(labelgroup, VPos.TOP);
        detail.label.getStyleClass().add(DETAIL_LABEL_STYLE);

        final HBox valuegroup = new HBox(detail.valueLabel);
        GridPane.setConstraints(valuegroup, VALUE_COLUMN, row);
        GridPane.setHalignment(valuegroup, HPos.LEFT);
        GridPane.setValignment(valuegroup, VPos.TOP);
        detail.valueLabel.getStyleClass().add("detail-value");
                        
        // TODO:
        if(type != Type.PUBLIC && (property instanceof HeightProperty || property instanceof WidthProperty) && id == null && (property.getBean() instanceof Mark)) { // This only concerns the Self Properties pane
        	Mark mark = (Mark)property.getBean();
        	
         	detail.createLockIcon(mark.ratiolock.get(), property instanceof WidthProperty);
        	
         	labelgroup.getChildren().add(0, detail.lockIcon);
         	
        	detail.lockIcon.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					mark.ratiolock.set(!mark.ratiolock.get());
					//detail.switchLock(mark.ratiolock.get());
				}
			});     
        	
        	
        	if(property instanceof HeightProperty) {
        		if(heightListener != null) mark.ratiolock.removeListener(heightListener);
        		heightListener = new ChangeListener<Boolean>() {
    				@Override
    				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
    					detail.switchLock(newValue);
    				}
    			};
    			mark.ratiolock.addListener(heightListener);
        	} else if (property instanceof WidthProperty) {
           		if(widthListener != null) mark.ratiolock.removeListener(widthListener);
           		widthListener = new ChangeListener<Boolean>() {
    				@Override
    				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
    					detail.switchLock(newValue);
    				}
    			};
    			mark.ratiolock.addListener(widthListener);
        	}
        }
        else if(structure != null) {
        	boolean flag = structure.getCommon().contains(id);
        	detail.createExpandIcon(flag);
        	labelgroup.getChildren().add(0, detail.lockIcon);
        	detail.lockIcon.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if(flag) structure.moveToVariable(id);
					else structure.moveToCommon(id);
					detail.switchLock(!flag);
					
					// Dirty solution to update the public properties
					FlexibleListProperty property = structure.getProperties().get(id);
					BooleanProperty prop = ((Shareable)property.get(0)).getPublicProperty();
					prop.set(!prop.get());
					prop.set(!prop.get());
					
					// Propagate change to groups under the same collection
					// TODO: There is a problem when the properties of the siblings do not allow 
					// for a sharing ... Do I need a way to force the sharing?
					Object owner = property.getBean();
					if(owner instanceof Mark) {
						Container group = ((Mark)owner).getContainer();			
						if(group instanceof VisCollection) {											
							for(Mark mark: group.getComponents()) {
								if(mark != owner && mark instanceof VisGroup) {
									GroupPropertyStructure struct = ((VisGroup)mark).getChildPropertyStructure();
									if(flag) struct.moveToVariable(id);
									else struct.moveToCommon(id);
									FlexibleListProperty property2 = structure.getProperties().get(id);
									BooleanProperty prop2 = ((Shareable)property2.get(0)).getPublicProperty();
									prop2.set(!prop2.get());
									prop2.set(!prop2.get());
								}
							}
							
							// TODO: May contain bugs...Do other collections in the hierarchy need updating?
							((VisCollection)group).getChildPropertyStructure().refresh(new DefaultSharingStrategy());
						}
					}
				}
			});  
        }
        
        addToPane(labelgroup, valuegroup);
        
        details.add(detail);
		updateColumnWidth();
    }
    
    
    protected void addToPane(final Node... nodes) {
        getChildren().addAll(nodes);
    }
    
    protected void clearPane() {
        getChildren().clear();
        details.clear();
        // TODO....
    }

    public void addDetails(final Collection<Property> properties) {   
        for(Property property: properties) {
        	addDetail(property, null);
        }
    }
    
	public void addDetails(final Collection<Property> properties, Set<PropertyName> common) {
		Iterator<PropertyName> iterator = common.iterator();
		
        for (Property property: properties) { // Hide non-shareable group properties
        	if(property instanceof Shareable && (!((Shareable) property).getPublicProperty().get()
        			|| ((Shareable) property).getHiddenProperty().get())) {
        		iterator.next();
        		continue;
        	}
        	else addDetail(property, iterator.next());
        }
	}
		
	public void addDetails(GroupPropertyStructure structure) {
		// TODO Auto-generated method stub
	
		Iterator<PropertyName> iterator = structure.getNames().iterator();
        for (Property property: structure.getFlattenedProperties()) {
        	PropertyName name = iterator.next();
            addDetail(property, details.size(), name, structure);            
        }
	}
    
    public void addDetail(Property detail, PropertyName id) { //System.err.println(id + " " + detail);
    	addDetail(detail, details.size(), id, null);
    }

    public void refresh() {
        for (int i = 0; i < details.size(); i++) {
            details.get(i).refresh();           
        }
    }
}
