package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;

public class RefYStringConverter extends GeneralStringConverter {

	@Override
	public Object fromString(String string) {
		if(string.equalsIgnoreCase("top")) return RefY.Top;
		else if(string.equalsIgnoreCase("center")) return RefY.Center;
		else return RefY.Bottom;
	}

}
