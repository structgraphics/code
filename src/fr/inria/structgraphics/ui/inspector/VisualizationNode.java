package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.Mark;
import javafx.scene.image.Image;

public class VisualizationNode {
	private Container container;
	
	public VisualizationNode(Container container) {
		this.container = container;
	}
	
	public String toString() {
		return getName();
	}
	
    public Image getIcon() {
    	return null;
    }
    
    public Container getNode() {
    	return container;
    }
    
    public String getName() {
    	return (container instanceof Mark) ? container.getName() + " " + container.id.get() : container.getName();
    }
    
    public String getType() {
    	return container.getType();
    }

	public boolean isVisible() {
		return true;
	}

	public boolean isFocused() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof VisualizationNode && container == ((VisualizationNode)obj).container);
	}
    
}
