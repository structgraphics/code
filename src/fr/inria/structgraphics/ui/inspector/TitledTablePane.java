package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.ui.DragManager;
import javafx.scene.control.TitledPane;

public class TitledTablePane extends TitledPane {
	
    static final String DETAIL_LABEL_STYLE = "detail-label";
    private TabularPropertiesGrid gridPane;
    
    public TitledTablePane(String name, TabularPropertiesGrid gridPane) {
    	super(name, null);

       getStyleClass().add("titled-table-pane");
    	
        this.gridPane = gridPane;
        
        setOnMousePressed(event -> {
           if(BasePropertiesPane.activeDetail != null) BasePropertiesPane.activeDetail.recover();
        });
        
        gridPane.setHgap(10);
        gridPane.setVgap(2);
        gridPane.setSnapToPixel(true);
                
        setContent(gridPane);
        setCollapsible(false);
    }
    
    protected void addPropertiesTable(PropertiesTable table) {
    	gridPane.addPropertiesTable(table);
    	
    	table.setHandle(this);
    	DragManager.getSingleton().observe(table);
    }
    
    public PropertiesTable getTable() {
    	return gridPane.getTable();
    }
    
    
    public void refreshGrid() {
    	gridPane.refreshGrid();
    }
    
    
    protected void clearPane() {
    	gridPane.clearPane();
    }
    
    public void refresh() {
    	gridPane.refresh();
    }

}
