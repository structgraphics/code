package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.types.ColoringSchemeProperty;

public class ColoringStringConverter extends GeneralStringConverter {

	@Override
	public Object fromString(String string) {
		if(string.equalsIgnoreCase("common")) return ColoringSchemeProperty.Scheme.Common;
		else if(string.equalsIgnoreCase("source")) return ColoringSchemeProperty.Scheme.Source;
		else return ColoringSchemeProperty.Scheme.Destination;
	}

}
