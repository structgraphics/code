package fr.inria.structgraphics.ui.inspector;

import java.text.DecimalFormat;

public class DoubleStringConverter extends GeneralStringConverter {

	private boolean decimals = false;

	public DoubleStringConverter() {
		this(false);
	}
	
	public DoubleStringConverter(boolean decimals) {
		this.decimals = decimals;
	}
		
	@Override
	public Object fromString(String string) {
		return Double.parseDouble(string);
	}

	@Override
	public String toString(Object object) { 
		DecimalFormat df = new DecimalFormat(decimals ? "#.0" : "#"); 
		return df.format((Double)object);
	}
}
