package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.types.AlignmentProperty.XSticky;

public class YAlignmentStringConverter extends GeneralStringConverter {

	@Override
	public Object fromString(String string) {
		if(string.equalsIgnoreCase("no")) return XSticky.No;
		else return XSticky.Yes;
	}

}
