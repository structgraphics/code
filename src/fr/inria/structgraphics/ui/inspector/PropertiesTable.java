package fr.inria.structgraphics.ui.inspector;

import java.util.ArrayList;
import java.util.Map;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.ui.Draggable;
import fr.inria.structgraphics.ui.viscanvas.groupings.CollectionPropertyStructure;
import javafx.scene.Node;

public class PropertiesTable extends ArrayList<PropertyDetailColumn> implements Draggable {

	private String name = null;
	private CollectionPropertyStructure structure = null;
	protected boolean full = false;
	private Node handle;
	protected Map<PropertyName, FlexibleListProperty> props;
	protected Container node;
	
	public PropertiesTable(String name, CollectionPropertyStructure structure, Container node) {
		this(name, structure, false, node);
	}
	
	public PropertiesTable(String name, CollectionPropertyStructure structure, boolean full, Container node) {
		this(name, node);
		this.structure = structure;
		this.full = full;
		
		props = full ? structure.getFullVariableList() : structure.getVariableList();
		addEntries(props);
	}
	
	public PropertiesTable(String name, Container node) {
		this.name = name;
		this.node = node;
	}
	
	public String getName() {
		return name;
	}
		
	public FlexibleListProperty getProperties(PropertyName col){
		return props.get(col);//structure.getProperties().get(col);
	}
	
	protected void addEntries(Map<PropertyName, FlexibleListProperty>  tabproperties) {
		for(FlexibleListProperty properties: tabproperties.values()) {
    		PropertyDetailColumn column = new PropertyDetailColumn(this, properties, new PropertyName(properties.getName()), node);
    		column.add(properties);
    		add(column);	
		}
	}
	
	public void refresh() {
		clear();
		props = full ? structure.getFullVariableList() : structure.getVariableList();
		addEntries(props);
	}
	
	@Override
	public int getColumnsNum() {
		int counter = 0;
		
		for(PropertyDetailColumn column:this) {
			counter += column.getColumnsNum();
		}
		
		return counter;
	}
	
	@Override
	public int getRowsNum() {
		if(isEmpty()) return 0;
		else return get(0).size() + 1;
	}
	
	public void setHandle(Node node) {
		this.handle = node;
	}

	@Override
	public Node getNode() {
		return handle;
	}

	@Override
	public Type getType() {
		return Type.Table;
	}

	@Override
	public Object getPropertiesContent() {
		return props.values();
	}

	@Override
	public void update() {
		for(PropertyDetailColumn column:this)
			column.update();
	}

	@Override
	public PropertyName getID() {
		// TODO Auto-generated method stub
		return new PropertyName("");
	}

	@Override
	public VisBody getTopGroup() {
		return node.getRootVirtualGroup();
	}

	@Override
	public VisBody getGroup() {
		return node.getVirtualGroup();
	}
	
	@Override
	public Container getContainer() {
		return node;
	}

	@Override
	public boolean isInWide() {
		return !full;
	}

	@Override
	public boolean isNetwork() {
		return false;
	}

}
