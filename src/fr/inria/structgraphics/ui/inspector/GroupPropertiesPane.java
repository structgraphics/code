package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.graphics.VisGroup;
import fr.inria.structgraphics.ui.inspector.PropertiesTab.Type;
import fr.inria.structgraphics.ui.viscanvas.groupings.GroupPropertyStructure;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

public class GroupPropertiesPane extends BasePropertiesPane {
	        	
	private GroupPropertyStructure structure;
	
    public GroupPropertiesPane(InspectorView inspector, String name, VisGroup node, Type type) {
    	super(inspector, name, node, type);
    	this.structure = node.getChildPropertyStructure();
    	
    	structure.getFlagProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				inspector.getPropertiesTable().updateStructures();
			}
		});
    }
           
	public void addTabDetails() {
		basegrid.addDetails(structure);
		//basegrid.addDetails(structure.getFlattenedProperties(), structure.getNames(), structure.getCommon());		
	}
		
	@Override
	public void update() {
		if(basegrid  != null) {
			basegrid.clearPane();
			basegrid.addDetails(structure);
		}
		refresh();
	}

	
	/*
	protected void dragEnded(Draggable dragged) {
		//Object content = dragged.getPropertiesContent();
		if(dragged instanceof PropertyDetail) { // Need to move to the simple properties space		
			structure.moveToVariable(dragged.getID());			
		} else { // TODO: add it to the table
			structure.moveToCommon(dragged.getID());
			//inspector.getVisualizationFrame().update();
		}			

		// TODO...
		// inspector.getPropertiesTable().updateStructures();
	}*/
	
}

