package fr.inria.structgraphics.ui.inspector;

import javafx.scene.paint.Color;

public class ColorStringConverter extends GeneralStringConverter {

	@Override
	public Object fromString(String string) {
		return Color.web(string);
	}

}
