package fr.inria.structgraphics.ui.inspector;

import java.util.Collection;
import java.util.List;
import java.util.SortedSet;

import fr.inria.structgraphics.types.PropertyName;
import javafx.beans.property.Property;
import javafx.scene.Node;
import javafx.scene.control.TitledPane;


public class TitledIndividualPropertiesPane extends TitledPane {
	
    static final String DETAIL_LABEL_STYLE = "detail-label";
    private IndividualPropertiesGrid gridPane;
    
    public TitledIndividualPropertiesPane(String name, IndividualPropertiesGrid gridPane) {
    	super(name, null);

        this.gridPane = gridPane;
        
        setOnMousePressed(event -> {
           if(BasePropertiesPane.activeDetail != null) BasePropertiesPane.activeDetail.recover();
        });
        
        gridPane.setHgap(10);
        gridPane.setVgap(2);
        gridPane.setSnapToPixel(true);
                
        setContent(gridPane);
        setCollapsible(false);
    }
        
    protected void addToPane(final Node... nodes) {
        gridPane.addToPane(nodes);
    }
    
    protected void clearPane() {
    	gridPane.clearPane();
    }

    public void addDetails(final Collection<Property> properties) {   
    	gridPane.addDetails(properties);
    }
    
    
	public void addDetails(final Collection<Property> properties, SortedSet<PropertyName> common) {
		gridPane.addDetails(properties, common);
	}
    
    public void refresh() {
    	gridPane.refresh();
    }

}
