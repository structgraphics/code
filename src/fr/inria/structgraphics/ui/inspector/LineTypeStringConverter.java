package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.types.LineTypeProperty.Type;

public class LineTypeStringConverter extends GeneralStringConverter {

	@Override
	public Object fromString(String string) {
		if(string.equalsIgnoreCase("None")) return Type.None;
		else if(string.equalsIgnoreCase("StraightSolid")) return Type.StraightSolid;
		else if(string.equalsIgnoreCase("Straight")) return Type.Straight;
		else if(string.equalsIgnoreCase("Bezier")) return Type.Bezier;
		else if(string.equalsIgnoreCase("BezierSolid")) return Type.BezierSolid;
		else if(string.equalsIgnoreCase("TopBottom")) return Type.TopBottom;
		else /*if(string.equalsIgnoreCase("Area")*/ return Type.Area;
	}
}
