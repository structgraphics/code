package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.ui.inspector.PropertiesTab.Type;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnections;

public class FlowConnectionsPane extends BasePropertiesPane {
	        	
	private FlowConnections flowConnections;
	
    public FlowConnectionsPane(InspectorView inspector, String name, LineConnectedCollection node, Type type) {
    	super(inspector, name, node, type);
    	this.flowConnections = node.getFlowConnections();
    	
        sharedpane = new TitledIndividualPropertiesPane("visuals", new IndividualPropertiesGrid(node, type));
        addPane(sharedpane);
    	
    	// TODO: Add something like that to observe changes!!!!
    	/*
    	flowConnections.getFlagProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				//inspector.getPropertiesTable().updateStructures();
			}
		});*/
    }
           
    // This adds the extended (detailed) tabular form
    public void addExtendedTable(String name) { // TODO: This needs work !!!

		//tabularpane = new TabularPropertiesPane(name);
		tabularpane = new TitledTablePane(name, new TabularPropertiesGrid(true));
		FlowConnectionsTable table = new FlowConnectionsTable(name, flowConnections, node);
		tabularpane = new TitledTablePane(name, new TabularPropertiesGrid(true));
		tabularpane.addPropertiesTable(table);
		
		//fulltabularpane = new TabularPropertiesGrid(false);
		//fulltabularpane.addPropertiesTable(table);
		
		addPane(tabularpane);
    }
    	
	@Override
	public void update() {
		if(fulltabularpane !=null) {
			fulltabularpane.getTable().refresh();
			fulltabularpane.refreshGrid();
		}
		
		refresh();
	}

}

