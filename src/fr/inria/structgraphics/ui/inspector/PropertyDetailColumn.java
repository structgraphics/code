package fr.inria.structgraphics.ui.inspector;

import java.util.ArrayList;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.ui.Draggable;
import javafx.beans.property.ListProperty;
import javafx.beans.property.Property;
import javafx.scene.Node;
import javafx.scene.control.Label;

public class PropertyDetailColumn extends ArrayList<PropertyDetail> implements Draggable {
	
	protected FlexibleListProperty propList;
	protected Node handle;
	protected PropertyName col;
	protected PropertiesTable table;
	
	private Container node;
	
	public PropertyDetailColumn(PropertiesTable table, FlexibleListProperty properties, PropertyName col, Container node) {
		this.propList = properties;
		this.col = col;
		this.table = table;
		
		this.node = node;
	}
			
	public void add(ListProperty<Property> properties) {
		for(Property property: properties) { 
			if(property != null) add(new PropertyDetail(property, true, col, node));
		}
	}
	
	public String getName() {
		//return propList.getName();
		return col.toString();
	}

	@Override
	public Node getNode() {
		return handle;
	}

	@Override
	public Type getType() {
		return Type.Column;
	}

	@Override
	public Object getPropertiesContent() {
		return table.getProperties(col);
	}

	@Override
	public int getColumnsNum() {
		int length = 1;
		for(PropertyDetail detail:this)
			length = Math.max(length, detail.getColumnsNum());
		
		return length;
	}

	@Override
	public int getRowsNum() {
		return size() + 1;
	}

	@Override
	public void update() {
		for(PropertyDetail detail:this)
			detail.updateLabels();
	}

	public void setHandle(Label label) {
		this.handle = label;
	}

	@Override
	public PropertyName getID() {
		return col;
	}

	@Override
	public VisBody getTopGroup() {
		return node.getRootVirtualGroup();
	}

	@Override
	public VisBody getGroup() {
		return node.getVirtualGroup();
	}
	
	@Override
	public Container getContainer() {
		return node;
	}

	@Override
	public boolean isInWide() {
		return table.isInWide();
	}
	
	@Override
	public boolean isNetwork() {
		return table.isNetwork();
	}
}
