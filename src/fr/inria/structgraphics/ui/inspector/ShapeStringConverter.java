package fr.inria.structgraphics.ui.inspector;

import fr.inria.structgraphics.types.ShapeProperty.Type;

public class ShapeStringConverter extends GeneralStringConverter {

	@Override
	public Object fromString(String string) {
		if(string.equalsIgnoreCase("triangle")) return Type.Triangle;
		else if(string.equalsIgnoreCase("ellipse")) return Type.Ellipse;
		else return Type.Rectangle;
	}

}
