package fr.inria.structgraphics.ui.utils;

import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.PathElement;
import javafx.scene.shape.QuadCurveTo;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public class Cloner {
	
	public static Shape clone(Shape shape) {
		
		if(shape instanceof Rectangle){
			Rectangle clone = new Rectangle(((Rectangle) shape).getX(), ((Rectangle) shape).getY(), ((Rectangle) shape).getWidth(), ((Rectangle) shape).getHeight());
			
			/*
			clone.xProperty().bind(((Rectangle) shape).xProperty());
			clone.yProperty().bind(((Rectangle) shape).yProperty());
			clone.widthProperty().bind(((Rectangle) shape).widthProperty());
			clone.heightProperty().bind(((Rectangle) shape).heightProperty());
			*/
			
			return clone;
		} else if(shape instanceof Circle){
			Circle clone = new Circle(((Circle) shape).getCenterX(), ((Circle) shape).getCenterY(), ((Circle) shape).getRadius());
			
			/*
			clone.centerXProperty().bind(((Circle) shape).centerXProperty());
			clone.centerYProperty().bind(((Circle) shape).centerYProperty());
			clone.radiusProperty().bind(((Circle) shape).radiusProperty());
			*/
			
			return clone;
		} else if(shape instanceof Line){
			Line clone = new Line(((Line) shape).getStartX(), ((Line) shape).getStartY(), ((Line) shape).getEndX(), ((Line) shape).getEndY());
			
			/*
			clone.startXProperty().bind(((Line) shape).startXProperty());
			clone.startYProperty().bind(((Line) shape).startYProperty());
			clone.endXProperty().bind(((Line) shape).endXProperty());
			clone.endYProperty().bind(((Line) shape).endYProperty());
			*/
			
			return clone;
		}  else if(shape instanceof Path){
			Path clone = new Path(((Path) shape).getElements());
				
			/*
			Path clone = new Path();
			for(PathElement e: ((Path) shape).getElements()) {
				if(e instanceof MoveTo) {
					MoveTo mt = new MoveTo();
					mt.xProperty().bind(((MoveTo)e).xProperty());
					mt.yProperty().bind(((MoveTo)e).yProperty());
					clone.getElements().add(mt);
				} else if(e instanceof LineTo) {
					LineTo lt = new LineTo();
					lt.xProperty().bind(((LineTo)e).xProperty());
					lt.yProperty().bind(((LineTo)e).yProperty());
					clone.getElements().add(lt);					
				} else if(e instanceof QuadCurveTo) {
					QuadCurveTo qt = new QuadCurveTo();
					qt.xProperty().bind(((QuadCurveTo)e).xProperty());
					qt.yProperty().bind(((QuadCurveTo)e).yProperty());				
					qt.controlXProperty().bind(((QuadCurveTo)e).controlXProperty());
					qt.controlYProperty().bind(((QuadCurveTo)e).controlYProperty());
					clone.getElements().add(qt);					
				} else if(e instanceof CubicCurveTo) {
					CubicCurveTo ct = new CubicCurveTo();
					ct.xProperty().bind(((CubicCurveTo)e).xProperty());
					ct.yProperty().bind(((CubicCurveTo)e).yProperty());				
					ct.controlX1Property().bind(((CubicCurveTo)e).controlX1Property());
					ct.controlY1Property().bind(((CubicCurveTo)e).controlY1Property());
					ct.controlX2Property().bind(((CubicCurveTo)e).controlX2Property());
					ct.controlY2Property().bind(((CubicCurveTo)e).controlY2Property());
					clone.getElements().add(ct);					
				}
			}*/
			
			return clone;
		} 		
		
		// TODO: ...
		return null;
	}

}
