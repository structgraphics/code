package fr.inria.structgraphics.ui.utils;

import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.util.Pair;

// See: http://carl-witt.de/customized-bidirectional-bindings-in-javafx/

public class BidirectionalBindings {
	   /** Executes updateB when propertyA is changed. Executes updateA when propertyB is changed.
     * Makes sure that no update loops are caused by mutual updates.
	 * @return 
     */
    public static <A,B> Pair<ChangeListener<A>, ChangeListener<B>> bindBidirectional(Property<A> propertyA, Property<B> propertyB,
    		ChangeListener<A> updateB, ChangeListener<B> updateA){
        ChangeListener<A> listenerA = addFlaggedChangeListener(propertyA, updateB);
        ChangeListener<B> listenerB = addFlaggedChangeListener(propertyB, updateA);
        
        return new Pair<ChangeListener<A>, ChangeListener<B>>(listenerA, listenerB);
    }
    
    public static <A,B>  void unbindBidirectional(Property<A> property1, Property<B> property2, Pair<ChangeListener<A>, ChangeListener<B>> pair) {
    	property1.removeListener(pair.getKey());
    	property2.removeListener(pair.getValue());
    }

    private static <T> ChangeListener<T> addFlaggedChangeListener(Property<T> property, ChangeListener<T> updateProperty){
    	ChangeListener<T> listener = new ChangeListener<T>() {
	        private boolean alreadyCalled = false;
	        
	        @Override public void changed(ObservableValue<? extends T> observable, T oldValue, T newValue) {
	            if(alreadyCalled) return;
	            try {
	                alreadyCalled = true;
	                updateProperty.changed(observable,oldValue,newValue);
	            }
	            finally { alreadyCalled = false; }
	        }
        };
    	
        property.addListener(listener);
        
        return listener;
    }
}
