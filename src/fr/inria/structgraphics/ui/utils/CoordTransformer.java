package fr.inria.structgraphics.ui.utils;

import fr.inria.structgraphics.graphics.ReferenceCoords;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;

public class CoordTransformer {

	public static double getXPosition(ReferenceCoords refCoords, RefX ref, double containerWidth, double x, double w) {		
		RefX containerRef = refCoords.containerXRef.get();
		double offset = 0;
		if(containerRef == RefX.Center) offset = containerWidth / 2;
		else if(containerRef == RefX.Right) offset =  containerWidth;
		
		if(ref == RefX.Left) {
			return x - offset;
		}
		else if(ref == RefX.Center) {
			return x + w/2 - offset;
		}
		else {
			return x + w - offset;
		}
	}
	
	public static double getYPosition(ReferenceCoords refCoords, RefY ref, double containerHeight, double y, double h) {		
		RefY containerRef = refCoords.containerYRef.get();
		double offset = 0;
		if(containerRef == RefY.Center) offset = containerHeight / 2;
		else if(containerRef == RefY.Bottom) offset =  containerHeight;
		
		if(ref == RefY.Top) {
			return y + offset;
		}
		else if(ref == RefY.Center) {
			return y - h/2 + offset;
		}
		else {
			return y - h + offset;
		}
	}
}
