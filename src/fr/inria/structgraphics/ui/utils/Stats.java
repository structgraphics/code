package fr.inria.structgraphics.ui.utils;

import java.util.List;

public class Stats {
	public double min = Double.MAX_VALUE, max = Double.MIN_VALUE, mean = 0, var = 0;
	
	public Stats(List<Double> numbers){
		double sum = 0;

		for(double num: numbers) {
			sum += num;
			min = Math.min(num, min);
			max = Math.max(num, max);
		}
		
		mean = sum / numbers.size();
		
		sum = 0;
		for(double num: numbers) {
			sum += Math.pow(num - mean, 2);
		}
		
		var = sum / numbers.size();
	}
	
}