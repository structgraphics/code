package fr.inria.structgraphics.ui.utils;

import java.util.ArrayList;
import java.util.Hashtable;

import fr.inria.structgraphics.graphics.ShapeMark;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnection;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/*
 * This binding mechanism ensures that updates of flow marks heights and flow sizes are correctly propagates without loops
 */

public class FlowConnectionBinding {
	
	private ShapeMark mark;
	private ArrayList<FlowConnection> inwardConnections, outwardConnections;
	
	// Original Listeners
	private LockedChangeListener backwardsListener, forwardsListener;
	private Hashtable<FlowConnection, LockedChangeListener> fromBackListeners, fromFrontListeners;
	
	private boolean lock = false;
	
	public FlowConnectionBinding(ShapeMark mark) {
		this.mark = mark;
		inwardConnections = new ArrayList<FlowConnection>();
		outwardConnections = new ArrayList<FlowConnection>();
		
		fromBackListeners = new Hashtable<>();
		fromFrontListeners = new Hashtable<>();
		
		backwardsListener = new LockedChangeListener() {			
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
	            if(alreadyCalled || lock) return;
	            try {
	            	for(LockedChangeListener listener: fromBackListeners.values()) {
	            		listener.alreadyCalled = true;
	            	}
	            
	            	updateInwardConnections(oldValue.doubleValue(), newValue.doubleValue());
	            	
	            }
	            finally { 
	            	for(LockedChangeListener listener: fromBackListeners.values()) {
	            		listener.alreadyCalled = false;
	            	}
	            }
			}
		};
		
		forwardsListener = new LockedChangeListener() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
	            if(alreadyCalled || lock) return;
	            try {
	            	for(LockedChangeListener listener: fromFrontListeners.values()) {
	            		listener.alreadyCalled = true;
	            	}
	            
	            	updateOutwardConnections(oldValue.doubleValue(), newValue.doubleValue());
	            	
	            }
	            finally { 
	            	for(LockedChangeListener listener: fromFrontListeners.values()) {
	            		listener.alreadyCalled = false;
	            	}
	            }
			}
		};
		
		mark.height.addListener(backwardsListener);
		mark.height.addListener(forwardsListener);
	}
	
	public ArrayList<FlowConnection> getInwardConnections(){
		return inwardConnections;
	}
	
	public ArrayList<FlowConnection> getOutwardConnections(){
		return outwardConnections;
	}
	
	private void updateOutwardConnections(double oldvalue, double newvalue) {		
		if(outwardConnections.isEmpty() || newvalue > oldvalue) return;
		
		double sum = 0;
		for(FlowConnection conn:outwardConnections) {
			sum += conn.weight.get();
		}
		
		if(sum <= mark.height.get()) return;
		
		double sum_ = 0;
		for(int i = 0; i < outwardConnections.size() - 1; ++i) {
			FlowConnection conn = outwardConnections.get(i);
			double ratio = conn.weight.doubleValue()/sum;
			double w = abs(ratio*newvalue);
			conn.weight.set(w);
			sum_ += w;
		}
		
		FlowConnection conn = outwardConnections.get(outwardConnections.size() - 1);
		conn.weight.set(abs(newvalue - sum_));
	}
	
	private void updateInwardConnections(double oldvalue, double newvalue) {
		if(inwardConnections.isEmpty() || newvalue > oldvalue) return;
		
		double sum = 0;
		for(FlowConnection conn:inwardConnections) {
			sum += conn.weight.get();
		}
		
		if(sum <= mark.height.get()) return;
		
		// TODO: Find a more precise calculation method...
		double ratio = newvalue / oldvalue;

		sum = 0;
		for(int i = 0; i < inwardConnections.size() - 1; ++i) {
			FlowConnection conn = inwardConnections.get(i);
			double w = abs(ratio*conn.weight.doubleValue());
			conn.weight.set(w);
			sum += w;
		}
		
		FlowConnection conn = inwardConnections.get(inwardConnections.size() - 1);
		conn.weight.set(abs(newvalue - sum));
	}
	
	
	private void updateOutwardConnections2(double oldvalue, double newvalue) {		
		if(outwardConnections.isEmpty()) return;
		
		double sum = 0;
		for(FlowConnection conn:outwardConnections) {
			sum += conn.weight.get();
		}

// TODO: To uncomment to allow for inpomplete outbound flows
//		if(newvalue > sum) return;
		
		double sum_ = 0;
		for(int i = 0; i < outwardConnections.size() - 1; ++i) {
			FlowConnection conn = outwardConnections.get(i);
			double ratio = conn.weight.doubleValue()/sum;
			double w = abs(ratio*newvalue);
			conn.weight.set(w);
			sum_ += w;
		}
		
		FlowConnection conn = outwardConnections.get(outwardConnections.size() - 1);
		conn.weight.set(abs(newvalue - sum_));
	}
	
	private void updateInwardConnections2(double oldvalue, double newvalue) {
		if(inwardConnections.isEmpty()) return;
		
		// TODO: Find a more precise calculation method...
		double ratio = newvalue / oldvalue;

		double sum = 0;
		for(int i = 0; i < inwardConnections.size() - 1; ++i) {
			FlowConnection conn = inwardConnections.get(i);
			double w = abs(ratio*conn.weight.doubleValue());
			conn.weight.set(w);
			sum += w;
		}
		
		FlowConnection conn = inwardConnections.get(inwardConnections.size() - 1);
		conn.weight.set(abs(newvalue - sum));
	}
	
		
	private void updateFromFront2(FlowConnection destination, double oldvalue, double newvalue) {
		double w = 0;
		for(FlowConnection connection: outwardConnections) {
			w += connection.weight.get();
		}
		
		// TODO: To uncomment to allow for inpomplete outbound flows
		//if(mark.height() < w) 
			mark.height.set(w);
	}
	
	private void updateFromBack2(FlowConnection origin, double oldvalue, double newvalue) {
		double w = 0;
		for(FlowConnection connection: inwardConnections) {
			w += connection.weight.get();
		}
		
		mark.height.set(w);
			
		//mark.height.set(mark.height() - oldvalue + newvalue);
	}

	
	private void updateFromFront(FlowConnection destination, double oldvalue, double newvalue) {
		updateHeight();
	}
	
	private void updateFromBack(FlowConnection origin, double oldvalue, double newvalue) {
		updateHeight();
	}
	
	private void updateHeight() {
		double w1 = 0;
		for(FlowConnection connection: outwardConnections) {
			w1 += connection.weight.get();
		}
		
		double w2 = 0;
		for(FlowConnection connection: inwardConnections) {
			w2 += connection.weight.get();
		}
		
		mark.height.set(Math.max(w1, w2));
	}
	
	private double abs(double w) {
		return Math.abs(w);
	}
	
	public void addInwardConnection(FlowConnection connection) {
		inwardConnections.add(connection);
		
		LockedChangeListener listener = new LockedChangeListener() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
	            if(alreadyCalled || lock) return;
	            try {
	            	backwardsListener.alreadyCalled = true;
	            	updateFromBack(connection, oldValue.doubleValue(), newValue.doubleValue());
	            }
	            finally { 
	            	backwardsListener.alreadyCalled = false;
	            }
			}
		};
		
		fromBackListeners.put(connection, listener);
		connection.weight.addListener(listener);
	}
	
	public void addOutwardConnection(FlowConnection connection) {
		outwardConnections.add(connection);
		
		LockedChangeListener listener = new LockedChangeListener(){
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
	            if(alreadyCalled || lock) return;
	            try {
	            	forwardsListener.alreadyCalled = true;
	            	updateFromFront(connection, oldValue.doubleValue(), newValue.doubleValue());
	            }
	            finally { 
	            	forwardsListener.alreadyCalled = false;
	            }
			}
		};
		
		fromFrontListeners.put(connection, listener);
		connection.weight.addListener(listener);
	}
	
	public void removeInwardConnection(FlowConnection connection) {
		inwardConnections.remove(connection);
		
		LockedChangeListener listener = fromFrontListeners.get(connection);
		if(listener != null) connection.weight.removeListener(listener);
		fromFrontListeners.remove(connection);
	}
	
	public void removeOutwardConnection(FlowConnection connection) {
		outwardConnections.remove(connection);
		
		LockedChangeListener listener = fromBackListeners.get(connection);
		if(listener != null) connection.weight.removeListener(listener);
		fromBackListeners.remove(connection);
	}
	
	
	public void destroy() {
    	for(FlowConnection connection: fromFrontListeners.keySet()) {
    		connection.weight.removeListener(fromFrontListeners.get(connection));
    		connection.destroy();
    		connection.detachFront();
    	}
		
    	for(FlowConnection connection: fromBackListeners.keySet()) {
    		connection.weight.removeListener(fromBackListeners.get(connection));
    		connection.destroy();
    		connection.detachBack();
    	}

		mark.height.removeListener(backwardsListener);
		mark.height.removeListener(forwardsListener);
    	
    	fromFrontListeners.clear();
    	fromBackListeners.clear();
    	inwardConnections.clear(); 
    	outwardConnections.clear();    	
	}
	
	class LockedChangeListener implements ChangeListener<Number> {
		protected boolean alreadyCalled = false;

		@Override
		public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {}
	}

	public void lockListeners(boolean lock) {
		this.lock = lock;
	}
}
