package fr.inria.structgraphics.ui.utils;

import java.util.Comparator;
import java.util.TreeSet;

import fr.inria.structgraphics.graphics.Mark;

public class SortedMarkSet extends TreeSet<Mark> {
	
	public SortedMarkSet(boolean xaxis) {
		super(new Comparator<Mark>() {
			@Override
			public int compare(Mark mark1, Mark mark2) {		
				if(xaxis) {
					if(mark1.getCoords().getX() <= mark2.getCoords().getX()) return -1;
					else return 1;
				} else {
					if(mark1.getCoords().getY() <= mark2.getCoords().getY()) return -1;
					else return 1;
				}
			}
		});
	}
}
