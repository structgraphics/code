package fr.inria.structgraphics.ui.utils;

import java.util.ArrayList;

import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.Shareable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

// See: http://carl-witt.de/customized-bidirectional-bindings-in-javafx/

public class GroupBinding<A> {
	
	private ArrayList<ChangeListener<A>> originListeners = null;
	private ArrayList<LockedChangeListener> listeners = new ArrayList<>();
	private ArrayList<Property<A>> properties;
		
	private GroupBinding<Boolean> visibilityBindings = null;
	
	
	public GroupBinding(ArrayList<Property<A>> properties, boolean withVisibility) {
		this.properties = properties;
		
		if(withVisibility) {
			visibilityBindings = createVisibilityBindings();
			rebind();
		}			
	}
	
	public GroupBinding(ArrayList<Property<A>> properties) {
		this(properties, true);
	}
	
	public GroupBinding(ArrayList<Property<A>> properties, ArrayList<ChangeListener<A>> listeners) {		
		this.properties = properties;
		this.originListeners = listeners;
		visibilityBindings = createVisibilityBindings();
		
		rebind();
	}
	
	private GroupBinding<Boolean> createVisibilityBindings() {
		ArrayList<BooleanProperty> visProperties = new ArrayList<>();
		for(Property prop: properties)
			visProperties.add(((Shareable)prop).getPublicProperty());
		
		return new GroupBinding(visProperties, false);
	}
	
	public boolean hasValue(Property<A> property) {
		return (!properties.isEmpty() && properties.get(0).getValue().equals(property.getValue()));
	}
	
	public boolean contains(Property<A> property) {
		return properties.contains(property);
	}

	public ArrayList<Property<A>> getProperties(){
		return properties;
	}
	
    public void unbind() {
    	if(visibilityBindings != null) visibilityBindings.unbind();
    	
    	for(int i = 0; i<properties.size(); ++i) {
    		properties.get(i).removeListener(listeners.get(i));        		
    	}
    	
    	listeners.clear();
    }
    
    public void rebind() { 
    	if(originListeners == null) {
    		originListeners = new ArrayList<>();
    		
    		for(Property<A> prop: properties) {
    			originListeners.add(new ChangeListener<A>() {
    				@Override
    				public void changed(ObservableValue<? extends A> observable, A oldValue, A newValue) {
    					for(Property<A> prop_: properties) {
    						if(prop != prop_) prop_.setValue(newValue);
    					}
    				}
    			});
    		}
    	}
    	
		listeners = addFlaggedChangeListeners();
		
		if(visibilityBindings != null) visibilityBindings.rebind();
    }

    public Property<A> firstProperty(){
    	if(isEmpty()) return null;
    	else return properties.get(0);
    }
    
    public void resetProperties(ArrayList<Property<A>> properties_) {
		unbind();
		properties.clear();
		properties.addAll(properties_);
		originListeners.clear();
		originListeners = null;
		rebind();
		
		if(visibilityBindings != null) {
			ArrayList<Property<Boolean>> pubPRoperties = new ArrayList<>();
			for(Property<A> prop: properties_)
				pubPRoperties.add(((Shareable)prop).getPublicProperty());
			
			visibilityBindings.resetProperties(pubPRoperties);
		}
    }
    
	public void add(Property<A> property) {
		unbind();
		properties.add(property);
		rebind();
		
		if(visibilityBindings != null) visibilityBindings.add(((Shareable)property).getPublicProperty());
	}
	
	public void remove(Property<A> property) {
		unbind();
		properties.remove(property);
		rebind();
		
		if(visibilityBindings != null) visibilityBindings.remove(((Shareable)property).getPublicProperty());
	}
    
	public boolean isEmpty() {
		return properties.isEmpty();
	}
	
    protected ArrayList<LockedChangeListener> addFlaggedChangeListeners(){
    	ArrayList<LockedChangeListener> newlisteners = new ArrayList<>();
    	
    	int i = 0;
    	for(Property<A> prop: properties) {
    		ChangeListener<A> updateProperty = originListeners.get(i++);
    		LockedChangeListener listener = new LockedChangeListener(updateProperty);
            newlisteners.add(listener);
            
            prop.addListener(listener);
    	}
    	        
        return newlisteners;
    }
    
        
	class LockedChangeListener implements ChangeListener<A> {
		protected boolean alreadyCalled = false;
		protected ChangeListener<A> updateProperty = null;
		
		LockedChangeListener(ChangeListener<A> updateProperty){
			this.updateProperty = updateProperty;
		}
		
        @Override 
        public void changed(ObservableValue<? extends A> observable, A oldValue, A newValue) {
            if(alreadyCalled) return;
            try {
            	for(GroupBinding<A>.LockedChangeListener listener: listeners) {
            		listener.alreadyCalled = true;
            	}
               // alreadyCalled = true;
                updateProperty.changed(observable, oldValue, newValue);
            }
            finally { 
            	for(GroupBinding<A>.LockedChangeListener listener: listeners) {
            		listener.alreadyCalled = false;
            	}
            }
        }
	}
    
}
