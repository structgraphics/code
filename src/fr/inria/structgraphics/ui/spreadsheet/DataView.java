package fr.inria.structgraphics.ui.spreadsheet;

import java.util.Optional;

import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.ui.DragManager;
import fr.inria.structgraphics.ui.inspector.DisplayUtils;
import fr.inria.structgraphics.ui.inspector.InspectorView;
import fr.inria.structgraphics.ui.tools.SelectTool;
import fr.inria.structgraphics.ui.tools.Tool;
import impl.org.controlsfx.spreadsheet.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.PickResult;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class DataView extends BorderPane {
	
	public static final String STYLESHEETS = DataView.class.getResource("spreadsheet.css").toExternalForm();

	private Spreadsheet spreadsheet;
	private TransformationField transformField;
	
	private InspectorView inspector;
	private AreaInteractor interactor;
	
	public DataView(InspectorView inspector) {
		this.inspector = inspector;
		inspector.setDataView(this);
		
		buildUI();
	}	
	
	public Spreadsheet getSpreadsheet() {
		return spreadsheet;
	}
	
	private void cleanSpreadsheet() {
		spreadsheet.clean();
		transformField.disable(true);
		transformField.setDefaultLabel();
	}
	
	private void buildUI() {		
		Button clean = new Button("Clean", new ImageView(DisplayUtils.getIcon("cleaning")));
		clean.setOnAction(
				new EventHandler<ActionEvent>() {
					@Override public void handle(ActionEvent e) {
						Alert alert = new Alert(AlertType.CONFIRMATION);
						alert.setTitle("Confirmation Dialog");
						alert.setHeaderText(null);
						alert.setContentText("Are you sure you want to clean the spreadsheet?");

						Optional<ButtonType> result = alert.showAndWait();
						if (result.get() == ButtonType.OK){
							cleanSpreadsheet();
						} else {
							// ... user chose CANCEL or closed the dialog
						}
					}
				});

		HBox toolBar = new HBox(clean);
		toolBar.getChildren().add(new Separator(Orientation.VERTICAL));
		
		transformField = new TransformationField(toolBar);
		StackPane stack = new StackPane();
		
		interactor = new AreaInteractor(transformField);

		spreadsheet = new Spreadsheet(this, interactor);
			
		stack.getChildren().setAll(spreadsheet);
	
		setTop(transformField);
		setCenter(stack);
						
		spreadsheet.setOnDragOver(new EventHandler<DragEvent>() {
		    public void handle(DragEvent event) {
		    	Dragboard db = event.getDragboard();
		    	
		        /* data is dragged over the target */
		        /* accept it only if it is not dragged from the same node 
		         * and if it has a string data */
		        if (event.getGestureSource() != spreadsheet && db.hasString()) {
		            /* allow for both copying and moving, whatever user chooses */
		            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		           
		            PickResult pick = event.getPickResult();
		            if(pick != null) {
		            	Node node = pick.getIntersectedNode();
		            	
		            	if(node instanceof CellView  && db.getString().equals(DragManager.DRAG_CODE)) {
		            		int columns = inspector.getDragged().getColumnsNum();
		            		int rows = inspector.getDragged().getRowsNum();
		            		
		            		CellView cellView = (CellView)node;
		            		spreadsheet.dragOver(cellView, rows, columns);
		            	}
		            }
		            
		           // spreadsheetView.addEventHandler(MouseEvent.MOUSE_MOVED, eventHandler);
		        }

		        event.consume();
		    }
		});
				
		spreadsheet.setOnDragDropped(new EventHandler <DragEvent>() {
	            public void handle(DragEvent event) {
	                /* data dropped */
	                /* if there is a string data on dragboard, read it and use it */
	                Dragboard db = event.getDragboard();
	                boolean success = false;
	                if (db.hasString() && db.getString().equals(DragManager.DRAG_CODE)) {
	                	spreadsheet.dragEnded(inspector.getDragged());
	                    success = true;
	                }
	                /* let the source know whether the string was successfully 
	                 * transferred and used */
	                event.setDropCompleted(success);
	                
	                event.consume();
	            }
	        });
	}
	
	public VisFrame getVisFrame() {
		return inspector.getVisualizationFrame();
	}
	
	public void reset() {
		spreadsheet.reset();
		transformField.disable(true);
		transformField.setDefaultLabel();
	}

	public void setSelector(SelectTool select) {
		interactor.setSelector(select);
	}

	public void update() {
		spreadsheet.update();
	}
}


