package fr.inria.structgraphics.ui.spreadsheet;

import static impl.org.controlsfx.i18n.Localization.asKey;
import static impl.org.controlsfx.i18n.Localization.localize;

import java.io.IOException;
import java.util.List;

import org.controlsfx.control.spreadsheet.Grid;
import org.controlsfx.control.spreadsheet.GridBase;
import org.controlsfx.control.spreadsheet.SpreadsheetCell;
import org.controlsfx.control.spreadsheet.SpreadsheetCell.CornerPosition;

import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.types.ColorProperty;
import fr.inria.structgraphics.types.ConstrainedDoubleProperty;
import fr.inria.structgraphics.ui.Draggable;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable.DataType;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnection;

import org.controlsfx.control.spreadsheet.SpreadsheetCellType;
import org.controlsfx.control.spreadsheet.SpreadsheetColumn;
import org.controlsfx.control.spreadsheet.SpreadsheetView;

import impl.org.controlsfx.spreadsheet.CellView;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TablePosition;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class Spreadsheet extends SpreadsheetView {
	
	private ObservableList<ObservableList<SpreadsheetCell>>	rows;

	private DataView dataView;
	private AreaInteractor interactor;
	private SpreadsheetArea dragOverArea = null;
	private MenuItem showAxis, showParentAxis, showLegend, showNode; 
	
	public Spreadsheet(DataView dataView, AreaInteractor interactor) {
		super(createEmptyGrid());

		this.dataView = dataView;
		
	    for(SpreadsheetColumn column: getColumns()){
	          column.setPrefWidth(100);
	    }
	    
	    rows = getGrid().getRows();
	    
	    getChildren().get(0).setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {				
				DataVariable variable = interactor.getActiveVariable();
				
				interactor.clickedOn(getSelectionModel().getSelectedCells());
				if(variable == null) {
					showAxis.setDisable(true);
					showParentAxis.setDisable(true);
					showLegend.setDisable(true);
				}
				else {
					if((variable.isX() || variable.isY() || variable.isHeight()) && variable.isNested()) {
						showAxis.setDisable(false);
						if(!variable.scaleShownProperty.get())
							showAxis.setText("Show on inner axis");
						else showAxis.setText("Hide from inner axis");
					} else {
						showAxis.setText("Show on inner axis");
						showAxis.setDisable(true);
					}
					
					if(!variable.getParentGroup().isEmpty() && ((variable.isX() /*&& !variable.isNested()*/) || variable.isY() || variable.isHeight() || (variable.isWidth() /*&& !variable.isNested()*/) )) { // TODO: Check if there is a parent axis!!!!
						showParentAxis.setDisable(false);
						if(!variable.scaleShownPropertyOuter.get())
							showParentAxis.setText("Show on outer axis");
						else showParentAxis.setText("Hide from outer axis");
					} else {
						showParentAxis.setText("Show on outer axis");
						showParentAxis.setDisable(true);
					}
					
					if(/*(variable.isFill() || variable.isShape() || variable.isStroke() || variable.isThickness()) &&*/ variable.type == DataType.Symbolic) {
						showLegend.setDisable(false);
						if(!variable.legendShownProperty.get())
							showLegend.setText("Show on legend");
						else showLegend.setText("Hide from legend");	
					} else {
						showLegend.setText("Show on legend");
						showLegend.setDisable(true);
					}
					
					if(variable.isID() || variable.isX() || variable.isY() || variable.isHeight() || variable.isWidth() || variable.isWeight()
							|| variable.isStroke()) {
						showNode.setDisable(false);
						if(!variable.nodeShownProperty.get())
							showNode.setText("Show on glyphs");
						else showNode.setText("Hide from glyphs");	
					} else {
						showNode.setText("Show on glyphs");
						showNode.setDisable(true);
					}
				}
			}
		});
	    
	    this.interactor = interactor;
	}
	
		
	public List<SpreadsheetArea> getAreas() {
		return interactor.getAreas();
	}
	
	public void addArea(SpreadsheetArea area) {
		interactor.addArea(area);
	}
	
	public void setCellValues(int row, int column, DataVariable variable) {
		for(int i = 0; i < variable.getLength(); ++i) {
			for(int j = 0; j < variable.getWidth(i); ++j)
				setCellValue(row + i, column + j, i, j, variable);
		}	
	}
	
	public void setCellValue(int row, int column, int vindex, int hindex, DataVariable variable) {
		SpreadsheetCellAdaptable cell = (SpreadsheetCellAdaptable)getCell(row, column);
		Property property = variable.getProperty(vindex, hindex);
		
		cell.setType(SpreadsheetCellType.OBJECT);
		cell.setGraphic(null);
			
		// TODO: For now, I only handle double properties.... I will come back here later and fix the rest of the types!!!! 
		// TODO: This is tricky!!!!
		cell.setPropertyListener(property, new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				//if(cell.isPropertyListenerLocked()) return;
				
				cell.lockCellListener(true);
				String value = variable.transformation.get().toData(property.getValue());
				if(value!=null) cell.itemProperty().set(value);
				cell.lockCellListener(false);
				
				if(property instanceof ColorProperty) cell.setStyle("-fx-background-color: #" + property.getValue().toString().substring(2) + ";");
			}
		}); 

		cell.itemProperty().set(variable.transformation.get().toData(property.getValue())); // Init the value....
		if(property instanceof ColorProperty) cell.setStyle("-fx-background-color: #" + property.getValue().toString().substring(2) + ";");
		
		
		cell.setCellListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
	
				if(cell.isCellListenerLocked()) return;				
				//cell.lockPropertyListener(true);
				
	            if (property instanceof ConstrainedDoubleProperty) {
	               ((ConstrainedDoubleProperty)property).updateValue((double) variable.transformation.get().fromData(property, cell.getText())); 
	            }
	            else property.setValue(variable.transformation.get().fromData(property, cell.getText()));	
	            
				//cell.lockPropertyListener(false);
			}
		});
		
		/*
		cell.setCellListener(new ChangeListener<Object>() {
			@Override
			public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
	
				if(cell.isCellListenerLocked()) return;				
				//cell.lockPropertyListener(true);
				
	            if (property instanceof ConstrainedDoubleProperty) {
	               ((ConstrainedDoubleProperty)property).updateValue((double) variable.transformation.get().fromData(property, oldValue, newValue)); 
	            }
	            else property.setValue(variable.transformation.get().fromData(property, oldValue, newValue));	
	            
				//cell.lockPropertyListener(false);
			}
		});*/
		
	}
	
	
	public void setCellLabel(int row, int column, int vindex, DataVariable variable) {
		SpreadsheetCellAdaptable cell = (SpreadsheetCellAdaptable)getCell(row, column);
		cell.setType(SpreadsheetCellType.STRING);
		
		Circle circle = new Circle(0,0,5);
		circle.setStroke(Color.CORNFLOWERBLUE);
		circle.setFill(null);
		cell.setGraphic(circle);
		
		cell.setStyle("-fx-background-color: #cece0d;");		
		
		cell.itemProperty().set(variable.getName(vindex).get());
		cell.setCellListener(new InvalidationListener() {
			/*
			@Override
			public void changed(ObservableValue observable, Object oldValue Object newValue) {
				StringProperty name = variable.getName(vindex);
				if(name != null) name.set(newValue.toString());
			}*/

			@Override
			public void invalidated(Observable observable) {
				StringProperty name = variable.getName(vindex);
				if(name != null) name.set(cell.getText());
			}
		});
		variable.getName(vindex).addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				cell.itemProperty().set(newValue);
			}
		});
	}

	public void setCellLabel(int row, int column, String newName) {
		SpreadsheetCellAdaptable cell = (SpreadsheetCellAdaptable)getCell(row, column);
		cell.itemProperty().set(newName);
	}
	
	public void clear(int row, int column) {// System.err.println("clear " + row + ", " + column);
		SpreadsheetCellAdaptable cell = (SpreadsheetCellAdaptable)getCell(row, column);
		cell.setCellListener(null);
		cell.setPropertyListener(null, null);
		cell.setGraphic(null);
		
		cell.setType(SpreadsheetCellType.STRING);
		cell.setStyle("-fx-background-color: #ffffff;");
		cell.itemProperty().set("");	
		
		cell.deactivateCorner(CornerPosition.BOTTOM_LEFT);
		cell.deactivateCorner(CornerPosition.BOTTOM_RIGHT);
		cell.deactivateCorner(CornerPosition.TOP_LEFT);
		cell.deactivateCorner(CornerPosition.TOP_RIGHT);
	}
	
	
	private static Grid createEmptyGrid() {
		GridBase grid = new GridBase(800, 26);

	     ObservableList<ObservableList<SpreadsheetCell>> rows = FXCollections.observableArrayList();
	     for (int row = 0; row < grid.getRowCount(); ++row) {
	         final ObservableList<SpreadsheetCell> list = FXCollections.observableArrayList();
	         for (int column = 0; column < grid.getColumnCount(); ++column) {
	             //list.add(SpreadsheetCellType.DOUBLE.createCell(row, column, 1, 1, null));
	        	 SpreadsheetCellAdaptable cell = new SpreadsheetCellAdaptable(row, column, 1, 1, SpreadsheetCellType.OBJECT);

	        	 //cell.addEventHandler(eventType, eventHandler);
	        	 
	        	 list.add(cell);
	         }
	         rows.add(list);
	     }
	     grid.setRows(rows);
	     
	     return grid;
	}
	
	
	public static int toColumnIndex(CellView cellView) {
		return cellView.getTableView().getColumns().indexOf(cellView.getTableColumn());
	}
	
	public SpreadsheetCellAdaptable getCell(int row, int column) {		
		return (SpreadsheetCellAdaptable)rows.get(row).get(column);
	}

	public SpreadsheetCellAdaptable getCell(int row, String columnName) {
		return getCell(row, columnName.charAt(0) - 'A');
	}
		
	public SpreadsheetCellAdaptable toCell(CellView cellView) {
		return getCell(cellView.getIndex(), cellView.getTableColumn().getText());
	}
	
	public void dragOver(CellView cellView, int width, int height) {		
		if(dragOverArea != null) {
			dragOverArea.updateDrag(cellView);
		}
		else dragOverArea = new SpreadsheetArea(this, cellView, width, height);
		
		dragOverArea.updateDrag(cellView);
	}
	
	public void dragEnded(Draggable draggable) {		
		if(dragOverArea != null) dragOverArea.dragEnded();
		dragOverArea.setContent(draggable);
		
		if(dragOverArea != null) {
			interactor.addArea(dragOverArea);
			dragOverArea = null;
		}
	}

	public void reset() {
		dragOverArea = null;
		
		setGrid(createEmptyGrid());

	    for(SpreadsheetColumn column: getColumns()){
	          column.setPrefWidth(100);
	    }
	    
	    rows = getGrid().getRows();
	    interactor.reset();
	}
	
	
	private void markVariable(DataVariable variable, SpreadsheetCell.CornerPosition position, boolean activate) {
		int row = variable.row();
		int column = variable.column();
		int width = variable.getWidth();
		
		for(int i = 0; i < width; ++i) {
			SpreadsheetCell cell = getGrid().getRows().get(row).get(column + i);
			if(activate) cell.activateCorner(position);
			else cell.deactivateCorner(position);
		}
	}
	
	private void showOnGraph() {
		showOnGraph(interactor.getActiveVariable());
	}
	
	private void showOnParentGraph() {
		showOnParentGraph(interactor.getActiveVariable());
	}
	
	private void showOnLegend() {
		showOnLegend(interactor.getActiveVariable());
	}	

	private void showOnNode() {
		showOnNode(interactor.getActiveVariable());
	}	
	
	public void showOnGraph(DataVariable variable) {
		if(variable != null) {			
            markVariable(variable, SpreadsheetCell.CornerPosition.TOP_LEFT, !variable.scaleShownProperty.get());
			
           	List<VisBody> collections = variable.getLevelGroups();
           	for(VisBody collection: collections) ((VisCollection)collection).showVariableOnAxis(variable, false);
 			
			variable.scaleShownProperty.set(!variable.scaleShownProperty.get());
			
			if(variable.scaleShownProperty.get()) {
				variable.scaleShownProperty.addListener(new InvalidationListener() {
					@Override
					public void invalidated(Observable observable) {
						markVariable(variable, SpreadsheetCell.CornerPosition.TOP_LEFT, false);
						variable.scaleShownProperty.removeListener(this);
					}
				});
			}
		}
	}
	
	public void showOnParentGraph(DataVariable variable) {
		if(variable != null) {		
            markVariable(variable, SpreadsheetCell.CornerPosition.BOTTOM_LEFT, !variable.scaleShownPropertyOuter.get());

           	List<VisBody> collections = variable.getParentGroup();
           	for(VisBody collection: collections) {
           		((VisCollection)collection).showVariableOnAxis(variable, true);
           	}
			
			variable.scaleShownPropertyOuter.set(!variable.scaleShownPropertyOuter.get());
			
			if(variable.scaleShownPropertyOuter.get()) {
				variable.scaleShownPropertyOuter.addListener(new InvalidationListener() {
					@Override
					public void invalidated(Observable observable) {
						markVariable(variable, SpreadsheetCell.CornerPosition.BOTTOM_LEFT, false);
						variable.scaleShownPropertyOuter.removeListener(this);
					}
				});
			}
		}
	}
	
	public void showOnLegend(DataVariable variable) {
		if(variable != null) {
	        markVariable(variable, SpreadsheetCell.CornerPosition.TOP_RIGHT, !variable.legendShownProperty.get());

			VisBody collection = variable.getCollection();
			((VisCollection)collection).showVariableOnLegend(variable);
			
			variable.legendShownProperty.set(!variable.legendShownProperty.get());			
		}
	}
	
	public void showOnNode(DataVariable variable) {
		if(variable != null) {        			
	        markVariable(variable, SpreadsheetCell.CornerPosition.BOTTOM_RIGHT, !variable.nodeShownProperty.get());
			variable.nodeShownProperty.set(!variable.nodeShownProperty.get());		
	        
			if(variable.isWeight() && variable.getCollection() instanceof LineConnectedCollection) {
				((LineConnectedCollection)variable.getCollection()).showLabels(variable);
			} else {
	            for(int i=0; i < variable.getLength(); ++i) {
	            	for(int j = 0; j < variable.getWidth(); ++j) {
	            		Property property = variable.getProperty(i,j);
	            		if(property.getBean() instanceof Mark) {
	            			((Mark)property.getBean()).showLabel(variable, property);
	            		}
	            	}
	            }				
			}
		}
	}
	
	
    /**
     * Create a menu on rightClick with two options: Copy/Paste This can be
     * overridden by developers for custom behavior.
     * 
     * @return the ContextMenu to use.
     */
    public ContextMenu getSpreadsheetViewContextMenu() {
        final ContextMenu contextMenu = new ContextMenu();
        
        showAxis = new MenuItem(localize(asKey("spreadsheet.view.menu.axis")));
        showAxis.setGraphic(new ImageView(new Image(Spreadsheet.class.getResourceAsStream("NumberAxis.png"))));
        showAxis.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.SHORTCUT_DOWN));
        showAxis.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                showOnGraph();
            }
        });
        
        showParentAxis = new MenuItem(localize(asKey("spreadsheet.view.menu.parentaxis")));
        showParentAxis.setGraphic(new ImageView(new Image(Spreadsheet.class.getResourceAsStream("NumberAxis.png"))));
        showParentAxis.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.SHORTCUT_DOWN));
        showParentAxis.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                showOnParentGraph();
            }
        });
        
        showLegend = new MenuItem(localize(asKey("spreadsheet.view.menu.legend")));
        showLegend.setGraphic(new ImageView(new Image(Spreadsheet.class.getResourceAsStream("legend.png"))));
        showLegend.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.SHORTCUT_DOWN));
        showLegend.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                showOnLegend();
            }
        });
        
        showNode = new MenuItem(localize(asKey("spreadsheet.view.menu.node")));
        showNode.setGraphic(new ImageView(new Image(Spreadsheet.class.getResourceAsStream("legend.png"))));
        showNode.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.SHORTCUT_DOWN));
        showNode.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                showOnNode();
            }
        });
                
  	    final MenuItem copyItem = new MenuItem(localize(asKey("spreadsheet.view.menu.copy"))); //$NON-NLS-1$  
        copyItem.setGraphic(new ImageView(new Image(Spreadsheet.class.getResourceAsStream("copySpreadsheetView.png")))); //$NON-NLS-1$
        copyItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHORTCUT_DOWN));
        copyItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                copyClipboard();
            }
        });

        final MenuItem pasteItem = new MenuItem(localize(asKey("spreadsheet.view.menu.paste"))); //$NON-NLS-1$
        pasteItem.setGraphic(new ImageView(new Image(Spreadsheet.class
                .getResourceAsStream("pasteSpreadsheetView.png")))); //$NON-NLS-1$
        pasteItem.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.SHORTCUT_DOWN));
        pasteItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                pasteClipboard();
            }
        });
        
        final Menu cornerMenu = new Menu(localize(asKey("spreadsheet.view.menu.comment"))); //$NON-NLS-1$
        cornerMenu.setGraphic(new ImageView(new Image(Spreadsheet.class
                .getResourceAsStream("comment.png")))); //$NON-NLS-1$

        final MenuItem topLeftItem = new MenuItem(localize(asKey("spreadsheet.view.menu.comment.top-left"))); //$NON-NLS-1$
        topLeftItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
              //  TablePosition<ObservableList<SpreadsheetCell>, ?> pos =  getCellsView().getFocusModel().getFocusedCell();
                TablePosition<ObservableList<SpreadsheetCell>, ?> pos = getEditingCell();
                SpreadsheetCell cell = getGrid().getRows().get(pos.getRow()).get(pos.getColumn());
                if(cell.isCornerActivated(SpreadsheetCell.CornerPosition.TOP_LEFT)) 
                	cell.deactivateCorner(SpreadsheetCell.CornerPosition.TOP_LEFT);
                else cell.activateCorner(SpreadsheetCell.CornerPosition.TOP_LEFT);
                
                }
        });
        final MenuItem topRightItem = new MenuItem(localize(asKey("spreadsheet.view.menu.comment.top-right"))); //$NON-NLS-1$
        topRightItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                //TablePosition<ObservableList<SpreadsheetCell>, ?> pos = cellsView.getFocusModel().getFocusedCell();
            	TablePosition<ObservableList<SpreadsheetCell>, ?> pos = getEditingCell();
                SpreadsheetCell cell = getGrid().getRows().get(pos.getRow()).get(pos.getColumn());
                if(cell.isCornerActivated(SpreadsheetCell.CornerPosition.TOP_RIGHT)) 
                	cell.deactivateCorner(SpreadsheetCell.CornerPosition.TOP_RIGHT);
                else cell.activateCorner(SpreadsheetCell.CornerPosition.TOP_RIGHT);
            }
        });
        final MenuItem bottomRightItem = new MenuItem(localize(asKey("spreadsheet.view.menu.comment.bottom-right"))); //$NON-NLS-1$
        bottomRightItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                //TablePosition<ObservableList<SpreadsheetCell>, ?> pos = cellsView.getFocusModel().getFocusedCell();
            	TablePosition<ObservableList<SpreadsheetCell>, ?> pos = getEditingCell();
                SpreadsheetCell cell = getGrid().getRows().get(pos.getRow()).get(pos.getColumn());
                if(cell.isCornerActivated(SpreadsheetCell.CornerPosition.BOTTOM_RIGHT)) 
                	cell.deactivateCorner(SpreadsheetCell.CornerPosition.BOTTOM_RIGHT);
                else cell.activateCorner(SpreadsheetCell.CornerPosition.BOTTOM_RIGHT);
            }
        });
        final MenuItem bottomLeftItem = new MenuItem(localize(asKey("spreadsheet.view.menu.comment.bottom-left"))); //$NON-NLS-1$
        bottomLeftItem.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                //TablePosition<ObservableList<SpreadsheetCell>, ?> pos = cellsView.getFocusModel().getFocusedCell();
            	TablePosition<ObservableList<SpreadsheetCell>, ?> pos = getEditingCell();
                SpreadsheetCell cell = getGrid().getRows().get(pos.getRow()).get(pos.getColumn());
                if(cell.isCornerActivated(SpreadsheetCell.CornerPosition.BOTTOM_LEFT)) 
                	cell.deactivateCorner(SpreadsheetCell.CornerPosition.BOTTOM_LEFT);
                else cell.activateCorner(SpreadsheetCell.CornerPosition.BOTTOM_LEFT);
            }
        });

        // I use corner annotations for marking visualized variables
      //  cornerMenu.getItems().addAll(topLeftItem, topRightItem, bottomRightItem, bottomLeftItem);
        
        contextMenu.getItems().addAll(showAxis, showParentAxis, showLegend, showNode, copyItem, pasteItem);
        
        return contextMenu;

    }


	public void clean() {
		interactor.reset();
	}

	public void update() {
		interactor.update();
	}

}
