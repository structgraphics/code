package fr.inria.structgraphics.ui.spreadsheet;

import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class MappingProperty extends SimpleObjectProperty<Object> implements Comparable<MappingProperty> {

	public MappingProperty(Property property) {
		super(property.getBean(), property.getName());
		set(property.getValue());
		this.bind(property);
	}
	
	public MappingProperty(Object obj) {
		set(obj);
	}
	
	public MappingProperty(Object bean, String name) {
		super(bean, name);
	}

	@Override
	public int compareTo(MappingProperty prop) {
		if(equals(prop)) {
			return 0;
		}
		else {
			if(get() instanceof Double) return ((Double)get()).compareTo((Double)prop.get());
			else return get().toString().compareTo(prop.get().toString());		
		}
	}
	
	public MappingProperty getCopy() {
		MappingProperty copy = new MappingProperty(getBean(), getName());
		copy.set(this.get());
		
		return copy;
	}
	
	@Override
	public boolean equals(Object o) { 
		if(o instanceof Property) {
			Object val1 = getValue();
			Object val2 = ((Property)o).getValue();
			if(val1 instanceof Double && val2 instanceof Double) {
				return Math.abs(((Double)val1) - ((Double)val2)) < .001;
			}
			else return ((Property)o).getValue().equals(getValue());
		}
		else return false;
	}
}
