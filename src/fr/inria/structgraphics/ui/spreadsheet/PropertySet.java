package fr.inria.structgraphics.ui.spreadsheet;

import java.util.TreeSet;

public class PropertySet extends TreeSet<MappingProperty> {

	public PropertySet(DataVariable variable) {
		for(int i = 0; i < variable.getLength(); ++i) {
			for(int j = 0; j< variable.getWidth(); ++j)
				add(new MappingProperty(variable.getProperty(i, j)));
		}
	}
}
