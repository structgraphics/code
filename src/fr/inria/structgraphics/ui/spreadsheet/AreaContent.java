package fr.inria.structgraphics.ui.spreadsheet;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class AreaContent {
	
	private Map<String, DataVariable> variables = new TreeMap<>();

	public AreaContent() {}
	
	public AreaContent(String name, DataVariable variable) {
		variables.put(name, variable);
	}
	
	public void addVariable(String name, DataVariable variable) {
		variables.put(name, variable);
	}
	
	public DataVariable getVariable(String name) {
		return variables.get(name);
	}
	
	public Set<String> getVariableNames(){
		return variables.keySet();
	}
	
	public Collection<DataVariable> getVariables(){
		return variables.values();
	}

	public DataVariable getVariable(int row, int column) {
		for(DataVariable var: variables.values()) 
			if(var.contains(row, column)) return var;
			//if(var.inHeader(row, column)) return var;
		
		return null;
	}

	public int getVIndex(DataVariable variable, int column) {
		if(variable == null) return 0;
		else return variable.getColumnIndex(column);
	}
	
}
