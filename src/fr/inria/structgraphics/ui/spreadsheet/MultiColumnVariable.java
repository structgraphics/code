package fr.inria.structgraphics.ui.spreadsheet;

import java.util.ArrayList;
import java.util.List;

import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import javafx.beans.property.ListProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MultiColumnVariable extends DataVariable {

	private ListProperty<Property> properties;
	protected List<SimpleStringProperty> names = new ArrayList<SimpleStringProperty>();

	private boolean isConnectionId = false;
	
	public MultiColumnVariable(VisBody collection, int row, int column, ListProperty<Property> properties) {
		super(collection, row, column);
		this.properties = properties;		
		
		for(int i=0; i < getWidth(); ++i)
			names.add(new SimpleStringProperty(properties.getName()));
				
		setTransformation();		
	}
	
	@Override
	public String getPropertyName() {
		return properties.getName();
	}
	
	public void setConnectionId() {
		isConnectionId = true;
	}
	
	public void updateProperties(ListProperty<Property> properties) {
		this.properties = properties;	
	}

	@Override
	public StringProperty getName(int vindex) {
		return names.get(vindex);
	}
	
	@Override 
	public int getWidth() {
		int w = 0;
		for(int i = 0; i < getLength(); ++i) {
			w = Math.max(w, getWidth(i));
		}
		
		return w;
	}
	
	public boolean isConnectionNodeID() {
		return isConnectionId;
	}
	
	@Override
	public int getWidth(int vindex) {
		Property prop = properties.get(vindex);
		if(prop instanceof ListProperty) return ((ListProperty)properties.get(vindex)).size();
		else return 1;
	}
	
	@Override
	public int getLength() {
		return properties.getSize(); 
	}
	
	@Override
	public int getColumnIndex(int columnIndex) {
		return columnIndex - this.column;
	}
	
	@Override
	public Property getProperty(int vindex, int hindex) {
		Property prop = properties.get(vindex);
		if(prop instanceof ListProperty) {
			if(hindex < getWidth(vindex)) return (Property) ((ListProperty)prop).get(hindex);
			else return null;
		}
		else if(hindex < 1) return prop;
		else return null;
	}
	
	@Override
	public boolean contains(int row, int column) {
		return column >= this.column && column < this.column + getWidth() && row >= this.row && row <= this.row + getLength();
	}
	
}
