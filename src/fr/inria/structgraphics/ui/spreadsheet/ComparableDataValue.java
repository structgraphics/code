package fr.inria.structgraphics.ui.spreadsheet;

import javafx.beans.property.Property;

public class ComparableDataValue implements Comparable<ComparableDataValue> {

	private Property value;
	
	public ComparableDataValue(Property value) {
		this.value = value;
	}
	
	public Object getValue() {
		return value.getValue();
	}
	
	public Property getProperty() {
		return value;
	}
	
	@Override
	public int compareTo(ComparableDataValue obj) {
		if(value.getValue() instanceof Double && obj.value.getValue() instanceof Double)
			return ((Double)value.getValue()).compareTo((Double)obj.value.getValue());
		else return value.getValue().toString().compareTo(obj.value.getValue().toString());
	}

	@Override 
	public boolean equals(Object obj) {
		if(obj instanceof ComparableDataValue) return value.getValue().equals(((ComparableDataValue)obj).value.getValue());
		else return false;
	}
	
	@Override
	public String toString() {
		return value.getValue().toString();
	}
}
