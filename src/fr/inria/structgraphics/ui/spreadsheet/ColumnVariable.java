package fr.inria.structgraphics.ui.spreadsheet;

import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import javafx.beans.property.ListProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ColumnVariable extends DataVariable {

	private ListProperty<Property> properties;
	protected StringProperty name = new SimpleStringProperty();

	public ColumnVariable(VisBody collection, int row, int column, ListProperty<Property> properties) {
		super(collection, row, column);
		name.set(properties.getName());
		this.properties = properties;		
		
		setTransformation();		
	}
	
	public void updateProperties(ListProperty<Property> properties) {
		this.properties = properties;	
	}
	
	@Override
	public StringProperty getName(int vindex) {
		return name;
	}
	
	@Override
	public String getPropertyName() {
		return properties.getName();
	}
	
	@Override
	public int getLength() {
		return properties.size();
	}
	
	@Override
	public int getWidth() {
		return 1;
	}
	
	@Override
	public Property getProperty(int vindex, int hindex) {
		return properties.get(vindex);
	}
	
	@Override
	public boolean contains(int row, int column) {
		return this.column == column && row >= this.row && row <= this.row + properties.size();
	}
	
}
