package fr.inria.structgraphics.ui.spreadsheet;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import fr.inria.structgraphics.graphics.Mark;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class DiscreteTransformation extends DataTransformation {
	
	private final static String[] DEFAULT_CATEGORIES = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
	
	private TreeMap<MappingProperty, StringProperty> map;
	private int i = 0;
	
	private Hashtable<Mark, String> symbols = new Hashtable<>();
	
	public DiscreteTransformation(DataVariable variable) {
		super(variable);
		
		expression = new SimpleStringProperty();
		
		PropertySet propertySet = new PropertySet(variable);
		map = new TreeMap<>();
						
		for(MappingProperty property: propertySet) {	
			MappingProperty copy = property.getCopy();
			
			// TODO: This is to synchronize the x,y with the mapped symbols
			// I may need to consider two different types of discrete transforms to
			// support this behavior
			if(variable.isX() || variable.isY()) copy.bind(property);
			
			SimpleStringProperty symbol = (i < DEFAULT_CATEGORIES.length) ?  new SimpleStringProperty(DEFAULT_CATEGORIES[i++]) : new SimpleStringProperty("A" + (i++));			
			symbols.put((Mark)property.getBean(), symbol.get());
			map.put(copy, symbol);
			
			property.addListener(new ChangeListener() {
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {
					updateExpression();
					// TODO: Need to find a way to handle Ids!!!
				}
			});
		}
		
		updateExpression();
		change.set(!change.get());
	}
	
	void printMap() {
		for(MappingProperty prop:map.keySet()) {
			System.err.println(prop + "----> " + map.get(prop));
		}
	}
	
	public TreeMap<MappingProperty, StringProperty> getMap(){
		return map;
	}

	@Override
	public void refresh() {
		/////
		PropertySet propertySet = new PropertySet(variable);
		
		TreeMap<MappingProperty, StringProperty> map_ = new TreeMap<>();
		for(MappingProperty property: propertySet) {
			MappingProperty copy = property.getCopy();
			
			// TODO: This is to synchronize the x,y with the mapped symbols
			// I may need to consider two different types of discrete transforms to
			// support this behavior
			if(variable.isX() || variable.isY()) copy.bind(property);
			
			if(map.containsKey(property)) map_.put(copy, map.get(property));
			else {
				String symbol = symbols.get((Mark)property.getBean());
				if(symbol == null)
					map_.put(copy, (i < DEFAULT_CATEGORIES.length) ?  new SimpleStringProperty(DEFAULT_CATEGORIES[i++]) : new SimpleStringProperty("A" + (i++)));
				else map_.put(copy, new SimpleStringProperty(symbol));
			}
		}
		
		map.clear();
		map = map_;
		
		updateExpression();
	}
	

	public void replace(String from, String to) {
		for(MappingProperty prop: map.keySet()) {
			if(prop.get().toString().equals(from)) {
				map.get(prop).set(to);
				symbols.put((Mark)prop.getBean(), to);	
				updateExpression();
				change.set(!change.get());
			}				
		}
	}
	
	@Override
	public Object fromData(Property source/*, Object oldValue*/, Object newValue) {
		if(variable.isID()) {
			for(MappingProperty prop: map.keySet()) {
				if(prop.getValue().equals(source.getValue())) {
					map.get(prop).set(newValue.toString());
					symbols.put((Mark)prop.getBean(), newValue.toString());	
					updateExpression();
					change.set(!change.get());
					return prop.get();
				}				
			}
		}
		
		
		// TODO: Do it better!!!
		for(MappingProperty prop: map.keySet()) {
			if(map.get(prop).get().equals(newValue)) {
				symbols.put((Mark)prop.getBean(), newValue.toString());				
				return prop.get();
			}
		}

		String oldValue = toData(source.getValue());
		for(MappingProperty prop: map.keySet()) {
			StringProperty entry = map.get(prop);
			if(entry.get().equals(oldValue)) { // System.err.println(prop + " >>> " + newValue);
				map.get(prop).set(newValue.toString());
				symbols.put((Mark)prop.getBean(), newValue.toString());				
				
			//	map.put(prop, new SimpleStringProperty(newValue.toString())); // TODO Update this!!!!!!
				updateExpression();
				change.set(!change.get());
				return prop.get();
			}
		}
		
		return null;
	}
	
	
	@Override
	public String toData(Object val) { 
		StringProperty value = map.get(new MappingProperty(val));
		if(value != null) return map.get(new MappingProperty(val)).get();
		else return null;
	}
	
	@Override
	public boolean setExpression(String expr) {
		change.set(!change.get());
		
		return false;
	}
	
	private void updateExpression() {
		StringBuffer str = new StringBuffer(variable.getPropertyName() + ": ");

		List<MappingProperty> unmapped = new ArrayList<>();
		for(MappingProperty prop:map.keySet()) {
			StringProperty value = map.get(prop);
			if(value != null)
				str.append("{ " + prop.get() + " -> " + value.get() + " } ");
			else // TODO: ....
				str.append("{ " + prop.get() + " -> " + "?" + " } ");
		}
		
		expression.set(str.substring(0, str.length() - 1));
		
		//System.err.println(expression.get());
		// TODO: There is still some buggy behavior, not sure why.... 
	}

	/*
	public void update(Object value, String newValue) { 
		map.put(new MappingProperty(value), new SimpleStringProperty(newValue));
		updateExpression();
	}*/
}
