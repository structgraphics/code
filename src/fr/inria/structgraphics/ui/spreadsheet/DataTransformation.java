package fr.inria.structgraphics.ui.spreadsheet;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.StringProperty;

public abstract class DataTransformation {	
	protected DataVariable variable;
	protected StringProperty expression;
	protected BooleanProperty change = new SimpleBooleanProperty(true);

	public DataTransformation(DataVariable variable) {
		this.variable = variable;
	}
	
//	public abstract Object fromData(Property source, Object oldValue, Object newValue);
	public abstract Object fromData(Property source, Object newValue);
	public abstract String toData(Object val);
	public abstract boolean setExpression(String expr);
	
	public BooleanProperty changeProperty() {
		return change;
	}
	
	public StringProperty getExpression() {
		return expression;
	}

	public void refresh() {}
}
