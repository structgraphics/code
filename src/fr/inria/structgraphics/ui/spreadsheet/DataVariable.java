package fr.inria.structgraphics.ui.spreadsheet;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.types.PropertyName;
import javafx.beans.InvalidationListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public abstract class DataVariable {

	public enum DataType {
		Symbolic, Functional
	}
	
	public BooleanProperty scaleShownProperty = new SimpleBooleanProperty(false);
	public BooleanProperty scaleShownPropertyOuter = new SimpleBooleanProperty(false);
	public BooleanProperty legendShownProperty = new SimpleBooleanProperty(false);
	public BooleanProperty nodeShownProperty = new SimpleBooleanProperty(false);
	
	protected MathTransformation mathTransformation;
	protected DiscreteTransformation discreteTransformation;
	
	protected VisBody collection;
	
	protected int row, column;
	protected DataType type = DataType.Functional;
	protected DataTransformationProperty transformation = new DataTransformationProperty();
	
	protected InvalidationListener listener;
	
	public DataVariable(VisBody collection, int row, int column) {
		this.collection = collection;
		this.row = row;
		this.column = column;
		
		transformation.addListener(new ChangeListener<DataTransformation>() {
			@Override
			public void changed(ObservableValue<? extends DataTransformation> observable, DataTransformation oldTransform,
					DataTransformation newTranform) {
				if(newTranform instanceof DiscreteTransformation) type = DataType.Symbolic;
				else type = DataType.Functional;
			}
		});
	}
	
	protected void setTransformation() {
		if(type == DataType.Functional) {
			if(mathTransformation == null) mathTransformation = new MathTransformation(this);
			transformation.set(mathTransformation);
		}
		else {
			discreteTransformation = new DiscreteTransformation(this);
			//if(discreteTransformation == null) discreteTransformation = new DiscreteTransformation(this);
			transformation.set(discreteTransformation);
		}
	}
	
	public boolean isConnectionNodeID() {
		return false;
	}

	public boolean isNested() {
		if(collection == null) return false;
			
		int level = getLevel() + 1;
		int colLevel = collection.getLevel();
		
		return level < colLevel;		
	}
	
	public abstract String getPropertyName();
	public abstract int getLength();
	public abstract int getWidth();
	public abstract Property getProperty(int vindex, int hindex);
	public abstract boolean contains(int row, int column);
	public abstract StringProperty getName(int vindex);
	
	public ArrayList<String> getNames(){
		ArrayList<String> names = new ArrayList<>();
		for(int i = 0; i < getWidth(); ++i) {
			names.add(getName(i).get());
		}
		
		return names;
	}
		
	public void setInvalidationListener(InvalidationListener listener) {
		if(this.listener != null)
			transformationProperty().removeListener(this.listener);
		
		this.listener = listener;
		transformationProperty().addListener(listener);
	}
	
	public void updatePosition(int row, int column) {
		this.row = row;
		this.column = column;
	}
	
	public int getWidth(int row) {
		return getWidth();
	}

	public int row() {
		return row;
	}
	public int column() {
		return column;
	}
	
	public boolean isNumeric() {
		return firstProperty() instanceof DoubleProperty;
	}
	
	public Property firstProperty() {
		return getProperty(0, 0);
	}
	
	public DataType getType() {
		return type;
	}
	
	public DataType getDataType() {
		return type;
	}
	
	public void setDataType(DataType type) {
		this.type = type;
		setTransformation();
	}
	
	public boolean inHeader(int row, int column) {
		return this.column == column && this.row == row;
	}
	
	public ObjectProperty<DataTransformation> transformationProperty() {
		return transformation;
	}

	public int getColumnIndex(int columnIndex) {
		return 0;
	}
	
	public VisBody getCollection() {
		return collection;
	}
	
	public List<VisBody> getLevelGroups(){
		List<VisBody> list = new ArrayList<VisBody>();
		
		/*
		int level = getLevel() + 1;
		collection.addToGroupLevel(list, level);
		*/
		if(collection != null) collection.addToInnerCollections(list);
				
		return list;
	}
	
	public List<VisBody> getParentGroup() {
		List<VisBody> list = new ArrayList<VisBody>();
		
		/*
		int level = getLevel() + 2;
		if(collection != null) collection.addToParentGroupLevel(list, level);
		*/
		if(collection != null) collection.addToOuterCollection(list);
		
		return list;		
	}
	
	public boolean isX() {
		return getPropertyName().contains("x");
	}
	
	public boolean isY() {
		return getPropertyName().contains("y");
	}
	
	public boolean isWidth() {
		return getPropertyName().contains("width");
	}
	
	public boolean isHeight() {
		return getPropertyName().contains("height");
	}
	
	public boolean isWeight() {
		return getPropertyName().contains("weight");
	}
	
	
	public boolean isShape() {
		return getPropertyName().contains("shape");
	}
	
	public boolean isFill() {
		return getPropertyName().contains("fill");
	}
		
	public boolean isStroke() {
		return getPropertyName().contains("stroke");
	}
	
	public boolean isThickness() {
		return getPropertyName().contains("thickness");
	}
	
	public boolean isID() {
		return getPropertyName().endsWith("id");
	}

	public int getLevel() {
		return new PropertyName(getPropertyName()).getLevel();
		/*
		String name = getPropertyName();
		String str = name.replaceAll("[a-z]", "").replace("-", "");
		if(str.isEmpty()) return 0;
		else return Integer.parseInt(str);*/
	}
	
	public SortedSet<ComparableDataValue> getDiscreteValues() {
		SortedSet<ComparableDataValue> list = new TreeSet<>();
		
		for(int i = 0; i < getWidth(); ++i) {
			for(int j = 0; j < getLength(); ++j) {
				list.add(new ComparableDataValue(getProperty(j, i)));
			}
		}
		return list;
	}


}
