package fr.inria.structgraphics.ui.spreadsheet;

import java.util.ArrayList;
import java.util.Map;

import org.controlsfx.control.spreadsheet.*;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.ShapeMark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.types.ColorProperty;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.ui.Draggable;
import fr.inria.structgraphics.ui.Draggable.Type;
import fr.inria.structgraphics.ui.viscanvas.groupings.PropertyStructure;
import impl.org.controlsfx.spreadsheet.CellView;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.Property;
import javafx.scene.paint.Paint;

public class SpreadsheetArea {
	private int x, y, w, h;
	
	private Spreadsheet sheet;
	private AreaContent content;
	
	private AreaSourceInfo info;
	
	public SpreadsheetArea(Spreadsheet sheet, int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		
		this.sheet = sheet;
	}
	
	public SpreadsheetArea(Spreadsheet sheet, CellView cellView, int w, int h) {
		this(sheet, cellView.getIndex(), Spreadsheet.toColumnIndex(cellView), w, h);
	}
	
	public void dragEnded() {		
		for(int row = x; row < x + w; ++row) {
			for(int column = y; column < y + h; ++column) {
				sheet.getCell(row, column).recoverStyle();
			}	
		}
	}

	public void updateDrag(CellView cellView) {
		int x_ =  cellView.getIndex();
		int y_ = Spreadsheet.toColumnIndex(cellView);
		Grid grid = sheet.getGrid();

		for(int i = Math.min(x, x_); i <  Math.max(x, x_) + w && i < grid.getRowCount(); ++i) {
			for(int j = Math.min(y, y_); j <  Math.max(y, y_) + h && j < grid.getColumnCount(); ++j) {				
				if(i >= x_ && i < x_ + w && j >= y_ && j < y_ + h) {
					SpreadsheetCellAdaptable cell = sheet.getCell(i, j);
					cell.saveStyle();
					cell.setStyle("-fx-background-color: #eeff66;");	
				}
				else {
					sheet.getCell(i, j).recoverStyle();
				}
			}
		}
		
		x = x_;
		y = y_;

	}
	
	public int getStartRow() {
		return x;
	}

	public int getStartColumn() {
		return y;
	}
	
	public int nrows() {
		return h;
	}
	
	public int ncolumns() {
		return w;
	}
	
	public DataVariable getVariable(int row, int column) {
		return content.getVariable(row, column);
	}
	
	public int getVIndex(DataVariable variable, int column) {
		return content.getVIndex(variable, column);
	}

	public boolean contains(int row, int column) {
		if(row >= x && row < row + h && column >= y && column < y + w) return true;
		else return false;
	}
	
	public void update(CellView cellView) {
		x = cellView.getIndex();
		y = Spreadsheet.toColumnIndex(cellView);
	}

	
	public void setContent(Draggable draggable) {		
		info = new AreaSourceInfo(draggable);
		refresh();
	}

	public void setInfo(AreaSourceInfo info) {
		this.info = info;
		refresh();
	}
	
	
	public void clearSketcherLabels() {
		for(DataVariable variable : content.getVariables()) {
			if(variable.scaleShownProperty.get()) sheet.showOnGraph(variable);
			if(variable.scaleShownPropertyOuter.get()) sheet.showOnParentGraph(variable);
			if(variable.legendShownProperty.get()) sheet.showOnLegend(variable);
			if(variable.nodeShownProperty.get()) sheet.showOnNode(variable);
		}
	}
	
	public void clear() {
		for(int row = x; row < x + h + 1; ++row) {
			for(int column = y; column < y + w; ++column) {
				sheet.clear(row, column);	
			}	
		}
	}
	
	public Type getType() {
		return info.getType();
	}
	
	public boolean refresh() {
		if(!info.sourceExists()) {
			info = null;
			return false;
		}
		
		switch(info.getType()) {
			case Value:
				if(content != null) return true;
				
				VisBody visbody = info.getVisBody();
				
				final Property property = info.getProperty();
				DataVariable variable = new VectorVariable(visbody, x, y, property);
				content = new AreaContent(property.getName(), variable);

				///////////////////////////
				sheet.setCellLabel(x, y, 0, variable);	
				sheet.setCellValues(x + 1, y, variable);
				
				variable.setInvalidationListener(new InvalidationListener() {
					@Override
					public void invalidated(Observable observable) {
						Object val = property.getValue(); 
						if(property instanceof DoubleProperty) ((DoubleProperty)property).setValue((Number)val);
						else if(property instanceof ColorProperty) ((ColorProperty)property).setValue((Paint)val);
						else property.setValue(val);
					}
				});
				
				return true;
			
			case Column:			
			case Table:
				if(content == null) content = new AreaContent();
				
				visbody = info.getVisBody();
				Map<PropertyName, FlexibleListProperty> propertiesMap = null;
				
				/// TODO: This needs more work, no?
				if(info.isNetwork) {
					propertiesMap = ((LineConnectedCollection)visbody).getFlowConnections().getCompactVariableList(info.getNames());
				} else {
					PropertyStructure structure = visbody.getChildPropertyStructure();
					propertiesMap = info.isWide() ? structure.getVariableList(info.getNames()) : structure.getFullVariableList(info.getNames());					
				}

			// TODO: UPdata the properties within the variables!!!!			
				int j = 0;
				int rows = 0;
				for(FlexibleListProperty col: propertiesMap.values()) {
					variable = content.getVariable(col.getName());
					
					if(!info.isWide()) {
						if(variable == null) {
							variable = new ColumnVariable(visbody, x, y + j, col);
							content.addVariable(col.getName(), variable);
						} else {
							variable.updatePosition(x, y + j);
							((ColumnVariable)variable).updateProperties(col);
							variable.transformation.get().refresh();
						}
						
						sheet.setCellLabel(x, y + j, 0, variable);
						sheet.setCellValues(x + 1, y + j, variable);
						j++;
						rows = Math.max(rows, variable.getLength());
						
					} else {
						if(variable == null) {
							variable = new MultiColumnVariable(visbody, x, y + j, col);
							content.addVariable(col.getName(), variable);
						} else {
							variable.updatePosition(x, y + j);
							((MultiColumnVariable)variable).updateProperties(col);
							variable.transformation.get().refresh();
						}
												
						for(int k = 0; k < variable.getWidth(); ++k) {
							sheet.setCellLabel(x, y + j + k, k, variable);							
						}
						
						// This is just to handle flow the names of connection ids
						if(info.isNetwork && variable.isID()) {
							((MultiColumnVariable)variable).setConnectionId();
							sheet.setCellLabel(x, y + j, "source");
							sheet.setCellLabel(x, y + j + 1, "destination");
						}

						sheet.setCellValues(x + 1, y + j, variable);
						j+=variable.getWidth();
						rows = Math.max(rows, variable.getLength());
					}
					
					
					variable.setInvalidationListener(new InvalidationListener() {
						@Override
						public void invalidated(Observable observable) {
							for(Property prop: col) {
								if(prop instanceof ListProperty) {
									ListProperty<Property> list = (ListProperty<Property>)prop;
									for(Property prop_: list) {
										// TODO: an uggly way to enforce the property change!!!!!
										Object val = prop_.getValue(); 
										if(prop_ instanceof DoubleProperty) ((DoubleProperty)prop_).setValue((Number)val);
										else if(prop_ instanceof ColorProperty) ((ColorProperty)prop_).setValue((Paint)val);
										else prop_.setValue(val);
									}
								} else {
									// TODO: an uggly way to enforce the property change!!!!!
									Object val = prop.getValue(); 
									if(prop instanceof DoubleProperty) ((DoubleProperty)prop).setValue((Number)val);
									else if(prop instanceof ColorProperty) ((ColorProperty)prop).setValue((Paint)val);
									else prop.setValue(val);
								}
							}
						}
					});
					
					// Update Labels on Nodes
					for(Property prop: col) {
						if(prop instanceof ListProperty) {
							ListProperty<Property> list = (ListProperty<Property>)prop;
							for(Property prop_: list) {
			            		if(prop_.getBean() instanceof ShapeMark) {
			            			ShapeMark mark = (ShapeMark)prop_.getBean();
			            			if(variable.nodeShownProperty.get() && !mark.isLabelShown(variable))
			            				mark.showLabel(variable, prop_);
			            		}
							}
						} else {
		            		if(prop.getBean() instanceof ShapeMark) {
		            			ShapeMark mark = (ShapeMark)prop.getBean();
		            			if(variable.nodeShownProperty.get() && !mark.isLabelShown(variable))
		            				mark.showLabel(variable, prop);
		            		}
						}
					}
					
				}
				
				w = j;
				h = rows;
				
				return true;
		}		
		
		return false;
	}

	
	public Container getSource() {
		return info.source;
	}
	
	public VisBody getVisBody() {
		return info.visbody;
	}
	
	public boolean isWide() {
		return info.wide;
	}
	
	public boolean isNetwork() {
		return info.isNetwork;
	}
	
	public ArrayList<PropertyName> getPropertyNames() {
		return info.names;
	}

	public AreaContent getAreaContent() {
		return content;
	}
}
