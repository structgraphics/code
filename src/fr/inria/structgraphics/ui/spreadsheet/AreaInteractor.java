/**
 * Copyright (c) 2014, 2015 ControlsFX
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *     * Neither the name of ControlsFX, any associated website, nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL CONTROLSFX BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package fr.inria.structgraphics.ui.spreadsheet;

import java.util.ArrayList;
import java.util.List;

import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.ui.tools.SelectTool;
import fr.inria.structgraphics.ui.tools.Tool;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnection;
import javafx.beans.property.Property;
import javafx.collections.ObservableList;
import javafx.scene.control.TablePosition;

public class AreaInteractor {
	
	private List<SpreadsheetArea> areas = new ArrayList<>();
	private DataVariable activeVariable = null;
	private TransformationField transformField;
	
	private SelectTool selectTool;
	
	public AreaInteractor(TransformationField transformField){
		this.transformField = transformField;
	}

	public List<SpreadsheetArea> getAreas(){
		return areas;
	}
	
	private LineConnectedCollection selectedCollection;
	
	// It controls the interaction with the spreadsheet... 
	public void clickedOn(ObservableList<TablePosition> selectedCells) { //System.err.println(x);
		activeVariable = null;
		
		if(selectedCells.size() == 1) {
			TablePosition pos = selectedCells.get(0);
			
			for(SpreadsheetArea area: areas) {
				activeVariable = area.getVariable(pos.getRow(), pos.getColumn());
				if(activeVariable != null) {
					int vindex = area.getVIndex(activeVariable, pos.getColumn());
					transformField.disable(false);
					transformField.setVariable(activeVariable, vindex); // TODO: ?????
					
					int rowindex = pos.getRow() - activeVariable.row - 1;
					if(rowindex >= 0) {
						Property property = activeVariable.getProperty(rowindex, vindex);
						if(property == null) {
							selectTool.select(null, this);
							break;
						}
						Object obj = property.getBean();
						
						if(obj instanceof Mark) {
							if(selectedCollection != null) {
								selectedCollection.selectConnection(null);
								selectedCollection = null;
							}
							selectTool.setActive(true);
							selectTool.select((Mark)obj, this);
						} else if(obj instanceof FlowConnection) {
							LineConnectedCollection collection = ((FlowConnection)obj).getCollection();
							if(selectedCollection != null && selectedCollection != collection) {
								selectedCollection.selectConnection(null);
							}
							
							collection.selectConnection((FlowConnection)obj);
							selectedCollection = collection;
						}
					} else {
						if(selectedCollection != null) {
							selectedCollection.selectConnection(null);
							selectedCollection = null;
						}
						selectTool.select(null, this);
					}

					return;
				}
			}
		}

		if(selectedCollection != null) {
			selectedCollection.selectConnection(null);
			selectedCollection = null;
		}
		selectTool.select(null, this);
		transformField.disable(true);
		transformField.setDefaultLabel();
	}
	
	public void addArea(SpreadsheetArea area) {
		areas.add(area);
	}
	
	public void update() { 
		for(int i = areas.size() - 1; i >=0; --i) {
			SpreadsheetArea area = areas.get(i);
			area.clear();
			if(!area.refresh()) areas.remove(i);
		}
	}
	
	public void reset() {
		for(SpreadsheetArea area: areas) {
			area.clearSketcherLabels();
			area.clear();
		}
		
		areas.clear();
	}
	
	public DataVariable getActiveVariable() {
		return activeVariable;
	}

	public void setSelector(SelectTool select) {
		this.selectTool = select;
	}
}
