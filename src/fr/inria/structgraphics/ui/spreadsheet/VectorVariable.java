package fr.inria.structgraphics.ui.spreadsheet;

import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.types.FlexibleListProperty;
import javafx.beans.binding.ListExpression;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class VectorVariable extends DataVariable {

	private Property property;
	protected StringProperty name = new SimpleStringProperty();

	public VectorVariable(VisBody collection, int row, int column, Property property) {
		super(collection, row, column);
		
		name.set(property.getName());
		
		this.property =  property;
		/*
		if(property instanceof FlexibleListProperty) {
			this.property = ((FlexibleListProperty) property).get(0);
		} else this.property = property;*/
						
		setTransformation();
	}
	
	@Override
	public String getPropertyName() {
		return property.getName();
	}
	
	@Override
	public StringProperty getName(int vindex) {
		return name;
	}
	
	@Override
	public int getLength() {
		return 1; 
	}
	
	@Override
	public int getWidth() {
		if(property instanceof FlexibleListProperty)
			return ((ListExpression<Property>) property).size();
		else return 1;
	}

	@Override
	public Property getProperty(int vindex, int hindex) {
		if(property instanceof FlexibleListProperty)
			return ((FlexibleListProperty)property).get(hindex);
		else return property;
	}

	@Override
	public boolean contains(int row, int column) {
		return (this.column == column && row >= this.row && row <= this.row + 1);
	}
}
