package fr.inria.structgraphics.ui.spreadsheet;

import java.text.DecimalFormat;

import org.matheclipse.core.eval.ExprEvaluator;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.parser.client.SyntaxError;
import org.matheclipse.parser.client.math.MathException;

import fr.inria.structgraphics.types.AlignmentXProperty;
import fr.inria.structgraphics.types.AlignmentYProperty;
import fr.inria.structgraphics.types.ColorProperty;
import fr.inria.structgraphics.types.ComponentRefXProperty;
import fr.inria.structgraphics.types.ComponentRefYProperty;
import fr.inria.structgraphics.types.ShapeProperty;
import fr.inria.structgraphics.ui.inspector.ColorStringConverter;
import fr.inria.structgraphics.ui.inspector.DoubleStringConverter;
import fr.inria.structgraphics.ui.inspector.GeneralStringConverter;
import fr.inria.structgraphics.ui.inspector.RefXStringConverter;
import fr.inria.structgraphics.ui.inspector.RefYStringConverter;
import fr.inria.structgraphics.ui.inspector.ShapeStringConverter;
import fr.inria.structgraphics.ui.inspector.XAlignmentStringConverter;
import fr.inria.structgraphics.ui.inspector.YAlignmentStringConverter;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class MathTransformation extends DataTransformation {
	private boolean decimals = false;
	
	private StringProperty invexpression;
	private GeneralStringConverter converter;

	private StringProperty expression0;
	
	private String varName;
	
	public MathTransformation(DataVariable variable) {
		super(variable);
		
		expression0 = new SimpleStringProperty(variable.getPropertyName());
		varName = fixVarName(variable.getPropertyName());
		expression = new SimpleStringProperty(varName);
		invexpression = new SimpleStringProperty("$Y");
		
		expression0.addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				expression.set(fixVarName(newValue));
			}
		});
		
		expression.addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				expression0.set(newValue.replaceAll(varName, variable.getPropertyName()));
			}
		});
		
		Property prop = variable.firstProperty();
		if(prop instanceof ColorProperty) {
			converter = new ColorStringConverter();
		} else if(prop instanceof ShapeProperty) {
			converter = new ShapeStringConverter();
		} else if(prop instanceof DoubleProperty) {
			converter = new DoubleStringConverter();
		} else if(prop instanceof AlignmentXProperty) {
			converter = new XAlignmentStringConverter();
		} else if(prop instanceof AlignmentYProperty) {
			converter = new YAlignmentStringConverter();
		} else if(prop instanceof ComponentRefXProperty) {
			converter = new  RefXStringConverter();
		} else if(prop instanceof ComponentRefYProperty) {
			converter = new  RefYStringConverter();
		}
	}

	@Override
	public Object fromData(Property source, /*Object oldValue,*/ Object newValue) {
		if(variable.firstProperty() instanceof ColorProperty) {
			String stringValue = newValue.toString();
			if(stringValue == null || stringValue.isEmpty()) newValue = toData(source.getValue());//oldValue;
			
			/*
			long val = paintToNumber(newValue.toString());
			ExprEvaluator util = new ExprEvaluator();
			String expr = "$Y=" + val + ";" + invexpression.get();		
			IExpr result = util.evaluate(expr);
			*/
			return stringToPaint(newValue.toString());
			//return stringToPaint(newValue);
		} else if(variable.firstProperty() instanceof DoubleProperty) {
			double val;
			
			try{
				val = Double.parseDouble(newValue.toString());
				
				ExprEvaluator util = new ExprEvaluator();
				String expr = "$Y=" + val + ";" + invexpression.get();	
				IExpr result = util.evaluate(expr);	
				
				return result.evalDouble(); 

			} catch(NumberFormatException e) {
				//val = Double.parseDouble(oldValue.toString());
				return source.getValue();
			}	
			
		} else if(variable.firstProperty() instanceof StringProperty) {
			return newValue;
		}
		else return converter.fromString(newValue.toString());
	}
	
	
	public double transform(Object val) {
		if(val instanceof Double){
			ExprEvaluator util = new ExprEvaluator();
			String expr = varName + "=" + val +";" + expression.get();
			return util.evaluate(expr).evalDouble();	
		} else return 0;
	}
	
	public double inverseTransform(double val) {
		ExprEvaluator util = new ExprEvaluator();
		String expr = "$Y=" + val + ";" + invexpression.get();	
		IExpr result = util.evaluate(expr);	
		
		return result.evalDouble(); 
	}
	
	@Override
	public String toData(Object val) {
		if(val instanceof Paint) {
			//ExprEvaluator util = new ExprEvaluator();
			//String expr = varName + "=" + val +";" + expression.get();
			
			//return numberToPaint((long) util.evaluate(expr).evalDouble()).toString();
			return val.toString();
		} else if(val instanceof Double){
			DecimalFormat df = new DecimalFormat(decimals ? "#.0" : "#"); 
			
			ExprEvaluator util = new ExprEvaluator();
			String expr = varName + "=" + val +";" + expression.get();
		
			return df.format(util.evaluate(expr).evalDouble());	
		} else if(val instanceof String || converter == null) {
			return val.toString();
		}
		else {
			return converter.toString(val);
		}
	}
	
	private Paint stringToPaint(String string) {
		Color color;
		try {
			color = Color.web(string);
		} catch(Exception e) {
			color = Color.LIGHTGRAY;
		}
		
		return color;
	}
	
	/*
	private Long paintToString(String paintValue) { 
		Color color;
		
		try {
			color = Color.web(paintValue);
		} catch(Exception e) {
			color = Color.LIGHTGRAY;
		}
				
		return Long.parseLong(color.toString().substring(2), 16);		
	}
	
	private Paint stringToPaint(String string) {
		try {
			//return Paint.valueOf("0x" + Long.toHexString(number));
			return Paint.valueOf(string);
		} catch(IllegalArgumentException e) {
			return null;
		}
	}*/
	
	@Override
	public StringProperty getExpression() {
		return expression0;
	}
	
	@Override
	public boolean setExpression(String expr) { 
		expr = fixVarName(expr);
		
		if(!expr.contains(varName)) return false;
		
		try {
			ExprEvaluator util = new ExprEvaluator();
			IExpr result = util.evaluate(expr);
			expr = result.toString();
			if(!expr.contains(varName)) return false;
			
			/*
			if(expr.contains("Abs(" + varName + ")")) {
				expr.replaceAll("Abs(" + varName + ")", varName);
				isAbsolute = true;
			}*/
			
			String str = "Roots($Y==" + expr + ", " +  varName +")";			
			str = util.evaluate(str).toString();
			
			invexpression.set(str.substring(str.indexOf("=") + 2));
			expression.set(expr);
			change.set(!change.get());
			
			return true;
		} catch (SyntaxError e) {
			return false;
        } catch (MathException me) { 
        	return false;
        } catch (Exception e) {
        	return false;
        }
	}
	
	
	// This is to artificially fix the variable name problems!!!!
	private static String fixVarName(String var) {
		return var.replaceAll("[A-Z].", "").replaceAll("[a-z]-[x-y]", "");
	}
	
	
}
