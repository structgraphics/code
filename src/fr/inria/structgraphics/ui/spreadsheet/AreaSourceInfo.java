package fr.inria.structgraphics.ui.spreadsheet;

import java.util.ArrayList;
import java.util.Collection;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.ui.Draggable;
import fr.inria.structgraphics.ui.Draggable.Type;
import javafx.beans.property.Property;

public class AreaSourceInfo {

	protected Container source;
	protected VisBody visbody = null;
	protected Type type;
	protected boolean wide = false;
	protected boolean isNetwork = false;
	protected ArrayList<PropertyName> names = new ArrayList<>();
	
	protected Property property = null; // Only used for single self properties.
	
	public AreaSourceInfo(Draggable draggable) {	
		
		source = draggable.getContainer();
		visbody = draggable.getGroup();
		type = draggable.getType();
		wide = draggable.isInWide();
		isNetwork = draggable.isNetwork();
				
		//if(source == null || source.getChildPropertyStructure().getProperties().containsKey(key))
		
		Object content = draggable.getPropertiesContent();
		if(content instanceof FlexibleListProperty) {
			PropertyName propName = ((FlexibleListProperty) content).getPropertyName();
			names.add(propName);

			if(type == Type.Value) {
				property = ((FlexibleListProperty) content).get(0);				
			}
			
		} else {
			for(FlexibleListProperty list: (Collection<FlexibleListProperty>)content) {
				names.add(list.getPropertyName());				
			}
		}
	}
	
	public AreaSourceInfo(Container container, VisBody visbody, ArrayList<PropertyName> names, Type type, boolean isWide, boolean isNetwork) {
		this.source = container;
		this.visbody = visbody;
		this.type = type;
		this.wide = isWide;
		this.isNetwork = isNetwork;
		this.names = names;
		
		if(type == Type.Value && !names.isEmpty()) { 
			property = ((Mark)container).getProperty(names.get(0));
		}
	}
	
	public Container getSource() {
		return source;
	}

	public VisBody getVisBody() {
		return source.getVirtualGroup();
	}
	
	public Type getType() {
		return type;
	}
	
	public boolean isWide() {
		return wide;
	}
	
	public boolean showsNetwork() {
		return isNetwork;
	}

	public Property getProperty() {
		return property;
	}
	
	public ArrayList<PropertyName> getNames(){
		return names;
	}
	
	public boolean sourceExists() {
		return (source != null) && (source instanceof VisFrame || source instanceof Mark && ((Mark)source).getRoot() != null);
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		
		if(property!= null) buffer.append("? ");
		else if(wide) buffer.append("> ");
		
		for(PropertyName name: names) {
			buffer.append(name.toString());
			buffer.append(" ");
		}
		
		buffer.append("\n");
		
		return buffer.toString();
	}
}
