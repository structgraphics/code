package fr.inria.structgraphics.ui.spreadsheet;

import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable.DataType;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class TransformationField extends HBox implements ChangeListener<String> {

	private DataVariable activeVariable = null;
	private int activeIndex = 0;
	private final static String DEFAULT_TEXT = "T(x)=";
	
	private ComboBox typeMenu = new ComboBox();
	private TextField textField = new TextField();
	private Label label;
		
	public TransformationField(HBox toolBar) {

		typeMenu.getItems().addAll("Symbolic", "Functional");
		typeMenu.setValue("Functional");
		
		typeMenu.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {				
				if(activeVariable != null && typeHasChanged(newValue)) {
					// activeVariable.transformation.get().change.set(!activeVariable.transformation.get().change.get());
			// TODO: This is buggy. It should not change when clicking on a different variable!!!!	
					if(newValue.equals("Symbolic")) {
						activeVariable.setDataType(DataType.Symbolic);
						activeVariable.transformation.get().expression.addListener(expressionListener);
					} else {
						activeVariable.setDataType(DataType.Functional);
						activeVariable.transformation.get().expression.removeListener(expressionListener);
					}
					
					textField.setText(activeVariable.transformationProperty().get().getExpression().get());	
					
					if(activeVariable.collection instanceof VisCollection) ((VisCollection)activeVariable.collection).updateExtraComponents();
				}
			}
		});
				
		textField.setOnKeyPressed(new EventHandler<KeyEvent>()
	    {
	        @Override
	        public void handle(KeyEvent e)
	        {
	            if (e.getCode().equals(KeyCode.ENTER))
	            {
	            	activeVariable.transformationProperty().get().setExpression(textField.getText());
	            	textField.setText(activeVariable.transformationProperty().get().getExpression().get());
	            		
	                label.requestFocus();
	            }
	        }
	    });
		
		label = new Label(DEFAULT_TEXT);
		label.setStyle("-fx-padding:5px;");
		label.setAlignment(Pos.CENTER_RIGHT);
		
		//textField.setPadding(new Insets(10));
		this.setPadding(new Insets(10));
		
		HBox.setHgrow(textField, Priority.ALWAYS);
		
		getChildren().add(toolBar);
		getChildren().add(typeMenu);
		getChildren().add(label);
		getChildren().add(textField);
		
		disable(true);
	}
	
	private boolean typeHasChanged(String value) {
		if(value.equals("Symbolic")) {
			 if(activeVariable.getDataType() == DataType.Functional) return true;
			 else return false;
		}
		else {
			if(activeVariable.getDataType() == DataType.Functional) return false;
			else return true;
		}
	}
	
	public void disable(boolean disable) {
		typeMenu.setDisable(disable);
		label.setDisable(disable);
		textField.setDisable(disable);
	}
	
	public void setDefaultLabel() {
		label.setText(DEFAULT_TEXT);
		textField.setText("");
	}

	public void setVariable(DataVariable variable, int vindex) {
		label.setText(variable.getName(vindex).get() + "=");
		textField.setText(variable.transformationProperty().get().getExpression().get());
		
		if(this.activeVariable == variable) return;
		else if(activeVariable != null) {
			// TODO: ????
			activeVariable.getName(activeIndex).removeListener(this);
			activeVariable.transformation.get().expression.removeListener(expressionListener);
		}
		
		activeVariable = variable;
		activeIndex = vindex;
		activeVariable.getName(vindex).addListener(this);
		
		if(variable.getDataType() == DataType.Symbolic) {
			typeMenu.setValue("Symbolic");
			variable.transformation.get().expression.addListener(expressionListener);
		} else typeMenu.setValue("Functional");		
	}
	
	
	private ChangeListener expressionListener = new ChangeListener<String>() {
		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) { 
			textField.setText(activeVariable.transformationProperty().get().getExpression().get());	
		}
	};
	
	@Override
	public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		label.setText(newValue + "=");	
	}
	
	

}
