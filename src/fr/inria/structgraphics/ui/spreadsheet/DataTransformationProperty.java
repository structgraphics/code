package fr.inria.structgraphics.ui.spreadsheet;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class DataTransformationProperty extends SimpleObjectProperty<DataTransformation>{

	public DataTransformationProperty() {
		super();
	}
	
	private ChangeListener listener = new ChangeListener<Boolean>() {
		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
			fireValueChangedEvent();
		}
	};
	
	@Override
	public void set(DataTransformation transformation) {
		if(get() != null) get().changeProperty().removeListener(listener);
		
		super.set(transformation);
		transformation.changeProperty().addListener(listener);
	}
	
	
}
