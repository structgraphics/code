package fr.inria.structgraphics.ui;

import fr.inria.structgraphics.ui.inspector.InspectorView;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

public class DragManager {
	public final static String DRAG_CODE = "InFoGraPHeR";

	private static DragManager instance;
	
	private InspectorView inspector;
	
	public DragManager(InspectorView inspector) {
		this.inspector = inspector;
	}
	
	public static DragManager create(InspectorView inspector) {
		instance = new DragManager(inspector);
		
		return instance;
	}
	
	public static DragManager getSingleton() {
		return instance;
	}
	
	public void observe(Draggable draggable) {
		Node node = draggable.getNode();
		
        node.setOnDragDetected(
        		new EventHandler<MouseEvent>() { public void handle(MouseEvent event) {
                /* drag was detected, start a drag-and-drop gesture*/
                /* allow any transfer mode */
                Dragboard db = node.startDragAndDrop(TransferMode.COPY);
//    System.err.println(draggable.getID());            
                /* Put a string on a dragboard */
                ClipboardContent content = new ClipboardContent();
                content.putString(DRAG_CODE);
                db.setContent(content);
                inspector.setDraggable(draggable);
                
                event.consume();
            }
        });
	}
}
