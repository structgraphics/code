package fr.inria.structgraphics.ui.tools;

import fr.inria.structgraphics.graphics.PositionCoords;
import fr.inria.structgraphics.graphics.ShapeMark;
import fr.inria.structgraphics.graphics.TextualMark;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.ShapeProperty;
import fr.inria.structgraphics.types.ShapeProperty.Type;
import fr.inria.structgraphics.ui.inspector.DisplayUtils;
import fr.inria.structgraphics.ui.utils.CoordTransformer;
import fr.inria.structgraphics.ui.viscanvas.CanvasFrame;
import javafx.scene.Cursor;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class ShapeDrawingTool extends DrawingTool {

	protected ShapeMark tempMark;
	private ShapeProperty.Type type;
	
	public ShapeDrawingTool(CanvasFrame canvas, ShapeProperty.Type type) {
		this.canvas = canvas;
		this.type = type;
		init();
	}
	
	@Override
	protected void init() {
		cursor = Cursor.CROSSHAIR;
		tbutton =  new ToggleButton("", new ImageView(DisplayUtils.getIcon(type.toString())));
		super.init();
	}
	
	public void update() {
		if(tempMark == null) {
			if(!cancelArea.contains(lineTrace.getEndX(), lineTrace.getEndY())) {
				tempMark = createMark();
				canvas.getInspector().update();
				canvas.getInspector().setView(tempMark);
			}
		} else {
			if(!cancelArea.contains(lineTrace.getEndX(), lineTrace.getEndX())) updateMark();
			else { 
				tempMark.dispose(); // TODO: ????
				
				canvas.getInspector().update();
				tempMark = null;
			}
		}
	}
	
	@Override
	public void cancelComplete() {
		super.cancelComplete();
		tempMark.dispose();
		canvas.getInspector().update();
		tempMark = null;
	}

	
	public void handleRelease(MouseEvent event) {
		super.handleRelease(event);
		
	//	tempMark.setHighlight(true, true);
		tempMark = null;
		
	//	canvas.setActiveTool(null);
		
		// TODO
	}


	protected ShapeMark createMark() {
		VisFrame visFrame = canvas.getVisFrame(); // This will eventually become more sophisticated! 
		
		RefX refX = RefX.Center;
		RefY refY = RefY.Bottom;	
	
		double w = (type == Type.Line) ? lineTrace.getEndX() - lineTrace.getStartX() : Math.abs((lineTrace.getEndX() - lineTrace.getStartX()));
		double h = (type == Type.Line) ? lineTrace.getEndY() - lineTrace.getStartY() : Math.abs((lineTrace.getEndY() - lineTrace.getStartY()));
		
		double x = (type == Type.Line) ?
				CoordTransformer.getXPosition(visFrame.getRefCoords(), refX, visFrame.width.get(), lineTrace.getStartX(), w) : 
				CoordTransformer.getXPosition(visFrame.getRefCoords(), refX, visFrame.width.get(), Math.min(lineTrace.getStartX(), lineTrace.getEndX()), w);
		
		double y = (type == Type.Line) ?
				CoordTransformer.getYPosition(visFrame.getRefCoords(), refY, visFrame.height.get(), -lineTrace.getStartY(), h) : 
				CoordTransformer.getYPosition(visFrame.getRefCoords(), refY, visFrame.height.get(), -Math.min(lineTrace.getStartY(), lineTrace.getEndY()), h);
		
		
		ShapeMark mark = ShapeMark.createInstance(visFrame, true, type, w, h);
		
		PositionCoords coords = new PositionCoords(mark, x, y);
		mark.setCoords(coords);
		
		// TODO
		mark.setDefaults();
		
		mark.initProperties();
		mark.updateShape();
		
		return mark;
	}
	
	
	protected void updateMark() { 
		if(tempMark!= null) {
					
			VisFrame visFrame = (VisFrame)tempMark.getContainer();
			
			double w = (type == Type.Line) ? lineTrace.getEndX() - lineTrace.getStartX() : Math.abs((lineTrace.getEndX() - lineTrace.getStartX()));
			double h = (type == Type.Line) ? lineTrace.getEndY() - lineTrace.getStartY() :  Math.abs((lineTrace.getEndY() - lineTrace.getStartY()));
			
			double x = (type == Type.Line) ?
					CoordTransformer.getXPosition(visFrame.getRefCoords(), tempMark.getCoords().getXRef(), visFrame.width.get(), lineTrace.getStartX(), w) : 
					CoordTransformer.getXPosition(visFrame.getRefCoords(), tempMark.getCoords().getXRef(), visFrame.width.get(), Math.min(lineTrace.getStartX(), lineTrace.getEndX()), w);
			
			double y = (type == Type.Line) ?
					CoordTransformer.getYPosition(visFrame.getRefCoords(), tempMark.getCoords().getYRef(), visFrame.height.get(), -lineTrace.getStartY(), h) :
					CoordTransformer.getYPosition(visFrame.getRefCoords(), tempMark.getCoords().getYRef(), visFrame.height.get(), -Math.min(lineTrace.getStartY(), lineTrace.getEndY()), h);
			
			tempMark.setPositionCoords(x, y);
			
			tempMark.width.set(w);
			tempMark.height.set(h);
		}
	}
	
}

