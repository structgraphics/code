package fr.inria.structgraphics.ui.tools;

import java.util.ArrayList;
import java.util.Collection;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.SimpleMark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.persistence.JsonObjectWriter;
import fr.inria.structgraphics.ui.inspector.DisplayUtils;
import fr.inria.structgraphics.ui.spreadsheet.AreaInteractor;
import fr.inria.structgraphics.ui.tools.MarkSelection.SelectionType;
import fr.inria.structgraphics.ui.viscanvas.CanvasFrame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.WindowEvent;

public class SelectTool extends Tool {
	
	protected Circle trace;
	protected double traceRadius = 2;
		
	private boolean pressed = false;
		
	private ContextMenu contextMenu;
	
	private ArrayList<SimpleMark> clipboard = new ArrayList<>();
	
	public SelectTool(CanvasFrame canvas) {
		super(canvas);
	
		trace = createPointTrace(-1, -1);
		trace.setVisible(false);
		
		rectangular = true;
	}
	
	@Override
	protected void init() {
		cursor = Cursor.DEFAULT;
		tbutton = new ToggleButton("", new ImageView(DisplayUtils.getIcon("drag")));
		
		super.init();			
	}
	
	@Override
	public void handleMove(MouseEvent event) { 
		super.handleMove(event);		
		trace.centerXProperty().set(event.getX());
		trace.centerYProperty().set(event.getY());
	}
	
	public void select(Mark mark, Object source) {
		select(mark, source, true);
	}
	
	public void select(Mark mark, Object source, boolean single) {
		selected().setDragAccept(true);		
		selected().archive();
		if(mark != null) selected().add(mark);
		selected().highlight(single);
		
		if(!selected().isEmpty()) {
			pressed = true;
			if(source instanceof AreaInteractor) canvas.getInspector().setView(selected().getMarks());
		} else pressed = false;
	}
	
	public void addSelection(Mark mark, Object source) {
		if(mark != null) selected().add(mark);
		selected().highlight(false);
	}
	
	@Override	
	public void handlePress(MouseEvent event) {				
		super.handlePress(event);
				
		canvas.getCanvas().show(trace);
		
		selected().setDragAccept(true);
		selected().archive();		
		canvas.getVisFrame().monoselect(selected(), trace);
		
		//selected().highlight(true);
		selected().highlightPress();

		if(!selected().isEmpty()) {
			rectangular = false;
			
			pressed = true;
			canvas.getInspector().setView(selected().getMarks());
		} else {
			//canvas.getInspector().setView(canvas.getVisFrame());
			rectangular = true;
			pressed = false;
		}
		
		if(event.isSecondaryButtonDown() && !selected().isEmpty() && selected().getMarks().get(0) instanceof SimpleMark ) {
				if(contextMenu != null) {
					contextMenu.hide();
					contextMenu.getItems().clear();
				}
				else {
					contextMenu = new ContextMenu();
				}
			  					
				addItems(contextMenu);
		        canvas.getSplitPane().setContextMenu(contextMenu);
		}
	}
	
	
	public void addItems(ContextMenu contextMenu) {
		// Move Layer Position Menu Items
		MenuItem item1 = new MenuItem("Bring to Front");
        item1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	selected().bringToFront();
            }
        });
                
		contextMenu.getItems().add(item1);
		//if(selected().isOrderConstrained()) item1.setDisable(true);
		
		MenuItem item2 = new MenuItem("Send to Back");
        item2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	selected().sendToBack();
            }
        });
		contextMenu.getItems().add(item2);
		//if(selected().isOrderConstrained()) item2.setDisable(true);
		
		
		contextMenu.getItems().add(new SeparatorMenuItem());
		
		// Remove marks!
		MenuItem item3 = new MenuItem("Delete");
        item3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	selected().delete();
                  	
        		canvas.getInspector().update();
        		canvas.getDataView().update();
            }
        });
		contextMenu.getItems().add(item3);
			
        MenuItem item4 = new MenuItem("Add to Library");
        item4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	canvas.getLibrary().addToLibrary((SimpleMark)selected().getMarks().get(0));
            }
        });
        contextMenu.getItems().add(item4);
        
        
        MenuItem item5 = new MenuItem("Save As");
        item5.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	canvas.saveAs(canvas.getStage(),
            			JsonObjectWriter.saveToJason(selected().getMarks(), false));
            }
        });
        contextMenu.getItems().add(item5);
        
		
		contextMenu.setOnShowing(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				boolean activeLayers = selected().canChangeLayers();
				
				item1.setDisable(!activeLayers);
				item2.setDisable(!activeLayers);
				item4.setDisable(!selected().canGoToLibrary());
			}
			
		});
		
        contextMenu.setOnHiding(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				canvas.getSplitPane().setContextMenu(null);
			}
		});
	}
	
	@Override
	public void update() {
		if(!pressed) {
			selected().archive();
			if(!cancelArea.contains(lineTrace.getEndX(), lineTrace.getEndY()))
				canvas.getVisFrame().select(selected(), /*lineTrace*/ rectTrace, new CommonContainerSelectFilter());
			
			selected().highlight(false);
			
			//if(selected().getMarks().size() > 1) selected().highlight(false);
			//else selected().highlight(true);
		} else {			
			//if(selected().acceptDragEvent()) 
			selected().translate(lineTrace);
			//canvas.getInspector().update();
		}
	}
	
	private MarkSelection selected() {
		return canvas.selected();
	}
	
	public Collection<Mark> getSelected(){
		return canvas.selected().getMarks();
	}
	
	// TODO: Need to deal with nesting. What if I erase a parent node? 
	@Override
	public void handleRelease(MouseEvent event) {
		if(!pressed){
			if(selected().getMarks().size() > 1) {
				selected().highlight(false);
			}
			else selected().highlight(true);
			
			canvas.getInspector().setView(selected().getMarks());
		}
		
		pressed = false;
		
		super.handleRelease(event);

		for(Mark mark: selected().getMarks()) { 
			mark.mouseRelease();
			// Update the coordinates of the marks in the virtual group enclosing the released mark!!!
			if(mark.getContainer() instanceof VisBody) {
				//((VirtualGroup)mark.getContainer()).lockProperties();
				//((VirtualGroup)mark.getContainer()).refresh();
				//((VirtualGroup)mark.getContainer()).unlockProperties();
			}
			// Linking code
//			mark.dispose();
		}
	}

	
	@Override
	public void setActive(boolean active) { 
		super.setActive(active);
		if(active) {
			selected().highlight(true);
		} else {
			selected().archive();
			selected().highlight(true);
		}
	}
	
	@Override
	public void cancelComplete() {
		super.cancelComplete();
		pressed = false;
	}
	
	
	protected Rectangle createRectSelection(double x, double y, double width, double height) {
		Rectangle rectTrace = new Rectangle(x, y, width, height);
		rectTrace.setStrokeWidth(rectWidth);
		rectTrace.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);
		rectTrace.setStroke(rectColor);
		rectTrace.setFill(rectFill);
		
		return rectTrace;
	}
	
	
	protected Circle createPointTrace(double x, double y) {
		Circle trace = new Circle(x, y, traceRadius);
		trace.setStrokeWidth(1);
		trace.setFill(null);
		trace.setStroke(traceColor);
		
		return trace;
	}
	
	
	protected void copyAction() {
		if(selected().isEmpty()) return;
		else {
			clipboard.clear();
		}
		
		for(Mark mark: selected().getMarks()) { 
			if(mark instanceof SimpleMark) clipboard.add((SimpleMark)mark);
		}
	}
	
	protected void pasteAction(double x, double y) {
		if(clipboard.isEmpty()) return;

		double meanX = 0, meanY = 0;
		for(Mark mark: clipboard) { 			
			meanX += mark.getGlobalX();
			meanY += mark.getGlobalY();
		}
		
		meanX = meanX / clipboard.size();
		meanY = meanY / clipboard.size();
		
		for(SimpleMark mark: clipboard) { 
			SimpleMark copy = mark.createCopy(canvas.getVisFrame(), 0, 0);
			copy.initProperties(); 
			copy.getCoords().x.set(x + mark.getGlobalX() - meanX);
			copy.getCoords().y.set((canvas.getVisFrame().height.get() - y) + mark.getGlobalY() - meanY);
			copy.update();
		}
		
		canvas.getInspector().update();
		
	}
	
}
