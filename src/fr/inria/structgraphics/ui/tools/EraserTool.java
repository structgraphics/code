package fr.inria.structgraphics.ui.tools;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.SimpleMark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.ui.inspector.DisplayUtils;
import fr.inria.structgraphics.ui.viscanvas.CanvasFrame;
import javafx.scene.Cursor;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class EraserTool extends Tool {
	
	private MarkSelection selectedMarks = new MarkSelection();
	
	public EraserTool(CanvasFrame canvas) {
		super(canvas);
				
		traceColor = Color.RED;
		traceWidth = 6;
		
		rectangular = true;
	}
	
	@Override
	protected void init() {
		cursor = Cursor.DEFAULT;
		tbutton =  new ToggleButton("", new ImageView(DisplayUtils.getIcon("eraser")));
		
		super.init();
	}
	
	
	private MarkSelection selected() {
		return canvas.selected();
	}
	
	@Override	
	public void handlePress(MouseEvent event) {	
		super.handlePress(event);		
		
		if(!selected().isEmpty()) {
			selected().archive();
			selected().getMarks().clear();
			selected().highlight(true);
		}
	}
	
	public void handleDrag(MouseEvent event) { 
		super.handleDrag(event);

		selectedMarks.archive();
		if(!cancelArea.contains(event.getX(), event.getY()))
			canvas.getVisFrame().select(selectedMarks, rectTrace, new CommonContainerSelectFilter());
		
		selectedMarks.highlight(false);
	}
	
	
	// TODO: Need to deal with nesting. What if I erase a parent node? 
	public void handleRelease(MouseEvent event) {
		super.handleRelease(event);

		for(Mark mark: selectedMarks.getMarks()) {
			mark.setHighlight(false, true);
			mark.fullDelete();
		}
		
		canvas.getVisFrame().update();
		canvas.getInspector().update();
		canvas.getDataView().update();
		
		selectedMarks.archive();
		//selectedMarks.highlight(false);

		canvas.setActiveTool(null);
	}
	
}
