package fr.inria.structgraphics.ui.tools;

import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.ShapeMark;
import fr.inria.structgraphics.ui.inspector.DisplayUtils;
import fr.inria.structgraphics.ui.viscanvas.CanvasFrame;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class FlowDrawingTool extends DrawingTool {
	
	private ShapeMark origin = null, destination = null;
	
	public FlowDrawingTool(CanvasFrame canvas) {
		super(canvas);
	}
	
	
	@Override
	protected void init() {
		tbutton =  new ToggleButton("", new ImageView(DisplayUtils.getIcon("flow")));
		super.init();
		
		traceColor = Color.FORESTGREEN;
		traceWidth = 2.5;
		
	}
	
	@Override
	public void handleMove(MouseEvent event) {	
		ShapeMark mark = canvas.getVisFrame().getShape(event.getX(), event.getY());
		
		if(mark != null && mark != origin) {
			if(origin != null) origin.showOrigin(false);
			mark.showOrigin(true);
			origin = mark;
		} else if(mark == null && origin != null){
			origin.showOrigin(false);
			origin = null;
		}
	}
	
	@Override
	public void handleDrag(MouseEvent event) {	
		if(origin != null) {
			lineTrace = createTrace(event.getX(), event.getY());
			canvas.getCanvas().show(lineTrace, null, null);
			
			ShapeMark mark = canvas.getVisFrame().getShape(event.getX(), event.getY());
			
			boolean accept = mark != null && mark != origin  && origin.getRootVirtualGroup() == mark.getRootVirtualGroup();
			
			if(accept) {
				LineConnectedCollection collection = (LineConnectedCollection)mark.getRootVirtualGroup();
				if(collection.containtsConnection(origin, mark) || collection.hasCycle(mark, origin)) accept = false;
				/*else if(collection.getChildPropertyStructure().areCommon(origin.height, mark.height)) {
					accept = false;
				}*/
			}
			
			if(accept && mark != destination) {
				if(destination != null) destination.showDestination(false);
				mark.showDestination(true); 
				destination = mark;
			} else if(!accept && destination != null ) {
				destination.showDestination(false);
				destination = null;
			}
		}
	}
	
	@Override
	public void handleRelease(MouseEvent event) {
		if(origin != null && destination != null) {
			LineConnectedCollection collection = (LineConnectedCollection)origin.getRootVirtualGroup();
			collection.createConnection(origin, destination, true);
			
			//canvas.getVisFrame().update();
			canvas.getInspector().generateView(collection);
			canvas.getInspector().update();
			canvas.getInspector().setView(collection);
			canvas.getDataView().update();
		}
		
		if(destination != null) {
			destination.showDestination(false);
			destination = null;
		}
		
		canvas.getCanvas().show(null, null, null);
		handleMove(event);
	}
	
	@Override
	public void handlePress(MouseEvent event) {		
		if(origin != null) {
			super.handlePress(event);
		}
	}
	
	
	@Override
	public void setActive(boolean active) { 
		super.setActive(active);
	}
	
}


