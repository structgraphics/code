package fr.inria.structgraphics.ui.tools;

import java.util.ArrayList;
import java.util.List;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.ui.inspector.DisplayUtils;
import fr.inria.structgraphics.ui.viscanvas.CanvasFrame;
import fr.inria.structgraphics.ui.viscanvas.groupings.Grouping;
import fr.inria.structgraphics.ui.viscanvas.groupings.Grouping.Type;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Cursor;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class GroupCreationTool extends Tool {
	
	private MarkSelection selectedMarks = new MarkSelection();
	
	public GroupCreationTool(CanvasFrame canvas) {
		super(canvas);
				
		traceColor = Color.BLUE;
		traceWidth = 2;
		
		rectangular = true;
	}
	
	@Override
	protected void init() {
		cursor = Cursor.DEFAULT;
		//tbutton =  new ToggleButton("", new ImageView(DisplayUtils.getIcon("construct")));
		tbutton =  new ToggleButton("", new ImageView(DisplayUtils.getIcon("Group")));
		
		super.init();
	}
	
	
	@Override
	public void update() { 
		selectedMarks.archive();
		if(!cancelArea.contains(lineTrace.getEndX(), lineTrace.getEndY())) {
			canvas.getVisFrame().select(selectedMarks, rectTrace, new CommonContainerSelectFilter());
			
			List<Mark> toremove = new ArrayList<>();
			for(Mark mark:selectedMarks.getMarks()) {
				if(mark.getContainer() instanceof VisBody) {
					toremove.add(mark);
				}
			}
			selectedMarks.getMarks().removeAll(toremove);
		}
		
		selectedMarks.highlight(false);
	}
	
	
	@Override
	public void cancelComplete() {
		super.cancelComplete();
	}
	
	
	private Mark nest(Mark mark, int targetLevel) {
		int level = mark.getLevel();
		if(level < targetLevel) {
			ObservableList<Mark> sublist = FXCollections.observableArrayList();
			sublist.add(mark);
			return nest(new Grouping(sublist, Type.Group).getGroup(), targetLevel);
		} else return mark;
	}
	
	
	// TODO: Need to deal with nesting. What if I erase a parent node? 
	public void handleRelease(MouseEvent event) {
		super.handleRelease(event);

		if(selectedMarks.isEmpty()) {
			canvas.setActiveTool(null);
			return;
		}

		// Take care to apply the same level of variables to all the marks that participate in the group
		int maxLevel = 0;
		for(Mark mark: selectedMarks.getMarks()) {
			maxLevel = Math.max(maxLevel, mark.getLevel());
		}
		
		ObservableList<Mark> marks = FXCollections.observableArrayList();
		
		for(Mark mark: selectedMarks.getMarks()) {
			marks.add(nest(mark, maxLevel));
		}
		//////////////////////////////////////////////////////////////////////////////////////////////
		
		Grouping grouping = new Grouping(marks, Type.Group);		
	
		canvas.getInspector().update();
		
		selectedMarks.archive();
		selectedMarks.highlight(false);

		canvas.setActiveTool(null);
	}
	
}
