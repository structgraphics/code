package fr.inria.structgraphics.ui.tools;

import fr.inria.structgraphics.ui.inspector.DisplayUtils;
import fr.inria.structgraphics.ui.viscanvas.CanvasFrame;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;

public class BezierDrawingTool extends DrawingTool {
	
	
	public BezierDrawingTool(CanvasFrame canvas) {
		super(canvas);
	}
	
	@Override
	protected void init() {
		tbutton =  new ToggleButton("", new ImageView(DisplayUtils.getIcon("SVGPath")));
		super.init();
	}
	
}


