package fr.inria.structgraphics.ui.tools;

import java.util.ArrayList;

import fr.inria.structgraphics.graphics.Mark;

public class CommonContainerSelectFilter extends SelectFilter {

	@Override
	public boolean accept(ArrayList<Mark> selectedMarks, Mark mark) {
		return selectedMarks.isEmpty() || selectedMarks.get(0).getContainer() == mark.getContainer();
	}
}
