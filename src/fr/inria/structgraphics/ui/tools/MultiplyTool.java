package fr.inria.structgraphics.ui.tools;

import java.util.List;

import fr.inria.structgraphics.graphics.ConnectedCollection;
import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.Shadow;
import fr.inria.structgraphics.graphics.SimpleMark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.AlignmentProperty.XSticky;
import fr.inria.structgraphics.types.AlignmentProperty.YSticky;
import fr.inria.structgraphics.ui.inspector.DisplayUtils;
import fr.inria.structgraphics.ui.viscanvas.CanvasFrame;
import fr.inria.structgraphics.ui.viscanvas.groupings.PropertyStructure;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Cursor;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;

public class MultiplyTool extends Tool {
		
	protected Circle trace;
	protected double traceRadius = 5;
		
//	private boolean pressed = false;
	
	private SimpleMark selected = null;
	
	public MultiplyTool(CanvasFrame canvas) {
		super(canvas);
	
		trace = createPointTrace(-1, -1);
		trace.setVisible(false);
	}
	
	@Override
	protected void init() {
		cursor = Cursor.DEFAULT;
		tbutton =  new ToggleButton("", new ImageView(DisplayUtils.getIcon("multiply")));
		
		super.init();
	}
	
	@Override
	public void handleMove(MouseEvent event) { 
		trace.centerXProperty().set(event.getX());
		trace.centerYProperty().set(event.getY());
	}
	
	@Override	
	public void handlePress(MouseEvent event) {	
		super.handlePress(event);		
		canvas.getCanvas().show(trace);

		if(!selected().isEmpty()) {
			selected().archive();
			canvas.getVisFrame().monoselect(selected(), null);
			selected().highlight(true);
		}
		
		selected().archive();	

		canvas.getVisFrame().monoselect(selected(), trace);
		selected().highlight(false);
		
		if(!selected().isEmpty()) {
		//	pressed = true;
			canvas.getInspector().setView(selected().getMarks());
			
			Mark mark = selected().getMarks().get(0);
			if(mark instanceof SimpleMark) {
				selected = (SimpleMark)mark;
				selected.setFeedForward(true);
			}

		}// else pressed = false;
	}
	
	@Override
	public void handleDrag(MouseEvent event) { 
		super.handleDrag(event);
		
		if(selected != null) {
			selected.selectCopies(lineTrace);
		}
	}
	
	
	private MarkSelection selected() {
		return canvas.selected();
	}

	
	// TODO: Need to deal with nesting. What if I erase a parent node? 
	@Override
	public void handleRelease(MouseEvent event) { 
		if(selected != null) { 
			List<Shadow> copies = selected.selectCopies(lineTrace);
			if(copies.isEmpty()) {
				selected.setFeedForward(false);
				selected = null;			
				super.handleRelease(event);
				return;
			}
		
			if(selected instanceof ConnectedCollection && ((ConnectedCollection)selected).isCurveSelected() && ((ConnectedCollection)selected).isSingleSibling()) {
				ObservableList<Mark> charts = FXCollections.observableArrayList();
				charts.add(selected);
				
				for(Shadow shadow: copies) {
					SimpleMark mark = shadow.createMark();
					if(mark == null) continue; // TODO : ????
					
					mark.initProperties();
					charts.add(mark);					
				}
				
				Container container = selected.getContainer();
				LineConnectedCollection collection = new LineConnectedCollection(container, true, charts);
				collection.getAlignXProperty().set(YSticky.Yes);
				collection.getAlignYProperty().set(XSticky.Yes);
				
				collection.getChildPropertyStructure().moveToVariable(new PropertyName(selected.strokePaint.getName()));
				
				// TODO
				if(container instanceof VisBody) {
					((VisBody)container).stickToYAxis();
					((VisBody)container).stickToXAxis();
				}
				
				for(Mark mark: collection.getComponents())
					if(mark instanceof ConnectedCollection) ((ConnectedCollection)mark).updateInteractor();
				
			} else {
				Container container = selected.getContainer();
				Mark firstMark = null; 
				PropertyStructure structure = null;
				
				if(container instanceof VisBody && container.getComponents().size() == 1) {
					firstMark = container.getComponentAt(0);
					structure = ((VisBody)container).getChildPropertyStructure();
				}
					
				for(Shadow shadow: copies) {
					SimpleMark mark = shadow.createMark();
					if(mark == null) continue; // TODO : ????
					
					mark.initProperties();			
					if(container instanceof VisBody) {
						if(firstMark != null) {
							if(mark.coords.getX() != firstMark.coords.getX() && structure.isXShared()) {
								structure.moveToVariable(new PropertyName(firstMark.coords.x.getName()));
							} else if(mark.coords.getY() != firstMark.coords.getY() && structure.isYShared()){
								structure.moveToVariable(new PropertyName(firstMark.coords.y.getName()));
							}
						}
						// TODO: I need something extra for marks that are groups themselves
						((VisBody)container).addToGroup(mark);					
					}
					
				}
				if(selected.getContainer() instanceof VisBody) {
					((VisBody)selected.getContainer()).stickToYAxis();
					((VisBody)selected.getContainer()).stickToXAxis();
					
					((VisBody)selected.getContainer()).updateLayout(((VisBody)selected.getContainer()).getComponentAt(0));
				}
			}
			
			canvas.getVisFrame().update();
			canvas.getInspector().update();
			canvas.getDataView().update();
			
			selected.setFeedForward(false);
			selected = null;
		}
		
//		pressed = false;
		
		super.handleRelease(event);

		selected().archive();		
		canvas.getVisFrame().monoselect(selected(), null);
		selected().highlight(false);
		
		canvas.setActiveTool(null);
//		for(Mark mark: selectedMarks.getMarks()) {
			// Linking code
//			mark.dispose();
//		}

		/*
		canvas.getInspector().update();
		
		selectedMarks.archive();
		*/
	}
	
	protected Circle createPointTrace(double x, double y) {
		Circle trace = new Circle(x, y, traceRadius);
		trace.setStrokeWidth(1);
		trace.setFill(null);
		trace.setStroke(traceColor);
		
		return trace;
	}
	
}
