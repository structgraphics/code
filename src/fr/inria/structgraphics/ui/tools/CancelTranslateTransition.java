package fr.inria.structgraphics.ui.tools;

import javafx.animation.Transition;
import javafx.scene.shape.Line;
import javafx.util.Duration;

public class CancelTranslateTransition extends Transition {

	private Line lineTrace;
	private double x0, y0, x1, y1;
	
	private Tool tool;
	
	public CancelTranslateTransition(Duration duration, Tool tool) {
		setCycleDuration(duration);
		
		this.lineTrace = tool.lineTrace;
		x0 = lineTrace.getStartX();
		y0 = lineTrace.getStartY();
		
		x1 = lineTrace.getEndX();
		y1 = lineTrace.getEndY();
		
		this.tool = tool;		
	}
	
    @Override
    protected void interpolate(double fraction) {
    	lineTrace.setEndX(fraction*x0 + (1 - fraction)*x1);
    	lineTrace.setEndY(fraction*y0 + (1 - fraction)*y1);
    	
    	tool.update();    	
    	if(fraction == 1) tool.cancelComplete();
    }
}
