package fr.inria.structgraphics.ui.tools;

import fr.inria.structgraphics.ui.viscanvas.CanvasFrame;
import javafx.scene.Cursor;
import javafx.scene.paint.Color;

public class DrawingTool extends Tool {

	protected DrawingTool() {}
	
	public DrawingTool(CanvasFrame canvas) {
		super(canvas);
	}
	
	@Override
	protected void init() {
		cursor = Cursor.CROSSHAIR;
		traceColor = Color.DODGERBLUE;
		traceWidth = 1;
		
		super.init();	
	}
		
}
