package fr.inria.structgraphics.ui.tools;

import java.util.ArrayList;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisGroup;
import javafx.scene.shape.Line;

public class MarkSelection {

	public enum SelectionType {
		INCOLLECTION, INGROUP, UNGROUPED
	}
	
	private ArrayList<Mark> marks, archived;
	private boolean acceptDragEvent = true; // TODO: May finally do not need it!!!!
	
	private SelectionType type = SelectionType.UNGROUPED;
	
	public MarkSelection () {
		marks = new ArrayList<>();
	}
	
	public void archive() {
		archived = marks;
		marks = new ArrayList<>();
	}
	
	public boolean isEmpty() {
		return marks.isEmpty();
	}
	
	public int size() {
		return marks.size();
	}
	
	public void removeAll() {
		marks.clear();
	}
	
	public void highlightPress() {
		if(marks.size() != 1 || !archived.contains(marks.get(0))) {
			highlight(true); 
		}
		else {
			marks = archived;
			for(Mark mark:marks) {
				mark.pin();
			}
		}
	}
	
	
	public void highlight(boolean single) {
		if(archived != null) {
			for(Mark mark:archived) {
				if(!marks.contains(mark)) {
					mark.setHighlight(false, single);
				}
			}
		}
		
		if(marks != null) {
			for(Mark mark:marks) {
				/*if(!mark.isHighlighted())*/ mark.setHighlight(true, single);
			}
		}
	}
	
	public void add(Mark mark) {
		Container container = mark.getContainer();
		
		if(container instanceof VisCollection) type = SelectionType.INCOLLECTION;
		else if(container instanceof VisGroup) type = SelectionType.INGROUP;
		else type = SelectionType.UNGROUPED;
		
		marks.add(mark);
	}
	
	public SelectionType getType() {
		return type;
	}
	
	
	private boolean isOrderConstrained() {
		if(marks == null || marks.isEmpty()) return false;
		Container container = marks.get(0).getContainer();
		
		if(container instanceof VisBody) {
			return ((VisBody) container).isOrderConstrained();
		} else return false;
	}
	
	
	public boolean canChangeLayers() {
		if(marks == null || marks.isEmpty()) return false;
		else {
			Mark first = marks.get(0);
			if(first instanceof LineConnectedCollection && ((LineConnectedCollection) first).isConnectionSelected()) return false;
		}
		
		return !isOrderConstrained();
	}
	
	public boolean canGoToLibrary() {
		if(marks == null || marks.isEmpty() || size() > 1) return false;
		else {
			Mark first = marks.get(0);
			if(first instanceof LineConnectedCollection && ((LineConnectedCollection) first).isConnectionSelected()) return false;
		}
		
		return true;
	}
	
	
	public ArrayList<Mark> getMarks(){
		return marks;
	}
	
	public void translate(Line lineTrace) {
		for(Mark mark:marks) {
			mark.translate(lineTrace);
		}
	}
	
	public void setDragAccept(boolean accept) {
		this.acceptDragEvent = accept;
	}
	
	/*
	public boolean acceptDragEvent() {
		return acceptDragEvent;
	}*/

	
	public void bringToFront() {
		Container container = marks.get(0).getContainer();
		
		for(Mark mark: marks) {
			container.bringToFront(mark);
		}
		
		container.updateReordering();
	}
	
	public void sendToBack() {
		Container container = marks.get(0).getContainer();
		
		for(int i = marks.size() - 1; i >=0; --i) {
			container.sendToBack(marks.get(i));
		}
		
		container.updateReordering();
	}

	public void delete() {
		for(Mark mark:marks) {
			mark.fullDelete();
		}
		
		marks.clear();
	}	
	
}
