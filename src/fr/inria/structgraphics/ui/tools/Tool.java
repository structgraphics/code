package fr.inria.structgraphics.ui.tools;

import fr.inria.structgraphics.ui.viscanvas.CanvasFrame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.util.Duration;

public class Tool {
	
	private final static int CANCEL_RADIUS = 0;
	
	protected double startX, startY, mouseX, mouseY;
	protected Line lineTrace;
	protected Rectangle rectTrace;
	protected Shape cancelArea;
		
	protected ToggleButton tbutton = null;
	protected boolean isActive = false;
	protected Cursor cursor = Cursor.DEFAULT;
	protected CanvasFrame canvas;
	
	protected double traceWidth = 3;
	protected Paint traceColor = Color.CORAL;
	
	protected double rectWidth = 1;
	protected Paint rectColor = Color.DARKGRAY;
	protected Paint rectFill = new Color(1, 1, 1, .1);
	
	protected boolean canceled = false;
	protected boolean rectangular = false;

	protected Tool() {}
	
	public Tool(CanvasFrame canvas) {
		this.canvas = canvas;
		init();
	}
	
	protected void init() {
		if(tbutton != null) tbutton.setOnAction(
				new EventHandler<ActionEvent>() {
					@Override 
					public void handle(ActionEvent e) {
						if(!isActive) setActive(true);
						else tbutton.setSelected(true);
					}
				});
	}
	
	public boolean hasToggleButton() {
		return true;
	}
	
	protected void cancelAction() {
		canceled = true;
		CancelTranslateTransition transition = new CancelTranslateTransition(Duration.millis(160), this);
		transition.play();
	}
	
	protected void copyAction() {}
	protected void pasteAction(double x, double y) {}
	
	public void cancelComplete() {
		canvas.getCanvas().show(null, null, null);
	}
	
	public ToggleButton getToggleButton() {
		return tbutton;
	}
	
	public void setActive(boolean active) { 
		this.isActive = active;
		if(active) {
			if(tbutton != null) tbutton.setSelected(true);
			canvas.setActiveTool(this);
			if(cursor!= null) canvas.setCanvasCursor(cursor);
			
			canvas.setOnKeyPressed(new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent event) {
					if(event.getCode() == KeyCode.ESCAPE)
						cancelAction();
					else if(event.isMetaDown() && event.getCode() == KeyCode.C)
						copyAction();
					else if(event.isMetaDown() && event.getCode() == KeyCode.V)
						pasteAction(mouseX, mouseY);
				}
			});
			
		} else if(tbutton != null) tbutton.setSelected(false);
	}
	
	public boolean getActive() {
		return isActive;
	}

	public void handleDrag(MouseEvent event) { 
		if(canceled) return;
		
		lineTrace = createTrace(event.getX(), event.getY());
		cancelArea = createCancelArea(event.getX(), event.getY());
		if(rectangular) {
			rectTrace = createRectTrace(event.getX(), event.getY());
		} else rectTrace = null;
			
		canvas.getCanvas().show(lineTrace, cancelArea, rectTrace);
		update();
	}
	
	public void update() {
		
	}
	
	public void handleMove(MouseEvent event) {
		mouseX = event.getX();
		mouseY = event.getY();
	}

	public void handlePress(MouseEvent event) {
		canceled = false;
		
		startX = event.getX();
		startY = event.getY();	
		lineTrace = createTrace(event.getX(), event.getY());

		cancelArea = createCancelArea(event.getX(), event.getY());
		canvas.getCanvas().show(null, cancelArea, null);
	}
	
	public void handleRelease(MouseEvent event) {
		canvas.getCanvas().show(null, null, null);
	}
	
	// This is a default implementation if the canel area, but it could be overriden
	protected Shape createCancelArea(double x, double y) {
		Shape cancelArea = new Circle(startX, startY, CANCEL_RADIUS);
		cancelArea.setStrokeWidth(1);
		cancelArea.setStroke(Color.RED);
		cancelArea.setFill(Color.TRANSPARENT);
		
		return cancelArea;
	}
	
	protected Line createTrace(double x, double y) {
		Line lineTrace = new Line(startX, startY, x, y);
		lineTrace.setStrokeWidth(traceWidth);
		lineTrace.setStroke(traceColor);
		
		return lineTrace;
	}
	
	protected Rectangle createRectTrace(double x, double y) {
		Rectangle rectTrace = new Rectangle(Math.min(startX, x), Math.min(startY, y), Math.abs(x - startX), Math.abs(y - startY));
		rectTrace.setStrokeWidth(rectWidth);
		rectTrace.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);
		rectTrace.setStroke(rectColor);
		rectTrace.setFill(rectFill);
		
		return rectTrace;
	}

	public void doubleClicked(MouseEvent event) {}

}

