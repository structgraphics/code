package fr.inria.structgraphics.ui;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.types.PropertyName;
import javafx.scene.Node;

public interface Draggable {
	
	public enum Type {
		Value, Table, Column
	}

	public VisBody getGroup();
	public VisBody getTopGroup();
	public Container getContainer();
	public Node getNode();
	public Type getType();
	public Object getPropertiesContent();
	public PropertyName getID();
	public int getColumnsNum();
	public int getRowsNum();
	public void update();	
	public boolean isInWide();
	public boolean isNetwork();
}
