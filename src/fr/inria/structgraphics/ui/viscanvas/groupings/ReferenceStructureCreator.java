package fr.inria.structgraphics.ui.viscanvas.groupings;

import java.util.ArrayList;
import java.util.List;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.XMarkComparator;
import fr.inria.structgraphics.graphics.YMarkComparator;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.DistributionProperty.Constraint;
import fr.inria.structgraphics.ui.utils.Stats;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;

public class ReferenceStructureCreator {

	private XReferenceStructure xReferenceStructure;
	private YReferenceStructure yReferenceStructure;
	
	public ReferenceStructureCreator(ObservableList<Mark> marks, Grouping.Type gtype) {
		
		double valx = marks.get(0).getCoords().getX(), valy = marks.get(0).getCoords().getY();
		
		if(marks.size() == 1) {
			RefX refx = marks.get(0).getCoords().getXRef();
			RefY refy = marks.get(0).getCoords().getYRef();
			
			if(!(marks.get(0) instanceof VisBody)) {
				switch(refx) {
					case Center: 
						valx = marks.get(0).centerX();
						break;
					case Left: 
						valx = marks.get(0).left();
						break;
					case Right: 
						valx = marks.get(0).right();
						break;
				}
			
				switch(refy) {
					case Center: 
						valy = marks.get(0).centerY();
						break;
					case Top: 
						valy = marks.get(0).top();
						break;
					case Bottom: 
						valy = marks.get(0).bottom();
						break;
				}
			}
				
			xReferenceStructure = new XReferenceStructure(refx, valx, false, Constraint.None);
			yReferenceStructure = new YReferenceStructure(refy, valy, true, Constraint.None);
			return;
		}
		
		SortedList<Mark> marksX = new SortedList<>(marks, new XMarkComparator());
		SortedList<Mark> marksY = new SortedList<>(marks, new YMarkComparator());

		/////////////////////////////////
		List<Double> left = getXLeft(marksX);
		Stats leftStats = new Stats(left);
		
		List<Double> centerx = getXCenter(marksX);
		Stats centerXStats = new Stats(centerx);
		
		List<Double> right = getXRight(marksX);
		Stats rightStats = new Stats(right);
		
		List<Double> bottom = getYBottom(marksY);
		Stats bottomStats = new Stats(bottom);
		
		List<Double> centery = getYCenter(marksY);
		Stats centerYStats = new Stats(centery);
		
		List<Double> top = getYTop(marksY);
		Stats topStats = new Stats(top);

		/////////////////////////////////
		List<Double> gapsX = getXGap(marksX);
		Stats gapsXStats = new Stats(gapsX);
		
		List<Double> gapsY = getYGap(marksY);
		Stats gapsYStats = new Stats(gapsY);
		/////////////////////////////////
		
		////////////////////////////////////
		List<Double> topDistr = getTopDistribution(marksY);
		Stats topDistrStats = new Stats(topDistr);
		
		List<Double> bottomDistr = getBottomDistribution(marksY);
		Stats bottomDistrStats = new Stats(bottomDistr);
		
		List<Double> centerYDistr = getCenterYDistribution(marksY);
		Stats centerYDistrStats = new Stats(centerYDistr);

		List<Double> leftDistr = getLeftDistribution(marksX);
		Stats leftDistrStats = new Stats(leftDistr);
		
		List<Double> rightDistr = getRightDistribution(marksX);
		Stats rightDistrStats = new Stats(rightDistr);
		
		List<Double> centerXDistr = getCenterXDistribution(marksX);
		Stats centerXDistrStats = new Stats(centerXDistr);
		
		List<Double> widthDistr = getWidths(marksX);
		Stats widthStats = new Stats(widthDistr);
		//////////////////////////////
		
		Constraint constraintX = Constraint.None, constraintY = Constraint.None;
		RefX refx = RefX.Center;
		RefY refy = RefY.Center;
		boolean sharedx = false, sharedy = false;
		valx = leftStats.min; 
		valy = bottomStats.min;
		
		double dx = 0; // This is to add a gap on the x-axis based on the distribution constraint (for collections)
		
		if(leftStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			sharedx = true;
			refx = RefX.Left;
			valx = leftStats.mean;
		}
		else if(centerXStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			sharedx = true;
			refx = RefX.Center;
			valx = centerXStats.mean;
		}
		else if(rightStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			sharedx = true;
			refx = RefX.Right;
			valx = rightStats.mean;
		}
		else if(centerXDistr.size() > 1 && centerXDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintX = Constraint.Distance;
			refx = RefX.Center;
			dx = centerXDistrStats.mean/8;
		} 
		else if(leftDistr.size() > 1 && leftDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintX = Constraint.Distance;
			refx = RefX.Left;
			dx = leftDistrStats.mean/8;
		} 
		else if(gapsX.size() > 1 && gapsXStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintX = Constraint.Spacing;
			refx = RefX.Left;
		}
		else if(rightDistr.size() > 1 && rightDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintX = Constraint.Distance;
			refx = RefX.Right;
			dx = rightDistrStats.mean/8;
		} 
		
		////////////// Y ############################
		if(bottomStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			sharedy = true;
			refy = RefY.Bottom;
			valy = bottomStats.mean;
		}
		else if(centerYStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			sharedy = true;
			refy = RefY.Center;
			valy = centerYStats.mean;
		}
		else if(topStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			sharedy = true;
			refy = RefY.Top;
			valy = topStats.mean;
		}
		else if(sharedx && (gapsY.size() == 1 || gapsYStats.var < DefaultSharingStrategy.MAX_VARIANCE)) {
			constraintY = Constraint.Spacing;
			refy = RefY.Bottom;
			
			// In this case, give priority to center x alignement
			if(centerXDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
				refx = RefX.Center;
				valx = centerXStats.mean;
			}
		} 
		else if(sharedx && bottomDistr.size() > 1 && bottomDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintY = Constraint.Distance;
			refy = RefY.Bottom;
		} 
		else if(sharedx && centerYDistr.size() > 1 && centerYDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintY = Constraint.Distance;
			refy = RefY.Center;
		} 
		else if(sharedx && topDistr.size() > 1 && topDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintY = Constraint.Distance;
			refy = RefY.Top;
		} 
		
		if(gtype != Grouping.Type.Collection && sharedy && (gapsX.size() == 1 || gapsXStats.var < DefaultSharingStrategy.MAX_VARIANCE)) {
			constraintX = Constraint.Spacing;
			if(widthStats.var < DefaultSharingStrategy.MAX_VARIANCE) refx = RefX.Center;
			else refx = RefX.Left;
		}
		
		
		if(gtype == Grouping.Type.Collection) valx -= dx;
		
		xReferenceStructure = new XReferenceStructure(refx, valx, sharedx, constraintX);
		yReferenceStructure = new YReferenceStructure(refy, valy, sharedy, constraintY);
	}
	
	/*
	public ReferenceStructureCreator(ObservableList<Mark> marks, Grouping.Type gtype) {
		
		double valx = marks.get(0).getCoords().getX(), valy = marks.get(0).getCoords().getY();
		
		if(marks.size() == 1) {
			RefX refx = marks.get(0).getCoords().getXRef();
			RefY refy = marks.get(0).getCoords().getYRef();
			
			if(!(marks.get(0) instanceof VisBody)) {
				switch(refx) {
					case Center: 
						valx = marks.get(0).centerX();
						break;
					case Left: 
						valx = marks.get(0).left();
						break;
					case Right: 
						valx = marks.get(0).right();
						break;
				}
			
				switch(refy) {
					case Center: 
						valy = marks.get(0).centerY();
						break;
					case Top: 
						valy = marks.get(0).top();
						break;
					case Bottom: 
						valy = marks.get(0).bottom();
						break;
				}
			}
				
			xReferenceStructure = new XReferenceStructure(refx, valx, false, Constraint.None);
			yReferenceStructure = new YReferenceStructure(refy, valy, true, Constraint.None);
			return;
		}
		
		SortedList<Mark> marksX = new SortedList<>(marks, new XMarkComparator());
		SortedList<Mark> marksY = new SortedList<>(marks, new YMarkComparator());

		/////////////////////////////////
		List<Double> left = getXLeft(marksX);
		Stats leftStats = new Stats(left);
		
		List<Double> centerx = getXCenter(marksX);
		Stats centerXStats = new Stats(centerx);
		
		List<Double> right = getXRight(marksX);
		Stats rightStats = new Stats(right);
		
		List<Double> bottom = getYBottom(marksY);
		Stats bottomStats = new Stats(bottom);
		
		List<Double> centery = getYCenter(marksY);
		Stats centerYStats = new Stats(centery);
		
		List<Double> top = getYTop(marksY);
		Stats topStats = new Stats(top);

		/////////////////////////////////
		List<Double> gapsX = getXGap(marksX);
		Stats gapsXStats = new Stats(gapsX);
		
		List<Double> gapsY = getYGap(marksY);
		Stats gapsYStats = new Stats(gapsY);
		/////////////////////////////////
		
		////////////////////////////////////
		List<Double> topDistr = getTopDistribution(marksY);
		Stats topDistrStats = new Stats(topDistr);
		
		List<Double> bottomDistr = getBottomDistribution(marksY);
		Stats bottomDistrStats = new Stats(bottomDistr);
		
		List<Double> centerYDistr = getCenterYDistribution(marksY);
		Stats centerYDistrStats = new Stats(centerYDistr);

		List<Double> leftDistr = getLeftDistribution(marksX);
		Stats leftDistrStats = new Stats(leftDistr);
		
		List<Double> rightDistr = getRightDistribution(marksX);
		Stats rightDistrStats = new Stats(rightDistr);
		
		List<Double> centerXDistr = getCenterXDistribution(marksX);
		Stats centerXDistrStats = new Stats(centerXDistr);
		
		List<Double> widthDistr = getWidths(marksX);
		Stats widthStats = new Stats(widthDistr);
		//////////////////////////////
		
		Constraint constraintX = Constraint.None, constraintY = Constraint.None;
		RefX refx = RefX.Center;
		RefY refy = RefY.Center;
		boolean sharedx = false, sharedy = false;
		valx = leftStats.min; 
		valy = bottomStats.min;
		
		double dx = 0; // This is to add a gap on the x-axis based on the distribution constraint (for collections)
		
		if(leftStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			sharedx = true;
			refx = RefX.Left;
			valx = leftStats.mean;
		}
		else if(centerXStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			sharedx = true;
			refx = RefX.Center;
			valx = centerXStats.mean;
		}
		else if(rightStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			sharedx = true;
			refx = RefX.Right;
			valx = rightStats.mean;
		}
		else if(centerXDistr.size() > 1 && centerXDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintX = Constraint.Distance;
			refx = RefX.Center;
			dx = centerXDistrStats.mean/8;
		} 
		else if(leftDistr.size() > 1 && leftDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintX = Constraint.Distance;
			refx = RefX.Left;
			dx = leftDistrStats.mean/8;
		} 
		else if(gapsX.size() > 1 && gapsXStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintX = Constraint.Spacing;
			refx = RefX.Left;
		}
		else if(rightDistr.size() > 1 && rightDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintX = Constraint.Distance;
			refx = RefX.Right;
			dx = rightDistrStats.mean/8;
		} 
		
		////////////// Y ############################
		if(bottomStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			sharedy = true;
			refy = RefY.Bottom;
			valy = bottomStats.mean;
		}
		else if(centerYStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			sharedy = true;
			refy = RefY.Center;
			valy = centerYStats.mean;
		}
		else if(topStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			sharedy = true;
			refy = RefY.Top;
			valy = topStats.mean;
		}
		else if(sharedx && (gapsY.size() == 1 || gapsYStats.var < DefaultSharingStrategy.MAX_VARIANCE)) {
			constraintY = Constraint.Spacing;
			refy = RefY.Bottom;
			
			// In this case, give priority to center x alignement
			if(centerXDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
				refx = RefX.Center;
				valx = centerXStats.mean;
			}
		} 
		else if(sharedx && bottomDistr.size() > 1 && bottomDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintY = Constraint.Distance;
			refy = RefY.Bottom;
		} 
		else if(sharedx && centerYDistr.size() > 1 && centerYDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintY = Constraint.Distance;
			refy = RefY.Center;
		} 
		else if(sharedx && topDistr.size() > 1 && topDistrStats.var < DefaultSharingStrategy.MAX_VARIANCE) {
			constraintY = Constraint.Distance;
			refy = RefY.Top;
		} 
		
		if(gtype != Grouping.Type.Collection && sharedy && (gapsX.size() == 1 || gapsXStats.var < DefaultSharingStrategy.MAX_VARIANCE)) {
			constraintX = Constraint.Spacing;
			if(widthStats.var < DefaultSharingStrategy.MAX_VARIANCE) refx = RefX.Center;
			else refx = RefX.Left;
		}
		
		
		if(gtype == Grouping.Type.Collection) valx -= dx;
		
		xReferenceStructure = new XReferenceStructure(refx, valx, sharedx, constraintX);
		yReferenceStructure = new YReferenceStructure(refy, valy, sharedy, constraintY);
	}
	*/
	
	public XReferenceStructure getXReferenceStructure() {
		return xReferenceStructure;
	}
	
	public YReferenceStructure getYReferenceStructure() {
		return yReferenceStructure;
	}
	
	protected List<Double> getXLeft(List<Mark> marks){
		List<Double> left = new ArrayList<>();		
		for(Mark mark: marks) {
			if(mark instanceof VisBody) left.add(mark.getCoords().getX());
			else left.add(mark.left());
		}
		return left;
	}
	
	protected List<Double> getXRight(List<Mark> marks){
		List<Double> right = new ArrayList<>();		
		for(Mark mark: marks) {
			if(mark instanceof VisBody) right.add(mark.getCoords().getX());
			else right.add(mark.right());
		}
		return right;
	}

	protected List<Double> getXCenter(List<Mark> marks){
		List<Double> center = new ArrayList<>();		
		for(Mark mark: marks) {
			if(mark instanceof VisBody) center.add(mark.getCoords().getX());
			else center.add(mark.centerX());
		}
		return center;
	}
	
	protected List<Double> getYTop(List<Mark> marks){
		List<Double> top = new ArrayList<>();		
		for(Mark mark: marks) {
			if(mark instanceof VisBody) top.add(mark.getCoords().getY());
			else top.add(mark.top());
		}
		return top;
	}
	
	protected List<Double> getYCenter(List<Mark> marks){
		List<Double> center = new ArrayList<>();		
		for(Mark mark: marks) {
			if(mark instanceof VisBody) center.add(mark.getCoords().getY());
			else center.add(mark.centerY());
		}
		return center;
	}
	
	protected List<Double> getYBottom(List<Mark> marks){
		List<Double> bottom = new ArrayList<>();		
		for(Mark mark: marks) {
			if(mark instanceof VisBody) bottom.add(mark.getCoords().getY());
			else bottom.add(mark.bottom());
		}
		return bottom ;
	}
	
	
	protected List<Double> getLeftDistribution(SortedList<Mark> marks){
		List<Double> dist = new ArrayList<>();	
		if(marks.size() < 2) return dist;
		
		if(marks.get(0) instanceof VisBody) {
			for(int i = 1; i < marks.size(); ++i) {
				dist.add(marks.get(i).getCoords().getX() - marks.get(i-1).getCoords().getX());
			}
		} else {
			for(int i = 1; i < marks.size(); ++i) {
				dist.add(marks.get(i).left() - marks.get(i-1).left());
			}
		}
				
		return dist;
	}
	
	protected List<Double> getCenterXDistribution(SortedList<Mark> marks){
		List<Double> dist = new ArrayList<>();	
		if(marks.size() < 2) return dist;
		
		if(marks.get(0) instanceof VisBody) {
			for(int i = 1; i < marks.size(); ++i) {
				dist.add(marks.get(i).getCoords().getX() - marks.get(i-1).getCoords().getX());
			}
		} else {
			for(int i = 1; i < marks.size(); ++i) {
				dist.add(marks.get(i).centerX() - marks.get(i-1).centerX());
			}
		}
		
		return dist;
	}
	
	protected List<Double> getRightDistribution(SortedList<Mark> marks){
		List<Double> dist = new ArrayList<>();	
		if(marks.size() < 2) return dist;
		
		if(marks.get(0) instanceof VisBody) {
			for(int i = 1; i < marks.size(); ++i) {
				dist.add(marks.get(i).getCoords().getX() - marks.get(i-1).getCoords().getX());
			}
		} else {
			for(int i = 1; i < marks.size(); ++i) {
				dist.add(marks.get(i).right() - marks.get(i-1).right());
			}
		}
		
		return dist;
	}
	
	protected List<Double> getBottomDistribution(SortedList<Mark> marks){
		List<Double> dist = new ArrayList<>();	
		if(marks.size() < 2) return dist;
		
		if(marks.get(0) instanceof VisBody) {
			for(int i = 1; i < marks.size(); ++i) {
				dist.add(marks.get(i).getCoords().getY() - marks.get(i-1).getCoords().getY());
			}
		} else {
			for(int i = 1; i < marks.size(); ++i) {
				dist.add(marks.get(i).bottom() - marks.get(i-1).bottom());
			}
		}
		
		return dist;
	}
	
	protected List<Double> getCenterYDistribution(SortedList<Mark> marks){
		List<Double> dist = new ArrayList<>();	
		if(marks.size() < 2) return dist;
		
		if(marks.get(0) instanceof VisBody) {
			for(int i = 1; i < marks.size(); ++i) {
				dist.add(marks.get(i).getCoords().getY() - marks.get(i-1).getCoords().getY());
			}
		} else {
			for(int i = 1; i < marks.size(); ++i) {
				dist.add(marks.get(i).centerY() - marks.get(i-1).centerY());
			}
		}
		
		return dist;
	}
	
	protected List<Double> getTopDistribution(SortedList<Mark> marks){
		List<Double> dist = new ArrayList<>();	
		if(marks.size() < 2) return dist;
		
		if(marks.get(0) instanceof VisBody) {
			for(int i = 1; i < marks.size(); ++i) {
				dist.add(marks.get(i).getCoords().getY() - marks.get(i-1).getCoords().getY());
			}
		} else {
			for(int i = 1; i < marks.size(); ++i) {
				dist.add(marks.get(i).top() - marks.get(i-1).top());
			}
		}
		
		return dist;
	}
	
	protected List<Double> getXGap(SortedList<Mark> marks){
		List<Double> gap = new ArrayList<>();	
		if(marks.size() < 2) return gap;
		
		for(int i = 1; i < marks.size(); ++i) {
			double right = marks.get(i-1).right();
			double left = marks.get(i).left();
			gap.add(left - right);
		}
		
		return gap;
	}
	
	protected List<Double> getYGap(SortedList<Mark> marks){		
		List<Double> gap = new ArrayList<>();	
		if(marks.size() < 2) return gap;
		
		for(int i = 1; i < marks.size(); ++i) {
			double top = marks.get(i-1).top();
			double bottom = marks.get(i).bottom();
			gap.add(bottom - top);
		}
		
		return gap;
	}
	
	protected List<Double> getWidths(SortedList<Mark> marks){
		List<Double> ws = new ArrayList<>();	
		if(marks.size() < 2) return ws;
		
		for(int i = 1; i < marks.size(); ++i) {
			double width = marks.get(i-1).width();
			ws.add(width);
		}
		
		return ws;
	}
	
}
