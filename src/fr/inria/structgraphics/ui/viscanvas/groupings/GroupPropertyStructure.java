package fr.inria.structgraphics.ui.viscanvas.groupings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisGroup;
import fr.inria.structgraphics.types.ConstrainedDoubleProperty;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.Shareable;
import fr.inria.structgraphics.ui.utils.GroupBinding;
import javafx.beans.property.Property;

public class GroupPropertyStructure extends PropertyStructure {

	protected TreeMap<PropertyName, ArrayList<GroupBinding>> bindings = new TreeMap<>();
	
	public GroupPropertyStructure(Collection<Mark> marks) {
		this(marks, new DefaultSharingStrategy());
	}
	
	public GroupPropertyStructure(Collection<Mark> marks, SharingStrategy strategy) {
		properties = new TreeMap<>();
				
		for(Mark mark: marks) {
			mark.addSelfProperties(false, properties);
			
			if(mark instanceof VisGroup) {
				((VisGroup) mark).getChildPropertyStructure().makeAllVariable();
			}
		}
		
		for(FlexibleListProperty property:properties.values())
		{
			if(property.getName().matches(PropertyName.getRegExpr("x"))) {
				id_x = new PropertyName(property.getName());
				if(id_y != null) break;
			}
			else if(property.getName().matches(PropertyName.getRegExpr("y"))) {
				id_y = new PropertyName(property.getName());
				if(id_x != null) break;
			}
		}

		common = new TreeSet<>();
		variable = new TreeSet<>();
		strategy.share(properties, bindings, common, variable);		
	}
		
	public GroupPropertyStructure(Collection<Mark> marks, GroupPropertyStructure structure) {
		properties = new TreeMap<>();
				
		for(Mark mark: marks) {
			mark.addSelfProperties(false, properties);
		}
		
		for(FlexibleListProperty property:properties.values())
		{
			if(property.getName().matches(PropertyName.getRegExpr("x"))) {
				id_x = new PropertyName(property.getName());
				if(id_y != null) break;
			}
			else if(property.getName().matches(PropertyName.getRegExpr("y"))) {
				id_y = new PropertyName(property.getName());
				if(id_x != null) break;
			}
		}

		common = new TreeSet<>();
		common.addAll(structure.common);
		variable = new TreeSet<>();
		variable.addAll(structure.variable);
		
		rebuildBindings();
	}
	
	
	public GroupPropertyStructure(Collection<Mark> marks, SortedSet<PropertyName> common, SortedSet<PropertyName> variable) {
		properties = new TreeMap<>();
		
		for(Mark mark: marks) {
			mark.addSelfProperties(false, properties);
		}
		
		for(FlexibleListProperty property:properties.values())
		{
			if(property.getName().matches(PropertyName.getRegExpr("x")) ) {
				id_x = new PropertyName(property.getName());
				if(id_y != null) break;
			}
			else if(property.getName().matches(PropertyName.getRegExpr("y")) ) {
				id_y = new PropertyName(property.getName());
				if(id_x != null) break;
			}
		}
		
		this.common = common;
		this.variable = variable;
		// rebuildBindings();
	}
	
	
	public ArrayList<GroupBinding> getGroups(PropertyName name) {
		return bindings.get(name);
	}
	
	public ArrayList<Property> getBindingsOf(Property property){
		ArrayList<GroupBinding> groups = bindings.get(new PropertyName(property.getName()));
		for(GroupBinding group: groups) {
			if(group.contains(property))
				return group.getProperties();
		}
		
		return null;
	}
	
	@Override
	public void addMark(Mark mark, SharingStrategy strategy) {
		this.addMark(mark);
	}

	@Override
	public void addMark(Mark mark) {
		mark.addSelfProperties(false, properties);
		List<Property> props = mark.getProperties();

		for(Property prop: props) {
			PropertyName name = new PropertyName(prop.getName());
			if(variable.contains(name)) break;
			
			ArrayList<GroupBinding> groups = bindings.get(name);
			if(!groups.isEmpty()) {
				for(GroupBinding group: groups) {
					if(group.hasValue(prop)) {
						group.add(prop);
						break;
					}
				}
			}
		}
	}

	@Override
	public void removeMark(Mark mark) {
		mark.removeSelfProperties(properties);
		List<Property> props = mark.getProperties();

		for(Property prop: props) {
			PropertyName name = new PropertyName(prop.getName());
			if(variable.contains(name)) break;
			
			ArrayList<GroupBinding> groups = bindings.get(name);
			if(!groups.isEmpty()) {
				GroupBinding group_ = null;
				
				for(GroupBinding group: groups) {
					if(group.hasValue(prop)) {
						group.remove(prop);
						if(group.isEmpty()) group_ = group;
						break;
					}
				}
				
				if(group_ != null) groups.remove(group_); 
			}
		}
	}

	public void setBindings(TreeMap<PropertyName, ArrayList<GroupBinding>> bindings) {
		this.bindings = bindings;
	}
	
	@Override
	public void destroyAllBindings() {
		for(ArrayList<GroupBinding> groups:bindings.values()) {
			for(GroupBinding group:groups) {
				group.unbind();
			}
			groups.clear();
		}
		
		bindings.clear();
	}

	@Override
	public void rebuildBindings() {
		for(PropertyName name: properties.keySet()) {		
			if(common.contains(name)) {
				FlexibleListProperty prop = properties.get(name);
				bindings.put(name, prop.createGroupBindings());	
			}
		}
	}

	@Override
	public void moveToCommon(PropertyName name) {
		if(common.contains(name)) return;
		
		// TODO: ?????
		fixYConstraint(name);
		fixXConstraint(name);
		
		variable.remove(name);
		common.add(name);
		
		FlexibleListProperty prop = properties.get(name);
		bindings.put(name, prop.createGroupBindings());	
		
		flagProperty.set(!flagProperty.get());		
		
		if(name.isWidth() || name.isHeight()) {
			FlexibleListProperty property = properties.get(name);
			ConstrainedDoubleProperty dprop = ((ConstrainedDoubleProperty)(property.flatten().get(0)));
			
			dprop.updateValue(dprop.getValue());
		}
	}

	@Override
	public void moveToVariable(PropertyName name) {
		if(variable.contains(name)) return;
		common.remove(name);
		variable.add(name);
		
		ArrayList<GroupBinding> groups = bindings.get(name);
		if(groups != null) {
			for(GroupBinding group:groups) group.unbind();
			groups.clear();
			bindings.remove(name);
		}
	
		flagProperty.set(!flagProperty.get());
		
	}
	
	public void makeAllVariable() {
		for(PropertyName propName:properties.keySet()) {
			if(common.contains(propName)) moveToVariable(propName);
		}
	}
	
	public Collection<Property> getFlattenedProperties(){
		Collection<Property> flattened = new ArrayList<>();
		
		for(PropertyName name: properties.keySet()) {
			FlexibleListProperty property = properties.get(name);
			if(common.contains(name)) {
				ArrayList<Property> propertyList = new ArrayList<>();
				
				ArrayList<GroupBinding> groups = bindings.get(name);
				for(GroupBinding group: groups) {
					if(!group.isEmpty()) propertyList.add(group.firstProperty());
				}
				
				flattened.add(FlexibleListProperty.createList(/*property.getBean()*/ container, propertyList));
			} else flattened.add(property);
		}
		
		return flattened;
	}
}
