package fr.inria.structgraphics.ui.viscanvas.groupings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.ShapeMark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisGroup;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.DistributionProperty.Constraint;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;

public abstract class PropertyStructure {	
	
	protected BooleanProperty flagProperty = new SimpleBooleanProperty(true);

	protected Map<PropertyName, FlexibleListProperty> properties;
	protected VisBody container;
	protected PropertyName id_x = null, id_y = null;
	protected SortedSet<PropertyName> common, variable;
	
	public abstract void addMark(Mark mark, SharingStrategy strategy);
	public abstract void addMark(Mark mark);
	public abstract void removeMark(Mark mark);
	public abstract void destroyAllBindings();
	public abstract void rebuildBindings();
	public abstract void moveToCommon(PropertyName propertyName);
	public abstract void moveToVariable(PropertyName name);
	
	public boolean isXShared() {
		return id_x != null && common.contains(id_x);
	}

	public boolean isYShared() {
		return id_y != null && common.contains(id_y);
	}
	
	public FlexibleListProperty getXListProperty() {
		return properties.get(id_x);
	}
	
	public FlexibleListProperty getYListProperty() {
		return properties.get(id_y);
	}
	
	public Map<PropertyName, FlexibleListProperty> getProperties(){
		return properties;
	}
	
	public FlexibleListProperty getProperty(PropertyName name) {
		return properties.get(name);
	}
	
	public boolean isEmpty() {
		return properties.isEmpty();
	}
	
	
	public Property getCompactFlat(PropertyName name) {
		Property property = properties.get(name);
		
		if(property instanceof FlexibleListProperty && container instanceof VisCollection) {					
			Property prop = ((FlexibleListProperty)property).getCompact(((VisCollection) container).getComponentAt(0));
			if(prop instanceof FlexibleListProperty) return ((FlexibleListProperty)prop).get(0);
			else return prop;
		} else return property;
	}
	
	public static PropertyStructure create(Collection<Mark> marks, PropertyStructure structure) {
		if(structure instanceof CollectionPropertyStructure) return new CollectionPropertyStructure(marks, (CollectionPropertyStructure)structure);
		else return new GroupPropertyStructure(marks, (GroupPropertyStructure)structure); // TODO
	}
	
	public void setContainer(VisBody container) {
		this.container = container;
	}
	
	public BooleanProperty getFlagProperty() {
		return flagProperty;
	}
	
	public SortedSet<PropertyName> getVariable(){
		return variable;
	}
	
	public SortedSet<PropertyName> getCommon(){
		return common;
	}
	
	public Set<PropertyName> getNames(){
		return properties.keySet();
	}
		
	public Collection<Property> getCommonProperties(){
		ArrayList<Property> list = new ArrayList<>();
		for(PropertyName name:common) {
			list.add(properties.get(name));
		}
		
		return list;
	}
	
	
	public Map<PropertyName, FlexibleListProperty> getVariableList(){
		Map<PropertyName, FlexibleListProperty> flat = new TreeMap<>();
		
		for(PropertyName name: variable) {
			FlexibleListProperty list = properties.get(name);
			
			if(list != null && list.getPublicProperty().get() && !list.getHiddenProperty().get())
				flat.put(name, (FlexibleListProperty)list.getCompact(container));
		}
		
		return flat;
	}
	
	
	public Map<PropertyName, FlexibleListProperty> getVariableList(Collection<PropertyName> names){
		Map<PropertyName, FlexibleListProperty> flat = new TreeMap<>();
		
		for(PropertyName name: names) {
			FlexibleListProperty list = properties.get(name);
			
			if(list != null && list.getPublicProperty().get() && !list.getHiddenProperty().get())
				flat.put(name, (FlexibleListProperty)list.getCompact2(container));
		}
		
		return flat;
	}
	

	private void addIDColumns(Map<PropertyName, FlexibleListProperty> flat, Collection<PropertyName> names) {
		int maxLevel = container.getLevel();
		int minLevel = container.getBottomLevel();
		
		for(int level = minLevel; level<maxLevel; ++level) {
			List<Property> ids = new ArrayList<>();
			container.addIDs(ids, level);
			FlexibleListProperty idsListProperty = FlexibleListProperty.createList(container, ids);	
			if(names == null || names.contains(idsListProperty.getPropertyName())) flat.put(idsListProperty.getPropertyName(), idsListProperty);
		}
	}
	
	public Map<PropertyName, FlexibleListProperty> getFullVariableList(){
		Map<PropertyName, FlexibleListProperty> flat = new TreeMap<>(); 
		
		addIDColumns(flat, null);
				
		for(FlexibleListProperty propertyList: properties.values()) { 
			if(propertyList.varies(container) && propertyList.getPublicProperty().get() 
					&& !propertyList.getHiddenProperty().get()) {				
				flat.put(propertyList.getPropertyName(), (FlexibleListProperty)propertyList.getExtended());				
			}
		}
				
		return flat;
	}
	
	
	public Map<PropertyName, FlexibleListProperty> getFullVariableList(Collection<PropertyName> names){
		Map<PropertyName, FlexibleListProperty> flat = new TreeMap<>(); 

		addIDColumns(flat, names);
		
		for(PropertyName name: names) {
			FlexibleListProperty list = properties.get(name);
			
			if(list != null && list.getPublicProperty().get() && !list.getHiddenProperty().get())
				flat.put(name, (FlexibleListProperty)list.getExtended());
		}
						
		return flat;
	}
	
	public void fixYSharing() {		
		if(container.getComponents().get(0) instanceof VisCollection) {
			VisBody child = (VisBody) container.getComponents().get(0);
			PropertyName yproperty = new PropertyName(child.getComponents().get(0).coords.y.getName());

			if(child.constraintYProperty.get() != Constraint.None && !variable.contains(yproperty)) {
				moveToVariable(yproperty);
			}
		} else if(container.getComponents().get(0) instanceof VisGroup){ /*
			VisBody child = (VisBody) container.getComponents().get(0);
			
			for(Mark mark:child.getComponents()) {
				PropertyName yproperty = new PropertyName(mark.coords.y.getName());
				if(child.constraintYProperty.get() != Constraint.None && !variable.contains(yproperty)) {
					moveToVariable(yproperty);
				}			
			}*/
		}
	}
	
	public void fixXSharing() {
		if(container.getComponents().get(0) instanceof VisCollection) {
			VisBody child = (VisBody) container.getComponents().get(0);
			PropertyName xproperty = new PropertyName(child.getComponents().get(0).coords.x.getName());
			
			if(child.constraintXProperty.get() != Constraint.None && !variable.contains(xproperty)) {
				moveToVariable(xproperty);
			}
		} else if(container.getComponents().get(0) instanceof VisGroup){/*
			VisBody child = (VisBody) container.getComponents().get(0);
			
			for(Mark mark:child.getComponents()) {
				PropertyName xproperty = new PropertyName(mark.coords.x.getName());
				if(child.constraintXProperty.get() != Constraint.None && !variable.contains(xproperty)) {
					moveToVariable(xproperty);
				}			
			}
			*/
		}
	}
	
	public void fixYConstraint(PropertyName name) {	// TODO: Do I need that???
		if(container.getComponents().get(0) instanceof VisBody && name.isY()) {
			VisBody child = (VisBody) container.getComponents().get(0);
			PropertyName yproperty = new PropertyName(child.getComponents().get(0).coords.y.getName());

			
			if(child.constraintYProperty.get() == Constraint.Spacing && yproperty.equals(name)) {
				child.constraintYProperty.set(Constraint.None);
			}
			
			/*
			if(child.constraintYProperty.get() != Constraint.None && yproperty.equals(name)) {
				child.constraintYProperty.set(Constraint.None);
			}*/
		}
	}
	
	public void fixXConstraint(PropertyName name) { // TODO: Do I need that???
		if(container.getComponents().get(0) instanceof VisBody && name.isX()) {
			VisBody child = (VisBody) container.getComponents().get(0);
			PropertyName xproperty = new PropertyName(child.getComponents().get(0).coords.x.getName());
			
			
			if(child.constraintXProperty.get() == Constraint.Spacing && xproperty.equals(name)) {
				child.constraintXProperty.set(Constraint.None);
			}
			/*
			if(child.constraintXProperty.get() != Constraint.None && xproperty.equals(name)) {
				child.constraintXProperty.set(Constraint.None);
			}*/
		}
	}
	
	
	public void removeHeightSharing(ShapeMark mark) {
		PropertyName heightPropertyName = new PropertyName(mark.height.getName());
		if(common.contains(heightPropertyName)) moveToVariable(heightPropertyName);
		for(Mark child:container.getComponents()) {
			if(child instanceof VisBody)
				((VisBody)child).getChildPropertyStructure().removeHeightSharing(mark);
		}
	}
	
}
