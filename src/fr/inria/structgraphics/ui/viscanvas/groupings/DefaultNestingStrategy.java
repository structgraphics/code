package fr.inria.structgraphics.ui.viscanvas.groupings;

import java.util.List;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.Mark;

public class DefaultNestingStrategy extends NestingStrategy {
	
	public static Container getRoot(List<Mark> marks) {
		return marks.get(0).getRoot();
	}
}
