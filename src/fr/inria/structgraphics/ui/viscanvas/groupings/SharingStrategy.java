package fr.inria.structgraphics.ui.viscanvas.groupings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisGroup;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.ui.utils.GroupBinding;
import javafx.collections.ObservableList;


public abstract class SharingStrategy {
	
	public void share(Collection<FlexibleListProperty> properties, SortedSet<PropertyName> common, SortedSet<PropertyName> variable) {
		for(FlexibleListProperty column: properties) {
			if(isVariable(column)) {
				variable.add(new PropertyName(column.getName()));
			}
			else common.add(new PropertyName(column.getName()));
		}
	}
	
	protected abstract boolean isVariable(FlexibleListProperty column);	
	public abstract VisCollection createCollection(ObservableList<Mark> marks);
	public abstract VisGroup createGroup(ObservableList<Mark> marks);
	public abstract void share(Map<PropertyName, FlexibleListProperty> properties, TreeMap<PropertyName, ArrayList<GroupBinding>> bindings, 
			SortedSet<PropertyName> common, SortedSet<PropertyName> variable);
}
