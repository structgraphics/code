package fr.inria.structgraphics.ui.viscanvas.groupings;

import java.util.ArrayList;
import java.util.List;

import fr.inria.structgraphics.types.FlexibleListProperty;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class DimensionalBinding {
	private CollectionPropertyStructure propertyStructure;
	private List<PropertyChangeListener> listeners = new ArrayList<>();
	
	public DimensionalBinding(CollectionPropertyStructure propertyStructure, FlexibleListProperty listProperty, FlexibleListProperty copies) {		
		this.propertyStructure = propertyStructure;
		
		List<FlexibleListProperty> transposed = listProperty.transpose();
		
		int i = 0;
		for(FlexibleListProperty sublist: transposed) {
			addBindings(sublist, copies.get(i++)); 
		}
	}
	
	private void addBindings(FlexibleListProperty sublist, Property copy) {	
		PropertyChangeListener listener = new PropertyChangeListener(sublist, copy);		
		
		for(Property property: sublist) { 
			property.addListener(listener);
			property.setValue(copy.getValue());
		}
		
		copy.addListener(listener);
		
		listeners.add(listener);		
	}

	class PropertyChangeListener implements ChangeListener {
		private FlexibleListProperty sublist;
		private Property copy;
		
		public PropertyChangeListener(FlexibleListProperty sublist, Property copy) {
			this.sublist = sublist;			
			this.copy = copy;
		}
		
		@Override
		public void changed(ObservableValue observable, Object oldValue, Object newValue) { 
		//	if(propertyStructure.isLocked()) return;
			
			for(Property property: sublist) {
				if(observable != property) {
					if(!property.getValue().equals(newValue)) property.setValue(newValue);
				}
			}
			if(observable != copy && !copy.getValue().equals(newValue)) copy.setValue(newValue);
		}
		
		public void disable(){
			for(Property property: sublist) {
				property.removeListener(this);
			}
			copy.removeListener(this);
		}
	}

	public void destroy() {
		for(PropertyChangeListener listener:listeners)
			listener.disable();
		
		listeners.clear();
	}
			
}

