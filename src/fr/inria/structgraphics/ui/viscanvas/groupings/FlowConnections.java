package fr.inria.structgraphics.ui.viscanvas.groupings;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.ShapeMark;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import javafx.beans.property.Property;

public class FlowConnections extends HashSet<FlowConnection> implements Comparator<FlowConnection>{

	protected LineConnectedCollection collection;
	
	public FlowConnections(LineConnectedCollection collection) {
		super();
		this.collection = collection;
	}
			
	// TODO: This is a very first draft...
	public FlowConnection addConnection(ShapeMark origin, ShapeMark destination, boolean clever) {
		FlowConnection conn = new FlowConnection(collection, origin, destination);
		conn.setParent(this);
		
		if(!clever) {
			add(conn);
			return conn;
		}
		
		if(!contains(conn)) {	
			origin.lockListeners(true);
			destination.lockListeners(true);
			
			if(!hasOutwardConnections(origin)) {	
				if(!hasInwardConnections(origin) && !hasInwardConnections(destination)) {
					if(hasOutwardConnections(destination)) {
						conn.weightProperty().set(destination.height());
						origin.height.updateValue(destination.height());
					} else {
						conn.weightProperty().set(origin.height());
						destination.height.updateValue(origin.height());
					}
				} else {
					conn.weightProperty().set(origin.height());
					if(!hasInwardConnections(destination) /*&& !hasFromConnections(destination)*/) {
						destination.lockListeners(false);
						destination.height.updateValue(origin.height());
						//Set<FlowConnection> fromDestination = from(destination);
						//for(FlowConnection fo:fromDestination) w += fo.weightProperty().get();
					}
					else {
						destination.height.updateValue(destination.height() + origin.height());
					}
				}	
			} else { // NOT fully complete	
				if(hasInwardConnections(destination)) {
					Set<FlowConnection> fromOrigin = from(origin);
					double w = 0;
					for(FlowConnection fo:fromOrigin) w += fo.weightProperty().get();
					double diff = origin.height() - w;
					if(diff == 0) {
						diff = 10;
						conn.weightProperty().set(diff);
						destination.height.updateValue(destination.height() + diff);
						
						//origin.lockListeners(false);
						origin.height.updateValue(origin.height() + diff);
					} else {
						conn.weightProperty().set(diff);
						destination.height.updateValue(destination.height() + diff);
					}
				}
				else {
					Set<FlowConnection> fromOrigin = from(origin);
					double w = 0;
					for(FlowConnection fo:fromOrigin) w += fo.weightProperty().get();
					double diff = origin.height() - w;
					
					if(diff > 0) {
						conn.weightProperty().set(diff);
						destination.height.updateValue(diff);
					} else {
						conn.weightProperty().set(destination.height());
						
						//origin.lockListeners(false);
						origin.height.updateValue(destination.height() + origin.height());						
					}
				}
			}
			
			add(conn);
			
			origin.lockListeners(false);
			destination.lockListeners(false);
			
			return conn;
		}
		else return null;
	}
		
	public void removeConnection(FlowConnection conn) {
		this.remove(conn);
	}
	
	public void removeConnection(ShapeMark origin, ShapeMark destination) {
		this.remove(new FlowConnection(collection, origin, destination));
	}
	
	public void removeFrom(ShapeMark origin) {
		Set<FlowConnection> connections = from(origin);
		this.removeAll(connections);
	}
	

	public void removeTo(ShapeMark destination) {
		Set<FlowConnection> connections = to(destination);
		this.removeAll(connections);
	}
	
	
	public boolean hasInwardConnections(ShapeMark shape) {
		for(FlowConnection conn: this) {
			if(conn.getDestination() == shape) return true;
		}
		
		return false;
	}
	
	public boolean hasOutwardConnections(ShapeMark shape) {
		for(FlowConnection conn: this) {
			if(conn.getOrigin() == shape) return true;
		}
		
		return false;
	}
	
	public Set<FlowConnection> from(ShapeMark origin){
		TreeSet subset = new TreeSet<>();
		
		for(FlowConnection conn: this) {
			if(conn.getOrigin() == origin)
				subset.add(conn);
		}
		
		return subset;
	}
	
	
	public Set<FlowConnection> to(ShapeMark destination){
		TreeSet subset = new TreeSet<>();
		
		for(FlowConnection conn: this) {
			if(conn.getDestination() == destination)
				subset.add(conn);
		}
		
		return subset;
	}
	
	/*
	 * This is used for the spreadsheet
	 */
	public Map<PropertyName, FlexibleListProperty> getCompactVariableList(ArrayList<PropertyName> names){
		Map<PropertyName, FlexibleListProperty> flat = new TreeMap<>(); 

		Set<FlowConnection> connections = this.sortByX();
		
		if(names.contains(new PropertyName("id"))) {
			List<Property> ids = new ArrayList<>();
			for(FlowConnection conn: connections) {
				List<Property> source_destination = new ArrayList<>();
				source_destination.add(conn.getOrigin().id);
				source_destination.add(conn.getDestination().id);
				ids.add(FlexibleListProperty.createList(collection, source_destination));
			}
			FlexibleListProperty connectionListProperty = FlexibleListProperty.createList(collection, ids);				
			flat.put(connectionListProperty.getPropertyName(), connectionListProperty);
		}
		if(names.contains(new PropertyName("weight"))) {
			List<Property> weights = new ArrayList<>();
			for(FlowConnection conn: connections) {
				weights.add(conn.weightProperty());
			}
			FlexibleListProperty weightListProperty = FlexibleListProperty.createList(collection, weights);				
			flat.put(weightListProperty.getPropertyName(), weightListProperty);
		}
				
		return flat;
	}

	
	/*
	 * This is used for the inspector
	 */

	public Map<PropertyName, FlexibleListProperty> getVariableList(){ 
		Map<PropertyName, FlexibleListProperty> flat = new TreeMap<>(); 
		
		// 1: First, we add the IDs of all nodes!!!
		//FlexibleListProperty ids = FlexibleListProperty.createList(bean, properties);
		List<Property> ids1 = new ArrayList<>();
		List<Property> ids2 = new ArrayList<>();
		List<Property> weights = new ArrayList<>();
		for(FlowConnection conn: this.sortByX()) {
			ids1.add(conn.getOrigin().id);
			ids2.add(conn.getDestination().id);
			weights.add(conn.weightProperty());
		}

		FlexibleListProperty originListProperty = FlexibleListProperty.createList(collection, ids1);	
		FlexibleListProperty destinationListProperty = FlexibleListProperty.createList(collection, ids2);	
		FlexibleListProperty weightListProperty = FlexibleListProperty.createList(collection, weights);	
		
		flat.put(/*originListProperty.getPropertyName()*/new PropertyName("source"), originListProperty);
		flat.put(/*destinationListProperty.getPropertyName()*/new PropertyName("destination"), destinationListProperty);
		flat.put(weightListProperty.getPropertyName(), weightListProperty);
		
		// TO add the extra property for weights
		
		return flat;
	}

	// Sort all connections by the X coordinate of their origin, destination
	public Set<FlowConnection> sortByX(){
		TreeSet<FlowConnection> subset = new TreeSet<FlowConnection>(new Comparator<FlowConnection>() {

			@Override
			public int compare(FlowConnection conn1, FlowConnection conn2) {
				ShapeMark origin1 = conn1.getOrigin();
				ShapeMark origin2 = conn2.getOrigin();
				
				if(origin1 != origin2) {
					double x1 = origin1.coords.getX();
					double x2 = origin2.coords.getX();
					
					if(x1 < x2) return -1;
					else if(x1 > x2) return 1;
					else if (origin1.coords.getY() <= origin2.coords.getY()) return -1;
					else return 1;
				} else {
					ShapeMark dest1 = conn1.getDestination();
					ShapeMark dest2 = conn2.getDestination();
					
					if(dest1 == dest2) return 0;
					
					double x1 = dest1.coords.getX();
					double x2 = dest2.coords.getX();
					
					if(x1 < x2) return -1;
					else if(x1 > x2) return 1;
					else if (dest1.coords.getY() <= dest2.coords.getY()) return -1;
					else return 1;
				}
			}
		});
		
		for(FlowConnection conn: this) {
			subset.add(conn);
		}
		
		return subset;
	}
	
	
	// Sort all connections by the Y coordinate of their origin, destination
	// Also, rest
	public Set<FlowConnection> sortByY(){
		TreeSet<FlowConnection> subset = new TreeSet<FlowConnection>(selfAsComparator());
		
		for(FlowConnection conn: this) {
			subset.add(conn);
		}
		
		return subset;
	}
	
	private Comparator<FlowConnection> selfAsComparator(){
		return this;
	}
	
	@Override
	public int compare(FlowConnection conn1, FlowConnection conn2) {
		ShapeMark origin1 = conn1.getOrigin();
		ShapeMark origin2 = conn2.getOrigin();
		
		if(origin1 != origin2) {
			double y1 = origin1.getYIn(collection);//origin1.coords.getY();
			double y2 = origin2.getYIn(collection);//origin2.coords.getY();
			
			if(y1 < y2) return 1;
			else if(y1 > y2) return -1;
			else if (origin1.getXIn(collection) <= origin2.getXIn(collection)) return 1;
			//else if (origin1.coords.getX() <= origin2.coords.getX()) return 1;
			else return -1;
		} else {
			ShapeMark dest1 = conn1.getDestination();
			ShapeMark dest2 = conn2.getDestination();
			
			if(dest1 == dest2) return 0;
			
			double y1 = dest1.getYIn(collection);//dest1.coords.getY();
			double y2 = dest2.getYIn(collection);//dest2.coords.getY();
			
			if(y1 < y2) return 1;
			else if(y1 > y2) return -1;
			else if (dest1.getXIn(collection) <= dest2.getXIn(collection)) return 1;
			//else if (dest1.coords.getX() <= dest2.coords.getX()) return 1;
			else return -1;
		}
	}
	
	
	
}
