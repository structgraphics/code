package fr.inria.structgraphics.ui.viscanvas.groupings;

import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.ShapeMark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.types.ConnectionWeightProperty;
import fr.inria.structgraphics.ui.utils.FlowConnectionBinding;

public class FlowConnection implements Comparable<FlowConnection> {

	private ShapeMark origin, destination;
	public ConnectionWeightProperty weight;
	private LineConnectedCollection collection;
	private FlowConnections parent;
	
	public FlowConnection(LineConnectedCollection collection, ShapeMark origin, ShapeMark destination) {
		this.collection = collection;
		this.origin = origin;
		this.destination = destination;
		weight = new ConnectionWeightProperty(this);
		weight.set(1);
	}
	
	public void destroy() {
		collection.removeConnection(this);
		parent.remove(this);
	}

	public void detachFront() {
		FlowConnectionBinding binding = destination.getFlowConnectionsBinding();
		binding.removeInwardConnection(this);
		
		if(parent.hasInwardConnections(destination)) {
			destination.height.updateValue(destination.height() - weight.get());
		}
	}
	
	public void detachBack() {
		FlowConnectionBinding binding = origin.getFlowConnectionsBinding();
		binding.removeOutwardConnection(this);
		
		if(parent.hasOutwardConnections(origin)) {
			origin.height.updateValue(origin.height() - weight.get());
		}
		
		origin.updateLabels();
	}
	
	public double startX() {
		double startX = origin.right();
		
		for(VisBody parent = (VisBody)origin.getContainer(); parent != collection; parent = (VisBody)parent.getContainer()) {
			startX += parent.coords.getX();
		}
		
		return startX;
	}
	
	public double startY() {
		double startY = -getOrigin().centerY();
		
		for(VisBody parent = (VisBody)origin.getContainer(); parent != collection; parent = (VisBody)parent.getContainer()) {
			startY -= parent.coords.getY();
		}

		return startY;
	}
	
	public double startYBottom() {		
		return startY() - Math.abs(getOrigin().height())/2;
	}
	
	public double endYBottom() {		
		return endY() - Math.abs(getDestination().height())/2;
	}
	
	public double endX() {
		double endX = getDestination().left();
		
		for(VisBody parent = (VisBody)destination.getContainer(); parent != collection; parent = (VisBody)parent.getContainer()) {
			endX += parent.coords.getX();
		}
		
		return endX;
	}
	
	public double endY() {
		double endY = -getDestination().centerY();
		
		for(VisBody parent = (VisBody)destination.getContainer(); parent != collection; parent = (VisBody)parent.getContainer()) {
			endY -= parent.coords.getY();
		}
		
		return endY;
	}
	
	public ShapeMark getOrigin() {
		return origin;
	}
	
	public ShapeMark getDestination() {
		return destination;
	}
	
	public ConnectionWeightProperty weightProperty() {
		return weight;
	}
	
	@Override
	public int compareTo(FlowConnection conn) {
		if(equals(conn)) return 0;
		
		int comp = origin.id.get().compareTo(conn.origin.id.get());
		if(comp == 0) return destination.id.get().compareTo(conn.destination.id.get());
		else return comp;
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof FlowConnection) {
			FlowConnection conn = (FlowConnection)o;
			if(conn.origin == origin && conn.destination == destination) {
				return true;
			}
		}
		
		return false;
	}

	public void setParent(FlowConnections flowConnections) {
		this.parent = flowConnections;
	}
	
	public LineConnectedCollection getCollection() {
		return collection;
	}


	@Override
	public int hashCode() { // Hmmm. This could generate a bug... very unlikely though...
	    return origin.hashCode() + destination.hashCode();
	}
}
