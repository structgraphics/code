package fr.inria.structgraphics.ui.viscanvas.groupings;

import fr.inria.structgraphics.types.DistributionProperty;
import fr.inria.structgraphics.types.DistributionProperty.Constraint;

public class ReferenceStructure {			
	protected boolean shared = false;
	public DistributionProperty.Constraint constraint = Constraint.None;
	
	public ReferenceStructure (boolean shared, Constraint constraint) {
		this.shared = shared;
		this.constraint = constraint;
	}
		
	public boolean isShared() {
		return shared;
	}
}
