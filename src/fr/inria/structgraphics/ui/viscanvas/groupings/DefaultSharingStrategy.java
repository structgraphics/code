package fr.inria.structgraphics.ui.viscanvas.groupings;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;

import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.PositionCoords;
import fr.inria.structgraphics.graphics.ReferenceCoords;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisGroup;
import fr.inria.structgraphics.graphics.XMarkComparator;
import fr.inria.structgraphics.graphics.YMarkComparator;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.DoubleListProperty;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.NestedListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.ui.utils.GroupBinding;
import fr.inria.structgraphics.ui.viscanvas.groupings.Grouping.Type;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.scene.paint.Color;

public class DefaultSharingStrategy extends SharingStrategy {

	public final static double MAX_VARIANCE = 15;
	
	protected boolean isVariable(FlexibleListProperty column) {		
		if(column.isEmpty()) return false;
		else if(column.getHiddenProperty().get()) return true;
		
		if(column instanceof DoubleListProperty) {
			double mean = ((DoubleListProperty) column).average();
			if(column.getName().startsWith("thickness")) {
				if(((DoubleListProperty) column).variance(mean) == 0) return false;
				else return true;
			}
			else {
				if(((DoubleListProperty) column).variance(mean) < MAX_VARIANCE) return false;
				else return true;
			}
			
		} 
		else if(column instanceof NestedListProperty) {
			List<FlexibleListProperty> transposed = ((NestedListProperty)column).transpose();

			for(FlexibleListProperty listProperty:transposed) {
				if(isVariable(listProperty)) return true;
			}
			return false;
		}
		else if(column.allEqual()) return false;
		
		return true;
	}


	/*
	 * Create a VisCollection Object 
	*/
	
	@Override
	public VisCollection createCollection(ObservableList<Mark> marks) {
		ReferenceStructureCreator creator = new ReferenceStructureCreator(marks, Type.Collection);
		XReferenceStructure xstruct = creator.getXReferenceStructure();
		YReferenceStructure ystruct = creator.getYReferenceStructure();
				
		SortedList<Mark> sorted; 
		if(xstruct.isShared()) sorted = new SortedList<>(marks, new YMarkComparator());
		else sorted = new SortedList<>(marks, new XMarkComparator());

		VisCollection group = new LineConnectedCollection(DefaultNestingStrategy.getRoot(sorted), true);
				//new VisCollection(DefaultNestingStrategy.getRoot(sorted));
		group.setLevel(sorted.get(0).getLevel() + 1);
		ReferenceCoords refCoords = new ReferenceCoords(RefX.Left, RefY.Bottom);
		
		group.setRefCoords(refCoords);
				
		PositionCoords coords = new PositionCoords(group, xstruct.getRefValue(), ystruct.getRefValue());
		coords.xRef.set(RefX.Left);
		coords.yRef.set(RefY.Bottom);
		group.setCoords(coords);
		
		for(Mark mark: sorted) {
			// TODO: Does it work?
			if(!(mark instanceof VisBody)) {
				mark.getCoords().xRef.set(xstruct.getRef());
				mark.getCoords().yRef.set(ystruct.getRef());
			}
			
			group.attach(mark, xstruct.isShared(), ystruct.isShared());
		}
		
		group.refresh();
		
		CollectionPropertyStructure propertyStructure = new CollectionPropertyStructure(sorted, this);
		group.setPropertyStructure(propertyStructure);
		group.initProperties();
		group.initVisibility();
				
		group.getConstraintXProperty().set(xstruct.constraint);
		group.getConstraintYProperty().set(ystruct.constraint);
		
		propertyStructure.fixXSharing(); 
		propertyStructure.fixYSharing(); 
		
		group.strokePaint.set(Color.color(.85, .8, .8, .5));
		
		group.getRoot().update();
				
		return group;
	}


	/*
	 * Create a VisGroup object 
	*/
	@Override
	public VisGroup createGroup(ObservableList<Mark> marks) {
		// TODO I just repeat here the collection stuff, but this needs to change
		ReferenceStructureCreator creator = new ReferenceStructureCreator(marks, Type.Group);
		XReferenceStructure xstruct = creator.getXReferenceStructure();
		YReferenceStructure ystruct = creator.getYReferenceStructure();
		
		SortedList<Mark> sorted; 
		if(xstruct.isShared()) sorted = new SortedList<>(marks, new YMarkComparator());
		else sorted = new SortedList<>(marks, new XMarkComparator());

		VisGroup group = new VisGroup(DefaultNestingStrategy.getRoot(sorted), true);
		group.setLevel(sorted.get(0).getLevel() + 1);
		ReferenceCoords refCoords = new ReferenceCoords(RefX.Left, RefY.Bottom);
		
		group.setRefCoords(refCoords);
				
		PositionCoords coords = new PositionCoords(group, xstruct.getRefValue(), ystruct.getRefValue());
		coords.xRef.set(RefX.Left);
		coords.yRef.set(RefY.Bottom);
		group.setCoords(coords);

		for(Mark mark: sorted) {
			// TODO: Does it work?
			if(!(mark instanceof VisBody)) {
		//		mark.getCoords().xRef.set(xstruct.getRef());
		//		mark.getCoords().yRef.set(ystruct.getRef());
			}
			
			group.attach(mark, xstruct.isShared(), ystruct.isShared());
		}
		
		group.refresh();
		
		GroupPropertyStructure propertyStructure = new GroupPropertyStructure(sorted, this);
		group.setPropertyStructure(propertyStructure);
		group.initProperties();
		group.initVisibility();
		group.getConstraintXProperty().set(xstruct.constraint);
		group.getConstraintYProperty().set(ystruct.constraint);
		
		propertyStructure.fixXSharing(); 
		propertyStructure.fixYSharing(); 
		
		group.getRoot().update();
		
		return group;
	}

	
	@Override
	public void share(Map<PropertyName, FlexibleListProperty> properties, 
			TreeMap<PropertyName, ArrayList<GroupBinding>> bindings, 
			SortedSet<PropertyName> common, SortedSet<PropertyName> variable) {
		
		for(PropertyName name:properties.keySet()) {
			FlexibleListProperty property = properties.get(name);
			ArrayList<GroupBinding> groups = property.createGroupBindings();
			
			if(groups.size() < property.size()) {
				bindings.put(name, groups);
				common.add(name);
			} else variable.add(name);
		}
	}
}
