package fr.inria.structgraphics.ui.viscanvas.groupings;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.types.DistributionProperty.Constraint;

public class XReferenceStructure extends ReferenceStructure {			
	private RefX refx = null;
	private double x;

	public XReferenceStructure (RefX refx, double x, boolean shared, Constraint constraint) {
		super(shared, constraint);
		this.refx = refx;
		this.x = x;				
	}
	
	public RefX getRef() {
		return refx;
	}
	
	public double getRefValue() {
		return x;
	}
	
	public String toString() {
		return "X: " + refx + ", " + x + " " + constraint;
	}
	
}
