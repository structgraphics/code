package fr.inria.structgraphics.ui.viscanvas.groupings;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.DistributionProperty.Constraint;

public class YReferenceStructure extends ReferenceStructure {			
	private RefY refy;
	private double y;
	
	public YReferenceStructure (RefY refy, double y, boolean shared, Constraint constraint) {
		super(shared, constraint);
		this.refy = refy;	
		this.y = y;		
	}
	
	public RefY getRef() {
		return refy;
	}
	
	public double getRefValue() {
		return y;
	}
	
	public String toString() {
		return "Y: " + refy + ", " + y + " " + constraint;
	}

}
