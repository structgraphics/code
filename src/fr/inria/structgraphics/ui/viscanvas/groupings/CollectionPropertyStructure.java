package fr.inria.structgraphics.ui.viscanvas.groupings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.ShapeMark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisGroup;
import fr.inria.structgraphics.types.ConstrainedDoubleProperty;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.NestedListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.Shareable;
import fr.inria.structgraphics.ui.inspector.util.PropertyCloner;
import fr.inria.structgraphics.ui.utils.GroupBinding;
import javafx.beans.property.Property;

public class CollectionPropertyStructure extends PropertyStructure {
	
	protected TreeMap<PropertyName, Property> shared;
	protected List<Property> merged;
	
	protected TreeMap<PropertyName, DimensionalBinding> bindings;
	protected TreeMap<PropertyName, GroupBinding> publicBindings;
	
	public CollectionPropertyStructure(Collection<Mark> marks) {
		this(marks, new DefaultSharingStrategy());
	}
	
	public CollectionPropertyStructure(Collection<Mark> marks, CollectionPropertyStructure structure) {		
		properties = new TreeMap<>();
		
		for(Mark mark: marks) {
			mark.addSelfProperties(true, properties);
		}
		
		for(FlexibleListProperty property:properties.values())
		{
			if(property.getName().matches(PropertyName.getRegExpr("x")) ) {
				id_x = new PropertyName(property.getName());
				if(id_y != null) break;
			}
			else if(property.getName().matches(PropertyName.getRegExpr("y")) ) {
				id_y = new PropertyName(property.getName());
				if(id_x != null) break;
			}
		}
		
		common = new TreeSet<>();
		common.addAll(structure.common);
		
		variable = new TreeSet<>();
		variable.addAll(structure.variable);
		
		shared = new TreeMap<>();
		bindings = new TreeMap<>();
		
		merge();
		
		bindGroupPublic(marks);
	}
	
	
	public CollectionPropertyStructure(Collection<Mark> marks, SortedSet<PropertyName> common, SortedSet<PropertyName> variable) {
		properties = new TreeMap<>();
		
		for(Mark mark: marks) {
			mark.addSelfProperties(true, properties);
		}
		
		for(FlexibleListProperty property:properties.values())
		{
			if(property.getName().matches(PropertyName.getRegExpr("x")) ) {
				id_x = new PropertyName(property.getName());
				if(id_y != null) break;
			}
			else if(property.getName().matches(PropertyName.getRegExpr("y")) ) {
				id_y = new PropertyName(property.getName());
				if(id_x != null) break;
			}
		}
		
		this.common = common;
		this.variable = variable;
		
		shared = new TreeMap<>();
		bindings = new TreeMap<>();
		
		merge();
		
		bindGroupPublic(marks);
	}
	

	public CollectionPropertyStructure(Collection<Mark> marks, SharingStrategy strategy) {
		properties = new TreeMap<>();
				
		for(Mark mark: marks) {
			mark.addSelfProperties(true, properties);
		}
		
		for(FlexibleListProperty property:properties.values())
		{
			if(property.getName().matches(PropertyName.getRegExpr("x"))) {
				id_x = new PropertyName(property.getName());
				if(id_y != null) break;
			}
			else if(property.getName().matches(PropertyName.getRegExpr("y"))) {
				id_y = new PropertyName(property.getName());
				if(id_x != null) break;
			}
		}
		
		common = new TreeSet<>();
		variable = new TreeSet<>();
		shared = new TreeMap<>();
		bindings = new TreeMap<>();
		
		strategy.share(properties.values(), common, variable);
		merge();
		
		bindGroupPublic(marks);
	}

	//TODO
	public void refresh(SharingStrategy strategy) {
		SortedSet<PropertyName> common_ = new TreeSet<>();
		SortedSet<PropertyName> variable_ = new TreeSet<>();
		strategy.share(properties.values(), common_, variable_);
				
		for(PropertyName name: properties.keySet()) {
			if(common.contains(name) && !common_.contains(name)) {
				moveToVariable(name);
			}
			else if(common_.contains(name) && !common.contains(name)) {
				moveToCommon(name);
			}
		}
	}
	
	
	private void bindGroupPublic(Collection<Mark> marks) {
		if(marks.iterator().next() instanceof VisGroup) {
			publicBindings = new TreeMap<>();
			
			for(FlexibleListProperty property:properties.values()) {
				ArrayList<Property> pubproperties = new ArrayList<>();
				for(Property p:property.getValue()) {
					pubproperties.add(((Shareable)p).getPublicProperty());					
				}
				
				// TODO: mode work needed....
				GroupBinding binding = new GroupBinding(pubproperties, false);
				publicBindings.put(property.getPropertyName(), binding);
				binding.rebind();
			}
		}
	}
	
	private void refreshPublicBindings() {
		if(publicBindings != null) {
			for(FlexibleListProperty property:properties.values()) {
				ArrayList<Property> pubproperties = new ArrayList<>();
				for(Property p: property.getValue()) {
					pubproperties.add(((Shareable)p).getPublicProperty());					
				}
				
				GroupBinding binding = publicBindings.get(property.getPropertyName());
				binding.resetProperties(pubproperties);
			}
		}
	}
	
	public void update(Mark mark, SharingStrategy strategy) {
		mark.addSelfProperties(true, properties);
		strategy.share(properties.values(), common, variable);
		merge();
	}

	@Override
	public void addMark(Mark mark, SharingStrategy strategy) {
		mark.addSelfProperties(true, properties);
		for(PropertyName key: common) {
			destroyBindings(key);
		}
	
//		common.clear();
//		variable.clear();
		shared.clear();
		bindings.clear();
		
//		strategy.share(properties.values(), common, variable);
		merge();
		
		refreshPublicBindings();
	}
	
	@Override
	public void addMark(Mark mark) {
		mark.addSelfProperties(true, properties);

		for(PropertyName name: common) {
			FlexibleListProperty column = properties.get(name);
			if(column.size() > 0) {
				column.get(mark.getPos()).bindBidirectional(shared.get(name));			
			}
		}
		
		refreshPublicBindings();
	}
	
	@Override
	public void removeMark(Mark mark) {	
		mark.removeSelfProperties(properties); // TODO: unnecessary to remove at the higher level groups
						
		rebuildBindings();
		refreshPublicBindings();
	}
	
	@Override
	public void rebuildBindings() {
		for(PropertyName key: common) {
			destroyBindings(key);
		}
		
		bindings.clear();
		merge();
	}
			
	
	// TODO: not complete
	public boolean areCommon(Property p1, Property p2) {
		PropertyName name = new PropertyName(p1.getName());
		if(common.contains(name) && p1.getValue().equals(p2.getValue())) return true;
		else {
			return false;
		}
	}
	
	@Override
	public void moveToCommon(PropertyName name) {
		if(common.contains(name)) return;
		if(name.isHeight()) {
			VisBody collection = container.getRootVirtualGroup();
			if(collection instanceof LineConnectedCollection && 
					((LineConnectedCollection)collection).hasConnections()) {
				return;

			}
		}
		
		fixYConstraint(name);
		fixXConstraint(name);
		
		variable.remove(name);
		common.add(name);
		merge();
		
		updateSiblings(name, true);
		updateChildren(name);
		
		flagProperty.set(!flagProperty.get());
		
		if(name.isWidth() || name.isHeight()) {
			FlexibleListProperty property = properties.get(name);
			ConstrainedDoubleProperty prop = ((ConstrainedDoubleProperty)(property.flatten().get(0)));
			
			prop.updateValue(prop.getValue());
		}
	}
	
	@Override
	public void moveToVariable(PropertyName name) {
		if(variable.contains(name)) return;
		
		common.remove(name);
		variable.add(name);
		destroyBindings(name);
		merge();
		
		updateSiblings(name, false);
		
		flagProperty.set(!flagProperty.get());
		
		if(name.isHeight() || name.isWidth()) {
			FlexibleListProperty list = properties.get(name);
			Property property = list.flatten().get(0);
			ShapeMark mark = (ShapeMark)property.getBean();
			if(mark.ratiolock.get()) { 
				if(name.isWidth()) moveToVariable(new PropertyName(mark.height.getName()));
				else moveToVariable(new PropertyName(mark.width.getName()));				
			}
		}
	}

	
	private void updateChildren(PropertyName name){ // TODO: check how I can link heterogenuos groups!!!
		if(container instanceof VisCollection) {
			VisCollection vgroup = (VisCollection) container;
			if(vgroup.getComponents().size() < 1) return;
			Mark mark = vgroup.getComponents().get(0);
			if(mark instanceof VisCollection) {
				boolean incommon = ((VisCollection) mark).getChildPropertyStructure().common.contains(name);
				for(int i = 1; i < vgroup.getComponents().size(); ++i) {
					mark = vgroup.getComponents().get(i);
					if(mark instanceof VisCollection && ((VisCollection) mark).getChildPropertyStructure().common.contains(name) != incommon) {
						if(incommon) ((VisCollection) mark).getChildPropertyStructure().moveToCommon(name);
						else ((VisCollection) mark).getChildPropertyStructure().moveToVariable(name);
					}
				}
			}
		}
	}
	
	
	private void updateSiblings(PropertyName name, boolean toCommon) {
		if(container instanceof VisCollection) {
			Container parent = ((VisCollection)container).getContainer();
			if(parent instanceof VisCollection) {
				VisCollection vgroup = ((VisCollection)parent);
		//		if(vgroup.getChildPropertyStructure().getCommon().contains(name)) { // Removed!!!
					
					for(Mark mark: vgroup.getComponents()) {
						if(mark instanceof VisCollection && mark != container) {
							if(toCommon) ((VisCollection)mark).getChildPropertyStructure().moveToCommon(name);
							else ((VisCollection)mark).getChildPropertyStructure().moveToVariable(name);
							
							//
						}
					}
		//			}
			}
		}
	}
	
	public List<Property> getMerged(){
		List<Property> flat = new ArrayList<>();
		
		for(Property property : merged) {
			if(property instanceof FlexibleListProperty && container instanceof VisCollection) {			
				flat.add(((FlexibleListProperty)property).getCompact(((VisCollection) container).getComponentAt(0)));		
			} else flat.add(property);
		}
		
		return flat;
	}
	
		
	@Override
	public void destroyAllBindings() {
		for(PropertyName name:common) {
			destroyBindings(name);
		}
	}
	
	
	// TODO: I need to change the bindings mechanism
	private void destroyBindings(PropertyName name) { // TODO:...
		Property prop = shared.get(name);
		
		FlexibleListProperty column = properties.get(name);
		if(column instanceof NestedListProperty) {
			DimensionalBinding binding = bindings.get(name);
			binding.destroy();
			bindings.remove(name);
		//	unmerge((NestedListProperty)column, id);
		}
		else if(column != null){
			for(int i = 0; i < column.size(); ++i) {
				column.get(i).unbindBidirectional(prop);
			}
		}

		shared.remove(name);
	}
	
	private void merge() {
		merged = new ArrayList<>();
		
		for(PropertyName name: common) {
			FlexibleListProperty column = properties.get(name);
			if(column == null) continue;
			if(column instanceof NestedListProperty) {
				FlexibleListProperty prop = (FlexibleListProperty)shared.get(name);
				if(prop != null) merged.add(prop);
				else {
					// Find the longest list as it has information for more items!!!
					int max = 0;
					int index = 0;
					for(int i = 0; i < column.size(); ++i) {
						int size = (((NestedListProperty)column).getPropertyList(i)).size();
						if(size > max) {
							index = i;
							max = size;
						}						
					}
					
					prop = (FlexibleListProperty)PropertyCloner.replicate(column.get(index));
					
					merged.add(prop);
					shared.put(name, prop);
					bindings.put(name, new DimensionalBinding(this, column, prop.flattenFlex()));					
				}
			}
			else {
				Property prop = shared.get(name);
				if(prop!= null) merged.add(prop);
				else if(column.size() >  0) { 
					prop = PropertyCloner.replicate(column.get(0));	
					
					merged.add(prop);
					shared.put(name, prop); 
					for(int i = 0; i < column.size(); ++i) {
						column.get(i).bindBidirectional(prop);
					}
				}
			}
		}
	}

	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Common: ");
		for(PropertyName name: common) buffer.append(properties.get(name).getName() + " =  " + properties.get(name).get() + ", ");
		buffer.append("\nVariable: ");
		for(PropertyName name: variable) buffer.append(properties.get(name).getName()+ " =  " + properties.get(name).get() + " ");
		
		return buffer.toString();
	}

}
