package fr.inria.structgraphics.ui.viscanvas.groupings;

import fr.inria.structgraphics.graphics.Mark;
import javafx.collections.ObservableList;

public class Grouping {
	
	public enum Type {Collection, Group} 
	
	protected Mark markGroup;
	protected CollectionPropertyStructure structure;
	
	public Grouping(ObservableList<Mark> marks, Type type) {
		if(type == Type.Collection) markGroup = new DefaultSharingStrategy().createCollection(marks);
		else markGroup = new DefaultSharingStrategy().createGroup(marks);
	}
		
	public Mark getGroup() {
		return markGroup;
	}
}
