package fr.inria.structgraphics.ui.viscanvas;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

import javax.json.JsonArray;
import javax.json.JsonObject;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.MarkFactory;
import fr.inria.structgraphics.graphics.SimpleMark;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.persistence.JsonDescription;
import fr.inria.structgraphics.persistence.JsonObjectReader;
import fr.inria.structgraphics.persistence.JsonObjectWriter;
import fr.inria.structgraphics.persistence.JsonSpreadsheetWriterReader;
import fr.inria.structgraphics.persistence.JsonWorkspaceWriter;
import fr.inria.structgraphics.types.ShapeProperty;
import fr.inria.structgraphics.ui.DisplayPreferences;
import fr.inria.structgraphics.ui.inspector.DisplayUtils;
import fr.inria.structgraphics.ui.inspector.InspectorView;
import fr.inria.structgraphics.ui.library.LibraryPane;
import fr.inria.structgraphics.ui.spreadsheet.DataView;
import fr.inria.structgraphics.ui.tools.CollectionCreationTool;
import fr.inria.structgraphics.ui.tools.EraserTool;
import fr.inria.structgraphics.ui.tools.FlowDrawingTool;
import fr.inria.structgraphics.ui.tools.GroupCreationTool;
import fr.inria.structgraphics.ui.tools.MarkSelection;
import fr.inria.structgraphics.ui.tools.MultiplyTool;
import fr.inria.structgraphics.ui.tools.SelectTool;
import fr.inria.structgraphics.ui.tools.ShapeDrawingTool;
import fr.inria.structgraphics.ui.tools.Tool;
import javafx.animation.Transition;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Control;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

public class CanvasFrame extends BorderPane {

    public static final String STYLESHEETS = CanvasFrame.class.getResource("canvasframe.css").toExternalForm();
	
    public final static int LIB_WIDTH = 150;
    
	private Cursor visCursor = Cursor.DEFAULT;

	VBox menus;

	private SplitPane splitPane;
	private BorderPane canvasPane;
	private LibraryPane libraryPane;
	
	private Button clean, open, saveAs;
	private Tool select, line, rect, circle, triangle, flowline, eraser, link, construct,  multiply, text;
	private Tool selectedTool;
	private ToggleButton showAnchors, showLibrary;
	
	private Canvas canvas;
	private VisFrame visframe;
	
	private InspectorView inspector;
	private DataView dataview;
	
	private SimpleMark draggable;
	
	private MarkSelection selectedMarks = new MarkSelection();
	private Stage stage;
	
	public CanvasFrame(Stage stage) {
		canvas = new Canvas();
		this.stage = stage;

		menus = new VBox();
		setTop(menus);	

		initTools();
		setDrawingListeners();
		
		canvasPane = new BorderPane();
		
		libraryPane = new LibraryPane(this);
		libraryPane.setMinWidth(LIB_WIDTH);
		ScrollPane scroller = new ScrollPane();
		scroller.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		scroller.setContent(libraryPane.getCanvas());
		scroller.setStyle("-fx-background: #FFFFFF;");
		
		libraryPane.getCanvas().minWidthProperty().bind(Bindings.createDoubleBinding(() -> 
        	scroller.getViewportBounds().getWidth(), scroller.viewportBoundsProperty()));
		libraryPane.setCenter(scroller);
		
		splitPane = new SplitPane();
        splitPane.setDividerPosition(0, 0);
        splitPane.getItems().addAll(libraryPane, canvasPane);
      
        SplitPane.setResizableWithParent(libraryPane, false);
        
		splitPane.setId("splitpane");
        
        setCenter(splitPane);
        
        libraryPane.load();
	}

	public Stage getStage() {
		return stage;
	}
	
	private void initTools() {
		select = new SelectTool(this);
		line = new ShapeDrawingTool(this, ShapeProperty.Type.Line);
		rect = new ShapeDrawingTool(this, ShapeProperty.Type.Rectangle);
		circle = new ShapeDrawingTool(this, ShapeProperty.Type.Ellipse);
		triangle = new ShapeDrawingTool(this, ShapeProperty.Type.Triangle);
		flowline = new FlowDrawingTool(this);
		text = new ShapeDrawingTool(this, ShapeProperty.Type.Text);
		eraser = new EraserTool(this);
		link = new CollectionCreationTool(this);
		multiply = new MultiplyTool(this);
		
		construct = new GroupCreationTool(this);
		
		setActiveTool(select);		
		select.setActive(true);
	}
	
	public LibraryPane getLibrary() {
		return libraryPane;
	}
	
	public MarkSelection selected() {
		return selectedMarks;
	}
	
	public void setVisFrame(VisFrame frame) {
		visframe = frame;
		canvas.setContent(frame.getGroup());
	}
	
	private void setDrawingListeners() {
		canvas.setOnMouseMoved(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) { 
				//if(select.getActive()) return;			
				selectedTool.handleMove(event);
			}
		});
		
		canvas.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) { 
				//if(select.getActive()) return;			
				selectedTool.handlePress(event);
			}
		});
		
		canvas.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				//if(select.getActive()) return;			
				selectedTool.handleDrag(event);
			}
		});
		
		canvas.setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				//if(select.getActive()) return;			
				selectedTool.handleRelease(event);
			}
		});
	}

	
	public VisFrame getVisFrame() {
		return visframe;
	}

	public void setViews(InspectorView inspector, DataView dataview) {
		this.inspector = inspector;
		this.dataview = dataview;
		
		dataview.setSelector((SelectTool)select);
		inspector.setSelector((SelectTool)select);
	}
	
	public InspectorView getInspector() {
		return inspector;
	}
	
	public DataView getDataView() {
		return dataview;
	}
	
	public void setMenubars(Stage stage, Stage inspectStage) {

		stage.getScene().cursorProperty().bindBidirectional(inspectStage.getScene().cursorProperty());
		stage.getScene().cursorProperty().bindBidirectional(canvas.cursorProperty());

		canvas.setOnMouseEntered(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				stage.getScene().cursorProperty().set(visCursor);
				//setCursor(visCursor); /// There is still a bug here, when i reactivate the window!!!!
				//getCenter().setCursor(visCursor);
			}
		});

		canvas.setOnMouseExited(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				stage.getScene().cursorProperty().set(Cursor.DEFAULT);
				//getCenter().setCursor(Cursor.DEFAULT);
			}
		});


		// VisMenuBar menuBar = new VisMenuBar(stage, inspectStage, this, inspector, dataview);
		// menus.getChildren().add(menuBar);

		open = new Button("Open", new ImageView(DisplayUtils.getIcon("open-file")));
		clean = new Button("Clean", new ImageView(DisplayUtils.getIcon("cleaning")));
		saveAs = new Button("Save", new ImageView(DisplayUtils.getIcon("save")));
		
		showAnchors = new ToggleButton(null, new ImageView(DisplayUtils.getIcon("axis")));
		showAnchors.setTooltip(new Tooltip("Show/Hide Anchors"));
		
		showLibrary = new ToggleButton(null, new ImageView(DisplayUtils.getIcon("library")));
		showLibrary.setTooltip(new Tooltip("Library"));

		select.getToggleButton().setTooltip(new Tooltip("Select"));
		multiply.getToggleButton().setTooltip(new Tooltip("Replicate"));
		link.getToggleButton().setTooltip(new Tooltip("Create Collection"));
		construct.getToggleButton().setTooltip(new Tooltip("Create Group"));
		eraser.getToggleButton().setTooltip(new Tooltip("Erase"));
		
		line.getToggleButton().setTooltip(new Tooltip("Draw Line"));
		rect.getToggleButton().setTooltip(new Tooltip("Draw Rectangle"));
		triangle.getToggleButton().setTooltip(new Tooltip("Draw Triangle"));
		text.getToggleButton().setTooltip(new Tooltip("Create Textbox"));
		flowline.getToggleButton().setTooltip(new Tooltip("Draw a Connection"));
		
		// From: https://www.flaticon.com/free-icon/cleaning_1215980

		select.setActive(true);

		ToolBar toolBar = new ToolBar(open, saveAs, clean,  new Separator(), select.getToggleButton(), multiply.getToggleButton(), link.getToggleButton(), construct.getToggleButton(), eraser.getToggleButton(),
				new Separator(),
		/*		groupshape.getToggleButton(),*/
				line.getToggleButton(),
				rect.getToggleButton(), circle.getToggleButton(), triangle.getToggleButton(), text.getToggleButton(), flowline.getToggleButton(),
				new Separator(), showAnchors, showLibrary);
		menus.getChildren().add(toolBar);

		// Add Listeners
		open.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				openSyntax(stage);
			}
		});    
		
		saveAs.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				saveWorkspace(stage);
			}
		});      


		clean.setOnAction(
				new EventHandler<ActionEvent>() {
					@Override public void handle(ActionEvent e) {
						Alert alert = new Alert(AlertType.CONFIRMATION);
						alert.setTitle("Confirmation Dialog");
						alert.setHeaderText(null);
						alert.setContentText("Are you sure you want to clean the canvas?");

						Optional<ButtonType> result = alert.showAndWait();
						if (result.get() == ButtonType.OK){
							cleanCanvas();
						} else {
							// ... user chose CANCEL or closed the dialog
						}
					}
				});
		
		showAnchors.setSelected(true);
		showAnchors.setOnAction(new EventHandler<ActionEvent>() {
					@Override 
					public void handle(ActionEvent e) {
						visframe.showGroupInteractors(showAnchors.isSelected());
					}
				});
		
		showLibrary.setSelected(true);
		showLibrary.setOnAction(new EventHandler<ActionEvent>() {
					@Override 
					public void handle(ActionEvent e) {
						showLibrary(showLibrary.isSelected());
					}
				});
	}

	private void showLibrary(boolean show) {
		if(show) {
			Transition transition = new Transition() {
			     {
			         setCycleDuration(Duration.millis(150));
			     }
			     
				@Override
				protected void interpolate(double frac) {
					libraryPane.setMinWidth(LIB_WIDTH*frac);
					splitPane.setDividerPosition(0, 0);
					
					if(frac == 1) libraryPane.setMaxWidth(-1);
				}
				
			};
			transition.play();
		}
		else {
			Transition transition = new Transition() {
			     {
			         setCycleDuration(Duration.millis(150));
			     }
			     
				@Override
				protected void interpolate(double frac) {
					double w = LIB_WIDTH*(1 - frac);
					libraryPane.setMinWidth(w);
					splitPane.setDividerPosition(0, 0);		
					
					if(frac == 1) libraryPane.setMaxWidth(w);
				}
				
			};
			transition.play();
			
		}
	}
	
	public Canvas getCanvas() {
		return canvas;
	}
	
	public void setCanvasCursor(Cursor cursor) {
		visCursor = cursor;
	}
	
	
	public void setActiveTool(Tool tool) {
		if(tool == null) tool = select;
		
		if(selectedTool != tool) { 	
			if(selectedTool != null) selectedTool.setActive(false);
			selectedTool = tool;
			if(!selectedTool.getActive()) selectedTool.setActive(true);
		}
	}

	private void openSyntax(Stage visStage) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(
				new File("./gallery")
				); 

		fileChooser.getExtensionFilters().add(
				new FileChooser.ExtensionFilter("Json", "*.json", "*.wrk")
				);

		fileChooser.setTitle("Open File (json or wrk)");
		File file = fileChooser.showOpenDialog(visStage);
		if(file == null) return;

		JsonDescription descr = new JsonDescription(file);
		if(descr.isWorkspace()) {
			libraryPane.clear();
			cleanCanvas();
			libraryPane.load(descr.getLibraryArray(true));
			JsonObjectReader.readChildrenFromJasonArray(visframe, descr.getVisArray(true));
			inspector.update();
			
			// TODO: Add the spreadsheet
			JsonSpreadsheetWriterReader.readSpreadsheetFromJasonObject(descr.getSpreadsheetArray(), visframe, dataview.getSpreadsheet());
		}
		else if(descr.isLibrary()) {
			libraryPane.clear();
			libraryPane.load(descr.getLibraryArray(false));
		}
		else {
			JsonArray visArray = descr.getVisArray(false);
			JsonObjectReader.readChildrenFromJasonArray(visframe, visArray);
			inspector.update();
		}
	}

	public void saveAs(Stage visStage, JsonObject visualizations) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(new File("./json")); 

		fileChooser.getExtensionFilters().add(
				new FileChooser.ExtensionFilter("Json", "*.json")
				);

		fileChooser.setTitle("Save As Json");	
		File file = fileChooser.showSaveDialog(visStage);
		if(file == null) return;
		else {
			try {
				FileWriter writer = new FileWriter(file);
				writer.write(JsonObjectWriter.fromObjectToReadableString(visualizations, 0).toString());
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void saveWorkspace(Stage visStage) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(new File("./sessions")); 

		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Workspace", "*.wrk"));
		fileChooser.setTitle("Save Workspace (Json)");
		File file = fileChooser.showSaveDialog(visStage);
		if(file == null) return;
		else {
			try {
				FileWriter writer = new FileWriter(file);
				JsonObject dataObject = JsonWorkspaceWriter.saveToJason(libraryPane.getComponents(), visframe.getComponents(), dataview.getSpreadsheet());
				
				writer.write(JsonObjectWriter.fromObjectToReadableString(dataObject, 0).toString());
				
				//writer.write(dataObject.toString());
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	private void cleanCanvas() {
		VisFrame visFrame = MarkFactory.createVisFrame(DisplayPreferences.CANVAS_WIDTH, DisplayPreferences.CANVAS_HEIGHT);
		visFrame.initProperties();

		//  ScrollPane scroller = (ScrollPane)getCenter();
		// scroller.setContent(new StackPane(visFrame.getGroup()));

		setVisFrame(visFrame);

		inspector.setVisualizationFrame(visFrame);
		inspector.update();

		dataview.reset();
	}

	public void setDraggable(SimpleMark mark) {
		draggable = mark;
	}
	
	public SimpleMark getDragged() {
		return draggable;
	}

	public void setSketchArea(ScrollPane scroller) {
		canvasPane.setCenter(scroller);
	}

	public Control getSplitPane() {
		return splitPane;
	}
	
	public void close() {
		libraryPane.save();
	}
}

