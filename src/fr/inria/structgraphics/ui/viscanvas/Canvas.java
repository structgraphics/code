package fr.inria.structgraphics.ui.viscanvas;

import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public class Canvas extends StackPane {

	private Group feedbackLayer;
	private Rectangle rect;

	public Canvas() {
		alignmentProperty().set(Pos.TOP_LEFT);
		feedbackLayer = new Group();
	}
	
	public void setContent(Group group) {
		getChildren().clear();
		getChildren().add(group);
				
		getChildren().add(feedbackLayer);
		rect = new Rectangle(getWidth(), getHeight());
		rect.setStroke(null);
		rect.setFill(Color.TRANSPARENT);
	}

	public void show(Shape shape, Shape cancelArea, Shape rectTrace) {
		feedbackLayer.getChildren().clear();
		if(cancelArea != null || shape != null || rectTrace != null) {
			feedbackLayer.getChildren().add(rect);
			
			if(rectTrace != null) feedbackLayer.getChildren().add(rectTrace);
			if(cancelArea != null) feedbackLayer.getChildren().add(cancelArea);
			if(shape != null) feedbackLayer.getChildren().add(shape);
		}
	}	
	
	public void show(Shape shape) {
		feedbackLayer.getChildren().add(shape);
	}	

	public void hide(Shape shape) {
		feedbackLayer.getChildren().remove(shape);
	}	

}
