package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.types.DistributionProperty;
import fr.inria.structgraphics.types.DistributionProperty.Constraint;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;

public abstract class ConstraintController extends Group {
	
	public enum ConstraintHandle {
		LEFT, RIGHT, UP, DOWN
	}
	
	protected DistributionProperty.Constraint type = Constraint.None;
	protected VisBody group;
	
	protected double distance_init;
	protected Mark activeMark = null;
	
	protected static boolean handlerhold = false; // A constraint handler is currently hold/dragged
	protected static int xselectionIndex  = 0, yselectionIndex = 0;
	protected static double xselectionPos = Double.MAX_VALUE, yselectionPos = Double.MAX_VALUE;
	
	protected static VisBody highlighted_group = null;
	
	public ConstraintController(VisBody group) {
		this.group = group;
	}

	public abstract void updateMarkPositions(Mark mark);
	public abstract void setMark(Mark mark, boolean highlight);

	
	public ConstraintController getSelf() {
		return this;
	}
	
	public boolean intersects(Circle trace) {
		if(!isVisible()) return false;
		
		if(trace == null) return false;
		
		for(Node shape: getChildren()) {
			if(shape instanceof Circle && shape.isVisible()) {
				Shape intersection = Shape.intersect((Circle) shape, trace);			
				if(!(intersection instanceof Path) || !((Path)intersection).getElements().isEmpty()) {
					
					selectHandle((Circle)shape);
					
					return true;
				}
			}
		}	
		return false;
	}

	protected void selectHandle(Circle shape) {
		ConstraintController.handlerhold = true;
	}
	
	public void unselectHandle() {
		if(activeMark != null) activeMark.setConstraintControl(null, null);
		ConstraintController.handlerhold = false;
	}

	
	public abstract void refresh();

	public abstract void translate(Line lineTrace, ConstraintHandle constraintHandler);
}
