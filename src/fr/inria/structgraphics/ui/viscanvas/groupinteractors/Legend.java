package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import java.util.SortedSet;
import java.util.TreeMap;

import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.ui.spreadsheet.ComparableDataValue;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable;
import fr.inria.structgraphics.ui.spreadsheet.DiscreteTransformation;
import fr.inria.structgraphics.ui.spreadsheet.MappingProperty;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable.DataType;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Legend extends Group {
	protected Font textFont = new Font(12);
	protected Font labelFont = Font.font("Verdana", FontWeight.BOLD, 12);
	protected Color stroke = Color.GREY;
	protected double rectStrokeWidth = 0.8;
	protected Color textColor = Color.GREY;
	
	protected double vgap = 4;
	protected double hgap = 2;
	
	protected double iconSize = 12;
	
	protected Text label;
	
	protected VBox values;
	
	protected VisBody group;
	protected DataVariable variable;
	
	public Legend(VisBody group, DataVariable variable) {
		this.group = group;
		this.variable = variable;
			
		values = new VBox(vgap);
		
		label = new Text();
		label.setFont(labelFont);
		label.setFill(textColor);
		
		getChildren().add(values);
			
		updateCaption();
		updateValues();
	}
	
	public Bounds bounds() {
		return getLayoutBounds();
	}
	
	public void update() { 
		updateCaption();
		updateValues();
	}
	
	private void updateValues() {		
		values.getChildren().clear();
		
		values.getChildren().add(label);
		
		if(variable.getType().equals(DataType.Functional) && variable.isNumeric()) {
			// TODO: ....

		} else {
			DiscreteTransformation transform = (DiscreteTransformation)variable.transformationProperty().get();
			TreeMap<MappingProperty, StringProperty> map = transform.getMap();
			
			for(MappingProperty key: map.keySet()) {
				
				StringProperty prop = map.get(key);
				Text textValue = new Text(prop.get());
				prop.addListener(new ChangeListener<String>() {

					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
						textValue.setText(newValue);
					}
				});
			
				textValue.setFont(textFont);
				textValue.setFill(textColor);
				
				Group icon = new LegendIcon(iconSize, variable, key);
				textValue.translateXProperty().set(icon.getTranslateX());
				
				HBox box = new HBox(hgap);
				box.alignmentProperty().set(Pos.CENTER_LEFT);
				box.translateXProperty().set(hgap);
				
				box.getChildren().add(icon);
				box.getChildren().add(textValue);
				values.getChildren().add(box);
			}
		}
	}
	
	
	private void updateValues2() {		
		values.getChildren().clear();
		
		values.getChildren().add(label);
		
		if(variable.getType().equals(DataType.Functional) && variable.isNumeric()) {
			// TODO: ....

		} else {
			SortedSet<ComparableDataValue> range = variable.getDiscreteValues();
			for(ComparableDataValue val: range) {
				
				Text textValue = new Text(variable.transformationProperty().get().toData(val.getValue()));
				
				val.getProperty().addListener(new InvalidationListener() {
					@Override
					public void invalidated(Observable observable) {
						textValue.setText(variable.transformationProperty().get().toData(val.getValue()));						
					}
				});
				
				
				textValue.setFont(textFont);
				textValue.setFill(textColor);
				
				Group icon = new LegendIcon(iconSize, variable, val);
				
				HBox box = new HBox(hgap);
				box.alignmentProperty().set(Pos.CENTER_LEFT);
				box.translateXProperty().set(hgap);
				
				box.getChildren().add(icon);
				box.getChildren().add(textValue);
				values.getChildren().add(box);
			}
		}
	}
	
	private void updateCaption() {
		label.textProperty().unbind();
		label.textProperty().bind(variable.getName(0));
		
		label.xProperty().set(0);
		label.yProperty().set(vgap + label.getLayoutBounds().getHeight());		
	}
}
