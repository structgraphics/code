package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.ui.spreadsheet.DataTransformation;
import fr.inria.structgraphics.ui.spreadsheet.DiscreteTransformation;
import fr.inria.structgraphics.ui.spreadsheet.MappingProperty;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable.DataType;
import fr.inria.structgraphics.ui.utils.Ticker;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class AxisX extends Axis {
		
	private double offset = 0; 
	
	public AxisX(VisCollection group) {
		super(group);
	}
	
	@Override
	protected void updateLine() {
		axisLine.startYProperty().set(group.getInteractor().getXAxis().startYProperty().get());
		axisLine.endYProperty().set(group.getInteractor().getXAxis().endYProperty().get());
		axisLine.startXProperty().set(min);
		axisLine.endXProperty().set(max);
	}
	
	@Override
	protected void updateTicksAndValues() {
		values.getChildren().clear();
		tics.getChildren().clear();
		offset = 0;
		
		List<Double> positions, values = null;
		double endY = axisLine.getStartY() + ticLength;
		
		if(variable.getType().equals(DataType.Functional)) {
			minValue = 0;
			maxValue = 0;
			
			if(variable.isX()) { // This is to show the axis only along the active y values
				FlexibleListProperty propValues = getValues();
				for(Property prop: propValues) {
					if((double)prop.getValue() < minValue) minValue = (double)prop.getValue();
					if((double)prop.getValue() > maxValue) maxValue = (double)prop.getValue();
				}
			} else {
				minValue = group.getInteractor().left.get();
				maxValue = group.getInteractor().right.get();
			}
			
			
			double min = transform(minValue);
			double max = transform(maxValue);
			
			//double min = transform(group.getInteractor().left.get());
			//double max = transform(group.getInteractor().right.get());
				
			Ticker ticker = new Ticker(min, max, 10, transform(new Double(20)) - transform(new Double(0)));
			positions = ticker.getTicks();
			
			for(int i = 0; i < positions.size(); ++i) {
				double val = positions.get(i); 
				double x = invTransform(val);
				
				if(i == 0) this.min = x;
				else if(i == positions.size() - 1) this.max = x;
				
				Line line = new Line(x, axisLine.getStartY(), x, endY);				
				line.strokeProperty().bind(((VisFrame)group.getRoot()).stroke);// setStroke(stroke);
				
				//line.setStroke(stroke);
				line.setStrokeWidth(tickStrokeWidth);
				tics.getChildren().add(line);
				
				if(values != null) updateValue(x, line.getEndY(), values.get(i));
				else updateValue(x, line.getEndY(), x);
			}
			
		} else {
			minValue = group.getInteractor().left.get();
			maxValue = group.getInteractor().right.get();
			
			this.min = group.getInteractor().getXAxis().startXProperty().get();
			this.max = group.getInteractor().getXAxis().endXProperty().get();
			
			positions = new ArrayList<>();
			values = new ArrayList<>();
			
			DiscreteTransformation transformation = (DiscreteTransformation)variable.transformationProperty().get();
			TreeMap<MappingProperty, StringProperty> map = transformation.getMap();
			for(MappingProperty prop:map.keySet()) {
				double dx = 0;
				values.add((double)prop.get());
				positions.add((double)prop.get() + dx);
			}
			
			/*
			List<Mark> marks = group.getComponents();
			for(Mark mark : marks) {
				double dx = 0;
				values.add(mark.getCoords().x.get());
				positions.add(mark.getCoords().x.get() + dx);
			}*/
			
			for(int i = 0; i < positions.size(); ++i) {
				double x = positions.get(i); 
				Line line = new Line(x, axisLine.getStartY(), x, endY);
				line.strokeProperty().bind(((VisFrame)group.getRoot()).stroke);// setStroke(stroke);
//				line.setStroke(stroke);
				line.setStrokeWidth(tickStrokeWidth);
				tics.getChildren().add(line);
				
				if(values != null) updateValue(x, line.getEndY(), values.get(i));
				else updateValue(x, line.getEndY(), x);
			}
		}
			

	}
	
	protected void updateValue(double posx, double posy, double xvalue) {		
		double dy = 0;
		if(variable.getType().equals(DataType.Symbolic)) {
			dy = group.coords.getY() - group.bottom();
		}
		
		String value = variable.transformationProperty().get().toData(xvalue);
		
		Text label = new Text(value);
		label.setFont(textFont);
		//label.setFill(textColor);
		label.fillProperty().bind(((VisFrame)group.getRoot()).stroke);// setStroke(stroke);
		
		label.xProperty().set(posx - label.getLayoutBounds().getWidth()/2);
		
		label.yProperty().set(posy + label.getLayoutBounds().getHeight() + labelGap + dy);
		offset = Math.max(offset, label.yProperty().get());
		
		values.getChildren().add(label);
	}
	
	@Override
	protected void updateLabel() {		
		label.textProperty().unbind();
		label.textProperty().bind(variable.getName(0));
		
		//double min = group.getInteractor().left.get();
		//double max = group.getInteractor().right.get();
		double centerx = (maxValue + minValue)/2;

		label.xProperty().set(centerx - label.getLayoutBounds().getWidth()/2);
		label.yProperty().set(offset + label.getLayoutBounds().getHeight() + labelGap);		
	}


}

