package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import java.util.Hashtable;
import java.util.Map;

import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.scene.layout.VBox;

public class LegendCollection extends VBox {
	
	private Map<DataVariable, Legend> map = new Hashtable<>();
	private VisBody vgroup;
	
	private double xGap = 20;
	
	private final static double VGap = 8;
	
	public LegendCollection(VisBody group) {
		super(VGap);
		this.vgroup = group;
				
		translateYProperty().bind(vgroup.getInteractor().top);
		vgroup.getInteractor().right.addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				translateXProperty().set(xGap + vgroup.getInteractor().right.get());
			}
		});
	}
	
	public void addLegend(DataVariable variable) {
		if(map.containsKey(variable)) {
			Legend legend = map.get(variable);
			map.remove(variable);
			getChildren().remove(legend);
		}
		else {
			Legend legend = new Legend(vgroup, variable);
			map.put(variable, legend);
			getChildren().add(legend);
		}
	}
	
	public void update() {
		for(Legend legend: map.values())
			legend.update();
	}
}
