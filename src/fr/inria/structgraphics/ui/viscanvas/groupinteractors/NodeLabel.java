package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.ShapeMark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.Property;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class NodeLabel extends Group implements InvalidationListener {

	protected Font labelFont = Font.font("Verdana", 10);
	protected Font collectionFont = Font.font("Verdana", FontWeight.BOLD, 11);
	
	protected Color textColor = Color.GREY;
		
	private Text text;
	private Mark mark;
	protected DataVariable variable;
	protected Property property;
	
	public NodeLabel(Mark mark, DataVariable variable, Property property) {
		this.mark = mark;
		this.variable = variable;
		this.property = property;
		
		text = new Text();
		text.setFont((mark instanceof VisCollection) ? collectionFont : labelFont);
			
		if(variable.isStroke() || variable.isFill()) text.setFill((Color)property.getValue());
		else {
			text.fillProperty().bind(((VisFrame)mark.getRoot()).stroke);
		}
		text.setTextAlignment(TextAlignment.CENTER);
		
		getChildren().add(text);
		
		showVariable(variable, property);
	}
	
	public void showVariable(DataVariable variable, Property property) {		
		variable.transformationProperty().addListener(this);
		update();
	}
	
	public void update() {	
		// TODO: this is a quick hack to show positive only values for the width variable
		// TODO I need an explict interaction from behalf of the user
		String value; 
		if(variable.isWidth()) {
			value = variable.transformationProperty().get().toData(Math.abs((double)property.getValue()));
		} else {
			value = variable.isID() ? variable.transformationProperty().get().toData(mark.id.get()) :
				variable.transformationProperty().get().toData(property.getValue()) ;
		}
		
		text.setText(value);
		
		text.xProperty().set(-text.getLayoutBounds().getWidth()/2);
		text.yProperty().set(text.getLayoutBounds().getHeight()/2);			
	
		this.translateXProperty().set(0);
		this.translateYProperty().set(0);
		
		if(variable.isStroke() && mark instanceof VisCollection) {
			text.setTextAlignment(TextAlignment.LEFT);
			
			Mark child = ((VisCollection)mark).getComponents().get(((VisCollection)mark).getComponents().size() - 1);
			double dx = child.left() - mark.coords.getX() + text.getLayoutBounds().getWidth()/2 + 25;
			this.translateXProperty().set(dx);
			
			double dy =  (-child.coords.getY() + mark.coords.getY()) - text.getLayoutBounds().getHeight()/4;
			this.translateYProperty().set(dy);
		}
		else if(mark instanceof VisBody /*&& variable.isID()*/) {
			double dx = (mark.left() + mark.right())/2 - mark.coords.getX();
			this.translateXProperty().set(dx);
			
			double dy =  (-mark.top() + mark.coords.getY()) - text.getLayoutBounds().getHeight() - 8;
			this.translateYProperty().set(dy);
		}
		else if(variable.isHeight()) {
			VisBody container = ((ShapeMark)mark).getRootVirtualGroup();
			if(mark instanceof ShapeMark && (container instanceof LineConnectedCollection) 
					&& ((LineConnectedCollection)container).hasConnections()) {
				if(((ShapeMark)mark).hasOutwardConnections()) {
					text.setTextAlignment(TextAlignment.RIGHT);
					this.translateXProperty().set(-mark.width()/2 - text.getLayoutBounds().getWidth()/2 - 3);
				} else {					
					text.setTextAlignment(TextAlignment.LEFT);
					this.translateXProperty().set(mark.width()/2 + text.getLayoutBounds().getWidth()/2 + 3);
				}
			}
			else {
				if(mark.height.get() > 0) this.translateYProperty().set(-mark.height()/2 - text.getLayoutBounds().getHeight());
				else this.translateYProperty().set(-mark.height()/2 + text.getLayoutBounds().getHeight()/2);
			}
		} else if(variable.isX() || variable.isY()) {
			this.translateYProperty().set(mark.height()/2 + text.getLayoutBounds().getHeight()/2);
		} else if(variable.isWidth()) {
			this.translateYProperty().set(-text.getLayoutBounds().getHeight()/4);
			if(mark.width() > 0) this.translateXProperty().set(mark.width()/2 + text.getLayoutBounds().getWidth()/2 + 3);
			else {
				text.setTextAlignment(TextAlignment.RIGHT);
				this.translateXProperty().set(mark.width()/2 - text.getLayoutBounds().getWidth()/2 - 3);
			}
		} else if(mark instanceof ShapeMark && variable.isConnectionNodeID()) {
			if(((ShapeMark)mark).hasOutwardConnections()) {
				text.setTextAlignment(TextAlignment.LEFT);
				this.translateXProperty().set(mark.width()/2 + text.getLayoutBounds().getWidth()/2 + 3);
			} else {
				text.setTextAlignment(TextAlignment.RIGHT);
				this.translateXProperty().set(-mark.width()/2 - text.getLayoutBounds().getWidth()/2 - 3);
			}
		}
	}

	@Override
	public void invalidated(Observable observable) {
		update();
	}
	
}
