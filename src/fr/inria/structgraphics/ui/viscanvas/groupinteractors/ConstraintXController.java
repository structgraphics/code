package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import java.util.List;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.types.DistributionProperty.Constraint;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;

public class ConstraintXController extends ConstraintController {

	private int dy = 10;
	
	protected Line leftline, rightline;
	protected Circle leftcircle, rightcircle;
	
	public ConstraintXController(VisBody group) {
		super(group);
		
		leftline = new Line();
		rightline = new Line();
		leftcircle = new Circle();
		rightcircle = new Circle();
		leftcircle.radiusProperty().set(4);
		rightcircle.radiusProperty().set(4);
		
		leftline.strokeProperty().set(Color.RED);
		rightline.strokeProperty().set(Color.RED);
		leftcircle.fillProperty().set(Color.RED);
		rightcircle.fillProperty().set(Color.RED);

		getChildren().add(leftline);
		getChildren().add(rightline);
		getChildren().add(leftcircle);
		getChildren().add(rightcircle);
		
		group.getConstraintXProperty().addListener(new InvalidationListener()  {
			@Override
			public void invalidated(Observable observable)  {
				type = group.getConstraintXProperty().get();
				
				if(type != Constraint.None && !group.getComponents().isEmpty()) {
					if(group.getConstraintYProperty().get() == Constraint.None) {
						group.reorderChildren(true);
					}
					refresh();
					updateRight(0);
				} else refresh();
			}
		});
		
		setVisible(false);
		
		group.distanceXProperty.addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				if(group.getComponents().isEmpty()) return;
					
				Mark mark = group.getComponents().get(Math.min(ConstraintController.xselectionIndex, group.getComponents().size() - 1));
				activeMark = mark;	
				group.invalidated(mark.getCoords().x);
				group.updateLayout(mark);
			}
		});
	}
	
	
	@Override
	public void translate(Line lineTrace, ConstraintHandle constraintHandler) {
		
		if(constraintHandler == ConstraintHandle.LEFT) {
			double dx = lineTrace.getStartX() - lineTrace.getEndX();
// TODO	: It does not find the correct Handler!!! That's all!!!		
			group.distanceXProperty.set(distance_init + dx);
			//group.invalidated(activeMark.getCoords().x);
		}
		else {
			double dx = lineTrace.getEndX() - lineTrace.getStartX();
			group.distanceXProperty.set(distance_init + dx);
			//group.invalidated(activeMark.getCoords().x);
		}
	}
	
	
	public void refresh() {
		if(type == Constraint.None) return;
		List<Mark> marks = group.getComponents();

		if(marks.size() == 1) {
			double width = marks.get(0).width();
			if(type == Constraint.Spacing) group.distanceXProperty.set(0);
			else group.distanceXProperty.set(width + 10); // TODO: This is arbitrary!!!
		} else if(marks.size() > 1) {
			if(type == Constraint.Spacing) {
				group.distanceXProperty.set(marks.get(1).left() - marks.get(0).right());				
			} else {
				group.distanceXProperty.set(marks.get(1).getCoords().getX() - marks.get(0).getCoords().getX());
			}
		}
	}
	
	@Override
	public void setMark(Mark mark, boolean highlight) {
		if(type == Constraint.None) {
			 setVisible(false);
			 return;
		}
			
		
		if(highlight) {			
			activeMark = mark;
			int index = group.getComponents().indexOf(mark);
			
			double ypos =  dy + group.getControlYPosition();
			
			if(index > 0) {
				if(type == Constraint.Spacing) {
					double left = group.getComponents().get(index).left();
					
					leftcircle.centerXProperty().set(left - group.distanceXProperty.get());
					leftcircle.centerYProperty().set(ypos);
		
					leftline.startXProperty().set(left - group.distanceXProperty.get());
					leftline.startYProperty().set(ypos);
					leftline.endXProperty().set(left);
					leftline.endYProperty().set(ypos);					
				}
				else {
					double x = group.getComponents().get(index).getCoords().getX();
					
					leftcircle.centerXProperty().set(x - group.distanceXProperty.get());
					leftcircle.centerYProperty().set(ypos);
					
					leftline.startXProperty().set(x - group.distanceXProperty.get());
					leftline.startYProperty().set(ypos);
					leftline.endXProperty().set(x);
					leftline.endYProperty().set(ypos);						
				}
				
				leftline.setVisible(true);
				leftcircle.setVisible(true);
			} else {
				leftcircle.centerXProperty().set(-1000);
				leftline.setVisible(false);
				leftcircle.setVisible(false);
			}
		
			if(index <  group.getComponents().size() - 1) {
				if(type == Constraint.Spacing) {
					double right = group.getComponents().get(index).right();
					
					rightline.startXProperty().set(right);
					rightline.startYProperty().set(ypos);
					rightline.endXProperty().set(right + group.distanceXProperty.get());
					rightline.endYProperty().set(ypos);			
					
					rightcircle.centerXProperty().set(right + group.distanceXProperty.get());
					rightcircle.centerYProperty().set(ypos);
				}
				else {
					double x = group.getComponents().get(index).getCoords().getX();
					
					rightline.startXProperty().set(x);
					rightline.startYProperty().set(ypos);
					rightline.endXProperty().set(x + group.distanceXProperty.get());
					rightline.endYProperty().set(ypos);	
					
					rightcircle.centerXProperty().set(x + group.distanceXProperty.get());
					rightcircle.centerYProperty().set(ypos);
				}	
				
				rightline.setVisible(true);
				rightcircle.setVisible(true);
			} else {
				rightcircle.centerXProperty().set(100000);
				rightline.setVisible(false);
				rightcircle.setVisible(false);
			}
			
			setVisible(true);
		}
		else {
			activeMark = null;
			setVisible(false);
			flagSelection();
		}
	}
		
	@Override
	public void updateMarkPositions(Mark mark) {
		if(type == Constraint.None) {
			return;
		}

		int index = group.getComponents().indexOf(mark);

		updateLeft(index);
		updateRight(index);
	}
	
	
	private void updateLeft(int index) {
		Mark mark = group.getComponents().get(index);
		if(mark == null) return;
		
		for(int i = index - 1; i >=0; --i) {
			Mark mark_ = group.getComponents().get(i);
			if(type == Constraint.Spacing) {
				if(mark_ instanceof VisBody) { 
					double dx = mark_.getCoords().getX() - mark_.left();
					mark_.setPositionX(mark.left() - group.distanceXProperty.get() - mark_.width() + dx);
				}
				else {
					switch(mark_.getCoords().getXRef()) {
						case Center: mark_.setPositionX(mark.left() - group.distanceXProperty.get() - mark_.width()/2);
							break;
						case Left: mark_.setPositionX(mark.left() - group.distanceXProperty.get() - mark_.width());
							break;
						case Right: mark_.setPositionX(mark.left() - group.distanceXProperty.get());
							break;
					}
				}
			}
			else {
				if(mark_ != null) mark_.setPositionX(mark.getCoords().getX() - group.distanceXProperty.get());
			}
			mark = mark_;
		}
	}
	
	private void updateRight(int index) { 
		if(group.getComponents().size() - 1 < index) return;
		
		Mark mark = group.getComponents().get(index);
		for(int i = index + 1; i < group.getComponents().size(); ++i) {
			Mark mark_ = group.getComponents().get(i);
			if(type == Constraint.Spacing) {
				if(mark_ instanceof VisBody) {
					double dx = mark_.getCoords().getX() - mark_.left();
					mark_.setPositionX(mark.right() + group.distanceXProperty.get() +  dx);
				} 
				else {
					switch(mark_.getCoords().getXRef()) {
						case Center: mark_.setPositionX(mark.right() + group.distanceXProperty.get() + mark_.width()/2);
							break;
						case Left: mark_.setPositionX(mark.right() + group.distanceXProperty.get());
							break;
						case Right: mark_.setPositionX(mark.right() + group.distanceXProperty.get() + mark_.width());
							break;
					}
				}
			}
			else {				
				mark_.setPositionX(mark.getCoords().getX() + group.distanceXProperty.get());
			}
			mark = mark_;
		}
	}


	@Override
	protected void selectHandle(Circle shape) {
		super.selectHandle(shape);

		if(shape == leftcircle) {			
			distance_init = group.distanceXProperty.doubleValue();
			activeMark.setConstraintControl(getSelf(), ConstraintHandle.LEFT);
			flagSelection();
		}
		else {
			distance_init = group.distanceXProperty.doubleValue();
			activeMark.setConstraintControl(getSelf(), ConstraintHandle.RIGHT);
			flagSelection();
		}
	}
	
	protected void flagSelection() {		
		if(!ConstraintController.handlerhold && activeMark == null) {
			xselectionIndex = 0;
			xselectionPos = Double.MAX_VALUE;
		}
		else if(activeMark != null) {
			xselectionIndex = group.getComponents().indexOf(activeMark);
			xselectionPos = activeMark.getCoords().getX();
		}
	}
	
}
