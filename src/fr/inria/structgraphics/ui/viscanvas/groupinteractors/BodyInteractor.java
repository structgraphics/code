package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import fr.inria.structgraphics.graphics.ComplexShape;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.ui.utils.Cloner;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;

public abstract class BodyInteractor extends ComplexShape {
	
	protected final static Color COL = new Color(Color.DEEPPINK.getRed(), Color.DEEPPINK.getGreen(), Color.DEEPPINK.getBlue(), 0.6);// Color.DEEPPINK;
	protected final static Color COL2 = new Color(Color.CORNFLOWERBLUE.getRed(), Color.CORNFLOWERBLUE.getGreen(), Color.CORNFLOWERBLUE.getBlue(), 0.6);// Color.DEEPPINK;

	protected final static Color COL3 = new Color(Color.GOLDENROD.getRed(), Color.GOLDENROD.getGreen(), Color.GOLDENROD.getBlue(), 0.6);
	
	protected Circle handler;
	public DoubleProperty left = new SimpleDoubleProperty(), right = new SimpleDoubleProperty(100), top = new SimpleDoubleProperty(-100), bottom = new SimpleDoubleProperty();
	
	protected VisBody group;
	
	public BodyInteractor(VisBody group) {		
		this.group = group;
		drawHandler();
		drawBoundary();
	}
	
	protected void drawHandler() {
		handler = new Circle(0, 0, group.isExternal() ? 10 : 6);
		
		getChildren().add(handler);
		handler.setFill(new Color(1, 1, 1, 0.1));
		handler.setStroke(group.isExternal() ? COL : COL2);
		
		addToGhost(Cloner.clone(handler));
	}
	
	protected abstract void drawBoundary();
	
	public boolean contains(Circle trace) {		
		return handler.contains(trace.getCenterX(), trace.getCenterY());
	}
	
	public boolean intersectsCenter(Shape trace) {
		if(trace == null || !handler.isVisible()) return false;
		
		Shape intersection = Shape.intersect((Shape) handler, trace);			
		if(!(intersection instanceof Path) || !((Path)intersection).getElements().isEmpty()) return true;
		
		return false;
	}
	
	@Override
	public boolean intersects(Shape trace) { 
		if(handler.isVisible()) return super.intersects(trace);
		else return false;
	}

	public void redraw() {
		clear();
		drawHandler();
		drawBoundary();
		refresh();
	}

	public void refresh() {		
		ghost.getChildren().clear();
		addToGhost(Cloner.clone(handler));
	}
	

	public double getXStart() {
		return handler.getCenterX();
	}
	
	public double getYStart() {
		return handler.getCenterY();
	}
	
	public abstract Group createCopy(double dx, double dy);

	public abstract double getWidth();
	public abstract double getHeight();
}
