package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import java.util.ArrayList;
import java.util.List;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.ui.spreadsheet.MathTransformation;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable.DataType;
import fr.inria.structgraphics.ui.utils.Ticker;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.Property;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;

public class AxisY extends Axis {
	
	private double offset = 0;
	
	public AxisY(VisCollection group) {
		super(group);
		
		Rotate rotation = new Rotate();
		rotation.pivotXProperty().set(0);
		rotation.pivotYProperty().set(0);
		rotation.setAngle(-90);
		label.getTransforms().add(rotation);
	}

	@Override
	protected void updateLine() {
		axisLine.startYProperty().set(min);
		axisLine.endYProperty().set(max);
		
		axisLine.startXProperty().set(group.getInteractor().getYAxis().startXProperty().get());
		axisLine.endXProperty().set(group.getInteractor().getYAxis().endXProperty().get());
	}
	
	@Override
	protected void updateTicksAndValues() {
		values.getChildren().clear();
		tics.getChildren().clear();
		offset = 0;
		
		List<Double> positions, values = null;
		double endX = axisLine.getStartX() - ticLength;
		
		if(variable.getType().equals(DataType.Functional)) {
			minValue = 0;
			maxValue = 0;
			
			if(variable.isY()) { // This is to show the axis only along the active y values
				FlexibleListProperty propValues = getValues();
				for(Property prop: propValues) {
					if((double)prop.getValue() < minValue) minValue = (double)prop.getValue();
					if((double)prop.getValue() > maxValue) maxValue = (double)prop.getValue();
				}
			} else {
				minValue = -group.getInteractor().bottom.get();
				maxValue = -group.getInteractor().top.get();
			}
			
			double min = transform(minValue);
			double max = transform(maxValue);
		
			//double min = transform(-group.getInteractor().bottom.get());
			//double max = transform(-group.getInteractor().top.get());
				
			Ticker ticker = new Ticker(min, max, 10, transform(new Double(15)) - transform(new Double(0)));
			positions = ticker.getTicks();
			
			for(int i = 0; i < positions.size(); ++i) { 
				double val = positions.get(i); 
				double y = invTransform(val);
				
				if(i == 0) this.min = -y;
				else if(i == positions.size() - 1) this.max = -y;
				
				Line line = new Line(axisLine.getStartX(), -y, endX, -y);
				line.strokeProperty().bind(((VisFrame)group.getRoot()).stroke);
				//line.setStroke(stroke);
				line.setStrokeWidth(tickStrokeWidth);
				tics.getChildren().add(line);
				
				if(values != null) updateValue(-y, line.getEndX(), -values.get(i));
				else updateValue(-y, line.getEndX(), -y);
			}
		} else {
			minValue = -group.getInteractor().bottom.get();
			maxValue = -group.getInteractor().top.get();
			
			this.min = group.getInteractor().getYAxis().startYProperty().get();
			this.max = group.getInteractor().getYAxis().endYProperty().get();
			
			positions = new ArrayList<>();
			values = new ArrayList<>();
			List<Mark> marks = group.getComponents();
			for(Mark mark : marks) {
				double dy = 0;
				values.add(mark.getCoords().y.get());
				positions.add(mark.getCoords().y.get() + dy);
			}
			
			for(int i = 0; i < positions.size(); ++i) {
				double y = positions.get(i); 
				Line line = new Line(axisLine.getStartX(), -y, endX, -y);
				line.strokeProperty().bind(((VisFrame)group.getRoot()).stroke);
				//line.setStroke(stroke);
				line.setStrokeWidth(tickStrokeWidth);
				tics.getChildren().add(line);
				
				if(values != null) updateValue(-y, line.getEndX(), -values.get(i));
				else updateValue(-y, line.getEndX(), -y);
			}
		}
	}


	/*
	protected double getInverseTransformed(double val) {
		return new Double(variable.transformationProperty().get().toData(val));
	}*/

	protected void updateValue(double posy, double posx, double yvalue) {
			String value = variable.transformationProperty().get().toData(yvalue == 0? 0 : -yvalue);
			
			Text label = new Text(value);
			label.setFont(textFont);
//			label.setFill(textColor);
			label.fillProperty().bind(((VisFrame)group.getRoot()).stroke);
			
			label.yProperty().set(posy + label.getLayoutBounds().getHeight()/4);
			label.xProperty().set(posx - label.getLayoutBounds().getWidth() - labelGap);
			offset = Math.min(offset, label.xProperty().get());
			
			values.getChildren().add(label);
	}
	
	
	InvalidationListener labelListener = new InvalidationListener() {
		@Override
		public void invalidated(Observable observable) {
			
		}
	};
	
	
	@Override
	protected void updateLabel() {
		label.textProperty().bind(variable.getName(0));	
		
		double centery = -(maxValue + minValue)/2;
		
		label.xProperty().set(-centery - label.getLayoutBounds().getWidth()/2);
		label.yProperty().set(offset - label.getLayoutBounds().getHeight() - labelGap);	
	}

}
