package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import fr.inria.structgraphics.types.ShapeProperty;
import fr.inria.structgraphics.ui.spreadsheet.ComparableDataValue;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable;
import fr.inria.structgraphics.ui.spreadsheet.MappingProperty;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public class LegendIcon extends Group {

	private double size;

	public LegendIcon(double size, DataVariable variable, MappingProperty prop) {
		this.size = size;
		
		if(variable.isShape()) createShape((ShapeProperty.Type)prop.getValue());
		else if(variable.isThickness()) createThickness((double)prop.getValue());
		else if(variable.isFill()) createFill((Color)prop.getValue());
		else if(variable.isStroke()) createStroke((Color)prop.getValue());
		else createSize((double)prop.getValue());
	}
	
	
	public LegendIcon(double size, DataVariable variable, ComparableDataValue value) {
		this.size = size;
		
		if(variable.isShape()) createShape((ShapeProperty.Type)value.getValue());
		else if(variable.isThickness()) createThickness((double)value.getValue());
		else if(variable.isFill()) createFill((Color)value.getValue());
		else if(variable.isStroke()) createStroke((Color)value.getValue());
		else createSize((double)value.getValue());
	}
	

	private void createStroke(Color value) {
		Line line = new Line(0, 0, size, size);
		line.setStrokeWidth(1);
		line.setStroke(value);
		
		getChildren().add(line);
	}

	
	private void createFill(Color value) {
		Rectangle rect = new Rectangle(size, size);
		rect.setStrokeWidth(.5);
		rect.setStroke(Color.GRAY);
		rect.setFill(value);
		
		getChildren().add(rect);
	}

	private void createThickness(double value) {
		Line line = new Line(0, 0, size, size);
		line.setStrokeWidth(value);
		line.setStroke(Color.BLACK);
		
		getChildren().add(line);
	}

	private void createSize(double value) {
		Circle circle = new Circle(0, 0, value/2);
		this.translateXProperty().set(-value/2);
		circle.setFill(null);
		circle.setStroke(Color.GRAY);
		
		getChildren().add(circle);
	}

	
	private void createShape(ShapeProperty.Type shapeType) {
		Shape shape = null;
		
		switch(shapeType) {
			case Ellipse:
				shape = new Circle(size/2, size/2, size/2);
				break;
				
			case Triangle:
				Path path = new Path();
				path.getElements().add(new MoveTo(size/2, 0));
				path.getElements().add(new LineTo(size, size));
				path.getElements().add(new LineTo(0, size));
				path.getElements().add(new LineTo(size/2, 0));
				shape = path;
				break;
				
			default:
				shape = new Rectangle(size, size);
				shape.setStrokeWidth(.6);
				shape.setStroke(Color.GRAY);
				shape.setFill(null);
				break;
		}
		
		shape.setStrokeWidth(.6);
		shape.setStroke(Color.GRAY);
		shape.setFill(null);
		
		getChildren().add(shape);
	}
}
