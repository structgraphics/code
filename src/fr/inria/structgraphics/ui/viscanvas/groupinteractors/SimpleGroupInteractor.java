package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import fr.inria.structgraphics.graphics.VisGroup;
import fr.inria.structgraphics.ui.utils.Cloner;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class SimpleGroupInteractor extends BodyInteractor {

	private Rectangle boundary;
			
	public SimpleGroupInteractor(VisGroup group) {
		super(group);
	}
	
	private void updateBoundary() {
		boundary.xProperty().set(left.get());
		boundary.yProperty().set(top.get());
		boundary.widthProperty().set(right.get() - left.get());
		boundary.heightProperty().set(bottom.get() - top.get());
	}
	
	@Override
	protected void drawHandler() {			
		if(group.getContainer() instanceof VisGroup) {
			handler = new Circle(0, 0, 0);	
			handler.setFill(null);
			handler.setStroke(null);
		}
		else {
			handler = new Circle(0, 0, 6);	
			handler.setFill(new Color(1, 1, 1, 0.1));
			handler.setStroke(Paint.valueOf("#303F9F"));
		}
		
		getChildren().add(handler);
		addToGhost(Cloner.clone(handler));
	}
	
	
	@Override
	protected void drawBoundary() { 
		boundary = new Rectangle(0,0,0,0);
		updateBoundary();
		
		left.addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				updateBoundary();
			}
		});
		right.addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				updateBoundary();
			}
		});
		top.addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				updateBoundary();
			}
		});
		bottom.addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				updateBoundary();
			}
		});
		
		getChildren().add(boundary);
		
		boundary.strokeWidthProperty().set(0.3);
		boundary.strokeProperty().set(Paint.valueOf("#303F9F"));		
		boundary.setFill(null);
		boundary.getStrokeDashArray().addAll(2d);
				
		addToGhost(Cloner.clone(boundary));
	}
		
	@Override
	public void refresh() {		
		super.refresh();
		addToGhost(Cloner.clone(boundary));
	}
	
	@Override
	public Group createCopy(double dx, double dy) { 
		Group group = new Group();
		Circle handler_ = new Circle(dx, dy, 6);
		
		Rectangle boundary_ = new Rectangle(boundary.getX() + dx, boundary.getY() + dy, boundary.getWidth(), boundary.getHeight());
		
		group.getChildren().add(handler_);
		group.getChildren().add(boundary_);
		
		return group;
	}

	@Override
	public double getWidth() { // TODO
		return Math.abs(boundary.widthProperty().get());
	}
	
	@Override
	public double getHeight() {
		return Math.abs(boundary.heightProperty().get());
	}

}
