package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable;
import fr.inria.structgraphics.ui.spreadsheet.MathTransformation;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public abstract class Axis extends Group implements ChangeListener<String>/*implements InvalidationListener*/ {

	protected Font textFont = new Font(10);
	protected Font labelFont = Font.font("Verdana", FontWeight.BOLD, 11);
	//protected Color stroke = Color.GREY;
	//protected Color textColor = Color.GREY;
	protected double axisStrokeWidth = 0.8;
	protected double tickStrokeWidth = 0.6;
	protected double ticLength = 6;
	protected double labelGap = 1;
	
	protected VisCollection group;
	protected DataVariable variable;
	
	protected Line axisLine;
	protected Group tics;
	protected Group values;
	protected Text label;
	
	protected double min, max;
	protected double minValue, maxValue;
	
	public Axis(VisCollection group) {
		this.group = group;
		setVisible(false);
				
		axisLine = new Line();
		tics = new Group();
		values = new Group();
		label = new Text();
		label.setFont(labelFont);
		
		label.fillProperty().bind(((VisFrame)group.getRoot()).stroke);
		axisLine.strokeProperty().bind(((VisFrame)group.getRoot()).stroke);// setStroke(stroke);
		axisLine.setStrokeWidth(axisStrokeWidth);
		
		getChildren().add(axisLine);
		getChildren().add(tics);
		getChildren().add(values);
		getChildren().add(label);
	}
	
	public void createCopy(Axis axis) {		
		axis.variable = variable;
		axis.setVisible(isVisible());
		axis.update();		
	}
	
	public void showVariable(DataVariable variable) {
		if(variable != this.variable && this.variable != null) {
			this.variable.scaleShownProperty.set(false);
			this.variable.transformationProperty().get().getExpression().removeListener(this);
		}
		this.variable = variable;
		
		if(!variable.scaleShownProperty.get()) {
			setVisible(true);
			update();
		}
		else setVisible(false);
		
		variable.transformationProperty().get().getExpression().addListener(this);
	}
	
	public void showVariableOnParent(DataVariable variable) {
		if(variable != this.variable && this.variable != null) {
			this.variable.scaleShownPropertyOuter.set(false);
			this.variable.transformationProperty().get().getExpression().removeListener(this);
		}
		this.variable = variable;
		
		if(!variable.scaleShownPropertyOuter.get()) {
			setVisible(true);
			update();
		}
		else setVisible(false);
		
		variable.transformationProperty().get().getExpression().addListener(this);
	}
	
	@Override
	public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		update();
	}
	
	protected double transform(Object val) {
		return ((MathTransformation)variable.transformationProperty().get()).transform(val);
	}
	
	protected double invTransform(double val) {
		return ((MathTransformation)variable.transformationProperty().get()).inverseTransform(val);
	}
/*
	@Override
	public void invalidated(Observable observable) {
		update();
	}
*/	
	public void update() { 
		if(!isVisible()) return; 
		updateTicksAndValues();
		updateLine();
		updateLabel();
	}
	
		
	protected FlexibleListProperty getValues() {
		return group.getChildPropertyStructure().getProperty(new PropertyName(variable.getPropertyName())).flattenFlex();
	}
	
	protected abstract void updateLine();
	protected abstract void updateTicksAndValues();
	protected abstract void updateLabel();
}
