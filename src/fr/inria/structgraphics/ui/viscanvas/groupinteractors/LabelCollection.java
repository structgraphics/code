package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import java.util.HashMap;
import java.util.Map;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable;
import javafx.beans.property.Property;
import javafx.scene.Group;

public class LabelCollection extends Group {

	private Mark mark;
	private Map<DataVariable, NodeLabel> labels = new HashMap<DataVariable, NodeLabel>();
	
	public LabelCollection(Mark mark) {
		this.mark = mark;
	}
	
	public void showVariable(DataVariable variable, Property property) {
		NodeLabel node = labels.get(variable);
		if(node != null && !variable.nodeShownProperty.get()) {
			getChildren().remove(node);
			labels.remove(variable);
		}
		else if(variable.nodeShownProperty.get()) {
			NodeLabel label = new NodeLabel(mark, variable, property);
			getChildren().add(label);
			labels.put(variable, label);
		}		
	}
	
	public void update() {
		for(NodeLabel label: labels.values())
			label.update();
	}

	public boolean isLabelShown(DataVariable variable) {
		return labels.get(variable) != null;
	}
}
