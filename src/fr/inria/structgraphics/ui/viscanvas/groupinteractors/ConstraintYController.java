package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import java.util.List;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.types.DistributionProperty.Constraint;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.ConstraintController.ConstraintHandle;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class ConstraintYController extends ConstraintController {

	private int dx = -10;
	
	protected Line bottomline, topline;
	protected Circle bottomcircle, topcircle;
		
	public ConstraintYController(VisBody group) {
		super(group);
		
		bottomline = new Line();
		topline = new Line();
		bottomcircle = new Circle();
		topcircle = new Circle();
		bottomcircle.radiusProperty().set(4);
		topcircle.radiusProperty().set(4);
		
		bottomline.strokeProperty().set(Color.RED);
		topline.strokeProperty().set(Color.RED);
		bottomcircle.fillProperty().set(Color.RED);
		topcircle.fillProperty().set(Color.RED);

		getChildren().add(bottomline);
		getChildren().add(topline);
		getChildren().add(bottomcircle);
		getChildren().add(topcircle);
		
		group.getConstraintYProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				type = group.getConstraintYProperty().get();

				if(type != Constraint.None && !group.getComponents().isEmpty()) {
					if(group.getConstraintXProperty().get() == Constraint.None) {
						group.reorderChildren(false);
					}
					refresh();
					updateTop(0);
				} else refresh();
			}
		});
		
		setVisible(false);
		
		group.distanceYProperty.addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) { 
				if(group.getComponents().isEmpty()) return;
				
				Mark mark = group.getComponents().get(Math.min(ConstraintController.yselectionIndex, group.getComponents().size() - 1));
				activeMark = mark;		
				group.invalidated(mark.getCoords().y);
				group.updateLayout(mark);
			}
		});
	}
		
	
	@Override
	public void translate(Line lineTrace, ConstraintHandle constraintHandler) {
		
		if(constraintHandler == ConstraintHandle.UP) {
			double dy = lineTrace.getStartY() - lineTrace.getEndY();
			
			group.distanceYProperty.set(distance_init + dy);
			//group.invalidated(activeMark.getCoords().y);
		}
		else {
			double dy = lineTrace.getEndY() - lineTrace.getStartY();
			
			group.distanceYProperty.set(distance_init + dy);
			//group.invalidated(activeMark.getCoords().y);
		}
	}
	
	
	@Override
	public void refresh() {
		if(type == Constraint.None) return;
		List<Mark> marks = group.getComponents();
		
		if(marks.size() == 1) {
			//double height = marks.get(0).height();
			//if(type == Constraint.Spacing) group.distanceYProperty.set(0);
			//else group.distanceYProperty.set(height + 10); // TODO: This is arbitrary!!!
		} else if(marks.size() > 1) {
			if(type == Constraint.Spacing) {
				group.distanceYProperty.set(marks.get(1).bottom() - marks.get(0).top());
			} else {
				group.distanceYProperty.set(marks.get(1).getCoords().getY() - marks.get(0).getCoords().getY());
			}
		}
		
	}
	
	@Override
	public void setMark(Mark mark, boolean highlight) {	
		if(type == Constraint.None) {
			 setVisible(false);
			 return;
		}
		
		if(highlight) {
			activeMark = mark;
			int index = group.getComponents().indexOf(mark);
						
			//ConstraintController.selectionIndex = index;
			//System.err.println(activeMark + " ^^ " + selectionIndex);		
			
			double xpos =  dx + group.getControlXPosition();
			
			if(index > 0) {
				if(type == Constraint.Spacing) {
					double bottom = group.getComponents().get(index).bottom();
					
					bottomcircle.centerYProperty().set(-(bottom - group.distanceYProperty.get()));
					bottomcircle.centerXProperty().set(xpos);
		
					bottomline.startYProperty().set(-(bottom - group.distanceYProperty.get()));
					bottomline.startXProperty().set(xpos);
					bottomline.endYProperty().set(-bottom);
					bottomline.endXProperty().set(xpos);					
				}
				else {
					double y = group.getComponents().get(index).getCoords().getY();
					
					bottomcircle.centerYProperty().set(-(y - group.distanceYProperty.get()));
					bottomcircle.centerXProperty().set(xpos);
					
					bottomline.startYProperty().set(-(y - group.distanceYProperty.get()));
					bottomline.startXProperty().set(xpos);
					bottomline.endYProperty().set(-y);
					bottomline.endXProperty().set(xpos);						
				}
				
				bottomline.setVisible(true);
				bottomcircle.setVisible(true);
			} else {
				bottomcircle.centerYProperty().set(-20000);
				bottomline.setVisible(false);
				bottomcircle.setVisible(false);
			}
		
			if(index <  group.getComponents().size() - 1) {
				if(type == Constraint.Spacing) {
					double top = group.getComponents().get(index).top();
					
					topline.startYProperty().set(-top);
					topline.startXProperty().set(xpos);
					topline.endYProperty().set(-(top + group.distanceYProperty.get()));
					topline.endXProperty().set(xpos);			
					
					topcircle.centerYProperty().set(-(top + group.distanceYProperty.get()));
					topcircle.centerXProperty().set(xpos);
				}
				else {
					double y = group.getComponents().get(index).getCoords().getY();
					
					topline.startYProperty().set(-y);
					topline.startXProperty().set(xpos);
					topline.endYProperty().set(-(y + group.distanceYProperty.get()));
					topline.endXProperty().set(xpos);	
					
					topcircle.centerYProperty().set(-(y + group.distanceYProperty.get()));
					topcircle.centerXProperty().set(xpos);
				}	
				
				topline.setVisible(true);
				topcircle.setVisible(true);
			} else {
				topcircle.centerYProperty().set(20000);
				topline.setVisible(false);
				topcircle.setVisible(false);
			}
			
			setVisible(true);
		}
		else {
			activeMark = null;
			setVisible(false);
			flagSelection();
		}
	}
		
	@Override
	public void updateMarkPositions(Mark mark) {
		if(type == Constraint.None) return;
		int index = group.getComponents().indexOf(mark);
/*
		int index; 
		if(mark.isHighlighted()) {
			highlighted_group = group;
			index = group.getComponents().indexOf(activeMark);
			yselectionIndex = index;
			yselectionPos = activeMark.getCoords().getY();
		} else if(highlighted_group == group) {
			return;
		}
		else index = getClosest(yselectionPos);*/
						
		updateBottom(index);
		updateTop(index);		
	}
	
	/*
	private int getClosest(double y) {
		double epsilon = 10;
		double dist = Double.MAX_VALUE;
		int index = -1;
		
		for(int i = 0; i < group.getComponents().size(); ++i) {
			Mark mark = group.getComponentAt(i);
			double d = Math.abs(mark.getCoords().getY() - y);
			if(d < dist) {
				index = i;
				dist = d;
			}
		}
		
		if(dist < epsilon) return index;
		else return Math.min(yselectionIndex, group.getComponents().size() - 1);
	}*/
	
	private void updateBottom(int index) {
		Mark mark = group.getComponents().get(index);
		for(int i = index - 1; i >=0; --i) {
			Mark mark_ = group.getComponents().get(i);
			if(type == Constraint.Spacing) {
				if(mark_ instanceof VisBody) { 
					double dy = mark_.getCoords().getY() - mark_.bottom();
					mark_.setPositionY(mark.bottom() - group.distanceYProperty.get() - mark_.height() + dy);
				}
				else {
					switch(mark_.getCoords().getYRef()) {
						case Center: mark_.setPositionY(mark.bottom() - group.distanceYProperty.get() - mark_.height()/2);
							break;
						case Bottom: mark_.setPositionY(mark.bottom() - group.distanceYProperty.get() - mark_.height());
							break;
						case Top: mark_.setPositionY(mark.bottom() - group.distanceYProperty.get());
							break;
					}
				}
			}
			else {
				mark_.setPositionY(mark.getCoords().getY() - group.distanceYProperty.get());
			}
			mark = mark_;
		}
	}
	
	private void updateTop(int index) { 
		Mark mark = group.getComponents().get(index);
		for(int i = index + 1; i < group.getComponents().size(); ++i) {
			Mark mark_ = group.getComponents().get(i);
			if(type == Constraint.Spacing) {
				if(mark_ instanceof VisBody) {
					double dy = mark_.getCoords().getY() - mark_.bottom();
					mark_.setPositionY(mark.top() + group.distanceYProperty.get() +  dy);
				} 
				else {
					switch(mark_.getCoords().getYRef()) {
						case Center: mark_.setPositionY(mark.top() + group.distanceYProperty.get() + mark_.height()/2);
							break;
						case Bottom: mark_.setPositionY(mark.top() + group.distanceYProperty.get());
							break;
						case Top: mark_.setPositionY(mark.top() + group.distanceYProperty.get() + mark_.height());
							break;
					}
				}
			}
			else {
				mark_.setPositionY(mark.getCoords().getY() + group.distanceYProperty.get());
			}
			mark = mark_;
		}
	}

	@Override
	protected void selectHandle(Circle shape) {
		super.selectHandle(shape);
		
		if(shape == bottomcircle) {
			distance_init = group.distanceYProperty.get();
			activeMark.setConstraintControl(getSelf(), ConstraintHandle.DOWN);
			flagSelection();
		}
		else {
			distance_init = group.distanceYProperty.get();
			activeMark.setConstraintControl(getSelf(), ConstraintHandle.UP);
			flagSelection();
		}
	}
	
	protected void flagSelection() {		
		if(!ConstraintController.handlerhold && activeMark == null) {
			yselectionIndex = 0;
			yselectionPos = Double.MAX_VALUE;
		}
		else if(activeMark != null) {
			yselectionIndex = group.getComponents().indexOf(activeMark);
			yselectionPos = activeMark.getCoords().getY();
		}
	}
}
