package fr.inria.structgraphics.ui.viscanvas.groupinteractors;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.types.AlignmentProperty;
import fr.inria.structgraphics.types.AlignmentXProperty;
import fr.inria.structgraphics.ui.utils.Cloner;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;

public class CollectionInteractor extends BodyInteractor {

	private Line xAxis, yAxis;
		
	public CollectionInteractor(VisCollection collection) {
		super(collection);
	}
	
	protected boolean isXNeeded() {
		if(group.isExternal()) return true;
		else {
			Container container = group.getContainer();
			if(container instanceof VisCollection && ((VisCollection)container).getAlignYProperty().get() == AlignmentProperty.XSticky.Yes)
				return false;
			
			return true;
		}
	}
	
	protected boolean isYNeeded() {
		if(group.isExternal()) return true;
		else {
			Container container = group.getContainer();
			if(container instanceof VisCollection && ((VisCollection)container).getAlignXProperty().get() == AlignmentProperty.YSticky.Yes)
				return false;
			
			return true;
		}
	}
	
	@Override
	protected void drawBoundary() {
		xAxis = new Line(0, 0, 100, 0);
		xAxis.startXProperty().bind(left);
		xAxis.endXProperty().bind(right);	
		getChildren().add(xAxis);
		xAxis.setStroke(group.isExternal() ? COL : COL2);
		if(!group.isExternal()) xAxis.getStrokeDashArray().addAll(2d);
		else xAxis.getStrokeDashArray().clear();
		
		yAxis = new Line(0, 0, 0, -100);
		yAxis.startYProperty().bind(bottom);
		yAxis.endYProperty().bind(top);
		getChildren().add(yAxis);
		yAxis.setStroke(group.isExternal() ? COL : COL2);
		if(!group.isExternal()) yAxis.getStrokeDashArray().addAll(2d);
		else yAxis.getStrokeDashArray().clear();
		
		addToGhost(Cloner.clone(xAxis));
		addToGhost(Cloner.clone(yAxis));
	}
	
	public Line getXAxis() {
		return xAxis;
	}
	
	public Line getYAxis() {
		return yAxis;
	}
	
	@Override
	public void refresh() {		
		super.refresh();
		addToGhost(Cloner.clone(xAxis));
		addToGhost(Cloner.clone(yAxis));
		
		boolean isX = isXNeeded();
		boolean isY = isYNeeded();
		
		xAxis.setVisible(isX);
		yAxis.setVisible(isY);
		handler.setVisible(isX || isY);
	}
	
	@Override
	public void setHighlight(boolean highlight) { 
		super.setHighlight(highlight && handler.isVisible());
	}
	
	@Override
	public Group createCopy(double dx, double dy) { 
		Group group = new Group();
		Circle handler_ = new Circle(dx, dy, 10);
		Line xAxis_ = new Line(xAxis.getStartX() + dx, xAxis.getStartY() + dy, xAxis.getEndX() + dx, xAxis.getEndY() + dy);
		Line yAxis_ = new Line(yAxis.getStartX() + dx, yAxis.getStartY() + dy, yAxis.getEndX() + dx, yAxis.getEndY() + dy);
			
		group.getChildren().add(handler_);
		group.getChildren().add(xAxis_);
		group.getChildren().add(yAxis_);
		
		return group;
	}

	@Override
	public double getWidth() {
		return Math.abs(xAxis.getEndX() - xAxis.getStartX());
	}
	
	@Override
	public double getHeight() {
		return Math.abs(yAxis.getEndY() - yAxis.getStartY());
	}
}

