package fr.inria.structgraphics;
	
import fr.inria.structgraphics.graphics.MarkFactory;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.ui.DisplayPreferences;
import fr.inria.structgraphics.ui.DragManager;
import fr.inria.structgraphics.ui.inspector.InspectorView;
import fr.inria.structgraphics.ui.inspector.util.TooltipCreator;
import fr.inria.structgraphics.ui.spreadsheet.DataView;
import fr.inria.structgraphics.ui.viscanvas.Canvas;
import fr.inria.structgraphics.ui.viscanvas.CanvasFrame;
import javafx.application.Application;
import javafx.beans.binding.Bindings;

import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.control.ScrollPane;

public class VisSketcher extends Application {

	@Override
	public void start(Stage stage) {
		double width = 800, height = 780;
		
		VisFrame frame = MarkFactory.createVisFrame(DisplayPreferences.CANVAS_WIDTH, DisplayPreferences.CANVAS_HEIGHT);
		frame.initProperties();
		
		try {
			CanvasFrame canvasFrame = new CanvasFrame(stage);	
			Canvas canvas = canvasFrame.getCanvas();
			
			ScrollPane scroller = new ScrollPane();
			scroller.setContent(canvas);
			canvasFrame.setSketchArea(scroller);
			
			canvas.minWidthProperty().bind(Bindings.createDoubleBinding(() -> 
	        	scroller.getViewportBounds().getWidth(), scroller.viewportBoundsProperty()));
			
			canvasFrame.setVisFrame(frame);
			//canvas.setContent(frame.getGroup());

			
			// TODO: Fix scrolling
			//canvasFrame.setCenter(scroller);
			
			Scene scene = new Scene(canvasFrame, width, height, false, SceneAntialiasing.BALANCED);
			scene.getStylesheets().addAll(getClass().getResource("application.css").toExternalForm(), CanvasFrame.STYLESHEETS);
			
			stage.setTitle("StructGraphics: Visualization Sketcher");
			stage.setScene(scene);
			stage.setX(100);
			stage.show();
			
			// This is the inspector window
			InspectorView inspector = new InspectorView();
			DragManager.create(inspector);
			
			inspector.setVisualizationFrame(frame);
			
			scene = new Scene(inspector, 600, height);
			scene.getStylesheets().addAll(InspectorView.STYLESHEETS);
		    Stage inspectStage = new Stage();
		    inspectStage.setTitle("StructGraphics: Inspector");
            inspectStage.setScene(scene);
            inspectStage.setX(stage.getX() + stage.getWidth() + 10);
            inspectStage.setY(stage.getY());
            inspectStage.show();
			            
            inspector.update();
			            
			// The data view
			DataView dataview = new DataView(inspector);
			scene = new Scene(dataview, 600, height);
			scene.getStylesheets().add(DataView.STYLESHEETS);
			
			Stage dataStage = new Stage();
			dataStage.setTitle("StructGraphics: Spreadsheet");
			dataStage.setScene(scene);
			dataStage.setX(inspectStage.getX() + inspectStage.getWidth() + 10);
            dataStage.setY(inspectStage.getY());
            dataStage.show();
                        		    
            stage.setOnCloseRequest( event -> {canvasFrame.close(); inspectStage.close(); dataStage.close();});
            inspectStage.setOnCloseRequest( event -> {canvasFrame.close(); stage.close(); dataStage.close();});
            dataStage.setOnCloseRequest( event -> {canvasFrame.close(); stage.close(); inspectStage.close();});           
            
            canvasFrame.setViews( inspector, dataview);
			canvasFrame.setMenubars(stage, inspectStage);
			
		//	TooltipCreator.hackTooltipStartTiming();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {				
		launch(args);
	}

}
