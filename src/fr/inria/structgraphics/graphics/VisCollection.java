package fr.inria.structgraphics.graphics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable;
import fr.inria.structgraphics.ui.viscanvas.groupings.CollectionPropertyStructure;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.Axis;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.AxisX;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.AxisY;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.BodyInteractor;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.CollectionInteractor;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.LegendCollection;
import javafx.beans.property.Property;

public class VisCollection extends VisBody {

	protected Axis axisX, axisY;
	protected LegendCollection legends;
		
	public VisCollection(Container parent, boolean toend) {
		super(parent, toend);
	}

	protected BodyInteractor createInteractor() { // TODO:...
		return new CollectionInteractor(this);
	}
	
	public void showVariableOnAxis(DataVariable variable, boolean parent) {
		if(variable.isX() || variable.isWidth()) {
			if(parent) axisX.showVariableOnParent(variable);
			else axisX.showVariable(variable);
		}
		else if(variable.isY() || variable.isHeight()) {
			if(parent) axisY.showVariableOnParent(variable);		
			else axisY.showVariable(variable);
		}
	}
	
	
	@Override
	public CollectionInteractor getInteractor() {
		return (CollectionInteractor)interactor;
	}
	
	public void showVariableOnLegend(DataVariable variable) {
		legends.addLegend(variable);
	}
	
	@Override
	public void addExtraComponents() {
		axisX = new AxisX(this);
		group.getChildren().add(axisX);
		axisY = new AxisY(this);
		group.getChildren().add(axisY);
		legends = new LegendCollection(this);
		group.getChildren().add(legends);
	}
	
	@Override
	public void updateExtraComponents() {
		axisX.update();
		axisY.update();
		legends.update();
	}
	
	@Override
	public String getName() {
		//return "Collection";
		return PropertyName.getPrefix(level) + " Collection";
	}
	
	@Override
	public String getType() {
		return "Collection";
	}

	@Override
	protected VisBody getInstance(Container container, boolean toEnd) {
		return new VisCollection(container, toEnd);
	}

	@Override
	public void addSelfProperties(boolean toCollection, Map<PropertyName, FlexibleListProperty> table) {
		Collection<FlexibleListProperty> childProperties;
		if(childPropertyStructure != null) childProperties = ((CollectionPropertyStructure)childPropertyStructure).getProperties().values();
		else childProperties = new ArrayList<>();
		
		if(table.isEmpty()) {
			for(Property property: properties) {
				FlexibleListProperty list = FlexibleListProperty.createList(this, property);
				table.put(new PropertyName(list.getName()), list);
			}
			for(Property property: childProperties) {
				FlexibleListProperty list = FlexibleListProperty.createList(this, property);
				table.put(new PropertyName(list.getName()), list);
			}
		} else {
			for(Property property: properties) {
				FlexibleListProperty list  = table.get(new PropertyName(property.getName()));
				if(list!=null) {
					if(isFirst()) list.add(0, property);
					else list.add(property);
				}
			}
			for(Property property: childProperties) {
				FlexibleListProperty list  = table.get(new PropertyName(property.getName()));
				if(list!=null) {
					if(isFirst()) list.add(0, property);
					else list.add(property);
				}
			}
		}	
	}
	
	@Override
	public boolean removeSelfProperties(Map<PropertyName, FlexibleListProperty> table) { 
		boolean removed = super.removeSelfProperties(table);
		
		Collection<FlexibleListProperty> childProperties = ((CollectionPropertyStructure)childPropertyStructure).getProperties().values();		
		for(Property property: childProperties) {
			FlexibleListProperty list  = table.get(new PropertyName(property.getName()));
			if(list!=null && list.remove(property)) removed = true;
		}
		
		return removed;
	}
	
	@Override
	public CollectionPropertyStructure getChildPropertyStructure(){
		return (CollectionPropertyStructure)super.getChildPropertyStructure();
	}

	public void addID(List<Property> ids, int level) {
		if(level == this.level) ids.add(id);
		else if(container instanceof VisCollection) ((VisCollection)container).addID(ids, level);
	}
}
