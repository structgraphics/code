package fr.inria.structgraphics.graphics;

import java.util.ArrayList;
import java.util.List;

import fr.inria.structgraphics.graphics.splines.CubicSpline;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnection;
import javafx.geometry.Point2D;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

public class FlowCreator {
	private static final double defaultArrowHeadSize = 8.0;

	public static void createCurve(Path path, FlowConnection flowConnection) {
		double dx;
		
		if(flowConnection.startX() < flowConnection.endX()) dx = Math.min(50, (flowConnection.endX() - flowConnection.startX())/3);
		else dx = Math.max(-50, (flowConnection.endX() - flowConnection.startX())/3);
			
		MoveTo moveTo = new MoveTo();
		moveTo.setX(flowConnection.startX());
		moveTo.setY(flowConnection.startY());

		CubicCurveTo cubicTo = new CubicCurveTo();
		cubicTo.setControlX1(flowConnection.startX() + dx);
		cubicTo.setControlY1(flowConnection.startY());
		cubicTo.setControlX2(flowConnection.endX() - dx);
		cubicTo.setControlY2(flowConnection.endY());
		cubicTo.setX(flowConnection.endX());
		cubicTo.setY(flowConnection.endY());

		path.getElements().add(moveTo);
		path.getElements().add(cubicTo);
	}
	
	public static void createFlowConnectionPath(FlowPath path, FlowConnection flowConnection) {
		double dx;
		
		if(flowConnection.startX() < flowConnection.endX()) dx = Math.min(50, (flowConnection.endX() - flowConnection.startX())/3);
		else dx = Math.max(-50, (flowConnection.endX() - flowConnection.startX())/3);
		
		double bottom1 = flowConnection.startYBottom();
		double bottom2 = flowConnection.endYBottom();
		
		double y1 = bottom1 + flowConnection.getOrigin().getOriginFlag();
		double y2 = y1 + flowConnection.weightProperty().get();
				
		double y4 = bottom2 + flowConnection.getDestination().getDestinationFlag();
		double y3 = y4 + flowConnection.weightProperty().get();
				
		double x1 = flowConnection.startX();
		double x2 = flowConnection.endX();
		
		path.setY1(y1, y2);
		path.setY2(y4, y3);
		path.setX(x1, x2);

		MoveTo moveTo = new MoveTo();
		moveTo.setX(x1);
		moveTo.setY(y1);

		LineTo lineTo = new LineTo();
		lineTo.setX(x1);
		lineTo.setY(y2);
		
		CubicCurveTo cubicTo = new CubicCurveTo();
		cubicTo.setControlX1(x1 + dx);
		cubicTo.setControlY1(y2);
		cubicTo.setControlX2(x2 - dx);
		cubicTo.setControlY2(y3);
		cubicTo.setX(x2);
		cubicTo.setY(y3);

		LineTo lineTo2 = new LineTo();
		lineTo2.setX(x2);
		lineTo2.setY(y4);
				
		CubicCurveTo cubicTo2 = new CubicCurveTo();
		cubicTo2.setControlX1(x2 - dx);
		cubicTo2.setControlY1(y4);
		cubicTo2.setControlX2(x1 + dx);
		cubicTo2.setControlY2(y1);
		cubicTo2.setX(x1);
		cubicTo2.setY(y1);
		
		path.getElements().add(moveTo);
		path.getElements().add(lineTo);
		path.getElements().add(cubicTo);
		path.getElements().add(lineTo2);
		path.getElements().add(cubicTo2);
		
		flowConnection.getOrigin().increaseOriginFlag(flowConnection.weightProperty().get());
		flowConnection.getDestination().increaseDestinationFlag(flowConnection.weightProperty().get());
	}
	
	public static void createLine(Path path, FlowConnection flowConnection) {
		MoveTo moveTo = new MoveTo();
		moveTo.setX(flowConnection.startX());
		moveTo.setY(flowConnection.startY());
		path.getElements().add(moveTo);
		
		LineTo lineTo = new LineTo();
		lineTo.setX(flowConnection.endX());
		lineTo.setY(flowConnection.endY());
		path.getElements().add(lineTo);
	}
	
	public static void createArrow(Path path, FlowConnection flowConnection) {
		double startX = flowConnection.startX();
		double startY = flowConnection.startY();
		double endX = flowConnection.endX();
		double endY = flowConnection.endY();
		
	      //Line
		path.getElements().add(new MoveTo(startX, startY));
		path.getElements().add(new LineTo(endX, endY));
        
        //ArrowHead
        double angle = Math.atan2((endY - startY), (endX - startX)) - Math.PI / 2.0;
        double sin = Math.sin(angle);
        double cos = Math.cos(angle);
        //point1
        double x1 = (- 1.0 / 2.0 * cos + Math.sqrt(3) / 2 * sin) * defaultArrowHeadSize + endX;
        double y1 = (- 1.0 / 2.0 * sin - Math.sqrt(3) / 2 * cos) * defaultArrowHeadSize  + endY;
        //point2
        double x2 = (1.0 / 2.0 * cos + Math.sqrt(3) / 2 * sin) * defaultArrowHeadSize  + endX;
        double y2 = (1.0 / 2.0 * sin - Math.sqrt(3) / 2 * cos) * defaultArrowHeadSize  + endY;
        
        path.getElements().add(new LineTo(x1, y1));
        path.getElements().add(new LineTo(x2, y2));
        path.getElements().add(new LineTo(endX, endY));
        
        path.setStrokeWidth(flowConnection.weightProperty().get());
	}


}
