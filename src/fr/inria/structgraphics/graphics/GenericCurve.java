package fr.inria.structgraphics.graphics;

import java.util.ArrayList;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.graphics.splines.FlattenedPath;
import fr.inria.structgraphics.types.DoubleListProperty;
import fr.inria.structgraphics.types.FlexibleListProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Rotate;

public abstract class GenericCurve extends Mark  {
	
	protected double width = 0, height = 0;
	protected double length = 0;
	protected Point2D center; // central point with respect to the first point of the curve
	
	protected FlexibleListProperty original_xs = new DoubleListProperty(this, "x");
	protected FlexibleListProperty original_ys = new DoubleListProperty(this, "y");

	protected ArrayList<Point2D> points;
	protected Shape path;
	protected FlattenedPath flattenedPath;
	
	protected Point2D middle;

	public GenericCurve(Container parent) {
		super(parent);		
	}
	
	public GenericCurve(Container parent, ArrayList<Point2D> points){
		super(parent);
		
		for(Point2D p: points) {
			original_xs.get().add(new SimpleDoubleProperty(p.getX()));
			original_ys.get().add(new SimpleDoubleProperty(p.getY()));
		}
	}
	
	@Override
	public void initProperties() {			
		super.initProperties();
				
		addProperty(angle);
		addProperty(border);
		addProperty(strokePaint);
		
		addPropertiesColumn(original_xs);
		addPropertiesColumn(original_ys);
	}
	
	public void addPoint(double x, double y) {		
		original_xs.get().add(new SimpleDoubleProperty(x));
		original_ys.get().add(new SimpleDoubleProperty(y));
	}
	
	public Point2D getStart() {
		return points.get(0);
	}
	
	public Point2D getEnd() {
		return points.get(points.size() - 1);
	}
	
	public Point2D getMiddle() {
		return middle;
	}
	
	public double width() {
		return width;
	}
	
	public double height() {
		return height;
	}
	
	@Override
	public void updateShape() {	// TODO: This probably needs some refreshing...
		points = new ArrayList<Point2D>();
		///////////////////
		for(int i = 0; i < original_xs.get().size(); ++i) {
			double x =  (double)original_xs.get().get(i).getValue();
			double y =  (double)original_ys.get().get(i).getValue();
			
			double cx = x + coords.getX(), cy = y + coords.getY();				
			points.add(container.getReferencePoint(cx, cy));
		}
				
		updateBasicShape();
		calculateLength();
		middle = getPoint(length/2, 0);
					
		// TODO: check this angle rotation (with respect to the center???)
		if(angle.get() != 0) group.getTransforms().add(new Rotate(angle.get(), center.getX(), center.getY()));
		
//		if(paint != null) shape.setFill(paint.get());
//		else shape.setFill(null);
		
		if(strokePaint != null) shape.setStroke(strokePaint.get());
		else shape.setStroke(Color.DARKGRAY);
		
		shape.setStrokeWidth(border.get());

	}
	
	protected abstract void calculateLength();
	
	protected double transformX(double cx, RefX containerXRef) {
		// X coordinate with respect to the left end (start point) of the curve
		if(containerXRef == RefX.Left) return cx; 
		else if(containerXRef == RefX.Right) return length + cx; 
		else return length/2 + cx;
	}
	
	/*
	 * It transforms to global coordinates
	 */
	@Override
	public Point2D getReferencePoint(double cx, double cy) {		
		Point2D p = getPoint(transformX(cx, refCoords.containerXRef.get()), cy);
//		Point2D center = getCenter();	
		return new Point2D(p.getX() /*- center.getX()*/, p.getY() /*- center.getY()*/);
	}
	
	@Override
	public Point2D getOffsetPoint() {
		return new Point2D(transformX(0, refCoords.containerXRef.get()), 0);
	}
	
	protected Point2D getCenter() {
		return center;
	}
	
	/* Point at l, y with respect to the first point of the curve
	 *  
	 */
	protected abstract Point2D getPoint(double l, double y);
}
