package fr.inria.structgraphics.graphics;

import fr.inria.structgraphics.types.ShapeProperty;
import fr.inria.structgraphics.types.ShapeProperty.Type;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public class GenericShapeMark extends ShapeMark  {

	protected GenericShapeMark(Container parent,  boolean toend, ShapeProperty.Type type, double w, double h){
		super(parent, toend, type, w, h);	
		
	}
	
	@Override
	public void initProperties() {
		super.initProperties();
		
		properties.add(shapeProperty);
		shapeProperty.addListener(new ChangeListener<ShapeProperty.Type>() {
			@Override
			public void changed(ObservableValue<? extends Type> observable, Type oldValue, Type newValue) {
				changeShape(newValue);
			}
		});	
	}
	
	private void changeShape(Type newShape) {
		primitif = createShape();
		shape.replaceBy(primitif);
		update();
	}
	
	@Override
	protected Shape createShape() {
		switch(shapeProperty.get()) {
			case Ellipse:
				return new Ellipse(0, 0, Math.abs(width.get()/2), Math.abs(height.get()/2));
			case Rectangle: 
				return new Rectangle(-Math.abs(width.get())/2, -Math.abs(height.get())/2, Math.abs(width.get()), Math.abs(height.get()));
			case Triangle:
				Path path = new Path();
				createTriangle(path, width.get(), height.get());
				return path;
			case Line:
				return new Line(-width.get()/2, -height.get()/2, width.get()/2, height.get()/2);
				
			default: return null;
		}
	} 

	@Override
	protected void refreshShape() {
		switch(shapeProperty.get()) {
			case Ellipse:
				Ellipse ellipse = (Ellipse)primitif;
				ellipse.radiusXProperty().set(Math.abs(width.get()/2));
				ellipse.radiusYProperty().set(Math.abs(height.get()/2));
				break;
				
			case Rectangle: 
				Rectangle rect = (Rectangle)primitif;
				rect.xProperty().set(-Math.abs(width.get())/2); 
				rect.yProperty().set(-Math.abs(height.get())/2);
				rect.widthProperty().set(Math.abs(width.get()));
				rect.heightProperty().set(Math.abs(height.get()));
				break;

			case Triangle:
				Path path = (Path)primitif;
				path.getElements().clear();
				createTriangle(path, width.get(), height.get());
				break;
			
			case Line:
								
				Line line = (Line)primitif;			
				line.startXProperty().set(-width.get()/2); 
				line.startYProperty().set(-height.get()/2);
				line.endXProperty().set(width.get()/2);
				line.endYProperty().set(height.get()/2);
			
			default:
		}
	}
	
			
	private void createTriangle(Path path, double w, double h) {
		MoveTo moveTo = new MoveTo();
		moveTo.setX(0.0);
		moveTo.setY(-h/2);
		path.getElements().add(moveTo);
		
		LineTo lineTo = new LineTo();
		lineTo.setX(Math.abs(w)/2);
		lineTo.setY(h/2);
		path.getElements().add(lineTo);
		
		lineTo = new LineTo();
		lineTo.setX(-Math.abs(w)/2);
		lineTo.setY(h/2);
		path.getElements().add(lineTo);
		
		lineTo = new LineTo();
		lineTo.setX(Math.abs(0));
		lineTo.setY(-h/2);
		path.getElements().add(lineTo);
	}
	
	@Override
	public String getName() {
		return shapeProperty.get().name();
	}

	@Override
	public String getType() {
		return shapeProperty.get().name();	}
				
	
	@Override
	public Shadow getShadow(double dx, double dy, double delta) {
		dx *= delta;
		dy *= delta;
		
		switch(shapeProperty.get()) {
			case Ellipse:
				return new Shadow(this, dx, dy, new Ellipse(dx, dy, Math.abs(width.get()/2), Math.abs(height.get()/2))); 
			case Line:
				return new Shadow(this, dx, dy, new Line(dx-width.get()/2, dy-height.get()/2, dx + width.get()/2, dy + height.get()/2)); 
			default: 
				return new Shadow(this, dx, dy, new Rectangle(dx-Math.abs(width.get())/2, dy-Math.abs(height.get())/2, 
					Math.abs(width.get()), Math.abs(height.get()))); // TODO: ???
		}		
	}
	
}
