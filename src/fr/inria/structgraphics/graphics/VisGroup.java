package fr.inria.structgraphics.graphics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.Shareable;
import fr.inria.structgraphics.types.XProperty;
import fr.inria.structgraphics.types.YProperty;
import fr.inria.structgraphics.types.AlignmentProperty.XSticky;
import fr.inria.structgraphics.types.AlignmentProperty.YSticky;
import fr.inria.structgraphics.ui.utils.GroupBinding;
import fr.inria.structgraphics.ui.viscanvas.groupings.CollectionPropertyStructure;
import fr.inria.structgraphics.ui.viscanvas.groupings.GroupPropertyStructure;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.BodyInteractor;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.CollectionInteractor;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.GroupInteractor;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.SimpleGroupInteractor;
import javafx.beans.property.Property;
import javafx.scene.shape.Line;

public class VisGroup extends VisBody {

	protected ArrayList<Property> allProperties;
	
	public VisGroup(Container parent, boolean toend) {
		super(parent, toend);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String getName() {
		return PropertyName.getPrefix(level) + " Group";
	}
	
	protected BodyInteractor createInteractor() { // TODO:...
		return new GroupInteractor(this);
	}

	@Override
	protected VisBody getInstance(Container container, boolean toEnd) {
		return new VisGroup(container, toEnd);
	}
	
	@Override
	public BodyInteractor getInteractor() {
		return interactor;
	}
	
	@Override
	public void addLeafIDs(List<Property> ids){
		ids.add(id);
	}
	
	@Override
	public void addIDs(List<Property> ids, int level) {
		if(this.level == level) ids.add(id);
		else if(container instanceof VisCollection) ((VisCollection)container).addID(ids, level);
	}
	
	@Override
	public int leafs() {
		return 1;
	}
	
	@Override
	public int getBottomLevel() {
		return level;
	}
	
	@Override
	public void addSelfProperties(boolean toCollection, Map<PropertyName, FlexibleListProperty> table) {
		
		if(toCollection) {
			for(Property property: allProperties) {				
				FlexibleListProperty list  = table.get(new PropertyName(property.getName()));
				if(list!=null) {
					if(isFirst()) list.add(0, property);
					else list.add(property);
				}
				else {
					list = FlexibleListProperty.createList(this, property, true);
					table.put(new PropertyName(property.getName()), list);
				}
			}			
		} else {	
			Collection<FlexibleListProperty> childProperties;
			if(childPropertyStructure != null) childProperties = ((GroupPropertyStructure)childPropertyStructure).getProperties().values();
			else childProperties = new ArrayList<>();
			
			for(Property property: properties) {
				FlexibleListProperty list  = table.get(new PropertyName(PropertyName.getCleanName(property.getName())));
				
				if(list!=null) {
					if(isFirst()) list.add(0, property);
					else list.add(property);
				}
				else {
					list = FlexibleListProperty.createList(this, property);
					table.put(new PropertyName(list.getName()), list);
				}
			}
			for(FlexibleListProperty property: childProperties) {				
				FlexibleListProperty list  = table.get(new PropertyName(property.getName()));

				if(list!=null) {
					if(isFirst()) list.addEach(0, property.flatten());
					else list.addEach(property.flatten());
				}
				else {
					list = FlexibleListProperty.createList(this, property.flatten());
					table.put(new PropertyName(property.getName()), list);					
				}
			}
		}
	}
	
	
	@Override
	public boolean removeSelfProperties(Map<PropertyName, FlexibleListProperty> table) { // TODO: Check what need to be done!!!!
		boolean removed = super.removeSelfProperties(table);
		
		Collection<FlexibleListProperty> childProperties = ((GroupPropertyStructure)childPropertyStructure).getProperties().values();		
		for(Property property: childProperties) {
			FlexibleListProperty list  = table.get(new PropertyName(property.getName()));
			if(list!=null && list.remove(property)) removed = true;
		}
		
		return removed;
	}
	
	@Override
	public void initVisibility() {
		super.initVisibility();
		
		for(Property property:properties) { // TODO: CHECK for nested groups !!!
			if(property instanceof Shareable) {
				PropertyName name = new PropertyName(property.getName());
				((Shareable)property).getPublicProperty().set(name.isX() || name.isY());
			}
		}
	}
	
	@Override
	public void initProperties() {		
		super.initProperties();
		
		ArrayList<Property> publicProperties = new ArrayList<>();
		ArrayList<Property> allProperties = new ArrayList<>();
    	splitProperties(publicProperties, allProperties);
	}
	
	@Override
	public SimpleMark createCopy(Container container, double dx, double dy) { // TODO: Correct?
		VisGroup copy = (VisGroup)super.createCopy(container, dx, dy);
    	
		return copy;
	}
	
	@Override
	public void pin() {
		if(container instanceof VisGroup) {
			super.pin();
			for(Mark child: getComponents()) { 
				child.pin();				
			}
		}
		else super.pin();
	}
	
	public void setPositionX(double x) {
		/*if(container instanceof VisGroup) {
			translateAllBy(x, 0);
		} else*/ coords.x.set(x);		
	}

	public void setPositionY(double y) {
		/*if(container instanceof VisGroup) {
			translateAllBy(0, y);
		} else*/ coords.y.set(y);		
	}
		
		
	// Makes sure that all are translated by dx, dy without undesirable binding activations 
	protected void translateAllBy(double dx, double dy) {
		for(Mark child: getComponents()) {
			child.setTentative(child.coords.getX() + dx, child.coords.getY() +  dy);
		}
		
		for(Mark child: getComponents()) {
			child.validateTentative();
		}
	}
	
	// Makes sure that all are translated by dx, dy without undesirable binding activations 
	protected void translateAll(Line lineTrace) {
		double dx = lineTrace.getEndX() - lineTrace.getStartX();
		double dy = -lineTrace.getEndY() + lineTrace.getStartY();
		for(Mark child: getComponents()) {
			child.setTentative(child.pinX + dx, child.pinY + dy);
		}
		
		for(Mark child: getComponents()) {
			child.validateTentative();
		}
	}
	
	@Override
	public void attach(Mark mark, boolean sharedX, boolean sharedY) {
		Container container = mark.getContainer();
		container.removeMark(mark);
		
		//////////
		for(Property property:mark.properties) {
			if(property instanceof Shareable) {
				if(mark instanceof VisGroup) {
					if(property instanceof XProperty || property instanceof YProperty) ((Shareable)property).getPublicProperty().set(false);
				}
				else ((Shareable)property).getPublicProperty().set(false);
			}
		}
		
		double xpos, ypos;
		
		RefX xref = mark.coords.xRef.get();
		RefY yref = mark.coords.yRef.get();

		if(xref == RefX.Left) xpos = mark.left();
		else if (xref == RefX.Right) xpos = mark.right();
		else xpos = mark.centerX();
			
		if(yref == RefY.Top) ypos = mark.top();
		else if (yref == RefY.Bottom) ypos = mark.bottom();
		else ypos = mark.centerY();
		
		mark.setPositionCoords(sharedX ? 0 : xpos - coords.getX(), sharedY ? 0 : ypos - coords.getY());
		/////////
						
		addMark(mark, true);		
	}
			
	@Override
	public GroupPropertyStructure getChildPropertyStructure(){
		return (GroupPropertyStructure)super.getChildPropertyStructure();
	}
	
	
	@Override
	public VisGroup getTopGroup() {
		if(container instanceof VisGroup) return ((VisGroup) container).getTopGroup();
		else return this;
	}
	
	public boolean isTopGroup() {
		if(container instanceof VisGroup) return false;
		else return true;
	}
	
	public void splitProperties(ArrayList<Property> publicProperties, ArrayList<Property> allProperties) {		
		Collection<Property> selfProperties = getSelfProperties().values();
		for(Property prop:selfProperties) {
			if(prop instanceof Shareable) {
				allProperties.add(prop);
				if(((Shareable)prop).getPublicProperty().get()) publicProperties.add(prop);
			}
		}
		
		// Children properties???
		Collection<FlexibleListProperty> childproperties = childPropertyStructure.getProperties().values();
		// TODO: ????
		for(FlexibleListProperty props: childproperties) {
			PropertyName name = props.getPropertyName();
			if(childPropertyStructure.getCommon().contains(name)) {				
				ArrayList<GroupBinding> groups = ((GroupPropertyStructure)childPropertyStructure).getGroups(name);
				if(groups != null) {
					for(GroupBinding group: groups) {
						if(!group.isEmpty()) {
							Property first = group.firstProperty();
							if(((Shareable)first).getPublicProperty().get()) publicProperties.add(first);
							for(Property property: (ArrayList<Property>)group.getProperties()) {						
								allProperties.add(property);
								((Shareable)property).getHiddenProperty().set(property != first);
							}							
						}
					}
				}
			} else {
				for(Property prop: props.getValue()) {
					if(prop instanceof Shareable) {
						((Shareable)prop).getHiddenProperty().set(false);
						allProperties.add(prop);
						if(((Shareable)prop).getPublicProperty().get()) publicProperties.add(prop);
					}
				}
			}
		}
		
		this.allProperties = allProperties;
	}
	
	public ArrayList<Property> getAllProperties(){
		return allProperties;
	}
}
