package fr.inria.structgraphics.graphics;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.graphics.controls.Control;
import fr.inria.structgraphics.graphics.controls.Control.ControlListener;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.ShapeProperty;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable;
import fr.inria.structgraphics.ui.tools.MarkSelection;
import fr.inria.structgraphics.ui.utils.FlowConnectionBinding;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnection;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.LabelCollection;
import javafx.beans.property.Property;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;

public abstract class ShapeMark extends SimpleMark  {
	
	protected Shape primitif;
	public ShapeProperty shapeProperty; // TODO: turn this to a property...
	
	protected Circle flowMarker;
	
	public ShapeMark(Container parent, boolean toend, ShapeProperty.Type type, double w, double h){
		super(parent, toend, w, h);	

		shapeProperty = new ShapeProperty(this, type);
		primitif = createShape();

		shape.add(primitif);	
		shape.addToGhost(xghost);
		shape.addToGhost(yghost);
		
	//	labels = new LabelCollection(this);
	//	group.getChildren().add(labels);
		
		initControls();
	}
	

	@Override
	public void stealProperties(SimpleMark mark) {
		super.stealProperties(mark);
		if(mark instanceof ShapeMark) {
			shapeProperty.set(((ShapeMark)mark).shapeProperty.get());	
			shapeProperty.getPublicProperty().set(((ShapeMark)mark).shapeProperty.getPublicProperty().get());
		}
		ratiolock.set(mark.ratiolock.get());
	}
		
	public ShapeProperty getShapeProperty() {
		return shapeProperty;
	}
	
	public ShapeProperty.Type getShapeType() {
		return shapeProperty.get();
	}
	
	public static ShapeMark createInstance(Container container, boolean toend, ShapeProperty.Type type, double width, double height) {
		// New code that has a single class for all types of shapes!!!
		if(type == ShapeProperty.Type.Text) return new TextualMark(container, toend, width, height);
		return new GenericShapeMark(container, toend, type, width, height);	
	}
	
	protected abstract Shape createShape();
	protected abstract void refreshShape();
				
	
	@Override
	public void dispose() {
		super.dispose();
		
		if(binding != null) {
			binding.destroy();
		}
	}
	
	@Override
	protected void updateBasicShape() {
		refreshShape();
		updateControls();
		updateGhost();
		labels.update();
	}
	
			
	@Override
	public void setHighlight(boolean highlight, boolean enableControls) { 
		this.highlighted = highlight;
		shape.setHighlight(highlight);
		
		if(enableControls && (width() > 3 || height() > 3)) controls.setVisible(highlight);
		else  controls.setVisible(false);
		
		if(container instanceof VisBody) {
			VisBody vgroup = (VisBody)container;
			vgroup.highlighted(this, highlight);			
		}
	}
	
	
	@Override
	public ShapeMark getShape(double x, double y) {
		if(coords.xRef.get() == RefX.Left) x-= width.get()/2;
		else if(coords.xRef.get() == RefX.Right) x+= width.get()/2;
		
		if(coords.yRef.get() == RefY.Bottom) y-= height.get()/2;
		else if(coords.yRef.get() == RefY.Top) y+= height.get()/2;
		
		if(primitif.contains(x, y)) return this;
		else return null;
	}
	
	@Override
	protected void initControls() { // TODO: 
		startX = new Control();
		startX.setControlListener(new ControlListener() {
			private double w, x;
			
			@Override
			public void pin() {
				x = coords.x.get();
				w = width.get();
			}
			
			@Override
			public void offsetX(double offset) {
				x -= offset;
			}
			
			@Override
			public void offsetY(double offset) {}
			
			@Override
			public void drag(Line lineTrace) {
				double delta = lineTrace.getEndX() - lineTrace.getStartX();
				
				switch(coords.getXRef()) {
					case Left:
						coords.x.set(x + delta);
						width.updateValue(w - delta);
						break;
					case Right:
						width.updateValue(w - delta);
						break;
						
					default:
						width.updateValue(w - 2*delta);
						break;
				}
			}
		});
		
		endX = new Control();
		endX.setControlListener(new ControlListener() {
			private double w, x;
			
			@Override
			public void pin() {
				x = coords.x.get();
				w = width.get();
			}
			
			@Override
			public void offsetX(double offset) {
				x -= offset;
			}
			
			@Override
			public void offsetY(double offset) {}
			
			@Override
			public void drag(Line lineTrace) {
				double delta = -lineTrace.getEndX() + lineTrace.getStartX();
				
				switch(coords.getXRef()) {
					case Left:
						width.updateValue(w - delta);
						break;
						
					case Right:
						coords.x.set(x - delta);
						width.updateValue(w - delta);
						break;
						
					default:
						width.updateValue(w - 2*delta);
						break;
				}
			}
		});
		
		endY = new Control();
		endY.setControlListener(new ControlListener() {
			private double h, y;
			
			@Override
			public void pin() {
				y = coords.y.get();
				h = height.get();
			}
			
			@Override
			public void offsetX(double offset) {}
			
			@Override
			public void offsetY(double offset) {
				y+=offset;
			}
			
			@Override
			public void drag(Line lineTrace) {
				double delta = lineTrace.getEndY() - lineTrace.getStartY();
				
				switch(coords.getYRef()) {
					case Bottom:
						height.updateValue(h - delta);
						break;
						
					case Top:
						coords.y.set(y - delta);
						height.updateValue(h - delta);
						break;
						
					default:
						height.updateValue(h - 2*delta);
						break;
				}
			}
		});
		
		startY = new Control();
		startY.setControlListener(new ControlListener() {
			private double h, y;
			
			@Override
			public void pin() {
				y = coords.y.get();
				h = height.get();
			}
			
			@Override
			public void offsetX(double offset) {}
			
			@Override
			public void offsetY(double offset) {
				y+=offset;
			}
			
			@Override
			public void drag(Line lineTrace) {
				double delta = -lineTrace.getEndY() + lineTrace.getStartY();
				
				switch(coords.getYRef()) {
					case Bottom:
						coords.y.set(y + delta);
						height.updateValue(h - delta);
						break;
						
					case Top:
						height.updateValue(h - delta);
						break;
						
					default:
						height.updateValue(h - 2*delta);
						break;
				}
			}
		});
		
		// Code for corner controls
		//////////////////////////
		topleft = new Control();
		topleft.setControlListener(new ControlListener() {
			private double w, x, y, h;
			
			@Override
			public void pin() {
				x = coords.x.get();
				w = width.get();
				y = coords.y.get();
				h = height.get();
			}
			
			@Override
			public void offsetX(double offset) {
				x -= offset;
			}
			
			@Override
			public void offsetY(double offset) {
				y+=offset;
			}
			
			@Override
			public void drag(Line lineTrace) {
				double deltax = lineTrace.getEndX() - lineTrace.getStartX();
				double deltay = -lineTrace.getEndY() + lineTrace.getStartY();
				
				switch(coords.getXRef()) {
					case Left:
						coords.x.set(x + deltax);
						width.updateValue(w - deltax);
						break;
					case Right:
						width.updateValue(w - deltax);
						break;					
					default:
						width.updateValue(w - 2*deltax);
						break;
				}
				
				switch(coords.getYRef()) {
					case Bottom:
						coords.y.set(y + deltay);
						height.updateValue(h - deltay);
						break;
						
					case Top:
						height.updateValue(h - deltay);
						break;
						
					default:
						height.updateValue(h - 2*deltay);
						break;
				}
			}
		});
		
		topright = new Control();
		topright.setControlListener(new ControlListener() {
			private double w, x, y, h;
			
			@Override
			public void pin() {
				x = coords.x.get();
				w = width.get();
				y = coords.y.get();
				h = height.get();
			}
			
			@Override
			public void offsetX(double offset) {
				x -= offset;
			}
			
			@Override
			public void offsetY(double offset) {
				y+=offset;
			}
			
			@Override
			public void drag(Line lineTrace) {
				double deltax = -lineTrace.getEndX() + lineTrace.getStartX();
				double deltay = -lineTrace.getEndY() + lineTrace.getStartY();
							
				switch(coords.getXRef()) {
					case Left:
						width.updateValue(w - deltax);
						break;
						
					case Right:
						coords.x.set(x - deltax);
						width.updateValue(w - deltax);
						break;
						
					default:
						width.updateValue(w - 2*deltax);
						break;
				}
				
				switch(coords.getYRef()) {
					case Bottom:
						coords.y.set(y + deltay);
						height.updateValue(h - deltay);
						break;
						
					case Top:
						height.updateValue(h - deltay);
						break;
						
					default:
						height.updateValue(h - 2*deltay);
						break;
				}
			}
		});
		
		bottomleft = new Control();
		bottomleft.setControlListener(new ControlListener() {
			private double w, x, y, h;
			
			@Override
			public void pin() {
				x = coords.x.get();
				w = width.get();
				y = coords.y.get();
				h = height.get();
			}
			
			@Override
			public void offsetX(double offset) {
				x -= offset;
			}
			
			@Override
			public void offsetY(double offset) {
				y+=offset;
			}
			
			@Override
			public void drag(Line lineTrace) {
				double deltax = lineTrace.getEndX() - lineTrace.getStartX();
				double deltay = lineTrace.getEndY() - lineTrace.getStartY();
				
				switch(coords.getXRef()) {
					case Left:
						coords.x.set(x + deltax);
						width.updateValue(w - deltax);
						break;
					case Right:
						width.updateValue(w - deltax);
						break;					
					default:
						width.updateValue(w - 2*deltax);
						break;
				}
								
				switch(coords.getYRef()) {
					case Bottom:
						height.updateValue(h - deltay);
						break;
						
					case Top:
						coords.y.set(y - deltay);
						height.updateValue(h - deltay);
						break;
						
					default:
						height.updateValue(h - 2*deltay);
						break;
				}
			}
		});
		
		bottomright = new Control();
		bottomright.setControlListener(new ControlListener() {
			private double w, x, y, h;
			
			@Override
			public void pin() {
				x = coords.x.get();
				w = width.get();
				y = coords.y.get();
				h = height.get();
			}
			
			@Override
			public void offsetX(double offset) {
				x -= offset;
			}
			
			@Override
			public void offsetY(double offset) {
				y+=offset;
			}
			
			@Override
			public void drag(Line lineTrace) {
				double deltax = -lineTrace.getEndX() + lineTrace.getStartX();
				double deltay = lineTrace.getEndY() - lineTrace.getStartY();
				
				switch(coords.getXRef()) {
					case Left:
						width.updateValue(w - deltax);
						break;
						
					case Right:
						coords.x.set(x - deltax);
						width.updateValue(w - deltax);
						break;
						
					default:
						width.updateValue(w - 2*deltax);
						break;
				}
								
				switch(coords.getYRef()) {
					case Bottom:
						height.updateValue(h - deltay);
						break;
						
					case Top:
						coords.y.set(y - deltay);
						height.updateValue(h - deltay);
						break;
						
					default:
						height.updateValue(h - 2*deltay);
						break;
				}
			}
		});
		
		//////////////////////////
		controls.getChildren().addAll(startX, endX, endY, startY, bottomleft, topright, topleft, bottomright);
	}
	
	protected void updateControls() { // TODO: Replace by bindings?
		startX.moveTo(-width.get()/2, 0);
		endX.moveTo(width.get()/2, 0);
		
		endY.moveTo(0, -height.get()/2);
		startY.moveTo(0, height.get()/2);
		
		topleft.moveTo(-width.get()/2, height.get()/2);
		topright.moveTo(width.get()/2, height.get()/2);
		
		bottomleft.moveTo(-width.get()/2, -height.get()/2);
		bottomright.moveTo(width.get()/2, -height.get()/2);
	}
	
	@Override
	public SimpleMark createCopy(Container container, double dx, double dy) { 
		double x = coords.getX() + dx;
		double y = coords.getY() + dy;
		
		ShapeMark shapeMark = ShapeMark.createInstance(container, container.addToEnd(x, y), shapeProperty.get(), width(), height());
		
		shapeMark.stealProperties(this);
		shapeMark.coords.x.set(x);
		shapeMark.coords.y.set(y);
								
		return shapeMark;
	}


	@Override
	public boolean monoselect(MarkSelection selectedMarks, Circle trace) {		
		if(trace == null) return false;
		if(width() > 3 || height() > 3) return super.monoselect(selectedMarks, trace);		
		boolean intersects = new Circle(trace.getCenterX(), getRootHeight() - trace.getCenterY(), 10).contains(getGlobalX(), getGlobalY());
		
		if(intersects) {
			pinX = coords.getX();
			pinY = coords.getY();
			selectedMarks.add(this);
		}
		
		return intersects;
	}


	public void setDefaults() {
		border.set(0.5);
		strokePaint.set(Paint.valueOf("#505080"));
		coords.xRef.set(RefX.Center);
	}
	
	/*
	@Override
	public void showLabel(DataVariable variable, Property property) {
		if(isLabelShown(variable) == variable.nodeShownProperty.get()) return;
		labels.showVariable(variable, property);
	}
	
	public boolean isLabelShown(DataVariable variable) {
		return labels.isLabelShown(variable);
	}
	*/
	
	/*
	 * The following is a set of methods for dealing with flow connections (flow diagrams)
	 * 
	 */
	protected FlowConnectionBinding binding;
	
	public FlowConnectionBinding getFlowConnectionsBinding() {
		return binding;
	}
	
	public void addInwardConnection(FlowConnection connection) {
		if(binding == null) binding = new FlowConnectionBinding(this);
		binding.addInwardConnection(connection);
	}
	
	public void addOutwardConnection(FlowConnection connection) {
		if(binding == null) binding = new FlowConnectionBinding(this);
		binding.addOutwardConnection(connection);
	}
	
	public void removeInwardConnection(FlowConnection connection) {
		binding.removeInwardConnection(connection);
	}
	
	public void removeOutwardConnection(FlowConnection connection) {
		binding.removeOutwardConnection(connection);
	}
	
	public boolean hasInwardConnections() {
		return binding != null && !binding.getInwardConnections().isEmpty();
	}
	
	public boolean hasOutwardConnections() {
		return binding != null && !binding.getOutwardConnections().isEmpty();
	}
	
	public void lockListeners(boolean lock) {
		if(binding != null) binding.lockListeners(lock);
	}
	
	////////////////
	
	public void showOrigin(boolean show) {
		if(show && flowMarker == null) {
			flowMarker = new Circle(5);
			flowMarker.setStroke(Color.CORNFLOWERBLUE);
			flowMarker.setFill(null);
			flowMarker.centerXProperty().set(Math.abs(width.get())/2);
			flowMarker.centerYProperty().set(0);
			group.getChildren().add(flowMarker);
		} else if(!show && flowMarker != null){
			group.getChildren().remove(flowMarker);
			flowMarker = null;
		}
	}
	
	public void showDestination(boolean show) {
		if(show && flowMarker == null) {
			flowMarker = new Circle(8);
			flowMarker.setStroke(Color.CORNFLOWERBLUE);
			flowMarker.setFill(null);
			flowMarker.centerXProperty().set(-Math.abs(width.get())/2);
			flowMarker.centerYProperty().set(0);
			group.getChildren().add(flowMarker);
		} else if(!show && flowMarker != null){
			group.getChildren().remove(flowMarker);
			flowMarker = null;
		}
	}
	
	///////////////////
	private double originYflag = 0, destinationYflag = 0;
	void increaseOriginFlag(double dy) {
		originYflag += dy;
	}
	
	void increaseDestinationFlag(double dy) {
		destinationYflag += dy;
	}
	
	void resetConnectionFlags() {
		originYflag = 0;
		destinationYflag = 0;
	}
	
	double getOriginFlag() {
		return originYflag;
	}
	
	double getDestinationFlag() {
		return destinationYflag;
	}
	
	@Override
	public Property getProperty(PropertyName name) {
		if(name.isShape()) return shapeProperty;
		else return super.getProperty(name);		
	}
	
}
