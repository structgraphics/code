package fr.inria.structgraphics.graphics;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.ComponentRefXProperty;
import fr.inria.structgraphics.types.ComponentRefYProperty;
import fr.inria.structgraphics.types.XProperty;
import fr.inria.structgraphics.types.YProperty;

public class PositionCoords {
			
	public XProperty x;
	public YProperty  y;
	
	public ComponentRefXProperty xRef;
	public ComponentRefYProperty yRef;
			
	public PositionCoords(Mark mark) {
		this.x = new XProperty(mark);
		this.y = new YProperty(mark);
		xRef = new ComponentRefXProperty(mark);
		yRef = new ComponentRefYProperty(mark);
	}
	
	public PositionCoords(Mark mark, double x, double y) {
		this.x = new XProperty(mark);
		this.y = new YProperty(mark);
		xRef = new ComponentRefXProperty(mark);
		yRef = new ComponentRefYProperty(mark);
		
		this.x.set(x);
		this.y.set(y);
	}
	
	public double getX() {
		return x.get();
	}
	
	public double getY() {
		return y.get();
	}	
	
	public XProperty getXProperty() {
		return x;
	}
	
	public YProperty getYProperty() {
		return y;
	}	
	
	public ComponentRefXProperty getXRefProperty() {
		return xRef;
	}
	
	public ComponentRefYProperty getYRefProperty() {
		return yRef;
	}
	
	public RefX getXRef() {
		return xRef.get();
	}
	
	public RefY getYRef() {
		return yRef.get();
	}
}

