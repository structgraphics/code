package fr.inria.structgraphics.graphics.controls;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public class Control extends Rectangle {
	private final static double SIZE = 6;
		
	protected ControlListener listener;
	
	public Control() {
		super(SIZE, SIZE);		
		setStroke(Color.GRAY);
		setStrokeWidth(0.5);
		setFill(Color.rgb(255, 255, 255, 0.5));
	}
	
	public double centerX() {
		return getX() + getWidth()/2;
	}
	
	public double centerY() {
		return getY() + getWidth()/2;
	}
	
	public void moveTo(double x, double y) {
		xProperty().set(x - getWidth()/2);
		yProperty().set(y - getWidth()/2);
	}
	
	public boolean intersects(Shape trace) {
		if(trace == null) return false;
		Shape intersection = Shape.intersect(this, trace);
		if(!(intersection instanceof Path) || !((Path)intersection).getElements().isEmpty()) 
			return true;
		else return false;
	}

	public void setControlListener(ControlListener listener) {
		this.listener = listener;
	}
	
	public void pin() {
		if(listener!=null) listener.pin();
	}
	
	public void drag(Line lineTrace) {
		if(listener!=null) listener.drag(lineTrace);
	}
	
	
	public interface ControlListener {
		public void drag(Line lineTrace);
		public void pin();
		public void offsetX(double offset);
		public void offsetY(double offset);
	}


	public void offsetXPin(double offset) {
		if(listener!=null) listener.offsetX(offset);
	}
	
	public void offsetYPin(double offset) {
		if(listener!=null) listener.offsetY(offset);
	}

}


