package fr.inria.structgraphics.graphics;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;

public class ComplexShape extends Group {
	
	// Create a ghost for highlighting the shape 
	//TODO: ...
	protected Group ghost; 
	private boolean highlighted = false;
	
	public ComplexShape() {
		super();
		
		ghost = new Group();
		getChildren().add(ghost);
		ghost.setVisible(false);
	}

	public void setFill(Paint paint) {
		for(Node shape: getChildren()) {
			if(shape instanceof Shape) ((Shape)shape).setFill(paint);
		}
	}

	public void setStroke(Paint paint) {
		for(Node shape: getChildren()) {
			if(shape instanceof Shape) ((Shape)shape).setStroke(paint);
		}
	}

	public void setStrokeWidth(double border) {
		for(Node shape: getChildren()) {
			if(shape instanceof Shape) ((Shape)shape).setStrokeWidth(border);
		}
	}

	public void clear() { // TODO
		getChildren().remove(1, getChildren().size());
		ghost.getChildren().clear();		
	}
	
	public void add(Shape shape) { 
		getChildren().add(0, shape);
		shape.setSmooth(true);
		//if(highlighted) addToGhost(shape);
	}
	
	public void remove(Shape shape) {
		getChildren().remove(shape);
	}
	
	public void replaceBy(Shape shape) {
		getChildren().remove(0);
		add(shape);
	}

	public boolean intersects(Shape trace) {
		if(trace == null) return false;
		for(Node shape: getChildren()) {
			if(shape instanceof Shape) {
				Shape intersection = Shape.intersect((Shape) shape, trace);			
				if(!(intersection instanceof Path) || !((Path)intersection).getElements().isEmpty()) 
					return true;
			}
		}
		
		return false;
	}

		
	public void addToGhost(Group group) {
		for(Node node: group.getChildren()) {
			Shape shape = (Shape)node;	
			shape.setStroke( Color.CORNFLOWERBLUE);
			shape.setFill(null);
			shape.setStrokeWidth(shape.getStrokeWidth() + 1.5);
		}
		
		ghost.getChildren().addAll(group.getChildren());
	}
	
	public void addToGhost(Shape shape) {
		ghost.getChildren().add(shape);	
		shape.setStroke( Color.CORNFLOWERBLUE);
		shape.setFill(null);
		shape.setStrokeWidth(shape.getStrokeWidth() + 1.5);
	}	
		
	public void setHighlight(boolean highlight) { 
		if(highlight && !highlighted) {
			highlighted = true;
			ghost.setVisible(true);
		} else if(!highlight && highlighted) {
			highlighted = false;
			ghost.setVisible(false);
		}
	}

	
}
