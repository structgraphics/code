package fr.inria.structgraphics.graphics;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.shape.Line;

public class MultiplyFeedForward extends Group {
	
	private double gapx = 20;
	private double gapy = 10;
	private int maxX = 20;
	private int maxY = 20;
	
	private SimpleMark mark;
	
	public MultiplyFeedForward(SimpleMark mark) {
		super();
		this.mark = mark;
		
		if(mark instanceof VisBody) {
			maxX = 11;
			maxY = 11;		
		}
		
		build();

	}
	
	private void build() {
		ArrayList<Point2D> positions = MultiplyUtils.getAvailablePositions(mark);
		if(positions == null) {
			buildalternative();
			return;
		}
		
		for(Point2D pos: positions) {
			getChildren().add(mark.getShadow(pos.getX(), pos.getY(), 1));
		}
	}
	
	private void buildalternative() {			
		double W = mark.getRootWidth();
		double H = mark.getRootHeight();
		double left = mark.getGlobalLeft();
		double right = mark.getGlobalRight();
		double top = -mark.getGlobalTop();
		double bottom = -mark.getGlobalBottom();

		double dx = Math.abs(mark.width()) + gapx;
		// TODO: For copying the line????
		
		boolean linechart = (mark instanceof LineConnectedCollection) && ((LineConnectedCollection)mark).curveSelected;
		
		double dy = linechart  ? 50 : Math.abs(mark.height()) + gapy;
		
		if(!linechart) {
			for(int i = 1; right + dx*i < W && i < maxX; ++ i) {
				getChildren().add(mark.getShadow(i, 0, dx));
			}
			
			for(int i = 1; left - dx*i > 0 && i < maxX; ++ i) {
				getChildren().add(mark.getShadow(-i, 0, dx));
			}
		}
				
		for(int i = 1; top + dy*i < H && i < maxY; ++ i) {
			getChildren().add(mark.getShadow(0, -i, dy));
		}
		
		for(int i = 1; bottom - dy*i > 0 && i < maxY; ++ i) {
			getChildren().add(mark.getShadow(0, i, dy));
		}

	}
	

	public List<Shadow> select(Line trace) {
		List<Shadow> selected = new ArrayList<Shadow>();
		if(trace == null || trace.getStartX() == trace.getEndX() && trace.getStartY() == trace.getEndY()) return selected;
		
		for(Node node:getChildren()) {
			Shadow shadow = (Shadow)node;
			if(shadow.intersects(trace))
				selected.add(shadow);
		}
		
		return selected;
	}
	
	
}
