package fr.inria.structgraphics.graphics;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.FillColorProperty;
import fr.inria.structgraphics.types.HeightProperty;
import fr.inria.structgraphics.types.StrokeColorProperty;
import fr.inria.structgraphics.types.WidthProperty;
import fr.inria.structgraphics.ui.inspector.InspectorView;
import javafx.beans.property.DoubleProperty;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

public class VisFrame extends Container {
		
	public DoubleProperty width, height;
	protected FillColorProperty paint = new FillColorProperty(this);
	public StrokeColorProperty stroke = new StrokeColorProperty(this);
	private Rectangle frame;
	
	private InspectorView inspector;
	
	private Rectangle bedsheet = null;
	
	public VisFrame(double width, double height){
		super();
		
		this.width = new WidthProperty(this, width);
		this.height = new HeightProperty(this, height);
				
		frame = new Rectangle(0, 0, width, height); // Bind with rectangle's dimensions
		this.width.bindBidirectional(frame.widthProperty());
		this.height.bindBidirectional(frame.heightProperty());
		
	//	paint.set(Color.web("#f1f1e8"));
		paint.set(Color.WHITE);
		frame.setFill(paint.get());
		
		stroke.set(Color.GREY);

		group.getChildren().add(frame);		
	}
		
	@Override
	public void initProperties() {
		addProperty(this.width);
		addProperty(this.height);
		addProperty(this.paint);
		addProperty(this.stroke);
		//super.initProperties();
	}
		
	public void setInspector(InspectorView inspector) {
		this.inspector = inspector;
	}
	
	public InspectorView getInspector() {
		return inspector;
	}
	
	@Override
	public Point2D getReferencePoint(double cx, double cy) {
		double x, y;
		
		if(refCoords.containerXRef.get() == RefX.Left) x = cx; 
		else if(refCoords.containerXRef.get() == RefX.Right) x = width.get() + cx; 
		else x = width.get()/2 + cx;
		
		if(refCoords.containerYRef.get() == RefY.Top) y = -cy; 
		else if(refCoords.containerYRef.get() == RefY.Bottom) y = height.get() - cy; 
		else y = height.get()/2 - cy;
		
		return new Point2D(x, y);
	}
	
	@Override
	public double getRefAngle(double x) {
		return 0;
	}
	
	@Override
	public String getName() {
		return "Root Frame";
	}

	@Override
	public String getType() {
		return "Pane";
	}


	@Override
	public void updateShape() {
		frame.setFill(paint.get());
	}


	public double getRootWidth() {
		return width.get();
	}
	
	public double getRootHeight() {
		return height.get();
	}

	@Override
	public void bringToFront(Mark mark) {
		if(components.get(components.size()-1) == mark) return;
		
		components.remove(mark);
		components.add(mark);

		group.getChildren().remove(mark.group);
		group.getChildren().add(mark.group);			
	}

	@Override
	public void sendToBack(Mark mark) {
		if(components.get(0) == mark) return;
		
		components.remove(mark);
		components.add(0, mark);
		
		group.getChildren().remove(mark.group);
		group.getChildren().add(1, mark.group);		
	}
	
	@Override
	public void updateReordering() {
		for(Mark mark:components) mark.refreshID();
		getInspector().updateOrder();
	}	
	
	public Rectangle createBedSheet() {
		if(bedsheet == null) {
			bedsheet = new Rectangle(0, 0, width.get(), height.get());
			bedsheet.setFill(Color.rgb(255, 255, 255, 0.6)); 			
		} else {
			bedsheet.setWidth(width.get());
			bedsheet.setHeight(height.get());
		}

		return bedsheet;
	}
	
	public Rectangle getBedSheet() {
		return bedsheet;
	}
	
	@Override
	public ShapeMark getShape(double x, double y) {
		for(Mark mark: components) {
			if(mark instanceof LineConnectedCollection) { // Only consider the shapes somewhere within collections
				double x_ = x - mark.coords.getX();
				double y_ = (height.get() - y) - mark.coords.getY();
				
				ShapeMark mark_ = mark.getShape(x_, y_);
				if(mark_ != null) return mark_;				
			}
		}
				
		return null;
	}
}

