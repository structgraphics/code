package fr.inria.structgraphics.graphics;

import java.util.List;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.graphics.controls.Control;
import fr.inria.structgraphics.types.Shareable;
import javafx.beans.property.Property;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

public abstract class SimpleMark extends Mark  {
	
	protected Arrow xghost = new Arrow(), yghost = new Arrow();
	protected MultiplyFeedForward mfeedforward = null;
	
	// These are less important properties to be somehow linked to the main properties
	protected Control startX, endX, startY, endY;
	
	// Additional controls for the corners
	protected Control topleft, topright, bottomleft, bottomright;
	
	public SimpleMark(Container parent, boolean toend, double width, double height){
		super(parent, toend, width, height);		
	}
	
	@Override
	public void initProperties() {
		super.initProperties();
		
		addProperty(this.width);
		addProperty(this.height);
		
		addProperty(border);
		addProperty(angle);
		
		addProperty(paint);
		addProperty(strokePaint);
	}
		
	
	public void stealProperties(SimpleMark mark) {
		border.set(mark.border.get());
		border.getPublicProperty().set(mark.border.getPublicProperty().get());
		
		if(mark.paint != null) {
			paint.set(mark.paint.get());
			paint.getPublicProperty().set(mark.paint.getPublicProperty().get());
		} else paint = null;

		
		angle.set(mark.angle.get());
		angle.getPublicProperty().set(mark.angle.getPublicProperty().get());
		
		if(mark.strokePaint != null) {
			strokePaint.set(mark.strokePaint.get());
			strokePaint.getPublicProperty().set(mark.strokePaint.getPublicProperty().get());
		} else mark.strokePaint = null;
		
		coords = new PositionCoords(this, mark.coords.getX(), mark.coords.getY());	
		coords.x.getPublicProperty().set(mark.coords.x.getPublicProperty().get());
		coords.y.getPublicProperty().set(mark.coords.y.getPublicProperty().get());
		
		coords.xRef.set(mark.coords.getXRef());
		coords.xRef.getPublicProperty().set(mark.coords.xRef.getPublicProperty().get());
		
		coords.yRef.set(mark.coords.getYRef());
		coords.yRef.getPublicProperty().set(mark.coords.yRef.getPublicProperty().get());
		
		width.getPublicProperty().set(mark.width.getPublicProperty().get());
		height.getPublicProperty().set(mark.height.getPublicProperty().get());
	}
		
	public double width() {
		return width.get();
	}
	
	public double height() {
		return height.get();
	}
	
	public Arrow getXGhost() {
		return xghost;
	}
	
	public Arrow getYGhost() {
		return yghost;
	}
	
	// This is some code for creating and handling the ghost
	public void updateGhost() {
		switch(coords.getXRef()) {
			case Left:
				xghost.setEnds(-width.get()/2, -width.get()/2, height.get()/2, -height.get()/2);
				break;
				
			case Right:
				xghost.setEnds(width.get()/2, width.get()/2, height.get()/2, -height.get()/2);
				break;
				
			default:
				xghost.setEnds(0, 0, height.get()/2, -height.get()/2);
				break;
		}
		
		switch(coords.getYRef()) {
			case Top:
				yghost.setEnds(-width.get()/2, width.get()/2, -height.get()/2, -height.get()/2);
				break;
				
			case Bottom:
				yghost.setEnds(-width.get()/2, width.get()/2, height.get()/2, height.get()/2);
				break;
				
			default:
				yghost.setEnds(-width.get()/2, width.get()/2, 0, 0);
				break;
		}
		
	}
	
	@Override
	public void updateShape() {
		double cx = 0, cy = 0;
		
		// X axis
		if(coords.getXRef() == RefX.Left) cx = coords.getX() + width.get()/2;
		else if(coords.getXRef() == RefX.Right) cx = coords.getX() - width.get()/2;
		else cx = coords.getX();
		
		// Y axis
		if(coords.getYRef() == RefY.Bottom) cy = coords.getY() + height.get()/2;
		else if(coords.getYRef() == RefY.Top) cy = coords.getY() - height.get()/2;
		else cy = coords.getY();
					
		// TODO: I also need to derive the angle in case the parent is a curve!!! 
				
		super.updateShape(cx, cy);
		
	}
	
	@Override
	public Point2D getReferencePoint(double cx, double cy) {
		double x, y;
	
		if(refCoords.containerXRef.get() == RefX.Left) x = cx - width.get()/2; 
		else if(refCoords.containerXRef.get() == RefX.Right) x = width.get()/2 + cx; 
		else x = cx;
		
		if(refCoords.containerYRef.get() == RefY.Top) y = -cy - height.get()/2; 
		else if(refCoords.containerYRef.get() == RefY.Bottom) y = height.get()/2 - cy; 
		else y = -cy;
		
		return new Point2D(x, y);
	}
	

	public void setFeedForward(boolean activate) {
		VisFrame frame = (VisFrame)getRoot();	
		
		if(activate) {
			mfeedforward = new MultiplyFeedForward(this);
			
			Point2D ref = getRotationPoint();
			mfeedforward.translateXProperty().set(mfeedforward.translateXProperty().get() + getGlobalX() - ref.getX());
			mfeedforward.translateYProperty().set(mfeedforward.translateYProperty().get() + getRootHeight() - getGlobalY() - ref.getY());
					
			getRoot().getGroup().getChildren().add(frame.createBedSheet());
			getRoot().getGroup().getChildren().add(mfeedforward);
		}
		else {
			getRoot().getGroup().getChildren().remove(mfeedforward);
			getRoot().getGroup().getChildren().remove(frame.getBedSheet());
			
			mfeedforward = null;
		}
	}
	
	public Shadow getShadow(double dx, double dy, double delta) { 
		return new Shadow(this, dx*delta, dy*delta); // TODO: ???
	}
	
	
	public SimpleMark createCopy(double dx, double dy) { // TODO: Turn to abstract?
		return createCopy(this.container, dx, dy);
	}
	
	public SimpleMark createCopy(Container container, double dx, double dy) {
		return null;
	}

	public List<Shadow> selectCopies(Line lineTrace) {
		return mfeedforward.select(lineTrace);
	}


}
