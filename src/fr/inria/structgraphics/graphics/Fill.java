package fr.inria.structgraphics.graphics;

import java.util.Map;
import java.util.TreeMap;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class Fill {

	private final static Fill singleton = new Fill();

	private Map<String, Paint> mapping; 
	
	private Fill() {
		mapping = new TreeMap<String, Paint>();
		mapping.put("empty", Color.WHITE);
		mapping.put("pattern1", Color.LIGHTGRAY);
		mapping.put("pattern2", Color.GRAY);
		mapping.put("pattern3", Color.DARKGRAY);		
	}
	
	
	public static Paint getPaint(String name) {
		return singleton.mapping.get(name);
	}
}
