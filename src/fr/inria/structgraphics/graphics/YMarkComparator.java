package fr.inria.structgraphics.graphics;

import java.util.Comparator;

public class YMarkComparator implements Comparator<Mark> {

	@Override
	public int compare(Mark mark1, Mark mark2) {
		return ((Double)mark1.coords.y.get()).compareTo((Double)mark2.coords.y.get());
	}

}
