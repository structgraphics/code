package fr.inria.structgraphics.graphics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.graphics.splines.CubicSpline;
import fr.inria.structgraphics.types.ColoringSchemeProperty;
import fr.inria.structgraphics.types.FillColorProperty;
import fr.inria.structgraphics.types.LineTypeProperty;
import fr.inria.structgraphics.types.OpacityProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.ColoringSchemeProperty.Scheme;
import fr.inria.structgraphics.types.LineTypeProperty.Type;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable;
import fr.inria.structgraphics.ui.tools.MarkSelection;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnection;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnections;
import fr.inria.structgraphics.ui.viscanvas.groupings.PropertyStructure;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class LineConnectedCollection extends ConnectedCollection {
	
	private FlowConnections flowConnections;
	public LineTypeProperty lineProperty;
	
	protected Map<FlowConnection, FlowPath> connectionShapes = new Hashtable<>();
	protected Map<FlowConnection, Text> connectionLabels = new Hashtable<>();
	protected FlowConnection connectionSelected = null;
	protected DataVariable activeVariable;
	
//	public StrokeWidthProperty  = new StrokeWidthProperty(this, 0);
	public FillColorProperty flowPaint = new FillColorProperty (this, Color.rgb(140, 140, 158));
	public OpacityProperty flowOpacity = new OpacityProperty(this, 0.5);
	public ColoringSchemeProperty coloringScheme = new ColoringSchemeProperty(this, Scheme.Common);
		
	public LineConnectedCollection(Container parent, boolean toend) {
		super(parent, toend);
	}	
	
	public LineConnectedCollection(Container parent, boolean toend, ObservableList<Mark> marks) {
		super(parent, toend, marks);
	
	}	
	
	@Override
	public void initProperties() {			
		super.initProperties();
		
		//if(flowPaint != null) flowPaint.set(Color.rgb(170, 170, 188));
		
		if(lineProperty == null) {
			lineProperty = new LineTypeProperty(this, LineTypeProperty.Type.None);
			border.getActiveProperty().set(false);
			strokePaint.getActiveProperty().set(false);
		}
		properties.add(lineProperty);
		
		if(lineProperty.get() == LineTypeProperty.Type.TopBottom) {
			bringConnectionsFront();
		}
		
		lineProperty.addListener(new ChangeListener<LineTypeProperty.Type>() {
			@Override
			public void changed(ObservableValue<? extends LineTypeProperty.Type> observable, LineTypeProperty.Type oldValue, LineTypeProperty.Type newValue) {
				if(newValue != LineTypeProperty.Type.None) reorderChildren(true);
				changeShape(newValue);
				
				if(newValue == LineTypeProperty.Type.None) {
					border.getActiveProperty().set(false);
					strokePaint.getActiveProperty().set(false);
					sendConnectionsBack();
				} else if(newValue == LineTypeProperty.Type.Straight || newValue == LineTypeProperty.Type.Bezier 
						|| newValue == LineTypeProperty.Type.TopBottom) {
					border.getActiveProperty().set(true);
					strokePaint.getActiveProperty().set(true);
					
					if(newValue == LineTypeProperty.Type.TopBottom) {
						bringConnectionsFront();
					} else {
						sendConnectionsBack();
					}
					
				} else {
					border.getActiveProperty().set(false);
					strokePaint.getActiveProperty().set(true);
					//strokePaint.set(Color.color(.85, .8, .8, .5));	
					sendConnectionsBack();
				}
			}
		});
		
		if(lineProperty.get() == LineTypeProperty.Type.StraightSolid || lineProperty.get() == LineTypeProperty.Type.BezierSolid || lineProperty.get() == LineTypeProperty.Type.Area) {
			paint.bind(strokePaint);
		} else if(flowConnections != null) {
			strokePaint = null;
			paint = null;
		}
		else paint.set(null);
		
		initializeShape();
		
		if(coloringScheme != null) {
			coloringScheme.addListener(new ChangeListener<Scheme>() {
				@Override
				public void changed(ObservableValue<? extends Scheme> observable, Scheme oldValue, Scheme newValue) {
					
					updateFlowColoring();
					flowPaint.getActiveProperty().set(newValue == Scheme.Common);
				}
			});			
		}
	}
	
	private void sendConnectionsBack() {
		group.getChildren().remove(shape);
		int index = group.getChildren().indexOf(shapes);
		group.getChildren().add(index, shape);
	}
	
	private void bringConnectionsFront() {
		group.getChildren().remove(shape);
		int index = group.getChildren().indexOf(shapes);
		group.getChildren().add(index + 1, shape);
	}
		
	@Override
	public String getName() {
		String name;
		if(lineProperty.get() == LineTypeProperty.Type.None) name = "Collection";
		else if(lineProperty.get() == LineTypeProperty.Type.Area) name = "Area Chart";
		else name = "Line Chart";
		
		return PropertyName.getPrefix(level) + " " + name;
	}

	@Override
	public String getType() {
		if(lineProperty.get() == LineTypeProperty.Type.None) return "Collection";
		else if(lineProperty.get() == LineTypeProperty.Type.Area) return "AreaChart";
		else return "LineChart";
	}
	
	
	protected void initializeShape() {
		primitif = createShape();
		shape.add(primitif);	
		
		ghost = primitifCopy(0, 0, true);
		shape.addToGhost(ghost);
				
		updateConnections();
			
		initControls();		
	}
	
	@Override
	protected Shape createShape() {
		Path path = new Path();
		
		switch(lineProperty.get()) {
			case StraightSolid:
				updateSolidLine(path, 0, 0);
				break;
			case Bezier:
				updateBezier(path, 0, 0);
				break;
			case BezierSolid:
				updateSolidBezier(path, 0, 0);
				break;
			case Straight:
				updatePolyline(path, 0, 0);
				break;
			case TopBottom:
				updateTopBottom(path, 0, 0);
				break;
			case Area:
				updateArea(path, 0, 0);
				break;
		}
		
		return path;
	}
	
	
	protected boolean updateConnections() {
		if(flowConnections == null) return false;

		Collection<FlowConnection> toremove= null;
		for(FlowConnection flowConnection : connectionShapes.keySet()) {			
			if(!flowConnections.contains(flowConnection)) {
				if(toremove == null) toremove = new ArrayList<>();
				toremove.add(flowConnection);
				shape.remove(connectionShapes.get(flowConnection));
			}
		}		
		if(toremove != null) {
			for(FlowConnection conn: toremove) {
				connectionShapes.remove(conn);
			}
			
			toremove.clear();
		}			
						
		for(FlowConnection flowConnection: flowConnections) {
			flowConnection.getOrigin().resetConnectionFlags();
			flowConnection.getDestination().resetConnectionFlags();			
		}
		
		Set<FlowConnection> sorted = flowConnections.sortByY();
		for(FlowConnection flowConnection: sorted) {
			FlowPath path;
			
			if(!connectionShapes.keySet().contains(flowConnection)) {
				path = new FlowPath();
				connectionShapes.put(flowConnection, path);
				shape.add(path);
				
				FillColorProperty paintProperty = getFlowPaint(flowConnection);
				path.fillProperty().bind(paintProperty);
				path.strokeProperty().bind(paintProperty);
				path.opacityProperty().bind(flowOpacity);
			} else {
				path = connectionShapes.get(flowConnection);
				path.getElements().clear();
			}
			
			FlowCreator.createFlowConnectionPath(path, flowConnection);
		}
		
		updateLabels();
		
		return true;
	}
	
	
	protected Font labelFont = Font.font("Verdana", 10);
	protected Color textColor = Color.GREY;
	
	public void addLabel(FlowConnection flowConnection) {
		final Text text = new Text();
		text.setFont(labelFont);
		text.setFill(textColor);
		text.setTextAlignment(TextAlignment.CENTER);
		text.setText(flowConnection.weight.get()+"");
		
		if(activeVariable != null && activeVariable.nodeShownProperty.get()) {
			text.setVisible(true);
			text.textProperty().set(activeVariable.transformationProperty().get().toData(flowConnection.weight.get()));
		} else text.setVisible(false);
				
		connectionLabels.put(flowConnection, text);
		shape.add(text);
	}
	
	public void removeLabel(FlowConnection flowConnection) {
		shape.remove(connectionLabels.get(flowConnection));
		connectionLabels.remove(flowConnection);
	}
	
	public void updateLabels() {

		for(FlowConnection flowConnection: flowConnections) {
			Text label = connectionLabels.get(flowConnection);
			FlowPath path = connectionShapes.get(flowConnection);
			
			label.xProperty().set(path.centerX() - label.getLayoutBounds().getWidth()/2);
			label.yProperty().set(path.centerY() + label.getLayoutBounds().getHeight()/2);
			
			if(activeVariable != null) label.textProperty().set(activeVariable.transformationProperty().get().toData(flowConnection.weight.get()));
		}
	}
	
	// Displays the labels for all flow connections
	public void showLabels(DataVariable variable) {
		activeVariable = variable;
		
		for(FlowConnection flowConnection: flowConnections) {
			Text label = connectionLabels.get(flowConnection);
			label.setVisible(variable.nodeShownProperty.get());			
		}	
		
		if(activeVariable.nodeShownProperty.get()) updateLabels();
	}
	

	private void updateFlowColoring() {
		for(FlowConnection flowConnection: flowConnections) {
			Shape path = connectionShapes.get(flowConnection);
			FillColorProperty paintProperty = getFlowPaint(flowConnection);
			
			if(connectionSelected == flowConnection) { 
				path.fillProperty().unbind(); 
				
				paintProperty = new FillColorProperty(this, ((Color)paintProperty.get()).darker());
				path.fillProperty().bind(paintProperty);
			}
			else {
				path.fillProperty().unbind();
				path.strokeProperty().unbind();
				path.fillProperty().bind(paintProperty);
				path.strokeProperty().bind(paintProperty);				
			}
		}	
	}
	
	private FillColorProperty getFlowPaint(FlowConnection connection) {
		if(coloringScheme.get() == Scheme.Destination) return connection.getDestination().paint;
		else if(coloringScheme.get() == Scheme.Source) return connection.getOrigin().paint;
		return flowPaint;
	}
	
	
	@Override
	public void updateGhost() { // TODO:....
		if(ghost == null) return;
		
		switch(lineProperty.get()) {
			case StraightSolid:
				updatePolyline((Path)ghost, 0, 0);
				break;
			case Bezier:
				updateBezier((Path)ghost, 0, 0);
				break;	
			case BezierSolid:
				updateBezier((Path)ghost, 0, 0);
				break;	
			case Straight:
				updatePolyline((Path)ghost, 0, 0);				
				break;
			case Area:
				updatePolyline((Path)ghost, 0, 0);
				break;
			case TopBottom:
				updateTopBottom((Path)ghost, 0, 0);
				break;
			default:
				((Path)ghost).getElements().clear();
				break;
		}		
	}
	
	@Override
	protected void refreshShape() {
		if(updateConnections()) return;
		
		if(lineProperty == null || primitif == null) return;
		
		switch(lineProperty.get()) {
			case StraightSolid:
				paint.bind(strokePaint);
				updateSolidLine((Path)primitif, 0, 0);
				break;
			case Bezier:
				paint.unbind();
				paint.set(null);
				updateBezier((Path)primitif, 0, 0);
				break;
			case BezierSolid:
				paint.bind(strokePaint);
				updateSolidBezier((Path)primitif, 0, 0);
				break;
			case Straight:
				paint.unbind();
				paint.set(null);
				updatePolyline((Path)primitif, 0, 0);
				break;
			case TopBottom:
				paint.unbind();
				paint.set(null);
				updateTopBottom((Path)primitif, 0, 0);
				break;
			case Area:
				paint.bind(strokePaint);
				updateArea((Path)primitif, 0, 0);
				break;
			default:
				paint.unbind();
				paint.set(null);
				((Path)primitif).getElements().clear();
				break;
		}
	}
	
	
	@Override
	public SimpleMark createCopy(Container container, double dx, double dy) { 
		double x = coords.getX() + (curveSelected ? 0 : dx);
		double y = coords.getY() + (curveSelected ? 0 : dy);
		
		LineConnectedCollection vgroup = new LineConnectedCollection(container, container.addToEnd(x, y));
		vgroup.setLevel(this.level);
			
		List<Mark> marks = new ArrayList<>();		
		for(Mark mark: components) {
			if(mark instanceof SimpleMark) {
				SimpleMark simpleMark = (SimpleMark)mark;
				SimpleMark copy = curveSelected ? simpleMark.createCopy(vgroup, dx, dy) : simpleMark.createCopy(vgroup, 0, 0);
				copy.initProperties();
				marks.add(copy);
			}
		}
		
		ReferenceCoords refCoords = new ReferenceCoords(RefX.Left, RefY.Bottom);
		vgroup.setRefCoords(refCoords);
		
		for(Mark mark: marks) {
			vgroup.attachCopy(mark/*, childPropertyStructure.isXShared(), childPropertyStructure.isYShared()*/);
		}
		
		vgroup.setPropertyStructure(PropertyStructure.create(marks, childPropertyStructure));
		
		vgroup.toUpdate(false); // Protect the parent tree from property updates
		vgroup.stealProperties(this);
		vgroup.coords.x.set(x);
		vgroup.coords.y.set(y);
		vgroup.coords.xRef.set(RefX.Left);
		vgroup.coords.yRef.set(RefY.Bottom);
		vgroup.toUpdate(true);
		
		copyConnectionsTo(vgroup);
		
		return vgroup;
	}

	
	public void setFlowConnections(FlowConnections connections) {
		this.flowConnections = connections;
		
		for(FlowConnection conn: connections) {
			addLabel(conn);				
			conn.getOrigin().addOutwardConnection(conn);
			conn.getDestination().addInwardConnection(conn);
		}
		
		updateConnections();
	}
	
	// TODO: Problem when including groups!!!!
	private void copyConnectionsTo(LineConnectedCollection vgroup) {
		if(flowConnections == null) return;
		
		vgroup.coloringScheme.set(coloringScheme.get());
		vgroup.flowPaint.set(flowPaint.get());
		
		vgroup.flowConnections = new FlowConnections(vgroup);
		for(FlowConnection connection: flowConnections) {
			ShapeMark source = connection.getOrigin();
			ShapeMark destination = connection.getDestination();
			
			HierarchyPos posSource = new HierarchyPos(source);
			HierarchyPos posDest = new HierarchyPos(destination);
			
			source = (ShapeMark) vgroup.getMarkAt(posSource);
			destination = (ShapeMark) vgroup.getMarkAt(posDest);
			
			vgroup.createConnection(source, destination, false).weight.set(connection.weight.get());
		}
		
		vgroup.updateConnections();
	}
	
	@Override
	protected Shape primitifCopy(double dx, double dy, boolean shadow) {
		Path shape = new Path();

		switch(lineProperty.get()) {
			case StraightSolid:
				if(shadow) updateBezier(shape, dx, dy);
				else updateSolidLine(shape, dx, dy);
				break;
			case Bezier:
				updateBezier(shape, dx, dy);
				break;
			case BezierSolid:
				if(shadow) updateBezier(shape, dx, dy); 
				else updateSolidBezier(shape, dx, dy);
				break;
			case Straight:
				if(shadow) updateBezier(shape, dx, dy);
				else updatePolyline(shape, dx, dy);
				break;
			case TopBottom:
				updateTopBottom(shape, dx, dy);
				break;
			case Area:
				if(shadow) updateBezier(shape, dx, dy);
				else updateArea(shape, dx, dy);
				break;
		}
		
		return shape;
	}
	
	
	private void changeShape(LineTypeProperty.Type newType) {
		lineProperty.set(newType);
		primitif = createShape();
		shape.replaceBy(primitif);
		update();
	}
	
	@Override
	public void stealProperties(SimpleMark mark) {
		super.stealProperties(mark);

		if(mark instanceof LineConnectedCollection) {
			LineConnectedCollection vgroup = (LineConnectedCollection)mark;
			lineProperty = new LineTypeProperty(this, vgroup.lineProperty.get());
		}
	}
	
		
	private void updatePolyline(Path path, double dx, double dy) {
		path.getElements().clear();
		
		List<Double> points = new ArrayList<>();
		
		for(Mark component: components) {
			points.add(component.coords.getX() + dx);
			points.add(-component.coords.getY() + dy);
		}
		
		MoveTo moveTo = new MoveTo();
		moveTo.setX(points.get(0) + dx);
		moveTo.setY(points.get(1) + dy);
		path.getElements().add(moveTo);
		
		for(int i = 2; i < points.size(); i+=2) {
			LineTo lineTo = new LineTo();
			lineTo.setX(points.get(i) + dx);
			lineTo.setY(points.get(i+1) + dy);
			path.getElements().add(lineTo);
		}
	}
	
	private void updateTopBottom(Path path, double dx, double dy) {
		path.getElements().clear();
		List<Double> points1 = new ArrayList<>();
		List<Double> points2 = new ArrayList<>();
		
		if(components.size() > 2) {
			for(int i = 1 ; i < components.size(); ++i) {
				points1.add(components.get(i - 1).left() + dx);
				points1.add(-components.get(i - 1).top() + dy);
				
				points2.add(components.get(i).right() + dx);
				points2.add(-components.get(i).bottom() + dy);
			}
		}
		
		for(int i = 0; i < points1.size(); i+=2) {
			MoveTo moveTo = new MoveTo();
			moveTo.setX(points1.get(i) + dx);
			moveTo.setY(points1.get(i + 1) + dy);
			path.getElements().add(moveTo);
			
			LineTo lineTo = new LineTo();
			lineTo.setX(points2.get(i) + dx);
			lineTo.setY(points2.get(i+1) + dy);
			path.getElements().add(lineTo);
		}
	}
	
	private void updateBezier(Path path, double dx, double dy) {
		path.getElements().clear();
		
		List<Point2D> points = new ArrayList<>();
		for(Mark component: components) {
			points.add(new Point2D(component.coords.getX() + dx, -component.coords.getY() + dy));
		}
		
		CubicSpline spline = new CubicSpline(path);
		spline.setPoints(points);
	}
	
	private void updateSolidBezier(Path path, double dx, double dy) { // TODO:...
		path.getElements().clear();
		
		List<Point2D> points = new ArrayList<>();
		for(Mark component: components) {
			points.add(new Point2D(component.coords.getX() + dx, dy - component.top()));
		}
		for(int i = components.size() - 1; i >= 0; --i) {
			Mark component = components.get(i);
			points.add(new Point2D(component.coords.getX() + dx, dy - component.bottom()));
		}

		//Mark component = components.get(0);
		//points.add(new Point2D(component.coords.getX() + dx, -component.coords.getY() + dy + component.height()/2));
		
		CubicSpline spline = new CubicSpline(path);
		spline.setPoints(points);
		
		path.getElements().add(new ClosePath());
	}
	
	private void updateSolidLine(Path path, double dx, double dy) {
		path.getElements().clear();
		
		double[] points = new double[components.size()*4];
		int index = 0;
		for(int i = 0; i < components.size(); i++) {
			points[index++] = components.get(i).coords.getX();
			points[index++] = -components.get(i).top();//-components.get(i).coords.getY() + components.get(i).height()/2;
		}
		for(int i = components.size() - 1; i >= 0; i--) {
			points[index++] = components.get(i).coords.getX();
			points[index++] = -components.get(i).bottom();//-components.get(i).coords.getY() - components.get(i).height()/2;
		}
		
		MoveTo moveTo = new MoveTo();
		moveTo.setX(points[0] + dx);
		moveTo.setY(points[1] + dy);
		path.getElements().add(moveTo);
		
		for(int i = 2; i < points.length; i+=2) {
			LineTo lineTo = new LineTo();
			lineTo.setX(points[i] + dx);
			lineTo.setY(points[i+1] + dy);
			path.getElements().add(lineTo);
		}
		
		path.getElements().add(new ClosePath());
	}
	
	
	private void updateArea(Path path, double dx, double dy) {
		path.getElements().clear();
		
		List<Double> points = new ArrayList<>();
		
		for(Mark component: components) {
			points.add(component.coords.getX() + dx);
			points.add(-component.coords.getY() + dy);
		}
		points.add(components.get(components.size() - 1).coords.getX() + dx);
		points.add((double) 0);
		
		points.add(components.get(0).coords.getX() + dx);
		points.add((double) 0);
		
		MoveTo moveTo = new MoveTo();
		moveTo.setX(points.get(0) + dx);
		moveTo.setY(points.get(1) + dy);
		path.getElements().add(moveTo);
		
		for(int i = 2; i < points.size(); i+=2) {
			LineTo lineTo = new LineTo();
			lineTo.setX(points.get(i) + dx);
			lineTo.setY(points.get(i+1) + dy);
			path.getElements().add(lineTo);
		}
		
		path.getElements().add(new ClosePath());
	}
	
	@Override
	public Shadow getShadow(double dx, double dy, double delta) {
		if(curveSelected) {
			double d = dy*delta;
			return new Shadow(this, 0, d, primitifCopy(0, d, true));
		}
		else return super.getShadow(dx, dy, delta);
	}


	/*
	 * This is to create arbitrary connection between the nodes of a top collection
	 */
	public FlowConnection createConnection(ShapeMark origin, ShapeMark destination, boolean clever) {
		if(flowConnections == null) {
			childPropertyStructure.removeHeightSharing(origin);
			
			lineProperty.set(Type.None);
			lineProperty.getActiveProperty().set(false);
			
			strokePaint = null;
			paint = null;
			flowConnections = new FlowConnections(this);
		}
		
		FlowConnection connection = flowConnections.addConnection(origin, destination, clever);
		addLabel(connection);
		
		if(connection != null) {
			updateConnections();
			origin.addOutwardConnection(connection);
			destination.addInwardConnection(connection);
		}

		origin.updateLabels();
		
		return connection;
	}
	
	
	public void removeConnection(FlowConnection flowConnection) {
		flowConnections.remove(flowConnection);
		Shape path = connectionShapes.get(flowConnection);
		shape.remove(path);
		connectionShapes.remove(flowConnection);
		
		removeLabel(flowConnection);
	}
	
	public FlowConnections getFlowConnections() {
		return flowConnections;
	}
	
	@Override
	public boolean hasConnections() {
		return flowConnections != null && !flowConnections.isEmpty();
	}

			
	public boolean containtsConnection(ShapeMark origin, ShapeMark destination) {
		return(flowConnections != null && flowConnections.contains(new FlowConnection(this, origin, destination)));
	}
	
	
	public boolean hasCycle(ShapeMark from, ShapeMark to) { 
		if(flowConnections == null) return false;
		else if(from == to) return true;
		
		Set<FlowConnection> connections = flowConnections.from(from);
		if(connections.isEmpty()) return false;
		
		for(FlowConnection conn: connections) {
			if(hasCycle(conn.getDestination(), to)) return true;
		}
		
		return false;
	}

	
	@Override
	public void translate(Line lineTrace) {
		if(connectionSelected == null) {
			super.translate(lineTrace);
			updateExtraComponents();
		} else {			
			double x = pinX + lineTrace.getEndX() - lineTrace.getStartX() - connectionSelected.getOrigin().coords.getX();
			double y = pinY - lineTrace.getEndY() + lineTrace.getStartY() - connectionSelected.getOrigin().coords.getY();
			
					
			Mark mark = connectionSelected.getDestination();
			double xdest = x + mark.coords.getX();
			double ydest = y + mark.coords.getY();

			mark = connectionSelected.getOrigin();
			mark.coords.x.updateValue(x + mark.coords.getX());
			mark.coords.y.updateValue(y + mark.coords.getY());

			mark = connectionSelected.getDestination();
			mark.coords.x.updateValue(xdest);
			mark.coords.y.updateValue(ydest);					
									
			updateInteractor();
			updateExtraComponents();
		}
	}
	
	
	@Override
	public void setHighlight(boolean highlight, boolean enableControllers) {	
		if(connectionSelected == null) super.setHighlight(highlight, enableControllers);
		else { 
			if(!highlight) {   
				connectionSelected = null;
				updateFlowColoring();
			}
		}
	}
	
	public boolean isConnectionSelected() {
		return connectionSelected != null;
	}
	
	public void fullDelete() {
		if(connectionSelected != null) {
			removeConnection(connectionSelected);
			connectionSelected.detachFront();
			connectionSelected.detachBack();
			
			connectionSelected = null;
			
			updateConnections();
		}
		else {
			setHighlight(false, true);
			super.fullDelete();
		}
		
	}
	
	@Override
	public void pin() { 
		if(connectionSelected == null) super.pin();
	}
	
	public void selectConnection(FlowConnection connection) {
		if(connection != null) {
			pinX = connection.getOrigin().coords.getX();
			pinY = connection.getOrigin().coords.getY();
		}
		connectionSelected = connection;
		updateFlowColoring();
	}
	
	@Override
	public boolean monoselect(MarkSelection selectedMarks, Circle trace) { 
		// Handling line connections
		if(lineProperty != null && lineProperty.get() != LineTypeProperty.Type.None) {
			curveSelected = false;

			boolean consumed = super.monoselect(selectedMarks, trace);
			if(consumed) {
				return consumed;
			}
			
			if(shape.intersects(trace) && components.size() > 0) { 
				pinX = components.get(0).coords.getX();
				pinY = components.get(0).coords.getY();
					
				selectedMarks.add(this);
		
				curveSelected = true;
						
				return true;
			}

			return false;
		} 
		else if(flowConnections == null || trace == null) {
			return super.monoselect(selectedMarks, trace);
		}
		else { 
			// Handling Flow connections
			boolean consumed = super.monoselect(selectedMarks, trace);	
			
			if(consumed) {
				if(connectionSelected != null) { 
					connectionSelected = null;
					updateFlowColoring();
				}
				return consumed;
			}
			
			for(FlowConnection connection:flowConnections) {
				Shape path = connectionShapes.get(connection);
				Shape intersection = Shape.intersect(path, trace);
				if(!(intersection instanceof Path) || !((Path)intersection).getElements().isEmpty()) {
					selectedMarks.add(this);

					pinX = connection.getOrigin().coords.getX();
					pinY = connection.getOrigin().coords.getY();
					connectionSelected = connection;
					
					updateFlowColoring();
					return true;
				}				
			}
			
			connectionSelected = null;
			updateFlowColoring();
						
			return super.monoselect(selectedMarks, trace);
		}
	}

	@Override
	public boolean controlselect(MarkSelection selectedMarks, Circle trace) {	
		if(flowConnections != null) {
			connectionSelected = null;
			updateFlowColoring();			
		}

		return super.controlselect(selectedMarks, trace);
	}


	@Override
	public Property getProperty(PropertyName name) {
		if(name.isCurve()) return lineProperty;
		else if(name.isOpacity()) return flowOpacity;
		else if(name.isColoring()) return coloringScheme;
		else if(name.isFill()) return flowPaint;
		else return super.getProperty(name);		
	}
}
