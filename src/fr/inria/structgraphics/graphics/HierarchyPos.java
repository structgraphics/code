package fr.inria.structgraphics.graphics;

import java.util.TreeMap;

public class HierarchyPos extends TreeMap<Integer, Integer>{
	
	private Mark mark;
	
	public HierarchyPos(Mark mark) {
		super();
		this.mark = mark;		
		
		if(mark.getContainer() instanceof VisBody) {
			put(mark.level, mark.getContainer().getComponents().indexOf(mark));
			goUp((VisBody)mark.getContainer());
		}
	}

	public void goUp(VisBody mark) {
		if(mark.getContainer() instanceof VisBody) {
			put(mark.level, mark.getContainer().getComponents().indexOf(mark));
			goUp((VisBody)mark.getContainer());
		}
	}
	
	public int getBottomLevel() {
		return mark.level;
	}

	public Integer getIndexAt(int level) {
		return get(level);
	}	
}
