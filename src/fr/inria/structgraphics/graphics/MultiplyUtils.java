package fr.inria.structgraphics.graphics;

import java.util.ArrayList;

import fr.inria.structgraphics.types.AlignmentProperty.XSticky;
import fr.inria.structgraphics.types.AlignmentProperty.YSticky;
import fr.inria.structgraphics.types.DistributionProperty.Constraint;
import javafx.geometry.Point2D;

public class MultiplyUtils {
	
	public static ArrayList<Point2D> getAvailablePositions(Mark mark){
		VisBody visbody;
		
		if(!(mark.getContainer() instanceof VisBody)) {
			return null;
		} else visbody = (VisBody) mark.getContainer();
		
		double W = visbody.getRootWidth();
		double H = visbody.getRootHeight();
		
		ArrayList<Point2D> positions = new ArrayList<>();

		// TODO: I do not currently consider the case where I have constraints in both x and y directions		
		if(visbody.constraintXProperty.get() != Constraint.None) {
			// first do the ones on the right
			Mark last = visbody.getComponentAt(visbody.components.size() - 1);
			if(visbody.constraintXProperty.get() == Constraint.Distance) {
				double xpos = last.getGlobalX() + visbody.distanceXProperty.get(); 

				while(xpos + mark.width() < W) { // TODO: Not correct....
					positions.add(new Point2D(xpos - mark.getGlobalX(), 0));
					xpos += visbody.distanceXProperty.get();
				}
			} else {
				double xpos = last.getGlobalX() + Math.abs(last.width()) + visbody.distanceXProperty.get();
				double dx = Math.abs(mark.width()) + visbody.distanceXProperty.get();

				while(xpos  + mark.width() < W) {
					positions.add(new Point2D(xpos - mark.getGlobalX(), 0));
					xpos += dx;
				}
			}
			
			// And then the ones on the left
			if(visbody.alignXProperty.get() == YSticky.No) {
				Mark first = visbody.getComponentAt(0);
				if(visbody.constraintXProperty.get() == Constraint.Distance) {
					double xpos = first.getGlobalX() - visbody.distanceXProperty.get(); 

					while(xpos - mark.width() > 0) { // TODO: Not correct....
						positions.add(new Point2D(xpos - mark.getGlobalX(), 0));
						xpos -= visbody.distanceXProperty.get();
					}
				} else {
					double xpos = first.getGlobalX() - Math.abs(mark.width()) - visbody.distanceXProperty.get();
					double dx = -Math.abs(mark.width()) - visbody.distanceXProperty.get();

					while(xpos  - mark.width() > 0) {
						positions.add(new Point2D(xpos - mark.getGlobalX(), 0));
						xpos += dx;
					}
				}
			}
			
		} else if(visbody.constraintYProperty.get() != Constraint.None) {
			// first do the ones on the right
			Mark last = visbody.getComponentAt(visbody.components.size() - 1);
			if(visbody.constraintYProperty.get() == Constraint.Distance) {				
				double ypos = last.getGlobalY() + visbody.distanceYProperty.get(); 

				while(ypos + mark.height() < H) { // TODO: Not correct....
					positions.add(new Point2D(0, -ypos + mark.getGlobalY()));
					ypos += visbody.distanceYProperty.get();
				}
			} else {
				double ypos = last.getGlobalY() + Math.abs(last.height()) + visbody.distanceYProperty.get();
				double dy = Math.abs(mark.height()) + visbody.distanceYProperty.get();

				while(ypos  + mark.height() < H) {
					positions.add(new Point2D(0, -ypos + mark.getGlobalY()));
					ypos += dy;
				}
			}
			
			
			// And then the ones on the BOTTOM
			if(visbody.alignYProperty.get() == XSticky.No) {
				Mark first = visbody.getComponentAt(0);
				if(visbody.constraintYProperty.get() == Constraint.Distance) {
					double ypos = first.getGlobalY() - visbody.distanceYProperty.get(); 

					while(ypos - mark.height() > 0) { // TODO: Not correct....
						positions.add(new Point2D(0, -ypos + mark.getGlobalY()));
						ypos -= visbody.distanceYProperty.get();
					}
				} else {
					double ypos = first.getGlobalY() - Math.abs(mark.height()) - visbody.distanceYProperty.get();
					double dy = -Math.abs(mark.height()) - visbody.distanceYProperty.get();

					while(ypos  - mark.height() > 0) {
						positions.add(new Point2D(0, -ypos + mark.getGlobalY()));
						ypos += dy;
					}
				}
			}
			
		} // TODO: To deal with x, y hsaring constrraints.... 
		else {
			positions = null;
		}
		
		return positions;
	}
}
