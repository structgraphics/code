package fr.inria.structgraphics.graphics.splines;

import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Point2D;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;

/**
 * A cubic spline.
 * @author Theophanis Tsandilas
 */
public class CubicSpline {

	private List<Vector2D> backwardTangents = new ArrayList<Vector2D>();
	private List<Point2D> controlPoints = new ArrayList<Point2D>();
	private List<Vector2D> forwardTangents = new ArrayList<Vector2D>();
	private Path linePath;
	private Path path;
	private List<Point2D> pathPoints = new ArrayList<Point2D>();
	
	private Path2D awtpath; // Used to flatten the curve (not available methods in FX)

	// .3 is pretty good...
	private double tension = .3;
	
	
	public CubicSpline() {
		path = new Path();
	}
	
	public CubicSpline(Path path) {
		this.path = path;
	}
	
	public void clear(){
		backwardTangents.clear();
		controlPoints.clear();
		forwardTangents.clear();
		linePath = new Path();
		path.getElements().clear();
		awtpath = new Path2D.Double();
		
		pathPoints.clear();
	}
	
	public List<Vector2D> getBackwardTangents() {
		return backwardTangents;
	}

	public List<Point2D> getControlPoints() {
		return controlPoints;
	}

	/**
	 * @return
	 */
	public List<Vector2D> getForwardTangents() {
		return forwardTangents;
	}

	/**
	 * @return
	 */
	public Shape getLineShape() {
		return linePath;
	}

	/**
	 * @return a copy of the path's points
	 */
	public List<Point2D> getPathPoints() {
		return pathPoints;
	}

	/**
	 * @return
	 */
	public Shape getShape() {
		return path;
	}
	
	/**
	 * Connect the points into line segments.
	 * 
	 * @param points
	 */
	private void makeConnectedLines() {
		Point2D p;
		
		if(pathPoints.size() > 0) {
			p = pathPoints.get(0);
			linePath.getElements().add(new MoveTo(p.getX(), p.getY()));
		}
		
		for (int i = 1; i < pathPoints.size(); i++) {
			p = pathPoints.get(i);
			linePath.getElements().add(new LineTo(p.getX(), p.getY()));
		}
	}

	/**
	 * 
	 */
	private void makePiecewiseBezier() {
		//path.getElements().clear();

		Point2D p1, c1, c2, p2;
				
		int j = 0; // control points index
		for (int i = 0; i < pathPoints.size() - 1; i++) {
			p1 = pathPoints.get(i);
			c1 = controlPoints.get(j++); // get j, and increment it
			c2 = controlPoints.get(j++);
			p2 = pathPoints.get(i + 1);

			if(i == 0) path.getElements().add(new MoveTo(p1.getX(), p1.getY()));
			path.getElements().add(new CubicCurveTo(c1.getX(), c1.getY(), c2.getX(), c2.getY(), p2.getX(), p2.getY()));
			
			// AWT object
			awtpath.moveTo(p1.getX(), p1.getY());
			awtpath.curveTo(c1.getX(), c1.getY(), c2.getX(), c2.getY(), p2.getX(), p2.getY());
		}		
	}
	
	/**.
	 * 
	 * @param points
	 */
	public void setPoints(List<Point2D> points) {
		if(points == null || points.size() == 0) return;
	
		clear();
			
		pathPoints.addAll(points);

		// if a single point, draw a dot
		if (points.size() == 1) {
			final Point2D point = points.get(0);
			path.getElements().add(new MoveTo(point.getX(), point.getY()));
			path.getElements().add(new LineTo(point.getX(), point.getY()));
			
			// AWT object
			awtpath.moveTo(point.getX(), point.getY());
			awtpath.lineTo(point.getX(), point.getY());
		}
		// if two points, draw a line segment
		else if (points.size() == 2) { 	
			final Point2D point1 = points.get(0);
			final Point2D point2 = points.get(1);
			path.getElements().add(new MoveTo(point1.getX(), point1.getY()));
			path.getElements().add(new LineTo(point2.getX(), point2.getY()));
			
			// AWT object
			awtpath.moveTo(point1.getX(), point1.getY());
			awtpath.lineTo(point2.getX(), point2.getY());
			return;
		}
		else {
			// if three points or more, do the piecewise business...
			// construct piecewise cubic bezier curves that generate the catmull-rom

			// tack on an extra last point, to enable the calculation of the last tangent
			Point2D pSecondToLast = points.get(points.size() - 2);
			Point2D pLast = points.get(points.size() - 1);
			points.add(Vector2D.add(pLast, Vector2D.subtract(pLast, pSecondToLast)));

			// p0 is the previous point
			Point2D p0 = Vector2D.add(points.get(0), Vector2D.subtract(points.get(0), points.get(1)));
			// p1 is the current point
			Point2D p1 = points.get(0);
			// p2 is the next point
			Point2D p2;

			// add tangents and control points
			for (int i = 0; i < points.size() - 1; i++) {
				p2 = points.get(i + 1);

				// === Backwards ===
				// add a backward tangent for each point, equal to the point before minus the point
				// after
				final Vector2D bkwd = Vector2D.getScaled(tension * Vector2D.subtract(p0, p1).magnitude(),
						Vector2D.subtract(p0, p2));
				backwardTangents.add(bkwd);

				// make a backward control point by adding it to the current point
				controlPoints.add(Vector2D.add(p1, bkwd));

				// === Forwards ===
				// add a tangent for each point, that is equal to the point after minus the point before
				// length is equal to half the distance to the next point
				final Vector2D fwd = Vector2D.getScaled(tension * Vector2D.subtract(p2, p1).magnitude(), Vector2D
						.subtract(p2, p0));
				forwardTangents.add(fwd);

				// make a forward control point by adding it to the current point
				controlPoints.add(Vector2D.add(p1, fwd));

				// shift the points over
				p0 = p1;
				p1 = p2;
			}

			// delete first and last control points (as they are useless)
			controlPoints.remove(0);
			controlPoints.remove(controlPoints.size() - 1);

			// don't remove tangents

			// line segments
			makeConnectedLines();

			// general path, piecewise cubic bezier
			makePiecewiseBezier();
		}
		
	}

	public FlattenedPath getFlattenedPath() {
		return new FlattenedPath(awtpath);
	}

}

