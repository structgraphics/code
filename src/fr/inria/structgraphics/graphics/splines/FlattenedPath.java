package fr.inria.structgraphics.graphics.splines;

import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.util.ArrayList;

import javafx.geometry.Point2D;

public class FlattenedPath {

	private final static double FLATNESS = 1; // maximum error for flattening the curve
	
	private ArrayList<Line2D> segments = new ArrayList<Line2D>();
	private ArrayList<Double> cumlength = new ArrayList<Double>(); // Cumulative length
	private ArrayList<Double> angles = new ArrayList<Double>(); // In radius (not in degrees)
	
	private double length = 0;
	
	public FlattenedPath(Path2D awtPath) {		
		PathIterator pathIterator = awtPath.getPathIterator(null, FLATNESS);
		double[] coords = new double[6];
				
		double x = 0, y = 0;
		for( ; !pathIterator.isDone(); pathIterator.next()) {
			int type = pathIterator.currentSegment(coords);
			
			if(type == PathIterator.SEG_MOVETO) {
				x = coords[0];
				y = coords[1];
			}
			else {
				Line2D segment = new Line2D.Double(x, y, coords[0], coords[1]);
				double dist = segment.getP1().distance(segment.getP2());
				
				x = coords[0];
				y = coords[1];
	
				segments.add(segment);
				cumlength.add(length);
				length += dist;

				angles.add(calculateAngle(segment));
			}
		}
	}
	
	public FlattenedPath(ArrayList<Point2D> points) {
		for(int i = 1; i < points.size(); ++i) {
			Line2D segment = new Line2D.Double(points.get(i - 1).getX(), points.get(i - 1).getY(), 
					points.get(i).getX(), points.get(i).getY());
			double dist = segment.getP1().distance(segment.getP2());
		
			segments.add(segment);
			cumlength.add(length);
			length += dist;

			angles.add(calculateAngle(segment));
		}
	}

	public double getLength() {
		return length;
	}
		
	private double calculateAngle(Line2D segment) {
		return Math.atan2(segment.getY1() - segment.getY2(), segment.getX2() - segment.getX1());
	}
	
	private int getIndex(double l) {
		if(cumlength.size() == 0) return -1;
		else if(cumlength.size() == 1) return 0;
		
		for(int i = 1; i < cumlength.size(); ++i)
			if(l < cumlength.get(i)) return i - 1;
		
		return cumlength.size() - 1;
	}
	
	private double getLength(int index) {
		if(index == cumlength.size() - 1) 
			return length - cumlength.get(index);
		else return cumlength.get(index + 1) - cumlength.get(index);
	}
	
	public double getAngleInDegrees(double l) {
		return Math.toDegrees(getAngleInRads(l));
	}
	
	public double getAngleInRads(double l) {
		int index = getIndex(l);
		
		if(index == -1) return 0;
		else return angles.get(index);		
	}
	
	public Point2D getPoint(double l) {
		int index = getIndex(l);
		if(index == -1) return null;
		
		double x0 = segments.get(index).getX1();
		double y0 = segments.get(index).getY1();
		double x1 = segments.get(index).getX2();
		double y1 = segments.get(index).getY2();
				
		double ratio = (l - cumlength.get(index))/getLength(index);
		
		return new Point2D(x0 + ratio*(x1 - x0), y0 + ratio*(y1 - y0));
	}
	
}
