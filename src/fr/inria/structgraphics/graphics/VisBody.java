package fr.inria.structgraphics.graphics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.AlignmentXProperty;
import fr.inria.structgraphics.types.AlignmentYProperty;
import fr.inria.structgraphics.types.DeltaXProperty;
import fr.inria.structgraphics.types.DeltaYProperty;
import fr.inria.structgraphics.types.DistributionProperty;
import fr.inria.structgraphics.types.DistributionXProperty;
import fr.inria.structgraphics.types.DistributionYProperty;
import fr.inria.structgraphics.types.IdentifierProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.XMinProperty;
import fr.inria.structgraphics.types.YMinProperty;
import fr.inria.structgraphics.types.AlignmentProperty.XSticky;
import fr.inria.structgraphics.types.AlignmentProperty.YSticky;
import fr.inria.structgraphics.types.DistributionProperty.Constraint;
import fr.inria.structgraphics.types.ShapeProperty.Type;
import fr.inria.structgraphics.ui.tools.MarkSelection;
import fr.inria.structgraphics.ui.tools.SelectFilter;
import fr.inria.structgraphics.ui.utils.SortedMarkSet;
import fr.inria.structgraphics.ui.viscanvas.groupings.DefaultSharingStrategy;
import fr.inria.structgraphics.ui.viscanvas.groupings.PropertyStructure;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.BodyInteractor;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.ConstraintController;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.ConstraintXController;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.ConstraintYController;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;

public abstract class VisBody extends SimpleMark implements InvalidationListener {
		
	protected PropertyStructure childPropertyStructure;
	
	public AlignmentXProperty alignXProperty = new AlignmentXProperty(this);
	public AlignmentYProperty alignYProperty = new AlignmentYProperty(this);
	
	public DistributionXProperty constraintXProperty = new DistributionXProperty(this);
	public DistributionYProperty constraintYProperty = new DistributionYProperty(this);
	
	public DeltaYProperty distanceYProperty = new DeltaYProperty(this);
	public DeltaXProperty distanceXProperty = new DeltaXProperty(this);

	// TODO: add them!!!!!
	public YMinProperty minYProperty = new YMinProperty(this);
	public XMinProperty minXProperty = new XMinProperty(this);
	
	// These are additional properties used for interaction and visualization purposes
	protected BodyInteractor interactor;
	protected ConstraintController controllerX, controllerY;

	protected Group shapes  = new Group();
	
	protected boolean toUpdateLayout = true;
	
	public VisBody(Container parent, boolean toend) {
		super(parent, toend, 0, 0);
	
		group.getChildren().add(shapes);
		
		interactor = createInteractor();
		group.getChildren().add(interactor);
		
		addExtraComponents();
		
		controllerX  = new ConstraintXController(this);
		group.getChildren().add(controllerX);
		
		controllerY  = new ConstraintYController(this);
		group.getChildren().add(controllerY);
				
		
		alignXProperty.addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				if(components.isEmpty()) return;
				
				if(alignXProperty.get() == YSticky.Yes) {
						if(constraintXProperty.get() == Constraint.None) {
							childPropertyStructure.moveToCommon(new PropertyName(components.get(0).coords.x.getName()));
							
							for(Mark mark: components) { // TODO: needed all this?
								mark.coords.x.set(0);
								mark.coords.x.getActiveProperty().set(false);
							}
							//constraintXProperty.setValue(Constraint.None);
							//constraintXProperty.getActiveProperty().set(false);
						} else { 
							// TODO: ????
							components.get(0).coords.x.set(0);
							HierarchyPos pos = new HierarchyPos(components.get(0));
							getRootVirtualGroup().updateTreeLayout(pos);
							//
						}
				} else {
					for(Mark mark: components) {
						mark.coords.x.getActiveProperty().set(true);
					}
				}
						
				invalidateInteractor();
			}
		});
		
		alignYProperty.addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				if(components.isEmpty()) return;
				
				if(alignYProperty.get() == XSticky.Yes) {
						if(constraintYProperty.get() == Constraint.None) {
							childPropertyStructure.moveToCommon(new PropertyName(components.get(0).coords.y.getName()));
							
							for(Mark mark: components) {
								mark.coords.y.set(0);
								mark.coords.y.getActiveProperty().set(false);
							}
							// constraintYProperty.setValue(Constraint.None);
							// constraintYProperty.getActiveProperty().set(false);					
						} else { // TODO:???
							components.get(0).coords.y.set(0);
							HierarchyPos pos = new HierarchyPos(components.get(0));
							getRootVirtualGroup().updateTreeLayout(pos);
						}
				} else {
					for(Mark mark: components) {
						mark.coords.y.getActiveProperty().set(true);
					}
				}
				
				invalidateInteractor();
			}
		});	
	}
	
	protected abstract BodyInteractor createInteractor();
	
	protected void addExtraComponents() {
		
	}
	
	protected void updateExtraComponents() {
		
	}
	
	////////////////////////////////////////////////////////////
	public boolean isOrderConstrained() {
		if(constraintXProperty.get() == DistributionProperty.Constraint.None 
				&& constraintYProperty.get() == DistributionProperty.Constraint.None) return false;
		else return true;
	}
			
	public DistributionXProperty getConstraintXProperty() {
		return constraintXProperty;
	}
	
	public DistributionYProperty getConstraintYProperty() {
		return constraintYProperty;
	}
	
	public AlignmentXProperty getAlignXProperty() {
		return alignXProperty;
	}
	
	public AlignmentYProperty getAlignYProperty() {
		return alignYProperty;
	}
	
	
	public void setLevel(int level) {
		this.level = level;
		id = new IdentifierProperty(this, PropertyName.getPrefix(level) + "id");
	}
	
	@Override
	protected void updateBasicShape() { 
		labels.update();
	}
	
	public BodyInteractor getInteractor() {
		return interactor;
	}
	
	@Override
	public void updateShape() {
		super.updateShape(coords.getX(), coords.getY());
		
	}
	
	@Override
	public Point2D getReferencePoint(double cx, double cy) {
		return new Point2D(cx, -cy);
	}
			
	@Override
	public void initProperties() {
		if(coords != null) {						
			addProperty(coords.getXProperty());
			addProperty(coords.getYProperty());
		}
			
		//if(this instanceof VisCollection) {
		addProperty(alignXProperty);
		addProperty(alignYProperty);
		//}

		addProperty(constraintXProperty);
		addProperty(constraintYProperty);
							
		constraintXProperty.addListener(new ChangeListener<Constraint>() {
			@Override
			public void changed(ObservableValue observable, Constraint oldValue, Constraint newValue) {
				// TODO Auto-generated method stub				
				if(newValue == Constraint.None 
						|| newValue == Constraint.Distance) { // TODO: CORRECT?
					distanceXProperty.getActiveProperty().set(false);
					alignXProperty.set(YSticky.No);
				}
				else {						
					distanceXProperty.getActiveProperty().set(true);
					if(alignXProperty.get() == YSticky.Yes) {
						
						for(Mark mark: components) {
							mark.coords.x.getActiveProperty().set(true);
						}
						childPropertyStructure.moveToVariable(new PropertyName(components.get(0).coords.x.getName()));
						distanceXProperty.set(10);
					} else childPropertyStructure.moveToVariable(new PropertyName(components.get(0).coords.x.getName()));
					
					if(container instanceof VisBody) {
						((VisBody) container).childPropertyStructure.fixXSharing();
					}
				}				
			}
		});
		
		constraintYProperty.addListener(new ChangeListener<Constraint>() {
			@Override
			public void changed(ObservableValue observable, Constraint oldValue, Constraint newValue) {
				if(newValue == Constraint.None 
						|| newValue == Constraint.Distance) { // TODO: Correct?
					distanceYProperty.getActiveProperty().set(false);
					alignYProperty.set(XSticky.No);
				}
				else {					
					distanceYProperty.getActiveProperty().set(true);
					if(alignYProperty.get() == XSticky.Yes) {
						for(Mark mark: components) {
							mark.coords.y.getActiveProperty().set(true);
						}
						childPropertyStructure.moveToVariable(new PropertyName(components.get(0).coords.y.getName()));
						distanceYProperty.set(10);
					} else {
						childPropertyStructure.moveToVariable(new PropertyName(components.get(0).coords.y.getName()));
					}
					
					
					if(container instanceof VisBody) {
						((VisBody) container).childPropertyStructure.fixYSharing();
					}
				}
			}
		});
				
		addProperty(angle);
		
		// TODO: To remove them from the UI - make them shared???
		addProperty(distanceXProperty);
		addProperty(distanceYProperty);	
		
		/*
		// Update order when necessary!
		coords.x.addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				List<Mark> siblings = container.getComponents();
				
				if(container instanceof VisCollection && ((VisCollection) container).constraintXProperty.get() == Constraint.None) {
					((VisCollection)container).reorderChildren(true);
				}
			}
		});*/
	}
		
	public void initVisibility() {
		distanceXProperty.getActiveProperty().set(false);
		distanceYProperty.getActiveProperty().set(false);
	}
	
	
	public void updateInteractor() {
		double left = Double.MAX_VALUE, right = -Double.MAX_VALUE, top = Double.MAX_VALUE, bottom = -Double.MAX_VALUE;
//		double minX = Double.MAX_VALUE, maxX = -Double.MAX_VALUE, maxY = Double.MAX_VALUE, minY = -Double.MAX_VALUE;
	
		for(Mark mark:components) {
			left = Math.min(left, mark.getLeft());	
			right = Math.max(right, mark.getRight());
			top = Math.min(top, mark.getTop());
			bottom = Math.max(bottom, mark.getBottom());			
		}
		
		YSticky alignX = alignXProperty.get();
		
		if(alignX == YSticky.No) {
			interactor.left.set(Math.min(0, left));
			interactor.right.set(Math.max(0, right));
		} else {
			interactor.left.set(left);
			interactor.right.set(right);
		}
				
		XSticky alignY = alignYProperty.get();
		if(alignY == XSticky.No) {
			interactor.top.set(Math.min(0, top));
			interactor.bottom.set(Math.max(0, bottom));
		} else {
			interactor.top.set(top);
			interactor.bottom.set(bottom);			
		}

		interactor.refresh();
	}
	
	/*
	 * Re-orders all children based on 
	 */
	public void reorderChildren(boolean xaxis) { 
		if(xaxis && (alignXProperty.get() == YSticky.Yes || childPropertyStructure.isXShared() 
				|| constraintYProperty.get() != Constraint.None)
				|| !xaxis && (alignYProperty.get() == XSticky.Yes || childPropertyStructure.isYShared()
				|| constraintXProperty.get() != Constraint.None)) return;
			
		boolean ordered = true;
		
		if(xaxis) {
			for(int i = 1; i < components.size() && ordered; ++ i) {
				if(components.get(i).coords.getX() <  components.get(i - 1).coords.getX())
					ordered = false;				
			}
		} else {
			// TODO: CHECK
			for(int i = 1; i < components.size() && ordered; ++ i) {
				if(components.get(i).coords.getY() <  components.get(i - 1).coords.getY())
					ordered = false;				
			}
		}
		if(ordered) return;
		
		//TODO: This is incomplete. i need to take care of the properties structure and update the inspector?
		SortedMarkSet sorted = new SortedMarkSet(xaxis);
		sorted.addAll(components);
		
		for(Mark mark: components) {
			shapes.getChildren().remove(mark.group);
		}		
		components.clear();
				
		for(Mark mark: sorted) {
			components.add(mark);
		}
		
		for(Mark mark: sorted) {
			shapes.getChildren().add(mark.group);
		}
	
		updateReordering(sorted);
		
	}
	
	protected void updateReordering(Collection<Mark> marks) {
		refreshID();
		
		PropertyStructure structure = PropertyStructure.create(marks, childPropertyStructure);
		childPropertyStructure.destroyAllBindings();
		setPropertyStructure(structure);
		
		if(container instanceof VisBody) ((VisBody)container).updateReordering();
		else {
			VisFrame frame = (VisFrame) getRoot();
			frame.getInspector().updateOrder();
		}		
	}
	
	public void addToPropertyStructure(Mark mark) {// TODO...
		childPropertyStructure.rebuildBindings();
		
		if(container instanceof VisBody)
			((VisBody)container).addToPropertyStructure(mark);
	}
	
	
	@Override
	public boolean addToEnd(double x, double y) {
		if(constraintXProperty.get() != Constraint.None && x < components.get(0).coords.getX()
				|| constraintYProperty.get() != Constraint.None && y < components.get(0).coords.getY()) {
			return false;
		}
		else return true;
	}
	
	@Override
	public void addMark(Mark mark, boolean toend) { 
		mark.container = this;
	
		if(toend) {
			components.add(mark);
			shapes.getChildren().add(mark.group);
		}
		else {
			components.add(0, mark);
			shapes.getChildren().add(0, mark.group);
		}
		
		if(mark instanceof VisBody && ((VisBody)mark).getInteractor() != null)
			((VisBody)mark).getInteractor().redraw();
	}
		
	public void addToGroup(Mark mark) { // TODO....		
		if(mark instanceof VisBody) childPropertyStructure.addMark(mark, new DefaultSharingStrategy());
		else childPropertyStructure.addMark(mark);
				
		if(container instanceof VisBody)
			((VisBody)container).addToPropertyStructure(mark);
		
		// TODO: These listener should act after the property sharings are activated
		// Listen to changes of the children nodes
		mark.coords.x.addListener(this);
		mark.coords.y.addListener(this);
		mark.width.addListener(this);
		mark.height.addListener(this);
		
		updateInteractor();		
	}
	
	
	/*
	public void removeFromPropertyStructure(Mark mark) {
		childPropertyStructure.removeNestedMark(mark);
	}*/
	
	public void attachCopy(Mark mark/*, boolean sharedX, boolean sharedY*/) {
		
		// Listen to changes of the children nodes
		mark.coords.x.addListener(this);
		mark.coords.y.addListener(this);
		mark.width.addListener(this);
		mark.height.addListener(this);
		
		//mark.coords.xRef.addListener(this);
		//mark.coords.yRef.addListener(this);
		
		updateInteractor();
	}
	
	public void attach(Mark mark, boolean sharedX, boolean sharedY) {
		Container container = mark.getContainer();
		container.removeMark(mark);
	
		double xpos, ypos;
		
		RefX xref = mark.coords.xRef.get();
		RefY yref = mark.coords.yRef.get();

		if(mark instanceof VisBody) {
			xpos = mark.getCoords().getX();
			ypos = mark.getCoords().getY();
		}
		else {
			if(xref == RefX.Left) xpos = mark.left();
			else if (xref == RefX.Right) xpos = mark.right();
			else xpos = mark.centerX();
				
			if(yref == RefY.Top) ypos = mark.top();
			else if (yref == RefY.Bottom) ypos = mark.bottom();
			else ypos = mark.centerY();
		}

				
		mark.setPositionCoords(sharedX ? 0 : xpos - coords.getX(), 
				sharedY ? 0 : ypos - coords.getY());
						
		addMark(mark, true);
		
		// Listen to changes of the children nodes
		mark.coords.x.addListener(this);
		mark.coords.y.addListener(this);
		mark.width.addListener(this);
		mark.height.addListener(this);
	}
	
	public void refresh() {
		refreshID();
		updateInteractor();
	}
	
	@Override
	public void removeMark(Mark mark) {
		if(components.contains(mark)) components.remove(mark); 
		if(childPropertyStructure != null)
			childPropertyStructure.removeMark(mark);		
		
		refreshID();
		
		//stickToYAxis();		
		//stickToXAxis();
		//updateInteractor();
		//updateTreeLayout(components.get(0));
		
		if(container instanceof VisBody) {
			((VisBody)container).removeMark(mark);
		}
		
		refreshID();
		
		if(!getComponents().isEmpty()) invalidateControllerAt(0, stickToXAxis(), stickToYAxis());
	}
	
	
	@Override
	public void select(MarkSelection  selectedMarks, Shape trace, SelectFilter filter) {
		if(!filter.accept(selectedMarks.getMarks(), this)) return;
		
		if(interactor.intersects(trace)) {	
			pinX = coords.getX();
			pinY = coords.getY();				
			selectedMarks.add(this);
			
			return;
		}
		else super.select(selectedMarks, trace, filter);
	}
	
		
	@Override
	public boolean monoselect(MarkSelection selectedMarks, Circle trace) { // To review how to prioritize its selection or not...	
		for(int i = components.size() - 1; i >=0; --i) {
			if(((Mark)components.get(i)).controlselect(selectedMarks, trace)) return true;
		}
		
			
		// TODO : This is just to improve picking of lines. It's a last-moment hack!!! It need replacement!!!!
		for(int i = components.size() - 1; i >=0; --i) {
			Mark mark = (Mark)components.get(i);
			if(mark instanceof GenericShapeMark && ((GenericShapeMark)mark).shapeProperty.get() == Type.Line && mark.monoselect(selectedMarks, trace)) 
				return true;
		}
		
	
		if(interactor.intersects(trace)) {
			pin();
			selectedMarks.add(this);
			return true;
		}
		
		for(int i = components.size() - 1; i >=0; --i) {
			if(((Mark)components.get(i)).monoselect(selectedMarks, trace)) return true;
		}
		
		return false;
	}
	
	@Override
	public boolean controlselect(MarkSelection selectedMarks, Circle trace) {	
		for(int i = components.size() - 1; i >=0; --i) {
			if(((Mark)components.get(i)).controlselect(selectedMarks, trace)) {
				return true;
			}
		}
		
		if(controllerX.intersects(trace) || controllerY.intersects(trace)) {
			Mark mark = getHighlightedChild();
						
			if(mark!=null) {
				selectedMarks.setDragAccept(false);
				selectedMarks.add(mark);
				return true;
			}
		}
		
		if(interactor.intersectsCenter(trace)) {
			pinX = coords.getX();
			pinY = coords.getY();
			selectedMarks.add(this);
			return true;
		}
		
		return false;
	}
	
	
	@Override
	public void setHighlight(boolean highlight, boolean enableControls) { 
		this.highlighted = highlight;
		
		interactor.setHighlight(highlight);
		
		if(container instanceof VisBody) {
			VisBody vgroup = (VisBody)container;
			vgroup.highlighted(this, highlight);
		}		
	}
	
	@Override
	public String getType() {
		return "Group";
	}

	public void dispose() {
		container.removeMark(this);
		group.getChildren().clear(); // TODO: Is this OK?
	}
	
		
	protected void invalidateInteractor() {
		updateBasicShape();
		
		// TODO: I commented that. Is there any reason to exist?
//		updateInteractor(); 
//		updateXAxisOrigin();
//		updateYAxisOrigin();
		updateInteractor();	
		
		updateExtraComponents();

		/*
		if(container instanceof VisBody) {
			((VisBody)container).invalidateController(this);
			((VisBody)container).invalidateInteractor();
		}*/
	}
	
	protected void invalidateConstraints(Mark mark) {
		HierarchyPos pos = new HierarchyPos(mark);
		getRootVirtualGroup().updateTreeLayout(pos);
		
		/*
		int index = Math.max(components.indexOf(mark) - 1, 0);
		invalidateController(components.get(index));
		invalidateInteractor();*/
	}
		
	@Override
	public Mark getHighlightedChild() {
		if(highlighted) return this;
		for(Mark mark:components) {
			if(mark.getHighlightedChild() != null) return mark;
		}
		
		return null;
		
	}
	
	@Override
	public boolean isHighlighted() {
		if(getHighlightedChild() == null) return false;
		else return true;
	}
	
	@Override
	public void invalidated(Observable observable) {}
	
	
	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	public boolean stickToYAxis() { 
		if(components.isEmpty()) return false;
		if(alignXProperty.get() == YSticky.Yes) {
			if(constraintXProperty.get() == Constraint.None) {
				for(Mark mark: components) {
					mark.coords.x.set(0);
				}
			} else { 
				components.get(0).coords.x.set(0);
			}
			return true;
		} else return false;
	}
	
	public boolean stickToXAxis() {
		if(components.isEmpty()) return false;		
		if(alignYProperty.get() == XSticky.Yes) {
			if(constraintYProperty.get() == Constraint.None) {
				for(Mark mark: components) {
					mark.coords.y.set(0);
				}
			} else { 
				components.get(0).coords.y.set(0);
			}
			return true;
		} else return false;
	}
	
	
	public Mark getMarkAt(HierarchyPos pos) {
		Integer index = pos.getIndexAt(level - 1);
		if(index == null) return null;
		else {
			Mark mark = components.get(index);
			if(mark instanceof VisBody) {
				return ((VisBody)mark).getMarkAt(pos);
			} else return mark;
		}
	}
	
	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	// TODO....
	public void updateLayout(Mark mark) {	
		if(!toUpdateLayout) return;
		
		//Mark mark = (Mark)((Property)observable).getBean();
		HierarchyPos pos = new HierarchyPos(mark);
		getRootVirtualGroup().updateTreeLayout(pos);
		//invalidateControllerAt(components.indexOf(mark));		
	}
	
	
	// TODO: This works fine although I update everything!!!
	// I could only update invalidated nodes for higher performance (invisible though)!!!!
	public void updateTreeLayout(HierarchyPos pos) {
		boolean stickToY = stickToYAxis();
		boolean stickToX = stickToXAxis();
		////////////////////
		
		Integer index = pos.getIndexAt(level - 1);
		
		if(index == null) return;
		else if(level > pos.getBottomLevel() && toUpdateLayout) {
			for(Mark mark: components) {
				if(mark instanceof VisBody) {
					((VisBody)mark).updateTreeLayout(pos);
				}
			}
		}

		invalidateControllerAt(index, stickToX, stickToY);
	}
	
	
	protected void invalidateControllerAt(int index, boolean stickToX, boolean stickToY) {
		index =  Math.min(components.size() - 1, Math.max(index, 0)); 
		
		Mark mark = components.get(stickToY ? 0 : index);
		controllerX.updateMarkPositions(mark);
		
		mark = components.get(stickToX ? 0 : index);
		controllerY.updateMarkPositions(mark);
		
		mark = components.get(index);
		if(mark.getHighlightedChild() != null) { // TODO: any problems with that?	
			if(controllerX.isVisible()) controllerX.setMark(mark, true);
			if(controllerY.isVisible()) controllerY.setMark(mark, true);
		}
				
		invalidateInteractor();
	}
	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	
	
	public void addToOuterCollection(List<VisBody> list) {
		VisBody outer = getRootVirtualGroup();
		if(outer instanceof VisCollection) list.add(outer);
	}
	
	public void addToInnerCollections(List<VisBody> list) {
		VisBody outer = getRootVirtualGroup();
		if(outer instanceof VisCollection) {
			for(Mark mark: outer.components) {
				if(mark instanceof VisCollection)
					list.add((VisCollection)mark);
			}
			
			if(list.isEmpty()) list.add(outer);
		}
	}
					
	public int getLevel() {
		return level;
	}
	
	@Override
	public double width() {
		return interactor.getWidth();
	}

	@Override
	public double height() {
		return interactor.getHeight();
	}
	
	
	@Override
	public double getLeft() {
		return coords.getX() + interactor.left.get();
	}
	
	@Override
	public double getRight() {
		return coords.getX() + interactor.right.get();
	}
	
	@Override
	public double getTop() {
		return -(coords.getY() - interactor.top.get());
	}
	
	@Override
	public double getBottom() {
		return -(coords.getY() - interactor.bottom.get());
	}
	
	
	@Override
	public Shadow getShadow(double dx, double dy, double delta) {
		dx *= delta;
		dy *= delta;
		
		return new Shadow(this, dx, dy, interactor.createCopy(dx, dy));
	}
	
	public Map<PropertyName, Property> getSelfProperties() {
		Map<PropertyName, Property> map = new TreeMap<>();
		for(Property property: properties) map.put(new PropertyName(property.getName()), property);
		return map;
	}
	
	@Override
	public void stealProperties(SimpleMark mark) {
		super.stealProperties(mark);
		if(mark instanceof VisBody) {
			VisBody vgroup = (VisBody)mark;
			
			constraintXProperty.set(vgroup.constraintXProperty.get());
			constraintXProperty.getPublicProperty().set(vgroup.constraintXProperty.getPublicProperty().get());
			
			constraintYProperty.set(vgroup.constraintYProperty.get());
			constraintYProperty.getPublicProperty().set(vgroup.constraintYProperty.getPublicProperty().get());
			
			// TODO:
			distanceXProperty.getPublicProperty().set(vgroup.distanceXProperty.getPublicProperty().get());
			distanceYProperty.getPublicProperty().set(vgroup.distanceYProperty.getPublicProperty().get());
			distanceXProperty.getActiveProperty().set(vgroup.distanceXProperty.getActiveProperty().get());
			distanceYProperty.getActiveProperty().set(vgroup.distanceYProperty.getActiveProperty().get());
			
			distanceXProperty.set(vgroup.distanceXProperty.get());
			distanceYProperty.set(vgroup.distanceYProperty.get());
			
			
			alignXProperty.set(vgroup.alignXProperty.get());
			alignXProperty.getPublicProperty().set(vgroup.alignXProperty.getPublicProperty().get());
			
			alignYProperty.set(vgroup.alignYProperty.get());
			alignYProperty.getPublicProperty().set(vgroup.alignYProperty.getPublicProperty().get());
		}
	}
	
	
	protected abstract VisBody getInstance(Container container, boolean toEnd);
	
	@Override
	public SimpleMark createCopy(Container container, double dx, double dy) { 
		double x = coords.getX() + dx;
		double y = coords.getY() + dy;
		
		VisBody vgroup = getInstance(container, container.addToEnd(x, y));
		vgroup.setLevel(this.level);
		ReferenceCoords refCoords = new ReferenceCoords(RefX.Left, RefY.Bottom);
		vgroup.setRefCoords(refCoords);
		
		List<Mark> marks = new ArrayList<>();		
		for(Mark mark: components) {
			if(mark instanceof SimpleMark) {
				SimpleMark simpleMark = (SimpleMark)mark;
				SimpleMark copy = simpleMark.createCopy(vgroup, 0, 0);
				copy.initProperties();
				marks.add(copy);
			}
		}

		for(Mark mark: marks) {
			vgroup.attachCopy(mark/*, childPropertyStructure.isXShared(), childPropertyStructure.isYShared()*/);
		}
		
		vgroup.setPropertyStructure(PropertyStructure.create(marks, childPropertyStructure));
				
		vgroup.toUpdate(false); // Protect the parent tree from property updates
		vgroup.stealProperties(this);
		vgroup.coords.x.set(x);
		vgroup.coords.y.set(y);
		vgroup.coords.xRef.set(RefX.Left);
		vgroup.coords.yRef.set(RefY.Bottom);
		vgroup.toUpdate(true);

		return vgroup;
	}
		
		
	public double left() {
		double left;		
		left = coords.getX() + interactor.left.get(); // TODO : FREEEEE
		
		return left;
	}
		
	public double right() {
		double right;
		right = coords.getX() + interactor.right.get(); // TODO : FREEEEE
		
		return right;
	}
	
	public double centerX() {
		double center;
		center = coords.getX();
		
		return center;
	}
	
	public double top() {
		if(components.size() == 1 || constraintYProperty.get() != DistributionProperty.Constraint.None) {
			return coords.getY() + components.get(components.size() - 1).top();
		}
		else {
			return coords.getY() - interactor.top.get();
		}
	}
	
	public double bottom() {		
		if(components.size() == 1 || constraintYProperty.get() != DistributionProperty.Constraint.None) {
			return coords.getY() + components.get(0).bottom();
		}
		else {
			return coords.getY() - interactor.bottom.get();
		}
	}
	
	public double centerY() {
		double center;
		center = coords.getY();
		
		return center;
	}
	
	@Override
	public VisBody getRootVirtualGroup() {
		if(container instanceof VisBody)
			return ((VisBody) container).getRootVirtualGroup();
		else return this;
	}
	
	@Override
	public String getNestingPropertyName(String name) {
		StringBuffer buffer = new StringBuffer("");
		
		buffer.append(PropertyName.getPrefix(level));		
		buffer.append(name);
		
		VisGroup topGroup = getTopGroup();
		if(topGroup != null && topGroup != this) {
			String id = createID();
			String pid = topGroup.createID() + ".";
			buffer.append(id.replaceAll(pid, ""));			
		}
		
		return buffer.toString();
	}
		
	
	public boolean isExternal() {
		return !(container instanceof VisBody);
	}

	public void highlighted(Mark mark, boolean highlight) {  
		if(controllerX != null) controllerX.setMark(mark, highlight);
		if(controllerY != null) controllerY.setMark(mark, highlight);
		
		/*
		if(container instanceof VisBody)
			((VisBody) container).highlighted(this, highlight);*/
	}


	public void showGroupInteractors(boolean show) {
		super.showGroupInteractors(show);
		interactor.setVisible(show);
	}
	
	public VisBody getVirtualGroup() {
		return this;
	}
	
	@Override
	public void mouseRelease() { 
		if(controllerX != null) controllerX.unselectHandle();
		if(controllerY != null) controllerY.unselectHandle();
		if(container instanceof VisBody) container.getVirtualGroup().mouseRelease();
	}
	
	
	@Override
	public void bringToFront(Mark mark) {
		if(components.get(components.size()-1) == mark) return;
		components.remove(mark);
		components.add(mark);

		shapes.getChildren().remove(mark.group);		
		shapes.getChildren().add(mark.group);				
	}

	@Override
	public void sendToBack(Mark mark) {
		if(components.get(0) == mark) return;
		
		components.remove(mark);
		components.add(0, mark);
		
		shapes.getChildren().remove(mark.group);
		shapes.getChildren().add(0, mark.group);				
	}
	
	@Override
	public void updateReordering() {
		updateReordering(components);
	}

	public void setPropertyStructure(PropertyStructure propertyStructure) {
		childPropertyStructure = propertyStructure;
		childPropertyStructure.setContainer(this);
	}
	
	public boolean hasChildProperties() {
		return childPropertyStructure != null && !childPropertyStructure.isEmpty();
	}
	
	public PropertyStructure getChildPropertyStructure(){
		return childPropertyStructure;
	}

	
	// TODO: May need to change -- when axes are hidden, not possible to see what's going on
	public double getControlYPosition() {
		return 0;
	}
	
	public double getControlXPosition() {
		return 0;
	}
	
	public void toUpdate(boolean toUpdate) {
		this.toUpdateLayout = toUpdate;
	}

	
	@Override
	public Property getProperty(PropertyName name) {
		if(name.isDistributionX()) return constraintXProperty;
		else if(name.isDistributionY()) return constraintYProperty;
		else if(name.isDeltaX()) return distanceXProperty;
		else if(name.isDeltaY()) return distanceYProperty;
		else if(name.isStickyX()) return alignYProperty;
		else if(name.isStickyY()) return alignXProperty;
		else return super.getProperty(name);		
	}
}
