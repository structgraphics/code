package fr.inria.structgraphics.graphics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.IdentifierProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.DistributionProperty.Constraint;
import fr.inria.structgraphics.ui.tools.MarkSelection;
import fr.inria.structgraphics.ui.tools.SelectFilter;
import fr.inria.structgraphics.ui.viscanvas.groupings.CollectionPropertyStructure;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.control.TextField;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;

public abstract class Container {	
	
	protected List<Mark> components = new ArrayList<Mark>();
	protected Group group  = new Group();
	protected List<Property> properties = new ArrayList<Property>();
	protected Map<PropertyName, FlexibleListProperty> tabularProperties = null; // TODO: Not currently used. To remove?
	
	protected ReferenceCoords refCoords;
	
	public StringProperty id;
			
	protected final ChangeListener changeListener = new ChangeListener() {
		@Override
		public void changed(ObservableValue observable, Object oldValue, Object newValue) {
			update();
		}
	};
	
	public Container() {
		createIdProperty();
	}
		
	protected void createIdProperty() {
		id = new IdentifierProperty(this, "id");
	}
	
	public void refreshID() {
		for(Mark mark:components)
			mark.refreshID();
	}
	
	// TODO: To change....
	public void addProperty(Property property) {
		property.addListener(changeListener);
		properties.add(property);
	}
	
	public void removeProperty(Property property) { // TODO: I will probably need to also remove it (if needed from )
		property.removeListener(changeListener);
		properties.remove(property);
	}
	
	public void addPropertiesColumn(FlexibleListProperty property) {
		if(tabularProperties == null) tabularProperties = new TreeMap<>();
		
		tabularProperties.put(new PropertyName(property.getName()), property); // TODO: To add change listeners!!!
		
		property.addListener(changeListener);
		for(Property p:property) p.addListener(changeListener);
	}
	
	
	public boolean hasTabularProperties() {
		return tabularProperties != null;
	}
	
	/*
	 * Check is the mark should be added to the end of the childrent or not. By default it is true
	 */
	public boolean addToEnd(double x, double y) {
		return true;
	}
	
	public void setRefCoords(ReferenceCoords coords) {
		this.refCoords = coords;
	}
	
	public ReferenceCoords getRefCoords() {
		return refCoords;
	}
	
	public VisBody getRootVirtualGroup() {
		return null;
	}
	
	public VisBody getVirtualGroup() {
		return null;
	}
	
	public int leafs() {
		if(components == null || components.size() == 0) return 1;
		
		int counter = 0; 
		for(Mark mark:components) {
			counter += mark.leafs();
		}
		
		return counter;
	}
	
	public void update() { 
		updateProperties();
		updateAll();
	}
	
	public void initProperties() {
		if(refCoords != null) {			
			addProperty(refCoords.getContainerXRefProperty());
			addProperty(refCoords.getContainerYRefProperty());
		}
	}
	
	public void updateProperties() {
		// empty
	}
	
	public void addMark(Mark mark, boolean toend){
		mark.container = this;
		
		if(toend) {
			components.add(mark);
			group.getChildren().add(mark.group);
		}
		else {
			components.add(0, mark);
			group.getChildren().add(0, mark.group);
		}		
		
		refreshID();
	}
		
	public void removeMark(Mark mark) {
		components.remove(mark); 
		refreshID();
	}
	
	public Group getGroup(){
		return group;
	}
			
	protected void updateAll() {
		clear();
		updateShape();
		
		for(Container component: components)
			component.updateAll();
	}
	
	public void clear() {
		group.getTransforms().clear();
	}
	
	public List<Property> getProperties(){
		return properties;
	}
	
	public Map<PropertyName, Property> getSelfProperties() {
		Map<PropertyName, Property> map = new TreeMap<>();
		for(Property property: properties) map.put(new PropertyName(property.getName()), property);
		return map;
	}
	
	public Map<PropertyName, FlexibleListProperty> getTabularProperties(){
		return tabularProperties;
	}

	public void addLeafIDs(List<Property> ids){
		for(Mark mark:components)
			mark.addLeafIDs(ids);
	}
	
	public void addIDs(List<Property> ids, int level) {
		for(Mark mark:components)
			mark.addIDs(ids, level);
	}
	
	public List<Mark> getComponents(){
		return components;
	}
	
	public Mark getComponentAt(int i){
		return components.get(i);
	}
	
	public Container getComponent(String sid){
		for(Mark component: components) {
			Container mark = component.getComponent(sid);
			if(mark != null) return mark;
		}
		
		return null;
	}
	
	public abstract void updateShape();
	// protected abstract Point2D getReferencePoint(double cx, double cy, RefX containerXRef, RefY containerYRef);
	public abstract Point2D getReferencePoint(double cx, double cy);
	public abstract double getRefAngle(double x);
	
	public Point2D getOffsetPoint() {
		return getReferencePoint(0, 0);
	}
	
	// protected abstract double getRefAngle(double x, RefX containerXRef);

	public abstract String getName();
	public abstract String getType();
	
			
	public void select(MarkSelection selectedMarks, Shape trace, SelectFilter filter) {
		for(Mark mark: components)
			mark.select(selectedMarks, trace, filter);
	}
	
	public boolean monoselect(MarkSelection selectedMarks, Circle trace) {
		for(int i = components.size() - 1; i >=0; --i) {
			if(((Mark)components.get(i)).controlselect(selectedMarks, trace)) return true;
		}
		
		for(int i = components.size() - 1; i >=0; --i) {
			if(((Mark)components.get(i)).monoselect(selectedMarks, trace)) return true;
		}
		
		return false;
	}

	
	public ShapeMark getShape(double x, double y) {
		for(Mark mark: components) {
			double x_ = x - mark.coords.getX();
			double y_ = y - mark.coords.getY();
			
			ShapeMark mark_ = mark.getShape(x_, y_);
			if(mark_ != null) return mark_;
		}
				
		return null;
	}
	
	
	public double getRootWidth() {
		return 0;
	}
	
	public double getRootHeight() {
		return 0;
	}


	public void addTextField(TextField textField, boolean add) {
		if(add) group.getChildren().add(textField);
		else group.getChildren().remove(textField);
	}
	
	public void showGroupInteractors(boolean show) {
		for(Mark mark:components)
			mark.showGroupInteractors(show);
	}

	
	public void bringToFront(Mark mark) {}

	public void sendToBack(Mark mark) {}
	
	public void updateReordering() {}
	
	
	public boolean hasConnections() {
		return false;
	}

	
	public double getYIn(Container container) {
		return 0;
	}
	
	public double getXIn(Container container) {
		return 0;
	}
}

