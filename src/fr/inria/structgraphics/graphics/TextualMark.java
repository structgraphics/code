package fr.inria.structgraphics.graphics;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.FontSizeProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.ShapeProperty;
import fr.inria.structgraphics.types.TextProperty;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class TextualMark extends ShapeMark  {
	
	public TextProperty textProperty = new TextProperty(this, "your text");
	public FontSizeProperty fontSize = new FontSizeProperty(this, 14);
	
	protected TextualMark(Container parent, boolean toend, double w, double h){
		super(parent, toend, ShapeProperty.Type.Text, w, h);	
	}
	
	@Override
	public void initProperties() {
		super.initProperties();
		removeProperty(border);
		removeProperty(strokePaint);
		
		switch(coords.xRef.get()) {
			case Center: ((Text)primitif).setTextAlignment(TextAlignment.CENTER);
				break;
			case Left: ((Text)primitif).setTextAlignment(TextAlignment.LEFT);
				break;
			case Right: ((Text)primitif).setTextAlignment(TextAlignment.RIGHT);
				break;
			default:
				break;
		}	
		
		coords.xRef.addListener(new ChangeListener<RefX>() {
			@Override
			public void changed(ObservableValue<? extends RefX> observable, RefX oldValue, RefX newValue) {
				switch(newValue) {
					case Center: ((Text)primitif).setTextAlignment(TextAlignment.CENTER);
						break;
					case Left: ((Text)primitif).setTextAlignment(TextAlignment.LEFT);
						break;
					case Right: ((Text)primitif).setTextAlignment(TextAlignment.RIGHT);
						break;
					default:
						break;
				}
			}
		});

		
		//Text text = new Text(-Math.abs(width.get())/2, Math.abs(height.get())/2, "title");
		coords.yRef.addListener(new ChangeListener<RefY>() {
			@Override
			public void changed(ObservableValue<? extends RefY> observable, RefY oldValue, RefY newValue) {
				switch(newValue) {
					case Center: ((Text)primitif).setY(0);
						break;
					case Bottom: ((Text)primitif).setY(height.get()/2);
						break;
					case Top: ((Text)primitif).setY(-height.get()/2);
						break;
					default:
						break;
				}
			}
		});
		switch(coords.yRef.get()) {
			case Center: ((Text)primitif).setY(0);
				break;
			case Bottom: ((Text)primitif).setY(height.get()/2);
				break;
			case Top: ((Text)primitif).setY(-height.get()/2);
				break;
			default:
				break;
		}
		
		((Text)primitif).setFont(new Font(fontSize.get()));		
		fontSize.addListener(
				new ChangeListener<Number>() {
					@Override
					public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
						((Text)primitif).setFont(new Font(fontSize.get()));
					}
		});
				
		((Text)primitif).textProperty().bindBidirectional(textProperty);
		
		addProperty(textProperty);
		addProperty(fontSize);
	}
	
	@Override
	public void stealProperties(SimpleMark mark) {
		super.stealProperties(mark);
		if(mark instanceof TextualMark) {
			textProperty.set(((TextualMark)mark).textProperty.get());	
			textProperty.getPublicProperty().set(((TextualMark)mark).textProperty.getPublicProperty().get());
			
			fontSize.set(((TextualMark)mark).fontSize.get());	
			fontSize.getPublicProperty().set(((TextualMark)mark).fontSize.getPublicProperty().get());
		}
	}
	
	@Override
	protected Shape createShape() {
		Text text = new Text(-width.get()/2, height.get()/2, "title");

		text.wrappingWidthProperty().bind(width);
		//width.set(text.getLayoutBounds().getWidth());
		
		text.onMouseClickedProperty().set(new EventHandler<MouseEvent>() {
			TextField field = null;
			@Override
			public void handle(MouseEvent event) {
				if(event.getClickCount() > 1 && field == null) {
					field = new TextField(text.getText());
					field.translateXProperty().set(getGlobalX());
					field.translateYProperty().set(getRootHeight() - getGlobalY());
					getRoot().addTextField(field, true);
					field.setOnKeyPressed(new EventHandler<KeyEvent>() { 	
			            public void handle(KeyEvent ke) { 
			                if (ke.getCode().equals(KeyCode.ENTER)) {
			                	text.setText(field.getText()); 
			                	getRoot().addTextField(field, false);
			                	getRoot().update();
			                	field = null;
			                } else {
			                	
			                }
			            }
			        });

					
				} else if(field != null) {
                	getRoot().addTextField(field, false);
                	getRoot().update();
                	field = null;
				}
			}
		});
		
		return text;
	} 

	@Override
	protected void refreshShape() {
		Text text = (Text)primitif;
		text.xProperty().set(-width.get()/2); 

		switch(coords.yRef.get()) {
			case Center: ((Text)primitif).setY(0);
				break;
			case Bottom: ((Text)primitif).setY( height.get()/2);
				break;
			case Top: ((Text)primitif).setY(-height.get()/2);
				break;
			default:
				break;
		}		
	}
	
			
	@Override
	public String getName() {
		return shapeProperty.get().name();
	}

	@Override
	public String getType() {
		return shapeProperty.get().name();	}
				
	
	@Override
	public Shadow getShadow(double dx, double dy, double delta) {
		dx *= delta;
		dy *= delta;
		
		return new Shadow(this, dx, dy, new Rectangle(dx-Math.abs(width.get())/2, dy-Math.abs(height.get())/2, 
				Math.abs(width.get()), Math.abs(height.get()))); // TODO: ???
	}

	@Override
	public void setDefaults() {
		border.set(0);
		paint.set(Color.DIMGREY);
	}
	
	@Override
	public Property getProperty(PropertyName name) {
		if(name.isFontSize()) return fontSize;
		else if(name.isText()) return textProperty;
		else return super.getProperty(name);		
	}
}
