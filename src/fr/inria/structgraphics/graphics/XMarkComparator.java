package fr.inria.structgraphics.graphics;

import java.util.Comparator;

public class XMarkComparator implements Comparator<Mark> {

	@Override
	public int compare(Mark mark1, Mark mark2) {
		return ((Double)mark1.coords.x.get()).compareTo((Double)mark2.coords.x.get());
	}

}
