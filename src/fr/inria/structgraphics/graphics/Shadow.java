package fr.inria.structgraphics.graphics;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Rotate;

public class Shadow extends Group {

	private double dx, dy;
	private SimpleMark mark;
	
	public Shadow(SimpleMark mark, double dx, double dy) {
		this.dx = dx;
		this.dy = dy;
		this.mark = mark;
	}
	
	public Shadow(SimpleMark mark, double dx, double dy, Shape shape) {
		this(mark, dx, dy);
		
		Point2D r = mark.getRotationPoint();
		if(mark.angle.get() != 0) shape.getTransforms().add(new Rotate(mark.angle.get(), r.getX() + dx, r.getY() + dy));
		
		getChildren().add(shape);
		
		highlight(false);
	}
	
	public Shadow(SimpleMark mark, double dx, double dy, Group group) {
		this(mark, dx, dy);
		
		Point2D r = mark.getRotationPoint();
		if(mark.angle.get() != 0) this.getTransforms().add(new Rotate(mark.angle.get(), r.getX() + dx, r.getY() + dy));
		
		getChildren().addAll(group.getChildren());
		highlight(false);
	}
	
	public void setStroke(Paint paint) {
		for(Node shape: getChildren()) {
			if(shape instanceof Shape) ((Shape)shape).setStroke(paint);
		}
	}

	public void setStrokeWidth(double border) {
		for(Node shape: getChildren()) {
			if(shape instanceof Shape) ((Shape)shape).setStrokeWidth(border);
		}
	}

	public void setFill(Paint paint) {
		for(Node shape: getChildren()) {
			if(shape instanceof Shape) ((Shape)shape).setFill(paint);
		}
	}
	
	private void highlight(boolean highlight) {
		setStrokeWidth(highlight ? 1.8 : 0.7);
		if(mark instanceof VisCollection) setFill(null);
		else setFill(new Color(1, 1, 1, 0.5));
		setStroke(highlight ? Color.CORNFLOWERBLUE : Color.CORNFLOWERBLUE.brighter());
	}
	
	public boolean intersects(Shape trace) {
		if(trace == null) {
			highlight(false);
			return false;
		}
		
		for(Node shape: getChildren()) {
			if(shape instanceof Shape) {
				Shape intersection = Shape.intersect((Shape) shape, trace);			
				if(!(intersection instanceof Path) || !((Path)intersection).getElements().isEmpty()) {
					highlight(true);
					return true;
				}		
			}
		}
		
		highlight(false);
		return false;
	}
	
	public double getDeltaX() {
		return dx;
	}
	
	public double getDeltaY() {
		return dy;
	}
	
	public SimpleMark createMark() {
		return mark.createCopy(dx, -dy);
	}
}
