package fr.inria.structgraphics.graphics;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.ContainerRefXProperty;
import fr.inria.structgraphics.types.ContainerRefYProperty;

public class ReferenceCoords {

	public enum RefX {
		Left, Right, Center
	}

	public enum RefY {
		Center, Top, Bottom
	}
	
	public ContainerRefXProperty containerXRef = new ContainerRefXProperty(this);
	public ContainerRefYProperty containerYRef = new ContainerRefYProperty(this);
	
	/*
	public ComponentRefXProperty xRef = new ComponentRefXProperty(this);
	public ComponentRefYProperty yRef = new ComponentRefYProperty(this);
	*/
		
	public ReferenceCoords() {}

	
	public ReferenceCoords(String contRefx, String contRefy) {
		setXRef(contRefx);
		setYRef(contRefy);
	}
	
	/*
	public ReferenceCoords(String contRefx, String compRefx, String contRefy, String compRefy) {
		setXRef(contRefx, compRefx);
		setYRef(contRefy, compRefy);
	}
	*/
			
	public ReferenceCoords(ReferenceCoords coords) {
		containerXRef = coords.containerXRef;
		containerYRef = coords.containerYRef;		
		
	//	xRef = coords.xRef;
	//	yRef = coords.yRef;	
	}

	public ReferenceCoords(RefX contRefX, RefY contRefY) {
		containerXRef.set(contRefX);
		containerYRef.set(contRefY);
	}
	
	/*
	public ReferenceCoords(RefX contRefX, RefX refX, RefY contRefY, RefY refY) {
		containerXRef.set(contRefX);
		xRef.set(refX);
		containerYRef.set(contRefY);
		yRef.set(refY);
	}
	*/
	
	/*
	public void setXRef(String contRef, String componentRef) {
		if(contRef.contains("left")) containerXRef.set(RefX.Left);
		else if(contRef.contains("right")) containerXRef.set(RefX.Right);
		else containerXRef.set(RefX.Center);	
		
		if(componentRef.equals("left")) xRef.set(RefX.Left);
		else if(componentRef.equals("right")) xRef.set(RefX.Right);
		else xRef.set(RefX.Center);
	}
	
	public void setYRef(String contRef, String componentRef) {
		if(contRef.contains("top")) containerYRef.set(RefY.Top);
		else if(contRef.contains("bottom")) containerYRef.set(RefY.Bottom);
		else containerYRef.set(RefY.Center);
		
		if(componentRef.equals("top")) yRef.set(RefY.Top);
		else if(componentRef.equals("bottom")) yRef.set(RefY.Bottom);
		else yRef.set(RefY.Center);
	}*/

	public static RefX getXRef(String contRef) {
		if(contRef.equalsIgnoreCase("left")) return RefX.Left;
		else if(contRef.equalsIgnoreCase("right")) return RefX.Right;
		else return RefX.Center;	
	}
	
	public static RefY getYRef(String contRef) {
		if(contRef.equalsIgnoreCase("top")) return RefY.Top;
		else if(contRef.equalsIgnoreCase("bottom")) return RefY.Bottom;
		else return RefY.Center;
	}
	
	public void setXRef(String contRef) {
		if(contRef.contains("left")) containerXRef.set(RefX.Left);
		else if(contRef.contains("right")) containerXRef.set(RefX.Right);
		else containerXRef.set(RefX.Center);	
	}
	
	public void setYRef(String contRef) {
		if(contRef.contains("top")) containerYRef.set(RefY.Top);
		else if(contRef.contains("bottom")) containerYRef.set(RefY.Bottom);
		else containerYRef.set(RefY.Center);
	}
	
	public ContainerRefXProperty getContainerXRefProperty() {
		return containerXRef;
	}
	
	public ContainerRefYProperty getContainerYRefProperty() {
		return containerYRef;
	}
	
	public RefX getContainerXRef() {
		return containerXRef.get();
	}
	
	public RefY getContainerYRef() {
		return containerYRef.get();
	}
	
	/*
	public ComponentRefXProperty getXRefProperty() {
		return xRef;
	}
	
	public ComponentRefYProperty getYRefProperty() {
		return yRef;
	}
	
	public RefX getXRef() {
		return xRef.get();
	}
	
	public RefY getYRef() {
		return yRef.get();
	}
	*/
}

