package fr.inria.structgraphics.graphics;

import java.util.Map;
import java.util.TreeMap;

import com.google.common.io.LineProcessor;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.AlignmentProperty.XSticky;
import fr.inria.structgraphics.types.AlignmentProperty.YSticky;
import fr.inria.structgraphics.ui.tools.MarkSelection;
import fr.inria.structgraphics.ui.tools.SelectFilter;
import fr.inria.structgraphics.ui.viscanvas.groupings.CollectionPropertyStructure;
import fr.inria.structgraphics.ui.viscanvas.groupings.ReferenceStructureCreator;
import fr.inria.structgraphics.ui.viscanvas.groupings.XReferenceStructure;
import fr.inria.structgraphics.ui.viscanvas.groupings.YReferenceStructure;
import fr.inria.structgraphics.ui.viscanvas.groupings.Grouping.Type;
import javafx.beans.Observable;
import javafx.beans.property.Property;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;

public abstract class ConnectedCollection extends VisCollection {
	protected Shape primitif;
	protected Shape ghost;

	protected boolean curveSelected = false;
	
	public ConnectedCollection(Container parent, boolean toend) {
		super(parent, toend);	
	}
		
	
	public ConnectedCollection(Container parent, boolean toend, ObservableList<Mark> marks) {
		super(parent, toend);	
		
		ReferenceStructureCreator creator = new ReferenceStructureCreator(marks, Type.Collection);
		XReferenceStructure xstruct = creator.getXReferenceStructure();
		YReferenceStructure ystruct = creator.getYReferenceStructure();
		
		setLevel(marks.get(0).getLevel() + 1); // TODO: To remove???
		ReferenceCoords refCoords = new ReferenceCoords(RefX.Left, RefY.Bottom);
		setRefCoords(refCoords);

		getConstraintXProperty().set(xstruct.constraint);
		getConstraintYProperty().set(ystruct.constraint);
		
		PositionCoords coords = new PositionCoords(this, xstruct.getRefValue(), ystruct.getRefValue());
		setCoords(coords);
		
		for(Mark mark: marks) {
			mark.getCoords().xRef.set(xstruct.getRef());
			mark.getCoords().yRef.set(ystruct.getRef());
			attach(mark, xstruct.isShared(), ystruct.isShared());
		}
		
		refresh();
		
		CollectionPropertyStructure propertyStructure = new CollectionPropertyStructure(marks);
		setPropertyStructure(propertyStructure);
				
		initProperties();
				
	//	initializeShape();
	}	
		
	public boolean isCurveSelected() {
		return curveSelected;
	}
	
	public boolean isSingleSibling() {
		if(!(container instanceof VisCollection) || 
				((VisCollection)container).alignXProperty.get() == YSticky.No &&
				((VisCollection)container).alignYProperty.get() == XSticky.No)
			return true;
		else return false;
	}
	
	
	@Override
	public void initProperties() {		
		border.set(1.5);
		addProperty(border);
		addProperty(strokePaint);
		
		super.initProperties();		
	}
	

	@Override
	protected void updateBasicShape() {
		labels.update();
		refreshShape();
		updateGhost();
	}
	
	protected abstract Shape createShape();
	protected abstract void refreshShape();
	
		
	/*
	@Override
	public boolean monoselect(MarkSelection selectedMarks, Circle trace) { // To review how to prioritize its selection or not...			
		
		curveSelected = false;

		boolean consumed = super.monoselect(selectedMarks, trace);
		if(consumed) {
			return consumed;
		}
		
		if(shape.intersects(trace) && components.size() > 0) { 
			pinX = components.get(0).coords.getX();
			pinY = components.get(0).coords.getY();
				
			selectedMarks.add(this);
	
			curveSelected = true;
					
			return true;
		}

		return false;
	}*/
	
	@Override
	public void pin() {
		if(!curveSelected) super.pin();
	}
	

	@Override
	public void select(MarkSelection selectedMarks, Shape trace, SelectFilter filter) {
		curveSelected = false;
		super.select(selectedMarks, trace, filter);
	}	
	
	
	@Override
	public boolean controlselect(MarkSelection selectedMarks, Circle trace) {	
		curveSelected = false;
		return super.controlselect(selectedMarks, trace);
	}
	
	
	@Override
	public void setHighlight(boolean highlight, boolean enableControllers) {
		super.setHighlight(highlight, enableControllers);
		shape.setHighlight(highlight);
	}
	
	
	@Override
	public void translate(Line lineTrace) {
		if(!curveSelected) {
			super.translate(lineTrace);
		}
		else if(components.size() > 0){		
			double x = pinX + lineTrace.getEndX() - lineTrace.getStartX() - components.get(0).coords.getX();
			double y = pinY - lineTrace.getEndY() + lineTrace.getStartY() - components.get(0).coords.getY();
			
			if(childPropertyStructure.isXShared()) {
				Mark mark = components.get(0);
				mark.coords.x.set(x + mark.coords.getX());
			} else for(Mark mark: components) {
				mark.coords.x.set(x + mark.coords.getX());
			}

			if(childPropertyStructure.isYShared()) {
				Mark mark = components.get(0);
				mark.coords.y.set(y + mark.coords.getY());
			} else for(Mark mark: components) {
				mark.coords.y.set(y + mark.coords.getY());
			}
			
			updateInteractor();
			//updateBasicShape();
		}
	}
	
	@Override
	public void invalidated(Observable observable) {
		super.invalidated(observable);
		updateBasicShape();
	}
	
	@Override
	public Map<PropertyName, Property> getSelfProperties() {
		Map<PropertyName, Property> map = new TreeMap<>();
		for(Property property: properties) map.put(new PropertyName(property.getName()), property);
		return map;
	}
		
	protected abstract Shape primitifCopy(double dx, double dy, boolean shadow); 
}
