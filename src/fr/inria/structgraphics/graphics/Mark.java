package fr.inria.structgraphics.graphics;

import java.util.List;
import java.util.Map;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.graphics.controls.Control;
import fr.inria.structgraphics.types.FillColorProperty;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.HeightProperty;
import fr.inria.structgraphics.types.OrderProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.RatioLockProperty;
import fr.inria.structgraphics.types.RatioProperty;
import fr.inria.structgraphics.types.RotationProperty;
import fr.inria.structgraphics.types.StrokeColorProperty;
import fr.inria.structgraphics.types.StrokeWidthProperty;
import fr.inria.structgraphics.types.WidthProperty;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable;
import fr.inria.structgraphics.ui.tools.MarkSelection;
import fr.inria.structgraphics.ui.tools.SelectFilter;
import fr.inria.structgraphics.ui.utils.BidirectionalBindings;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.ConstraintController;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.LabelCollection;
import fr.inria.structgraphics.ui.viscanvas.groupinteractors.ConstraintController.ConstraintHandle;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import javafx.util.Pair;

public abstract class Mark extends Container {

	public WidthProperty width;
	public HeightProperty height;
	
	public RatioProperty hwratio;
	public RatioLockProperty ratiolock;
	
	protected boolean highlighted = false;
	
	protected double pinX, pinY; 
	
	public PositionCoords coords;
	protected Container container;

	protected ComplexShape shape = new ComplexShape();

	protected Group controls = new Group();
	protected Control selectedControl = null;
	
	protected LabelCollection labels;
	
	// Distribution constraints
	protected ConstraintController constraintController = null;
	protected ConstraintController.ConstraintHandle constraintHandler = null;
	
	protected int level = 0;
	
	public StrokeWidthProperty border = new StrokeWidthProperty(this, 0);
	public FillColorProperty paint = new FillColorProperty(this);
	public StrokeColorProperty strokePaint = new StrokeColorProperty (this);
	public RotationProperty angle = new RotationProperty(this);
	
	//protected DoubleProperty distance = new SimpleDoubleProperty(), gap = new SimpleDoubleProperty();
	
	public Mark(Container container, boolean toend, double w, double h){
		super();
		
		if(container != null) {
			container.addMark(this, toend);
			container.refreshID();
		}
		
		group.getChildren().add(shape);
		group.getChildren().add(controls);
		controls.setVisible(false);
		
		labels = new LabelCollection(this);
		group.getChildren().add(labels);
		
		this.width = new WidthProperty(this, w);
		this.height = new HeightProperty(this, h);
		this.hwratio = new RatioProperty(this, "h-w ratio", w == 0 ? 1 : h/w);
		this.ratiolock = new RatioLockProperty(this, "lock", false);
		
		/*
		 * Support ratio dependencies between height and width
		 */
		ChangeListener<Number> widthListener = new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				hwratio.set(height.doubleValue()/newValue.doubleValue());
			}
		};
		ChangeListener<Number> heightListener = new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				hwratio.set(newValue.doubleValue()/width.doubleValue());
			}
		};
		width.addListener(widthListener);
		height.addListener(heightListener);
		
		ratiolock.addListener(new ChangeListener<Boolean>() {
			private Pair<ChangeListener<Number>, ChangeListener<Number>> listeners = null;
			
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(newValue) {
					width.removeListener(widthListener);
					height.removeListener(heightListener);
					
					listeners = BidirectionalBindings.bindBidirectional(width, height, 
						new ChangeListener<Number>() {
							@Override
							public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
								height.set(newValue.doubleValue()*hwratio.get());
							}
						}, 
						new ChangeListener<Number>() {
							@Override
							public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
								width.set(newValue.doubleValue()/hwratio.get());
							}
						}
					);
				} else {
					// TODO: remove binding and add the following
					BidirectionalBindings.unbindBidirectional(width, height, listeners);
					
					width.addListener(widthListener);
					height.addListener(heightListener);
				}
			}
		});
				
	}
	
	public void addSelfProperties(boolean toCollection, Map<PropertyName, FlexibleListProperty> table) { // TODO: Name IDS are fucked up !!!!		
		for(Property property: properties) {
			FlexibleListProperty list = table.get(new PropertyName(PropertyName.getCleanName(property.getName())));
			if(list == null) {
				list = FlexibleListProperty.createList(this, property);
				table.put(new PropertyName(list.getName()), list);
			}
			else {
				if(isFirst()) list.add(0, property);
				else list.add(property);
			}
		}
	}
	
	
	public boolean removeSelfProperties(Map<PropertyName, FlexibleListProperty> table) { 
		boolean removed = false;
		for(Property property: properties) {
			FlexibleListProperty list  = table.get(new PropertyName(property.getName()));
			
			if(list != null) {
				if(list.removeProperty(property)) { 
					removed = true;
				}
			}
		}
		
		return removed;
	}
	
	
	public Mark(Container container){
		this(container, true, 0, 0);
	}
	
	public boolean isFirst() {
		return container.components.indexOf(this) == 0;
	}
	
	@Override
	public void addLeafIDs(List<Property> ids){
		if(components.isEmpty()) ids.add(id);
		else super.addLeafIDs(ids);
	}
	
	
	@Override
	public void addIDs(List<Property> ids, int level) {
		if(components.isEmpty()) {
			if(this.level == level) ids.add(id);
			else if(container instanceof VisCollection) ((VisCollection)container).addID(ids, level);
		}
		else super.addIDs(ids, level);
	}
	
	public int getBottomLevel() {
		if(components.isEmpty()) return level;
		Mark component = components.get(0);
		return component.getBottomLevel();
	}
	
	@Override
	public void initProperties() {
		super.initProperties();
		
		if(coords != null) {			
			addProperty(coords.getXRefProperty());
			addProperty(coords.getYRefProperty());			
			
			Mark mark = this;
			
			coords.xRef.addListener(new ChangeListener<RefX>() {
				@Override
				public void changed(ObservableValue<? extends RefX> observable, RefX oldValue, RefX newValue) {
					if(container instanceof VisBody) {
						((VisBody) container).invalidateConstraints(mark);
						((VisBody)container).updateInteractor();
						return;
					}
						
					double dx = 0;
					
					switch(oldValue) {
						case Center:
							if(newValue == RefX.Left) dx = -width.get()/2;
							else if(newValue == RefX.Right) dx = width.get()/2;
							break;	
						case Left:
							if(newValue == RefX.Center) dx = width.get()/2;
							else if(newValue == RefX.Right) dx = width.get();
							break;
						case Right:
							if(newValue == RefX.Center) dx = -width.get()/2;
							else if(newValue == RefX.Left) dx = -width.get();
							break;
						default: break;
					}
					
					coords.x.set(coords.getX() + dx);
					container.updateAll();
				}
			});
			coords.yRef.addListener(new ChangeListener<RefY>() {
				@Override
				public void changed(ObservableValue<? extends RefY> observable, RefY oldValue, RefY newValue) {
					if(container instanceof VisBody) {
						((VisBody) container).invalidateConstraints(mark);
						((VisBody)container).updateInteractor();
						return;
					}
					
					double dy = 0;
					
					switch(oldValue) {
						case Center:
							if(newValue == RefY.Bottom) dy = -height.get()/2;
							else if(newValue == RefY.Top) dy = height.get()/2;
							break;	
						case Bottom:
							if(newValue == RefY.Center) dy = height.get()/2;
							else if(newValue == RefY.Top) dy = height.get();
							break;
						case Top:
							if(newValue == RefY.Center) dy = -height.get()/2;
							else if(newValue == RefY.Bottom) dy = -height.get();
							break;
						default: break;
					}
					
					coords.y.set(coords.getY() + dy);
					container.updateAll();	
				}
			});
			
			//addProperty(new DummyProperty(this));
			
			addProperty(coords.getXProperty());
			addProperty(coords.getYProperty());
		}
		
		/*
		coords.x.addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				List<Mark> siblings = container.getComponents();
				
				if(container instanceof VisCollection && ((VisCollection) container).constraintXProperty.get() == Constraint.None) {
					((VisCollection)container).reorderChildren(true);
				}
			}
		});*/
	}
	
	protected String createID() {
		int pos = container.getComponents().indexOf(this) + 1;
		if(container instanceof Mark) return ((Mark) container).createID() + "." + pos;
		else return "" + pos;
	}
	
	@Override
	public void refreshID() {
		id.set(createID());
		
		for(Mark mark:components)
			mark.refreshID();		
	}
		
	public void setLevel(int level) {
		this.level = level;
	}
	
	public int getLevel() {
		return level;
	}
	
	protected void initControls() {
		
	}
	
	public void dispose() {
		group.getChildren().remove(labels);
		container.removeMark(this);
		group.getChildren().remove(shape); // TODO: Is this OK?
	}
	
	public void updateLabels() {
		labels.update();
	}
	
	public boolean exists() {
		return container != null;
	}
	
	public void fullDelete() {
		setHighlight(false, true);
		
		dispose();
		
		if(container instanceof VisBody) {
			if(container.getComponents().isEmpty())
				((VisBody) container).fullDelete();
		}
		
		container = null;
	}
	
	public void setCoords(PositionCoords coords) {
		this.coords = coords;
	}

	public void setPositionCoords(double x, double y) {
		coords.x.updateValue(x);
		coords.y.updateValue(y);
	}
	
	public void setPositionX(double x) {
		coords.x.set(x);
	}

	public void setPositionY(double y) {
		coords.y.set(y);
	}

	
	public PositionCoords getCoords() {
		return coords;
	}
	
	public void setBorder(int border) {
		this.border.setValue(border);;
	}
	
	public void setAngle(int angle) {
		this.angle.set(angle);
	}
	
	public abstract double width();
	public abstract double height();
	
	public double left() {
		if(coords.getXRef() == RefX.Center) return coords.getX() - width()/2;
		else if(coords.getXRef() == RefX.Right) return coords.getX() - width();
		else return coords.getX();
	}
		
	public double right() {
		if(coords.getXRef() == RefX.Center) return coords.getX() + width()/2;
		else if(coords.getXRef()  == RefX.Left) return coords.getX() + width();
		else return coords.getX();
	}
	
	public double centerX() {
		if(coords.getXRef() == RefX.Center) return coords.getX();
		else if(coords.getXRef() == RefX.Left) return coords.getX() + width()/2;
		else return coords.getX() - width()/2;
	}
	
	public double top() {
		if(coords.getYRef() == RefY.Center) return coords.getY() + height()/2;
		else if(coords.getYRef()  == RefY.Bottom) return coords.getY() + height();
		else return coords.getY();
	}
	
	public double bottom() {
		if(coords.getYRef() == RefY.Center) return coords.getY() - height()/2;
		else if(coords.getYRef() == RefY.Bottom) return coords.getY();
		else return coords.getY() - height();
	}
	
	public double centerY() {
		if(coords.getYRef() == RefY.Center) return coords.getY();
		else if(coords.getYRef() == RefY.Bottom) return coords.getY()  + height()/2;
		else return coords.getY() - height()/2;
	}
	
	public void setFill(String fillname) {
		this.paint.set(Fill.getPaint(fillname));
	}

	public void setFillColor(String fillname) {
		if(fillname != null)
			this.paint.set(Color.web(fillname));
		else this.paint.set(null);
	}
	
	public void setStrokeColor(String colorname) {
		this.strokePaint.set(Color.web(colorname));
	}
	
	public Container getContainer(){
		return container;
	}
	
	public ComplexShape getShape(){
		return shape;
	}
		
	protected abstract void updateBasicShape();
	
	@Override
	public void clear() {
		super.clear();
	//	shape.clear();
	}
		
	protected Point2D getRotationPoint() {
		double cx = 0, cy = 0;
		
		// X axis
		if(coords.getXRef() == RefX.Left) cx = -width.get()/2;
		else if(coords.getXRef() == RefX.Right) cx = width.get()/2;
		else cx = 0;
		
		// Y axis
		if(coords.getYRef() == RefY.Bottom) cy = height.get()/2;
		else if(coords.getYRef() == RefY.Top) cy = -height.get()/2;
		else cy = 0;
		
		return new Point2D(cx, cy);
	}
	
	public void updateShape(double cx, double cy) {
		Point2D p = container.getReferencePoint(cx, cy);
				
		double xref = p.getX();
		double yref = p.getY();
		
		double refAngle = -container.getRefAngle(cx);
		if(refAngle != 0) group.getTransforms().add(new Rotate(refAngle, xref, yref));
		//if(angle.get() != 0) group.getTransforms().add(new Rotate(angle.get(), xref, yref));					
		
		//updateBasicShape();
				
		group.getTransforms().add(new Translate(xref, yref));
		updateBasicShape();		

		Point2D r = getRotationPoint();
		if(angle.get() != 0) group.getTransforms().add(new Rotate(angle.get(), r.getX(), r.getY()));

		if(paint != null) shape.setFill(paint.get());
		//else shape.setFill(null);
		
		if(strokePaint != null) {
			shape.setStroke(strokePaint.get());
			shape.setStrokeWidth(border.get());
		}
		//else shape.setStroke(Color.DARKGRAY);
			
	}
	
	
	@Override
	public double getRefAngle(double x) {
		return 0;
	}

	@Override
	public String getName() {
		return "General Mark";
	}

	@Override
	public String getType() {
		return "Graphic";
	}
	
	@Override
	public void select(MarkSelection selectedMarks, Shape trace, SelectFilter filter) {
		if(!filter.accept(selectedMarks.getMarks(), this)) return;
		
		if(shape.intersects(trace)) {
			pinX = coords.getX();
			pinY = coords.getY();
			selectedMarks.add(this);
		}
		
		super.select(selectedMarks, trace, filter);
	}
	
	
	public boolean controlselect(MarkSelection selectedMarks, Circle trace) {		
		selectedControl = null;
		if(controls.isVisible()) {
			for(Node node: controls.getChildren()) {
				if(((Control)node).intersects(trace)) {
					selectedControl = (Control)node;
					selectedControl.pin();
					selectedMarks.add(this);
					return true;
				}
			}
		}
		
		return false;
	}

	
	public void pin() {
		pinX = coords.getX();
		pinY = coords.getY();
	}
	
	@Override
	public boolean monoselect(MarkSelection selectedMarks, Circle trace) {		
		if(super.monoselect(selectedMarks, trace)) return true;
		
		if(shape.intersects(trace)) {
			pin();
			
			selectedMarks.add(this);
			return true;
		}
		else return false;
	}
		
	
	public void setHighlight(boolean highlight, boolean enableControls) { 
		this.highlighted = highlight;
		
		shape.setHighlight(highlight);		
		if(enableControls) controls.setVisible(highlight);
		
		//if(highlight) controls.setVisible(enableControls);
		//else controls.setVisible(false);
	}

	public boolean isHighlighted() {
		return highlighted;
	}
	
	public Mark getHighlightedChild() {
		return highlighted ? this : null;
	}
	
	public void setConstraintControl(ConstraintController constraintController, ConstraintHandle constraintHandler) {
		this.constraintController = constraintController;
		this.constraintHandler = constraintHandler;
	}
	
	
	public void translate(Line lineTrace) {		
		if(constraintController != null) {
			constraintController.translate(lineTrace, constraintHandler);
		}
		else if(selectedControl != null) {
			selectedControl.drag(lineTrace);
		} else {
			double dx = lineTrace.getEndX() - lineTrace.getStartX();
			coords.x.updateValue(pinX + dx);
			coords.y.updateValue(pinY - lineTrace.getEndY() + lineTrace.getStartY());
		}
	}
	
	public boolean hasParent(Container node) {
		if(node == container) return true;
		else if(container instanceof Mark) return ((Mark)container).hasParent(node);
		else return false;
	}
	
	public Container getRoot() {
		if(container instanceof Mark) return ((Mark)container).getRoot();
		else return container;
	}
	

	public double getLeft() {
		double x = coords.getX();
		double w = width.get();
		
		switch(coords.getXRef()) {
			case Left: return (w>=0) ? x  : x + w;
			case Right: return (w>=0) ? x - w : x;
			default: return (w>=0) ? x - w/2 : x + w/2;
		}
	}
	
	public double getRight() {
		double x = coords.getX();
		double w = width.get();
		
		switch(coords.getXRef()) {
			case Left: return (w>=0) ? x + w  : x;
			case Right: return (w>=0) ? x : x - w;
			default: return (w>=0) ? x + w/2 : x - w/2;
		}
	}
	
	public double getTop() {
		double y = coords.getY();
		double h = height.get();
		
		switch(coords.getYRef()) {
			case Bottom: return (h>=0) ? -y - h : -y;
			case Top: return (h>=0) ? -y : -y + h;
			default: return (h>=0) ? -y - h/2 : -y + h/2;
		}
	}
	
	public double getBottom() {
		double y = coords.getY();
		double h = height.get();
		
		switch(coords.getYRef()) {
			case Bottom: return (h>=0) ? -y : -y - h;
			case Top: return (h>=0) ? -y + h : -y;
			default: return (h>=0) ? -y + h/2 : -y - h/2;
		}
	}
	
	public double getGlobalX() {
		return (container instanceof Mark) ?
			coords.getX() + ((Mark)container).getGlobalX() : coords.getX();
	}
	
	public double getGlobalY() {
		return (container instanceof Mark) ?
			coords.getY() + ((Mark)container).getGlobalY() : coords.getY();
	}
	
	public double getGlobalLeft() {
		return (container instanceof Mark) ?
			getLeft() + ((Mark)container).getGlobalX() : getLeft();
	}
	
	public double getGlobalBottom() {
		return (container instanceof Mark) ?
			getBottom() - ((Mark)container).getGlobalY() : getBottom();
	}
	
	public double getGlobalRight() {
		return (container instanceof Mark) ?
			getRight() + ((Mark)container).getGlobalX() : getRight();
	}
	
	public double getGlobalTop() {
		return (container instanceof Mark) ?
			getTop() - ((Mark)container).getGlobalY() : getTop();
	}
	
	public double getRootWidth() {
		return container.getRootWidth();
	}
	
	public double getRootHeight() {
		return container.getRootHeight();
	}


	public String getNestingPropertyName(String name) {
		VisGroup topGroup = getTopGroup();
		if(topGroup == null) return name;
		else {
			String id = createID();
			String pid = topGroup.createID() + ".";
			
			return name + id.replaceFirst(pid, "");			
		}
	}
	
	public VisGroup getTopGroup() {
		if(container instanceof VisGroup) return ((VisGroup) container).getTopGroup();
		else return null;
	}
	
	@Override
	public VisBody getRootVirtualGroup() {
		if(container instanceof VisBody)
			return ((VisBody) container).getRootVirtualGroup();
		else return null;
	}
	
	public VisBody getVirtualGroup() {
		return container.getVirtualGroup();
	}

	//public void showLabel(DataVariable variable, Property property) {}
	
	//@Override
	public void showLabel(DataVariable variable, Property property) {
		if(isLabelShown(variable) == variable.nodeShownProperty.get()) return;
		labels.showVariable(variable, property);
	}
	
	public boolean isLabelShown(DataVariable variable) {
		return labels.isLabelShown(variable);
	}
	
	
	public void mouseRelease() {
		VisBody vgroup = getVirtualGroup();
		if(vgroup != null) getVirtualGroup().mouseRelease();
	}

	public void changeXReference(RefX oldValue, RefX newValue, double w) {
		if(oldValue == RefX.Left && newValue == RefX.Center || oldValue == RefX.Center && newValue == RefX.Right) {
			coords.x.set(coords.getX() - w/2);
		} 
		else if(oldValue == RefX.Left && newValue == RefX.Right) {
			coords.x.set(coords.getX() - w);
		}
		else if(oldValue == RefX.Center && newValue == RefX.Left || oldValue == RefX.Right && newValue == RefX.Center) {
			coords.x.set(coords.getX() + w/2);
		}
		else if(oldValue == RefX.Right && newValue == RefX.Left) {
			coords.x.set(coords.getX() + w);
		}
	}
	
	public void changeYReference(RefY oldValue, RefY newValue, double h) {
		if(oldValue == RefY.Bottom && newValue == RefY.Center || oldValue == RefY.Center && newValue == RefY.Top) {
			coords.y.set(coords.getY() - h/2);
		} 
		else if(oldValue == RefY.Bottom && newValue == RefY.Center) {
			coords.y.set(coords.getY() - h);
		}
		else if(oldValue == RefY.Center && newValue == RefY.Bottom || oldValue == RefY.Top && newValue == RefY.Center) {
			coords.y.set(coords.getY() + h/2);
		}
		else if(oldValue == RefY.Top && newValue == RefY.Bottom) {
			coords.y.set(coords.getY() + h);
		}
	}
	
	/*
	 * This is a mechanism for updating the coordinates of the marks without undesirable double binding activations
	*/
	protected double tentative_x = 0, tentative_y = 0;
	public void setTentative(double x, double y) {
		tentative_x = x;
		tentative_y = y;
	}
	
	public void validateTentative() {
		if(coords.x.get() != tentative_x) coords.x.set(tentative_x);
		if(coords.y.get() != tentative_y) coords.y.set(tentative_y);
	}

	public int getPos() {
		return container.components.indexOf(this);
	}
	
	public Mark getNodeWithId(String id) {
		if(id.equals(this.id.get())) return this;
		
		for(Mark child : components) {
			Mark mark = child.getNodeWithId(id);
			if(mark != null) return mark;
		}
		
		return null;
	}
	
	public Property getProperty(PropertyName name) {
		if(name.isX()) return coords.x;
		else if(name.isY()) return coords.y;
		else if(name.isHeight()) return height;
		else if(name.isWidth()) return width;
		else if(name.isFill()) return paint;
		else if(name.isRotation()) return angle;
		else if(name.isThickness()) return border;
		else if(name.isStroke()) return strokePaint;
		else if(name.isReferenceX()) return coords.xRef;
		else if(name.isReferenceY()) return coords.yRef;
		else return null;
	}
	
	public Container getComponent(String sid){
		if(id.get().equals(sid)) return this;
		else return super.getComponent(sid);
	}
	
	
	@Override
	public double getYIn(Container container) {
		if(this.container == container) return coords.getY();
		else return coords.getY() + this.container.getYIn(container);
	}
	
	@Override
	public double getXIn(Container container) {
		if(this.container == container) return coords.getX();
		else return coords.getX() + this.container.getXIn(container);
	}
}
