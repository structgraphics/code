package fr.inria.structgraphics.graphics;

public class MarkFactory {
	
	public static VisFrame createVisFrame(double width, double height) {
		VisFrame visFrame = new VisFrame(width, height);
		visFrame.setRefCoords(new ReferenceCoords("left", "bottom"));
		
		return visFrame;
	}

}
