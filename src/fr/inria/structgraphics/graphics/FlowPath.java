package fr.inria.structgraphics.graphics;

import javafx.scene.shape.Path;

public class FlowPath extends Path {
	private double y1bottom, y1top, y2bottom, y2top;
	private double left, right;
	
	public void setY1(double bottom, double top) {
		y1bottom = bottom;
		y1top = top;
	}

	public void setY2(double bottom, double top) {
		y2bottom = bottom;
		y2top = top;
	}
	
	public void setX(double left, double right) {
		this.left = left;
		this.right = right;
	}
	
	public double centerY() {
		//return (y1bottom + y2top)/2;
		
		return (2*y1bottom + y2top)/3;
	}
	
	public double centerX() {
		//return (left + right)/2;
		return (2*left + right)/3;
	}
}
