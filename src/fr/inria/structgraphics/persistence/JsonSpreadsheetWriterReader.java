package fr.inria.structgraphics.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.PositionCoords;
import fr.inria.structgraphics.graphics.ReferenceCoords;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisFrame;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.ui.Draggable;
import fr.inria.structgraphics.ui.Draggable.Type;
import fr.inria.structgraphics.ui.spreadsheet.AreaContent;
import fr.inria.structgraphics.ui.spreadsheet.AreaSourceInfo;
import fr.inria.structgraphics.ui.spreadsheet.ColumnVariable;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable;
import fr.inria.structgraphics.ui.spreadsheet.DiscreteTransformation;
import fr.inria.structgraphics.ui.spreadsheet.MappingProperty;
import fr.inria.structgraphics.ui.spreadsheet.MathTransformation;
import fr.inria.structgraphics.ui.spreadsheet.Spreadsheet;
import fr.inria.structgraphics.ui.spreadsheet.SpreadsheetArea;
import fr.inria.structgraphics.ui.spreadsheet.VectorVariable;
import fr.inria.structgraphics.ui.spreadsheet.DataVariable.DataType;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Paint;

public class JsonSpreadsheetWriterReader {
	
	public static JsonObjectBuilder buildJason(Spreadsheet sheet, JsonObjectBuilder visbuilder) {
		List<SpreadsheetArea> areas = sheet.getAreas();		
		JsonArrayBuilder builder = Json.createArrayBuilder();
		for(SpreadsheetArea area: areas) {
			save(builder, area);
		}
		
		visbuilder.add("datasheet", builder);
		
		return visbuilder;
	}
	
	public static JsonObjectBuilder buildJason(Spreadsheet sheet) {
		JsonObjectBuilder visbuilder = Json.createObjectBuilder();
		return buildJason(sheet, visbuilder);		
	}

	private static JsonArrayBuilder save(JsonArrayBuilder parentBuilder, SpreadsheetArea area) {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		
		builder.add("type", area.getType().toString());
		
		builder.add("row", area.getStartRow());
		builder.add("column", area.getStartColumn());
		builder.add("nrows", area.nrows());
		builder.add("ncolumns", area.ncolumns());
	
		builder.add("source", area.getSource().id.get());
		if(area.getVisBody() != null) builder.add("group", area.getVisBody().id.get());
		builder.add("wide", area.isWide());
		builder.add("network", area.isNetwork());
		
		ArrayList<PropertyName> names = area.getPropertyNames();
		JsonArrayBuilder namesArray = Json.createArrayBuilder();
		for(PropertyName name: names) {
			namesArray.add(name.toString());
		}
		builder.add("properties", namesArray);
		
		JsonArrayBuilder variablesArray = Json.createArrayBuilder();
		// TODO: To add the variable mappings
		AreaContent areaContent = area.getAreaContent();
		for(DataVariable variable : areaContent.getVariables()) {
			JsonObjectBuilder varObject = Json.createObjectBuilder();	
			
			varObject.add("name", variable.getPropertyName());
			varObject.add("type", variable.getType().toString());
			
			// Labels, axes, legends
			varObject.add("axis", variable.scaleShownProperty.get());
			varObject.add("axis-outer", variable.scaleShownPropertyOuter.get());
			varObject.add("legend", variable.legendShownProperty.get());
			varObject.add("node", variable.nodeShownProperty.get());
			
			JsonArrayBuilder labelsArray = Json.createArrayBuilder();
			for(String label: variable.getNames()) {
				labelsArray.add(label);
			}
			varObject.add("labels", labelsArray);

			/// Add the transformation
			if(variable.getType() == DataType.Functional) {
				varObject.add("function", variable.transformationProperty().get().getExpression().getValue());
			} else {
				DiscreteTransformation transform = (DiscreteTransformation) variable.transformationProperty().get();
				TreeMap<MappingProperty, StringProperty> map = transform.getMap();
				JsonArrayBuilder mappingsArray = Json.createArrayBuilder();
				
				for(MappingProperty prop: map.keySet()) {
					JsonObjectBuilder mappingObject = Json.createObjectBuilder();
					StringProperty val = map.get(prop);
					mappingObject.add("from", prop.getValue().toString());
					mappingObject.add("to", val.get());
					mappingsArray.add(mappingObject);
				}
				
				varObject.add("mappings", mappingsArray);
			}
			///
			variablesArray.add(varObject );
		}
		builder.add("variables", variablesArray);
		
		parentBuilder.add(builder);
		return parentBuilder;
	}
	

	public static void readSpreadsheetFromJasonObject(JsonArray jsonArray, VisFrame visFrame, Spreadsheet sheet) {		
		for(int i = 0; i < jsonArray.size(); ++i) {
			JsonValue jsonObject = jsonArray.get(i);
			
			if(jsonObject instanceof JsonObject) {
				readAreaFromJasonObject((JsonObject)jsonObject, visFrame, sheet);
			}
		}
	}
	
	private static void readAreaFromJasonObject(JsonObject jsonObject, VisFrame visFrame, Spreadsheet sheet) {
		Type type = Draggable.Type.valueOf(jsonObject.getString("type"));
		int row = jsonObject.getInt("row");
		int column = jsonObject.getInt("column");
		int nrows = jsonObject.getInt("nrows");
		int ncolumns = jsonObject.getInt("ncolumns");
		
		boolean isWide = jsonObject.getBoolean("wide");
		boolean isNetwork = jsonObject.getBoolean("network");
		
		Container source = visFrame.getComponent(jsonObject.getString("source"));
		JsonString groupID = jsonObject.getJsonString("group");
		VisBody visBody = (groupID == null) ? null : (VisBody)visFrame.getComponent(groupID.toString());
	
		JsonArray propertiesArray = jsonObject.getJsonArray("properties");
		ArrayList<PropertyName> propertyNames = new ArrayList<>();
		for(int i = 0; i < propertiesArray.size(); ++i) {
			PropertyName name = new PropertyName(propertiesArray.getJsonString(i).getString());
			propertyNames.add(name);
		}
		
		AreaSourceInfo info = new AreaSourceInfo(source, visBody, propertyNames, type, isWide, isNetwork);
		SpreadsheetArea area = new SpreadsheetArea(sheet, row, column, ncolumns, nrows);
		area.setInfo(info);
		sheet.addArea(area);
		
		AreaContent content = area.getAreaContent();
		
		/// Variables
		JsonArray variablesArray = jsonObject.getJsonArray("variables");
		for(int i = 0; i < variablesArray.size(); ++i) {
			JsonObject varObject = variablesArray.getJsonObject(i);
			
			String varName = varObject.getString("name");
			DataVariable variable = content.getVariable(varName);
			
			DataType datatype = DataType.valueOf(varObject.getString("type"));
			JsonArray labelsArray = varObject.getJsonArray("labels");
			for(int k = 0; k < labelsArray.size(); ++k) {
				String label = labelsArray.getString(k);
				variable.getName(k).set(label);
			}
						
			if(datatype == DataType.Functional) {
				String expression = varObject.getString("function");
				MathTransformation mathTransform = new MathTransformation(variable);
				mathTransform.setExpression(expression);
				variable.transformationProperty().set(mathTransform);
			} else {
				DiscreteTransformation discreteTransform = new DiscreteTransformation(variable);
				variable.transformationProperty().set(discreteTransform);
				
				JsonArray mappingsArray = varObject.getJsonArray("mappings");
				for(int k = 0; k < mappingsArray.size(); ++k) {				
					JsonObject mapping = mappingsArray.getJsonObject(k);
					discreteTransform.replace(mapping.getString("from"), mapping.getString("to"));					
				}
			}
			
			// Labels, axes, legends
			if(varObject.getBoolean("axis")) sheet.showOnGraph(variable);
			if(varObject.getBoolean("axis-outer")) sheet.showOnParentGraph(variable);
			if(varObject.getBoolean("legend")) sheet.showOnLegend(variable);
			if(varObject.getBoolean("node")) sheet.showOnNode(variable);			
		}
		
	}
}



