package fr.inria.structgraphics.persistence;

import java.util.Collection;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.ui.spreadsheet.Spreadsheet;

public class JsonWorkspaceWriter {
	
	public static JsonObject saveToJason(Collection<Mark> librarymarks, Collection<Mark> canvasmarks, Spreadsheet sheet) {	
		JsonObjectBuilder rootBuilder = Json.createObjectBuilder();
		
		JsonObjectBuilder spaceBuilder = JsonObjectWriter.buildJason(librarymarks, true); 
		JsonObjectWriter.buildJason(canvasmarks, false, spaceBuilder); 
		JsonSpreadsheetWriterReader.buildJason(sheet, spaceBuilder);
		
		rootBuilder.add("workspace", spaceBuilder);
		
		return rootBuilder.build();
	}

}

