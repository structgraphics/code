package fr.inria.structgraphics.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedSet;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;

import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisGroup;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.Shareable;
import fr.inria.structgraphics.ui.utils.GroupBinding;
import fr.inria.structgraphics.ui.viscanvas.groupings.CollectionPropertyStructure;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnection;
import fr.inria.structgraphics.ui.viscanvas.groupings.GroupPropertyStructure;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleDoubleProperty;

public class JsonObjectWriter {
	
	public static JsonObject saveToJason(Collection<Mark> marks, boolean isLibrary) {		
		return buildJason(marks, isLibrary).build();
	}
	
	public static JsonObjectBuilder buildJason(Collection<Mark> marks, boolean isLibrary, JsonObjectBuilder visbuilder) {
		JsonArrayBuilder builder = Json.createArrayBuilder();
		for(Mark mark: marks) {
			save(builder, mark, mark);
		}
		
		visbuilder.add(isLibrary? "library" : "visualizations", builder);
		
		return visbuilder;
	}
	
	public static JsonObjectBuilder buildJason(Collection<Mark> marks, boolean isLibrary) {
		JsonObjectBuilder visbuilder = Json.createObjectBuilder();
		return buildJason(marks, isLibrary, visbuilder);		
	}
	
	private static JsonArrayBuilder save(JsonArrayBuilder parentBuilder, Mark mark, Mark root) {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("type", mark.getType());	
		if(mark != root) { // Since the ID of the root node may change when re-loaded to the canvas
			builder.add("id", getRelativeId(root.id.get(), mark.id.get()));
		}
		builder.add("level", mark.getLevel());
		
		if(mark instanceof VisBody) {
			builder.add("prefix", PropertyName.getPrefix(mark.getLevel()));
		}
		
		List<Property> properties = mark.getProperties();
		for(Property prop: properties) saveProperty(builder, prop);
		
		if(mark instanceof VisCollection) {
			// Save the property structure
			CollectionPropertyStructure structure = ((VisCollection)mark).getChildPropertyStructure();
			SortedSet<PropertyName> common = structure.getCommon();
			JsonArrayBuilder commonBuilder = Json.createArrayBuilder();
			for(PropertyName name: common) commonBuilder.add(name.toString());
			builder.add("common", commonBuilder.build());
			
			SortedSet<PropertyName> variable = structure.getVariable();		
			JsonArrayBuilder variableBuilder = Json.createArrayBuilder();
			for(PropertyName name: variable) variableBuilder.add(name.toString());
			builder.add("variable", variableBuilder.build());	
		} else if(mark instanceof VisGroup) {
			////////////
			GroupPropertyStructure structure = ((VisGroup)mark).getChildPropertyStructure();
			SortedSet<PropertyName> common = structure.getCommon();
			JsonArrayBuilder commonBuilder = Json.createArrayBuilder();
			for(PropertyName name: common) commonBuilder.add(name.toString());
			builder.add("common", commonBuilder.build());
			
			SortedSet<PropertyName> variable = structure.getVariable();		
			JsonArrayBuilder variableBuilder = Json.createArrayBuilder();
			for(PropertyName name: variable) variableBuilder.add(name.toString());
			builder.add("variable", variableBuilder.build());	
			
			if(((VisGroup)mark).isTopGroup()) {
				JsonArrayBuilder publicBuilder = Json.createArrayBuilder();
				for(Property prop: ((VisGroup)mark).getAllProperties()) {
					if(prop instanceof Shareable && ((Shareable)prop).getPublicProperty().get())
						publicBuilder.add(prop.getName());
				}
				builder.add("public", publicBuilder.build());
					
				JsonArrayBuilder bindingsBuilder = Json.createArrayBuilder();
								
				for(PropertyName propName: structure.getProperties().keySet()) {
					
					ArrayList<GroupBinding> bindings = structure.getGroups(propName);
					if(bindings == null) continue;
					for(GroupBinding binding: bindings) {
						JsonArrayBuilder propBuilder = Json.createArrayBuilder();
						
						ArrayList boundproperties = binding.getProperties();
						for(int i = 0; i < boundproperties.size(); ++i) {
							propBuilder.add(((Property)boundproperties.get(i)).getName());
						}
						
						bindingsBuilder.add(propBuilder);
					}
				}
				
				builder.add("bindings", bindingsBuilder.build()); 
			}
		} else saveProperty(builder, mark.ratiolock);
		
		List<Mark> components = mark.getComponents();
		if(!components.isEmpty()) {
			JsonArrayBuilder childrenBuilder = Json.createArrayBuilder();			
			for(Mark child: components) {
				save(childrenBuilder, child, root);
			}
			
			builder.add("components", childrenBuilder);
		}
		
		
		if(mark instanceof LineConnectedCollection && ((LineConnectedCollection)mark).hasConnections()) {
			LineConnectedCollection collection = ((LineConnectedCollection)mark);
			builder.add(new PropertyName(collection.flowPaint.getName()).getCleanName(), collection.flowPaint.get().toString());
			builder.add(new PropertyName(collection.flowOpacity.getName()).getCleanName(), collection.flowOpacity.get());
			builder.add(new PropertyName(collection.coloringScheme.getName()).getCleanName(), collection.coloringScheme.get().toString());
			
			JsonArrayBuilder connectionsArrayBuilder = Json.createArrayBuilder();
			// Save the flow connections structure
			for(FlowConnection conn : collection.getFlowConnections()) {
				JsonObjectBuilder connectionBuilder = Json.createObjectBuilder();
				connectionBuilder.add("origin", getRelativeId(root.id.get(), conn.getOrigin().id.get()));
				connectionBuilder.add("destination", getRelativeId(root.id.get(), conn.getDestination().id.get()));
				connectionBuilder.add("weight", conn.weight.get());
				
				connectionsArrayBuilder.add(connectionBuilder);
			}
			
			builder.add("connections", connectionsArrayBuilder);
		}
		
		parentBuilder.add(builder);
		return parentBuilder;
	}
	
	private static void saveProperty(JsonObjectBuilder builder, Property property) {
		PropertyName propName = new PropertyName(property.getName());
		
		if(property instanceof SimpleDoubleProperty)
			builder.add(propName.getCleanName(), ((SimpleDoubleProperty)property).doubleValue());
		else builder.add(propName.getCleanName(), property.getValue().toString());
	}
	

	public static StringBuffer fromArrayToReadableString(JsonArray array, int level) {
		StringBuffer buffer = new StringBuffer();
		if(array.isEmpty()) {
			buffer.append("[]");
			return buffer;
		}

		if(array.get(0).getValueType() == ValueType.STRING) {
			buffer.append("[");
	        for(int i = 0; i < array.size(); ++i) {
	        	JsonString value = array.getJsonString(i);
	        	buffer.append(value);
	        	buffer.append(",");
	        }
	        buffer.deleteCharAt(buffer.length() - 1);
			buffer.append("]");
		} 
		else if(array.get(0).getValueType() == ValueType.ARRAY) {
			buffer.append("[\n");
			
	        for(int i = 0; i < array.size(); ++i) {
	        	JsonArray visObject = array.getJsonArray(i);
				buffer.append(tabs(level+1));
	        	buffer.append(fromArrayToReadableString(visObject, (level + 1)));
	        	buffer.append(",\n");
	        }

			buffer.deleteCharAt(buffer.length() - 2);
			buffer.append(tabs(level) + "]");
		}
		else {
			buffer.append("[\n");
			
	        for(int i = 0; i < array.size(); ++i) {
	        	JsonObject visObject = array.getJsonObject(i);
	        	buffer.append(fromObjectToReadableString(visObject, (level + 1)));
	        	buffer.append(",\n");
	        }

			buffer.deleteCharAt(buffer.length() - 2);
			buffer.append(tabs(level) + "]");
		}

		
		return buffer;
	}
	
	private static String getRelativeId(String rootId, String id) {
		return id.substring(rootId.length() + 1);
	}
	
	public static StringBuffer fromObjectToReadableString(JsonObject visObject, int level) {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(tabs(level) + "{\n");	
		
		for(Entry<String, JsonValue> entry: visObject.entrySet()) {
			buffer.append(tabs(level + 1));
			buffer.append("\"" + entry.getKey() + "\"");
			buffer.append(":");
			
			JsonValue value = entry.getValue();
			if(value instanceof JsonArray) {
				buffer.append(fromArrayToReadableString((JsonArray)value, (level + 2)));
			} else if(value instanceof JsonObject) {
				buffer.append(fromObjectToReadableString((JsonObject)value, (level + 2)));
			}
			else buffer.append(entry.getValue());
			
			buffer.append(",\n");
		}
		
		buffer.deleteCharAt(buffer.length() - 2);
		
		buffer.append(tabs(level) + "}");

		return buffer;
	}
	
	private static StringBuffer tabs(int num) {
		StringBuffer str = new StringBuffer();
		for(int i = 0; i < num; ++i) str.append("  ");
		
		return str;
	}

}

