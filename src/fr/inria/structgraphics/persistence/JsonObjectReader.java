package fr.inria.structgraphics.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.GenericShapeMark;
import fr.inria.structgraphics.graphics.LineConnectedCollection;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.PositionCoords;
import fr.inria.structgraphics.graphics.ReferenceCoords;
import fr.inria.structgraphics.graphics.ShapeMark;
import fr.inria.structgraphics.graphics.TextualMark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisGroup;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;
import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;
import fr.inria.structgraphics.types.ColoringSchemeProperty;
import fr.inria.structgraphics.types.FlexibleListProperty;
import fr.inria.structgraphics.types.LineTypeProperty;
import fr.inria.structgraphics.types.PropertyName;
import fr.inria.structgraphics.types.ShapeProperty;
import fr.inria.structgraphics.types.Shareable;
import fr.inria.structgraphics.types.AlignmentProperty.XSticky;
import fr.inria.structgraphics.types.AlignmentProperty.YSticky;
import fr.inria.structgraphics.types.ColoringSchemeProperty.Scheme;
import fr.inria.structgraphics.types.DistributionProperty.Constraint;
import fr.inria.structgraphics.types.ShapeProperty.Type;
import fr.inria.structgraphics.ui.utils.GroupBinding;
import fr.inria.structgraphics.ui.viscanvas.groupings.CollectionPropertyStructure;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnection;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnections;
import fr.inria.structgraphics.ui.viscanvas.groupings.GroupPropertyStructure;
import fr.inria.structgraphics.ui.viscanvas.groupings.PropertyStructure;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Paint;

public class JsonObjectReader {

	public static ArrayList<Mark> readChildrenFromJasonArray(Container parent, JsonArray jsonArray) {
		ArrayList<Mark> marks = new ArrayList<>();
		
		for(int i = 0; i < jsonArray.size(); ++i) {
			JsonValue jsonObject = jsonArray.get(i);
			
			if(jsonObject instanceof JsonObject) {
				Mark mark = readFromJasonObject(parent, (JsonObject)jsonObject);
				if(mark != null) marks.add(mark);
			}
		}
		
		return marks;
	}

	public static Mark readFromJasonObject(Container parent, JsonObject jsonObject) {
		Mark mark;
		
		String type = jsonObject.getString("type");
		if(type.contains("Collection") || type.contains("Chart")) { // COLLECTIONS
			mark = new LineConnectedCollection(parent, true);
			
			double thickness = jsonObject.getJsonNumber("thickness").doubleValue();
			mark.border.set(thickness);				
			String stroke = jsonObject.getString("stroke");
			mark.strokePaint.set(Paint.valueOf(stroke));
			double rotation = jsonObject.getJsonNumber("rotation").doubleValue();
			mark.angle.set(rotation);
			
			ReferenceCoords refCoords = new ReferenceCoords(RefX.Left, RefY.Bottom);
			mark.setRefCoords(refCoords);
			
			double x = jsonObject.getJsonNumber("x").doubleValue();
			double y = jsonObject.getJsonNumber("y").doubleValue();
			PositionCoords coords = new PositionCoords(mark, x, y);
			coords.xRef.set(RefX.Left);
			coords.yRef.set(RefY.Bottom);
			mark.coords = coords;
			
			mark.setLevel(jsonObject.getJsonNumber("level").intValue());
			
			String stickX = jsonObject.getString("sticky-x");
			((VisBody)mark).alignYProperty.set(XSticky.valueOf(stickX));
			String stickY = jsonObject.getString("sticky-y");
			((VisBody)mark).alignXProperty.set(YSticky.valueOf(stickY));
			
			String distrX = jsonObject.getString("distribution-x");
			((VisBody)mark).constraintXProperty.set(Constraint.valueOf(distrX));
			String distrY = jsonObject.getString("distribution-y");
			((VisBody)mark).constraintYProperty.set(Constraint.valueOf(distrY));
			
			double deltaX = jsonObject.getJsonNumber("delta-x").doubleValue();
			((VisBody)mark).distanceXProperty.set(deltaX);
			if(((VisBody)mark).constraintXProperty.get() == Constraint.None) 
				((VisBody)mark).distanceXProperty.getActiveProperty().set(false);
			
			double deltaY = jsonObject.getJsonNumber("delta-y").doubleValue();
			((VisBody)mark).distanceYProperty.set(deltaY);
			if(((VisBody)mark).constraintYProperty.get() == Constraint.None) 
				((VisBody)mark).distanceYProperty.getActiveProperty().set(false);
			
			String curve = jsonObject.getString("curve");
			((LineConnectedCollection)mark).lineProperty = new LineTypeProperty(mark, LineTypeProperty.Type.valueOf(curve));
		
			JsonArray componentsArray = jsonObject.getJsonArray("components");
			Collection<Mark> children = readChildrenFromJasonArray(mark, componentsArray);
			for(Mark child: children) {
				((VisBody)mark).attachCopy(child);
			}
			
			JsonArray commonArray = jsonObject.getJsonArray("common");
			JsonArray variableArray = jsonObject.getJsonArray("variable");
						
			///// Construct the property structure
			((VisCollection)mark).setPropertyStructure(new CollectionPropertyStructure(children, 
					readPropertyNames(commonArray), readPropertyNames(variableArray)));
			
			//// CONNECTIONS
			JsonArray connectionsArray = jsonObject.getJsonArray("connections");
			if(connectionsArray != null) {
				String fill = jsonObject.getString("fill");
				((LineConnectedCollection)mark).flowPaint.set(Paint.valueOf(fill));
				
				double opacity = jsonObject.getJsonNumber("opacity").doubleValue();
				((LineConnectedCollection)mark).flowOpacity.set(opacity);
				
				String coloringScheme = jsonObject.getString("coloring");
				((LineConnectedCollection)mark).coloringScheme.set(ColoringSchemeProperty.Scheme.valueOf(coloringScheme));
				
				if(((LineConnectedCollection)mark).coloringScheme.get() != Scheme.Common) 
					((LineConnectedCollection)mark).flowPaint.getActiveProperty().set(false);
				
				FlowConnections connections = readConnections(connectionsArray, (LineConnectedCollection)mark);
				((LineConnectedCollection)mark).setFlowConnections(connections);
			}
			
			mark.initProperties();
			
		} else if(type.contains("Group")) {  // GROUPS
			mark = new VisGroup(parent, true);
			
			double rotation = jsonObject.getJsonNumber("rotation").doubleValue();
			mark.angle.set(rotation);
			
			ReferenceCoords refCoords = new ReferenceCoords(RefX.Left, RefY.Bottom);
			mark.setRefCoords(refCoords);
			
			double x = jsonObject.getJsonNumber("x").doubleValue();
			double y = jsonObject.getJsonNumber("y").doubleValue();
			PositionCoords coords = new PositionCoords(mark, x, y);
			coords.xRef.set(RefX.Left);
			coords.yRef.set(RefY.Bottom);
			mark.coords = coords;
			
			mark.setLevel(jsonObject.getJsonNumber("level").intValue());
			
			String stickX = jsonObject.getString("sticky-x");
			((VisBody)mark).alignYProperty.set(XSticky.valueOf(stickX));
			String stickY = jsonObject.getString("sticky-y");
			((VisBody)mark).alignXProperty.set(YSticky.valueOf(stickY));
			
			String distrX = jsonObject.getString("distribution-x");
			((VisBody)mark).constraintXProperty.set(Constraint.valueOf(distrX));
			String distrY = jsonObject.getString("distribution-y");
			((VisBody)mark).constraintYProperty.set(Constraint.valueOf(distrY));
			
			double deltaX = jsonObject.getJsonNumber("delta-x").doubleValue();
			((VisBody)mark).distanceXProperty.set(deltaX);
			if(((VisBody)mark).constraintXProperty.get() == Constraint.None) 
				((VisBody)mark).distanceXProperty.getActiveProperty().set(false);
			
			double deltaY = jsonObject.getJsonNumber("delta-y").doubleValue();
			((VisBody)mark).distanceYProperty.set(deltaY);
			if(((VisBody)mark).constraintYProperty.get() == Constraint.None) 
				((VisBody)mark).distanceYProperty.getActiveProperty().set(false);
			
			///////////////
			JsonArray componentsArray = jsonObject.getJsonArray("components");
			Collection<Mark> children = readChildrenFromJasonArray(mark, componentsArray);
			for(Mark child: children) {
				((VisBody)mark).attachCopy(child);
			}
			
			JsonArray commonArray = jsonObject.getJsonArray("common");
			JsonArray variableArray = jsonObject.getJsonArray("variable");
						
			///// Construct the property structure
			((VisGroup)mark).setPropertyStructure(new GroupPropertyStructure(children, 
					readPropertyNames(commonArray), readPropertyNames(variableArray)));
			
			// Create and add bindings
			JsonArray bindingsArray = jsonObject.getJsonArray("bindings");
			if(bindingsArray != null) {
				TreeMap<PropertyName, ArrayList<GroupBinding>> bindings = readBindings(bindingsArray, (VisGroup)mark);
				((VisGroup)mark).getChildPropertyStructure().setBindings(bindings);
			}
			
			mark.initProperties();
			
			JsonArray publicArray = jsonObject.getJsonArray("public");
			if(publicArray != null) {
				SortedSet<PropertyName> publicProperties = readPropertyNames(publicArray);
				for(Property property : ((VisGroup)mark).getAllProperties()) {	//System.err.println(property.getName());		
					if(publicProperties.contains(new PropertyName(property.getName()))) 
						((Shareable)property).getPublicProperty().set(true);
					else ((Shareable)property).getPublicProperty().set(false);
				}
			}
			
		} else { // Shape Mark	
			ShapeProperty.Type shapeType;
			
			if(type.equals("Text")) {
				shapeType = Type.Text;
			} else {
				String shape = jsonObject.getString("shape");
				if(shape.equals("Rectangle")) shapeType = Type.Rectangle;
				else if(shape.equals("Ellipse")) shapeType = Type.Ellipse;
				else if(shape.equals("Triangle")) shapeType = Type.Triangle;
				else shapeType = Type.Line;
			}
			
			double width = jsonObject.getJsonNumber("width").doubleValue();
			double height = jsonObject.getJsonNumber("height").doubleValue();
			mark = ShapeMark.createInstance(parent, true, shapeType, width, height);
			
			((ShapeMark)mark).shapeProperty.set(shapeType);
			
			boolean lock = jsonObject.getString("lock").equals("true");
			mark.ratiolock.set(lock);
			
			double x = jsonObject.getJsonNumber("x").doubleValue();
			double y = jsonObject.getJsonNumber("y").doubleValue();
			PositionCoords coords = new PositionCoords(mark, x, y);
			RefX referencex = ReferenceCoords.getXRef(jsonObject.getString("reference-x"));
			RefY referencey = ReferenceCoords.getYRef(jsonObject.getString("reference-y"));
			coords.xRef.set(referencex);
			coords.yRef.set(referencey);
			mark.coords = coords;
			
			double rotation = jsonObject.getJsonNumber("rotation").doubleValue();
			mark.angle.set(rotation);
			
			String fill = jsonObject.getString("fill");
			mark.paint.set(Paint.valueOf(fill));
			
			if(shapeType == Type.Text) {
				String text = jsonObject.getString("text");
				((TextualMark)mark).textProperty.set(text);
				double fontsize = jsonObject.getJsonNumber("fontsize").doubleValue();
				((TextualMark)mark).fontSize.set(fontsize);
			} else {
				double thickness = jsonObject.getJsonNumber("thickness").doubleValue();
				mark.border.set(thickness);				
				String stroke = jsonObject.getString("stroke");
				mark.strokePaint.set(Paint.valueOf(stroke));
			}
			
			mark.initProperties();
		}
		
		/////////
		mark.update();
		
		// TODO Auto-generated method stub
		return mark;
	}
	
	
	private static SortedSet<PropertyName> readPropertyNames(JsonArray jsonArray){
		TreeSet<PropertyName> propertyNames = new TreeSet<>();
		
		for(int i = 0; i < jsonArray.size(); ++i) {
			PropertyName name = new PropertyName(jsonArray.getJsonString(i).getString());
			propertyNames.add(name);
		}
		
		return propertyNames;
	}
	
	private static FlowConnections readConnections(JsonArray jsonArray, LineConnectedCollection collection) {
		FlowConnections connections = new FlowConnections(collection);
		for(int i = 0; i < jsonArray.size(); ++i) {
			JsonObject connObject = jsonArray.getJsonObject(i);
			
			String colId = collection.id.get();
			String originId = colId +"." + connObject.getString("origin");
			String destinationId = colId +"." + connObject.getString("destination");
						
			double weight = connObject.getJsonNumber("weight").doubleValue();
			
			FlowConnection connection = new FlowConnection(collection, 
					(ShapeMark)collection.getNodeWithId(originId), (ShapeMark)collection.getNodeWithId(destinationId));
			connection.weight.set(weight);
			connections.add(connection);
		}
		
		return connections;
	}
	
	private static TreeMap<PropertyName, ArrayList<GroupBinding>> readBindings(JsonArray jsonArray, VisGroup group) {
		TreeMap<PropertyName, ArrayList<GroupBinding>> bindings = new TreeMap<>();
		
		Map<PropertyName, FlexibleListProperty> properties = group.getChildPropertyStructure().getProperties();
		
		for(int i = 0; i < jsonArray.size(); ++i) {
			JsonArray bindingArray = jsonArray.getJsonArray(i);
			ArrayList<Property> boundProps = new ArrayList<>();
			
			FlexibleListProperty propList = null;
			for(int k = 0; k < bindingArray.size(); ++k) {
				PropertyName name = new PropertyName(bindingArray.getString(k));
				if(propList == null) propList = properties.get(name);
				Property property = propList.getByName(name);
				if(property!=null) boundProps.add(property);
			}
			
			PropertyName name = new PropertyName(new PropertyName(boundProps.get(0).getName()).getNoPostfixName());
			ArrayList<GroupBinding> binding = bindings.get(name);
			if(binding == null) {
				binding = new ArrayList<>();
				bindings.put(name, binding);
			}
			binding.add(new GroupBinding(boundProps));			
		}
	
		return bindings;
	}
}

