package fr.inria.structgraphics.persistence;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class JsonDescription {		
	private JsonObject visObject;
		
	public JsonDescription(File file) {
		Path path = Paths.get(file.getPath());
		
		StringBuffer textContent = new StringBuffer();
		List<String> lines;
		try {
			lines = Files.readAllLines(path,  Charset.forName("ISO-8859-1"));
			
			for (String line : lines) {
				textContent.append(line + "\n");
			}
			
			JsonReader reader = Json.createReader(new StringReader(textContent.toString()));
			visObject = reader.readObject();
			reader.close();
		} catch (IOException e) {
			visObject = null;
		}
	}
			
	public boolean isWorkspace() {
		return visObject.containsKey("workspace");
	}
	
	public boolean isLibrary() {
		return visObject.containsKey("library");
	}
	
	public JsonArray getVisArray(boolean workspace) {
		if(visObject == null) return null;
		else return workspace ? visObject.getJsonObject("workspace").getJsonArray("visualizations") : visObject.getJsonArray("visualizations");
	}
	
	public JsonArray getLibraryArray(boolean workspace) {
		if(visObject == null) return null;
		else return workspace ? visObject.getJsonObject("workspace").getJsonArray("library") : visObject.getJsonArray("library");
	}
	
	public JsonArray getSpreadsheetArray() {
		return visObject.getJsonObject("workspace").getJsonArray("datasheet");
	}
}
