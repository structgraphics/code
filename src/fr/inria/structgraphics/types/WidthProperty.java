package fr.inria.structgraphics.types;

public class WidthProperty extends DistanceProperty {

	public WidthProperty(java.lang.Object bean) {
		super(bean, "width", 0);
	}
	
	public WidthProperty(java.lang.Object bean, double value) {
		super(bean, "width", value);
	}
}
