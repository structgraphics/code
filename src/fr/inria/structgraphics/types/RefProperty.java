package fr.inria.structgraphics.types;

import fr.inria.structgraphics.graphics.Mark;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

public class RefProperty<T> extends SimpleObjectProperty<T> implements Shareable {

	public RefProperty(Object bean, String name, T value) {
		super(bean, name, (T)value);
	}

	@Override
	public String getName() {
		if(getBean() instanceof Mark) { 
			return ((Mark)getBean()).getNestingPropertyName(super.getName());
		} else return super.getName();
	}
	
	private BooleanProperty activeProperty = new SimpleBooleanProperty(true);

	@Override
	public BooleanProperty getActiveProperty() {
		return activeProperty;
	}
	
	@Override
	public boolean isSimilar(Shareable property) {
		if(property instanceof RefProperty) {
			return getValue().equals(((RefProperty) property).getValue());
		}
		
		return false;
	}
	
	
	protected BooleanProperty publicProperty = new SimpleBooleanProperty(true);
	
	@Override
	public BooleanProperty getPublicProperty() {
		return publicProperty;
	}
	
	protected BooleanProperty hiddenProperty = new SimpleBooleanProperty(false);
	
	@Override
	public BooleanProperty getHiddenProperty() {
		return hiddenProperty;
	}
	
}
