package fr.inria.structgraphics.types;

import java.util.ArrayList;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

public class StringListProperty extends FlexibleListProperty {

	public StringListProperty(Object bean, StringProperty property) {
		super(bean, property.getName(), FXCollections.observableArrayList(new ArrayList<StringProperty>()));
		add(property);
	}

}
