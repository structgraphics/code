package fr.inria.structgraphics.types;

public class AlignmentXProperty extends AlignmentProperty<AlignmentProperty.YSticky> {

	public AlignmentXProperty(Object bean) {
		super(bean, "sticky-y", YSticky.No);
	}
	
	public AlignmentXProperty(Object bean, YSticky value) {
		super(bean, "sticky-y", value);
	}
	
	/*
	public String getIconName() {
		return "x-" + getValue().toString().toLowerCase();
	}*/
	
	@Override 
	public void setValue(AlignmentProperty.YSticky value) {
		if(!value.equals(get())) super.setValue(value);
		else fireValueChangedEvent();
	}

}
