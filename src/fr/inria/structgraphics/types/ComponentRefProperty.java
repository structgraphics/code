package fr.inria.structgraphics.types;


public class ComponentRefProperty<T> extends RefProperty<T> {

	public ComponentRefProperty(Object bean, String name, T value) {
		super(bean, name, value);
	}
	
}

