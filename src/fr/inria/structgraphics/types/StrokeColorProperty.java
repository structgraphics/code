package fr.inria.structgraphics.types;

import javafx.scene.paint.Color;

public class StrokeColorProperty extends ColorProperty {

	public StrokeColorProperty(java.lang.Object bean) {
		super(bean, "stroke", Color.GRAY);
	}
	
	public StrokeColorProperty(java.lang.Object bean, Color col) {
		super(bean, "stroke", col);
	}
}
