package fr.inria.structgraphics.types;

import fr.inria.structgraphics.graphics.Mark;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class RatioProperty extends SimpleDoubleProperty implements Shareable {

	protected final static double EPSILON = .1;
	
	public RatioProperty(Object bean, String name, double val) {
		super(bean, name, val);
	}
	
	@Override
	public String getName() {
		if(getBean() instanceof Mark) { 
			return ((Mark)getBean()).getNestingPropertyName(super.getName());
		} else return super.getName();
	}
	
	@Override 
	public void setValue(Number value) {
		if(!value.equals(get())) super.setValue(value);
		else fireValueChangedEvent();
	}
	
	private BooleanProperty activeProperty = new SimpleBooleanProperty(true);

	@Override
	public BooleanProperty getActiveProperty() {
		return activeProperty;
	}
	
	@Override
	public boolean isSimilar(Shareable property) {
		if(property instanceof RatioProperty) {
			if(Math.abs(((RatioProperty) property).get() - get()) < EPSILON) return true;
		}
		
		return false;
	}
	
	
	protected BooleanProperty publicProperty = new SimpleBooleanProperty(true);
	
	@Override
	public BooleanProperty getPublicProperty() {
		return publicProperty;
	}
	
	protected BooleanProperty hiddenProperty = new SimpleBooleanProperty(false);
	
	@Override
	public BooleanProperty getHiddenProperty() {
		return hiddenProperty;
	}
	
}
