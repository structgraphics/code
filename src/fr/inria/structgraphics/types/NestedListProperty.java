package fr.inria.structgraphics.types;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.Property;
import javafx.collections.FXCollections;

public class NestedListProperty extends FlexibleListProperty {
	
	public NestedListProperty(Object bean, FlexibleListProperty property) {
		super(bean, property.getName(), FXCollections.observableArrayList(new ArrayList<FlexibleListProperty>()));
		add(property);
	}


	public FlexibleListProperty getPropertyList(int i) {
		return (FlexibleListProperty)get(i);
	}
	
}
