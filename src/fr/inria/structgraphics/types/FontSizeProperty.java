package fr.inria.structgraphics.types;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class FontSizeProperty extends SimpleDoubleProperty implements Shareable {

	protected final static double EPSILON = .3;
	
	public FontSizeProperty(Object bean, double val) {
		super(bean, "fontsize", val);
	}

	private BooleanProperty activeProperty = new SimpleBooleanProperty(true);

	@Override
	public BooleanProperty getActiveProperty() {
		return activeProperty;
	}
	
	@Override
	public boolean isSimilar(Shareable property) {
		if(property instanceof FontSizeProperty ) {
			if(Math.abs(((FontSizeProperty ) property).get() - get()) < EPSILON) return true;
		}
		
		return false;
	}
	
	
	protected BooleanProperty publicProperty = new SimpleBooleanProperty(true);
	
	@Override
	public BooleanProperty getPublicProperty() {
		return publicProperty;
	}
	
	protected BooleanProperty hiddenProperty = new SimpleBooleanProperty(false);
	
	@Override
	public BooleanProperty getHiddenProperty() {
		return hiddenProperty;
	}
}
