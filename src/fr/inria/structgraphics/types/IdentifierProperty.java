package fr.inria.structgraphics.types;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

public class IdentifierProperty extends SimpleStringProperty implements Shareable {

	public IdentifierProperty(Object bean, String name) {
		super(bean, name);
	}
		
	@Override 
	public void setValue(String value) {
		if(!value.equals(get())) super.setValue(value);
		else {
			fireValueChangedEvent();
		}
	}
	
	private BooleanProperty activeProperty = new SimpleBooleanProperty(true);

	@Override
	public BooleanProperty getActiveProperty() {
		return activeProperty;
	}
	
	@Override
	public boolean isSimilar(Shareable property) {
		if(property instanceof IdentifierProperty) {
			return getValue().equals(((IdentifierProperty) property).getValue());
		}
		
		return false;
	}
	
	
	protected BooleanProperty publicProperty = new SimpleBooleanProperty(true);
	
	@Override
	public BooleanProperty getPublicProperty() {
		return publicProperty;
	}
	
	protected BooleanProperty hiddenProperty = new SimpleBooleanProperty(false);
	
	@Override
	public BooleanProperty getHiddenProperty() {
		return hiddenProperty;
	}
}
