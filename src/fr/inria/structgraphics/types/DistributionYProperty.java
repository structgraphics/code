package fr.inria.structgraphics.types;

public class DistributionYProperty extends DistributionProperty {

	public DistributionYProperty(Object bean) {
		super(bean, "distribution-y", Constraint.None);
	}
	
	public DistributionYProperty(Object bean, Constraint value) {
		super(bean, "distribution-y", value);
	}
}
