package fr.inria.structgraphics.types;

public class XProperty extends CoordinateProperty {

	public XProperty(java.lang.Object bean) {
		super(bean, "x", 0);
	}
	
	public XProperty(java.lang.Object bean, double value) {
		super(bean, "x", value);
	}
	

}
