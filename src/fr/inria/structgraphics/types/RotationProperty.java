package fr.inria.structgraphics.types;

public class RotationProperty extends AngleProperty {

	public RotationProperty(java.lang.Object bean) {
		super(bean, "rotation", 0);
	}
	
	public RotationProperty(java.lang.Object bean, double value) {
		super(bean, "rotation", value);
	}
}
