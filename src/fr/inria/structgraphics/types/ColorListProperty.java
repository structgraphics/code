package fr.inria.structgraphics.types;

import java.util.ArrayList;
import javafx.collections.FXCollections;

public class ColorListProperty extends FlexibleListProperty{

	public ColorListProperty(Object bean, ColorProperty property) {
		super(bean, property.getName(), FXCollections.observableArrayList(new ArrayList<ColorProperty>()));
		add(property);
	}
	
}
