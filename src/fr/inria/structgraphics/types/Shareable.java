package fr.inria.structgraphics.types;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;

public interface Shareable {	
	public BooleanProperty getActiveProperty();
	public boolean isSimilar(Shareable property);
	public BooleanProperty getPublicProperty();
	public BooleanProperty getHiddenProperty();
}
