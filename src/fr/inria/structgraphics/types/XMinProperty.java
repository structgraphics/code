package fr.inria.structgraphics.types;

public class XMinProperty extends DistanceProperty {

	public XMinProperty(java.lang.Object bean) {
		super(bean, "min-x", 0);
	}
	
	public XMinProperty(java.lang.Object bean, double value) {
		super(bean, "min-x", value);
	}
}
