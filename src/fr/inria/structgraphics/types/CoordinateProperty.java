package fr.inria.structgraphics.types;

public class CoordinateProperty extends ConstrainedDoubleProperty {
	
	public CoordinateProperty(Object bean, String name, double val) {
		super(bean, name, val);
	}
}
