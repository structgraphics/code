package fr.inria.structgraphics.types;

import fr.inria.structgraphics.graphics.Mark;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

public class ColoringSchemeProperty extends SimpleObjectProperty<ColoringSchemeProperty.Scheme> implements Shareable {

	public enum Scheme {
		Common, Source, Destination 
	}
	
	public ColoringSchemeProperty(Object bean, ColoringSchemeProperty.Scheme value) {
		super(bean, "coloring", value);
	}
	
	@Override
	public String getName() {
		if(getBean() instanceof Mark) { 
			return ((Mark)getBean()).getNestingPropertyName(super.getName());
		} else return super.getName();
	}
	
	@Override 
	public void setValue(ColoringSchemeProperty.Scheme value) {
		if(!value.equals(get())) super.setValue(value);
		else fireValueChangedEvent();
	}
	
	
	private BooleanProperty activeProperty = new SimpleBooleanProperty(true);

	@Override
	public BooleanProperty getActiveProperty() {
		return activeProperty;
	}

	@Override
	public boolean isSimilar(Shareable property) {
		if(property instanceof ColoringSchemeProperty) {
			return getValue().equals(((ColoringSchemeProperty) property).getValue());
		}
		
		return false;
	}
	
	
	protected BooleanProperty publicProperty = new SimpleBooleanProperty(true);
	
	@Override
	public BooleanProperty getPublicProperty() {
		return publicProperty;
	}
	
	protected BooleanProperty hiddenProperty = new SimpleBooleanProperty(false);
	
	@Override
	public BooleanProperty getHiddenProperty() {
		return hiddenProperty;
	}
}
