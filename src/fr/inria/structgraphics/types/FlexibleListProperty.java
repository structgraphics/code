package fr.inria.structgraphics.types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.graphics.VisCollection;
import fr.inria.structgraphics.graphics.VisGroup;
import fr.inria.structgraphics.ui.inspector.util.PropertyCloner;
import fr.inria.structgraphics.ui.utils.GroupBinding;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

public class FlexibleListProperty extends SimpleListProperty<Property> implements Shareable {

	private boolean protectFullName = false;
	
	protected FlexibleListProperty(Object bean, String name, ObservableList<Property> observableArrayList) {
		super(bean, name, observableArrayList);
	}

	public static FlexibleListProperty createList(Object bean, Property property, boolean protectName) {
		FlexibleListProperty list = createList(bean, property);
		if(protectName) list.protectFullName = true;
		return list;
	}
	
	public static FlexibleListProperty createList(Object bean, List<Property> properties, boolean protectName) {
		FlexibleListProperty list = createList(bean, properties);
		if(protectName) list.protectFullName = true;
		return list;
	}
	
	
	public static FlexibleListProperty createList(Object bean, Property property) {
		FlexibleListProperty listProperty = null;
		
		if(property instanceof DoubleProperty) {
			listProperty = new DoubleListProperty(bean, (DoubleProperty)property);
		} else if(property instanceof RefProperty) {
			listProperty = new ObjectListProperty(bean, (RefProperty)property);
		} else if(property instanceof ColorProperty) {
			listProperty = new ColorListProperty(bean, (ColorProperty)property);
		} else if(property instanceof AlignmentProperty) {
			listProperty = new ObjectListProperty(bean, (AlignmentProperty)property);
		} else if(property instanceof ShapeProperty) {
			listProperty = new ObjectListProperty(bean, (ShapeProperty)property);
		} else if (property instanceof LineTypeProperty) {
			listProperty = new ObjectListProperty(bean, (LineTypeProperty)property);
		} else if(property instanceof DistributionProperty) {
			listProperty = new ObjectListProperty(bean, (DistributionProperty)property);
		}
		else if(property instanceof TextProperty) {
			listProperty = new StringListProperty(bean, (TextProperty)property);
		}
		else if(property instanceof FlexibleListProperty) {
			listProperty = new NestedListProperty(bean, (FlexibleListProperty)property);
		} else if(property instanceof StringProperty) {
			listProperty = new StringListProperty(bean, (StringProperty)property);
		}
				
		return listProperty;
	}
	
	
	public static FlexibleListProperty createList(Object bean, List<Property> properties) {
		if(properties.isEmpty()) return null;
		
		FlexibleListProperty listProperty = createList(bean, properties.get(0));
		for(int i=1; i<properties.size(); ++i)
			listProperty.add(properties.get(i));
		
		return listProperty;
	}
	
	/*
	 * This was added to fix reassure that properties within groups appear with full name when shown within collections
	 */
	
	@Override
	public String getName() {
		//System.err.println(super.getName() + "  " + getBean());
		if(!protectFullName && getBean() instanceof VisGroup) return new PropertyName(super.getName()).getNoPostfixName();
		else return super.getName();
	}
	
	public void addEach(List<Property> properties) {
		for(Property prop: properties)
			add(prop);
	}
	
	public void addEach(int index, List<Property> properties) {
		for(Property prop: properties)
			add(index, prop);
	}
	
	public boolean allEqual() {
		if(isEmpty()) return true;
		
		Object val = get(0).getValue();
		for(int i = 1; i < size(); ++i ) {
			if(!get(i).getValue().equals(val)) return false;
		}
		
		return true;
	}
	
	public Object firstValue() {
		if(isEmpty()) return null;
		else return get(0).getValue();
	}
	
	public Object mode() {
		HashMap<Object, Integer> map = new HashMap<>();
		
		// The integer value of the map will contain the number of occurences of the property value
		for(Property property:get()) {
			Object val = property.getValue();
			if(map.containsKey(val)) 
				map.put(val, map.get(val) + 1);
			else map.put(val, 1);
		}
		
		int max = 0;
		Object val = null;
		for(Object key: map.keySet()) {
			int count = map.get(key);
			if(count > max) val = key;
		}
		
		return val;
	}
	
	public PropertyName getPropertyName() {
		return new PropertyName(getName());
	}
	
	@Override
	public Object getBean() {
		Object object = super.getBean();
		if(object instanceof Mark) {
			Mark mark = (Mark)object;
			if(mark.getContainer() instanceof VisGroup) {
				return mark.getTopGroup();
			}
		}
		
		return super.getBean();
	}
	
	/*
	@Override
	public String getName() {
		if(getBean() instanceof Mark) { 
			return ((Mark)getBean()).getNestingPrefix(super.getName()); // TODO
		} else return super.getName();
	}*/

	
	public void bindElements(FlexibleListProperty property) {
		for(int i = 0; i < size() && i < property.size(); ++i) {
			Property prop1 = get(i);
			Property prop2 = property.get(i);
			
			if(prop1 instanceof FlexibleListProperty) ((FlexibleListProperty)prop1).bindElements((FlexibleListProperty)prop2);
			else prop1.bindBidirectional(prop2);
		}
	}

	public void unbindElements(FlexibleListProperty property) {
		for(int i = 0; i < size() && i < property.size(); ++i) {
			Property prop1 = get(i);
			Property prop2 = property.get(i);
			
			if(prop1 instanceof FlexibleListProperty) ((FlexibleListProperty)prop1).unbindElements((FlexibleListProperty)prop2);
			else prop1.unbindBidirectional(prop2);
		}
	}
	
	
	public String getValueAsString() {
		StringBuffer buffer = new StringBuffer("[ ");
		
		String val;
		Property property = get(0);
		if(property instanceof FlexibleListProperty)
			val = ((FlexibleListProperty)property).getValueAsString();
		else val = property.getValue().toString();
		
		if(size() == 1) buffer.append(val);
		else if(size() > 1) buffer.append(val + "... ");
		
		buffer.append(" ]");				
		return buffer.toString();		
	}
	
		
	public boolean removeProperty(Property property) {
		for(Property p: get()) {
			if(p instanceof FlexibleListProperty) {
				if(((FlexibleListProperty) p).removeProperty(property)) { 
					return true;
				} 
			}
			else if(remove(property)) {
				return true;
			}
		}
		
		return false;
	}
	
	
	public List<FlexibleListProperty> transpose(){
		List<FlexibleListProperty> list = new ArrayList<>();
		
		for(Property property:this) {
			List<Property> flat; 
			if(property instanceof NestedListProperty)
				flat = ((NestedListProperty)property).flatten();	
			else flat = ((FlexibleListProperty)property).getValue();
			
			for(int i = 0; i < flat.size(); ++i) {
				FlexibleListProperty listProperty;// = list.get(i);
				if(list.size() <= i) {
					listProperty  = FlexibleListProperty.createList(getBean(), flat.get(i), protectFullName); // TODO: Check...
					
					list.add(listProperty);
				} else {
					listProperty = list.get(i);
					listProperty.add(flat.get(i));
				}
			}
		}

		return list;
	}
	
	
	public List<Property> flatten(){
		List<Property> list = new ArrayList<>();
		
		for(Property property:this) {
			if(property instanceof NestedListProperty) {
				list.addAll(((NestedListProperty)property).flatten());
			}
			else if(property instanceof FlexibleListProperty) {
				list.addAll(((FlexibleListProperty)property).get());
			}
			else list.add(property);
		}
		
		return list;
	}
	
	
	public FlexibleListProperty flattenFlex(){
		FlexibleListProperty list = createList(getBean(), flatten(), protectFullName);
		return list;
	}
	
		
	private Property getCompact(List<Mark> marks) {
		VisCollection tmpgroup = null, vgroup = null;
		boolean iscommon = false;
		
		for(Mark mark:marks) {
			tmpgroup = null;
			if(mark instanceof VisCollection) {
				tmpgroup = (VisCollection)mark;
				SortedSet<PropertyName> variable = tmpgroup.getChildPropertyStructure().getVariable();
				if(variable.contains(getPropertyName())) {
					vgroup = tmpgroup;
					break;
				}
			}
		}
		
		if(tmpgroup == null) return this;
		if(vgroup == null) {
			iscommon = true;
			vgroup = (VisCollection)marks.get(0);
		}
		
		if(iscommon && size() > 0) { 
			Property property = get(0);
			if(property instanceof FlexibleListProperty) {
				return ((FlexibleListProperty)property).getCompact(vgroup.getComponentAt(0));
			} else return property;
		} else {
			List<Property> list = new ArrayList<>();
			for(Property property: this) {
				if(property instanceof FlexibleListProperty) {
					list.add(((FlexibleListProperty)property).getCompact(vgroup.getComponentAt(0)));
				} else list.add(property);
			}
			
			return createList(getBean(), list, protectFullName);
		}				
	}
	
	
	public Property getCompact(Container refcontainer) {
		if(!(refcontainer instanceof VisCollection)) return this; 
		
		SortedSet<PropertyName> common = ((VisCollection)refcontainer).getChildPropertyStructure().getCommon();
		
		if(common.contains(getPropertyName())) { 
			Property property = get(0);
			if(property instanceof FlexibleListProperty) {
				return ((FlexibleListProperty)property).getCompact(refcontainer.getComponents());
			} else return property;
		} else {
			List<Property> list = new ArrayList<>();
			for(Property property: this) {
				if(property instanceof FlexibleListProperty) {
					list.add(((FlexibleListProperty)property).getCompact(refcontainer.getComponents()));
				} else list.add(property);
			}
			return createList(getBean(), list, protectFullName);
		}				
	}
	
	
	public Property getCompact2(Container refcontainer) {
		if(!(refcontainer instanceof VisCollection)) return this; 
		
		SortedSet<PropertyName> common = ((VisCollection)refcontainer).getChildPropertyStructure().getCommon();
		
		List<Property> list = new ArrayList<>();
		for(Property property: this) {
			if(property instanceof FlexibleListProperty) {
				list.add(((FlexibleListProperty)property).getCompact(refcontainer.getComponents()));
			} else list.add(property);
		}
		return createList(getBean(), list, protectFullName);				
	}
	
	public FlexibleListProperty getExtended() {
		List<Property> list = new ArrayList<>();
		
		for(Property property : flatten()) {
			Mark mark = (Mark)property.getBean();
			int count = mark.leafs();
			for(int i = 0 ; i < count; ++i)
				list.add(property);
		}
		
		return createList(getBean(), list, protectFullName);
	}

	
	public boolean varies(Container container) {
		if(!(container instanceof VisCollection)) return false;
		
		SortedSet<PropertyName> variable = ((VisCollection)container).getChildPropertyStructure().getVariable();
		if(variable.contains(getPropertyName())) return true;
		else {
			for(Mark mark:container.getComponents()) {
				if(varies(mark)) return true;
			}
			
			return false;
		}
	}
	
	
	// The following methods help to deal with the binding of groups of equally valued properties for groups
	public ArrayList<GroupBinding> createGroupBindings(){
		ArrayList<ArrayList<Property>> groupings = new ArrayList<>();
		for(Property property: this) {
			boolean found = false;
			for(ArrayList<Property> grouping: groupings) {				
				if(((Shareable)grouping.get(0)).isSimilar((Shareable)property) /*grouping.get(0).getValue().equals(property.getValue())*/) {
					property.setValue(grouping.get(0).getValue());
					grouping.add(property);
					found = true;
					break;
				}
			}
			if(!found) {
				ArrayList<Property> grouping = new ArrayList<>();
				grouping.add(property);
				groupings.add(grouping);
			}
		}
		
		ArrayList<GroupBinding> bindings = new ArrayList<>();
		for(ArrayList<Property> grouping:groupings) {
			GroupBinding binding = new GroupBinding(grouping);
			bindings.add(binding);
		}
		
		return bindings;
	}

	
	@Override
	public BooleanProperty getActiveProperty() {
		if(isEmpty() || !(get(0) instanceof Shareable)) return new SimpleBooleanProperty(false);
		else return ((Shareable)get(0)).getActiveProperty();		
	}

	@Override
	public boolean isSimilar(Shareable property) {
		return equals(property);
	}

	@Override
	public BooleanProperty getPublicProperty() {
		if(isEmpty() || !(get(0) instanceof Shareable)) return new SimpleBooleanProperty(false);
		else return ((Shareable)get(0)).getPublicProperty();		
	}

	@Override
	public BooleanProperty getHiddenProperty() {
		if(isEmpty() || !(get(0) instanceof Shareable)) return new SimpleBooleanProperty(false);
		else return ((Shareable)get(0)).getHiddenProperty();
	}

	
	public Property getByName(PropertyName pname) {
		for(Property prop:this) {
			String name_ = prop.getName();
			if(name_ != null && pname.toString().equals(name_)) return prop;
		}
		
		return null;
	}
}
