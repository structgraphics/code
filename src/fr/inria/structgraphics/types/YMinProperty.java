package fr.inria.structgraphics.types;

public class YMinProperty extends DistanceProperty {

	public YMinProperty(java.lang.Object bean) {
		super(bean, "min-y", 0);
	}
	
	public YMinProperty(java.lang.Object bean, double value) {
		super(bean, "min-y", value);
	}
}
