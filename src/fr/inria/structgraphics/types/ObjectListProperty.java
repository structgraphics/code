package fr.inria.structgraphics.types;

import java.util.ArrayList;
import javafx.beans.property.ObjectProperty;
import javafx.collections.FXCollections;

public class ObjectListProperty extends FlexibleListProperty {

	public ObjectListProperty(Object bean, ObjectProperty property) {
		super(bean, property.getName(), FXCollections.observableArrayList(new ArrayList<ObjectProperty>()));
		add(property);
	}

}
