package fr.inria.structgraphics.types;

public class DistributionXProperty extends DistributionProperty {

	public DistributionXProperty(Object bean) {
		super(bean, "distribution-x", Constraint.None);
	}
	
	public DistributionXProperty(Object bean, Constraint value) {
		super(bean, "distribution-x", value);
	}
}
