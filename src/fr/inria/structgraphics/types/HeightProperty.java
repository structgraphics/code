package fr.inria.structgraphics.types;

public class HeightProperty extends DistanceProperty {

	public HeightProperty(java.lang.Object bean) {
		super(bean, "height", 0);		
	}
	
	public HeightProperty(java.lang.Object bean, double value) {
		super(bean, "height", value);		
	}
	
}
