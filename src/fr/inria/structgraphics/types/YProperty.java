package fr.inria.structgraphics.types;

public class YProperty extends CoordinateProperty {

	public YProperty(java.lang.Object bean) {
		super(bean, "y", 0);
	}
	
	public YProperty(java.lang.Object bean, double value) {
		super(bean, "y", value);
	}
}
