package fr.inria.structgraphics.types;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefY;

public class ComponentRefYProperty extends ComponentRefProperty<RefY> {

	public ComponentRefYProperty(Object bean) {
		super(bean, "reference-y", RefY.Bottom);
	}
	
	public ComponentRefYProperty(Object bean, RefY value) {
		super(bean, "reference-y", value);
	}
	
	@Override 
	public void setValue(RefY value) {
		if(!value.equals(get())) super.setValue(value);
		else fireValueChangedEvent();
	}
}
