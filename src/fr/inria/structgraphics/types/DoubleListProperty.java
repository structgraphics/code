package fr.inria.structgraphics.types;

import java.util.ArrayList;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.Property;
import javafx.collections.FXCollections;

public class DoubleListProperty extends FlexibleListProperty {

	protected DoubleListProperty(Object bean, DoubleProperty property) {
		super(bean, property.getName(), FXCollections.observableArrayList(new ArrayList<DoubleProperty>()));
		add(property);
	}
		
	public DoubleListProperty(Object bean, String name) {
		super(bean, name, FXCollections.observableArrayList(new ArrayList<DoubleProperty>()));
	}
	
	public double average() {
		double sum = 0;
		for(Property property:get()) {
			sum += ((DoubleProperty)property).doubleValue();
		}

		return sum/size();
	}
	
	public double variance(double mean) {
		double sum = 0;
		
		for(Property property:get()) {
			sum += Math.pow(((DoubleProperty)property).doubleValue() - mean, 2);
		}

		return sum/size();
	}
}
