package fr.inria.structgraphics.types;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;

public class ContainerRefXProperty extends ContainerRefProperty<RefX> {

	public ContainerRefXProperty(Object bean) {
		super(bean, "reference-x", RefX.Left);
	}
	
	public ContainerRefXProperty(Object bean, RefX value) {
		super(bean, "reference-x", value);
	}
	
	@Override 
	public void setValue(RefX value) {
		if(!value.equals(get())) super.setValue(value);
		else fireValueChangedEvent();
	}
	
}
