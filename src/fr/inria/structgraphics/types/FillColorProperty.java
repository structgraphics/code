package fr.inria.structgraphics.types;

import javafx.scene.paint.Color;

public class FillColorProperty extends ColorProperty {

	public FillColorProperty(java.lang.Object bean) {
		super(bean, "fill", Color.WHITE);
	}
	
	public FillColorProperty(java.lang.Object bean, Color col) {
		super(bean, "fill", col);
	}
}
