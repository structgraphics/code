package fr.inria.structgraphics.types;

import fr.inria.structgraphics.graphics.Container;
import fr.inria.structgraphics.graphics.Mark;
import fr.inria.structgraphics.graphics.VisBody;
import fr.inria.structgraphics.ui.viscanvas.groupings.FlowConnection;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class ConstrainedDoubleProperty extends SimpleDoubleProperty implements Shareable {

	protected final static double EPSILON = 5;
	
	public ConstrainedDoubleProperty(Object bean, String name, double val) {
		super(bean, name, val);
	}
	
	@Override
	public String getName() {
		if(getBean() instanceof Mark) { 
			return ((Mark)getBean()).getNestingPropertyName(super.getName());
		} else return super.getName();
	}
	
	@Override 
	public void setValue(Number value) {
		if(!value.equals(get())) super.setValue(value);
		else {
			fireValueChangedEvent();
		}
	}
	
	private BooleanProperty activeProperty = new SimpleBooleanProperty(true);

	@Override
	public BooleanProperty getActiveProperty() {
		return activeProperty;
	}
	
	protected BooleanProperty publicProperty = new SimpleBooleanProperty(true);
	
	@Override
	public BooleanProperty getPublicProperty() {
		return publicProperty;
	}
	
	protected BooleanProperty hiddenProperty = new SimpleBooleanProperty(false);
	
	@Override
	public BooleanProperty getHiddenProperty() {
		return hiddenProperty;
	}
	
	public void updateValue(double value) {
		this.setValue(value);
	
		if(getBean() instanceof Mark) {
			Container container = ((Mark)getBean()).getContainer();
			if(container instanceof VisBody) {
				((VisBody)container).updateLayout((Mark)getBean());
			}	
		} else if(getBean() instanceof FlowConnection) {
			Container container = ((FlowConnection)getBean()).getOrigin().getContainer();
			if(container instanceof VisBody) {
				((VisBody)container).updateLayout(((FlowConnection)getBean()).getOrigin());
			}
		}
	}

	@Override
	public boolean isSimilar(Shareable property) {
		if(property instanceof ConstrainedDoubleProperty) {
			if(Math.abs(((ConstrainedDoubleProperty) property).get() - get()) < EPSILON) return true;
		}
		
		return false;
	}

}
