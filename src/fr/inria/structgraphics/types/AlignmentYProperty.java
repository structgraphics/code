package fr.inria.structgraphics.types;

import javafx.beans.property.Property;

public class AlignmentYProperty extends AlignmentProperty<AlignmentProperty.XSticky> {

	static int counter = 0;
	
	public AlignmentYProperty(Object bean) {
		super(bean, "sticky-x", XSticky.No);
	}
	
	public AlignmentYProperty(Object bean, XSticky value) {
		super(bean, "sticky-x", value);
	}
	
	/*
	public String getIconName() {
		return "y-" + getValue().toString().toLowerCase();
	}*/
	
	@Override 
	public void setValue(AlignmentProperty.XSticky value) {		
		if(!value.equals(get())) super.setValue(value);
		else fireValueChangedEvent();
	}
	
}
