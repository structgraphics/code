package fr.inria.structgraphics.types;

public class DeltaYProperty extends DistanceProperty {

	public DeltaYProperty(java.lang.Object bean) {
		super(bean, "delta-y", 0);
	}
	
	public DeltaYProperty(java.lang.Object bean, double value) {
		super(bean, "delta-y", value);
	}
}
