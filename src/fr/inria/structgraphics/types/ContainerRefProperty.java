package fr.inria.structgraphics.types;

public class ContainerRefProperty<T> extends RefProperty<T> {

	public ContainerRefProperty(Object bean, String name, T value) {
		super(bean, name, value);
	}

}
