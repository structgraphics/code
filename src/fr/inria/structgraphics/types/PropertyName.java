package fr.inria.structgraphics.types;

import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PropertyName implements Comparable<PropertyName>{
	
	private static Map<String, Integer> order = createOrderMap();	
	private static String prefixes = "ABCDEFGHIKLMNOP";
	
	private String name, groupingName, cleanName;
	private int level = 0;
	private String postfix = null; 
	
	public PropertyName(String name) { 
		this.name = name;

		groupingName = name.replaceAll("(\\.*[0-9]+)+", "");
		
		cleanName = name.replaceAll("[A-Z].", "").replaceAll("[0-9]+", "").replaceAll("\\.", "");

		int index = name.indexOf(".");		
		if(index >= 0) {
			level = prefixes.indexOf(name.substring(0, index)) + 1;
		}
		else level = 0;
		
		Pattern p = Pattern.compile("[0-9]");
		Matcher m = p.matcher(name);
		if (m.find()) {
			postfix = name.substring(m.start());
		}
	}

	public static String getRegExpr(String varname) {
		return "([A-Z]\\.)?" + varname;
	}
		
	@Override
	public int compareTo(PropertyName prop) {
		int res = 0;
		
		if(postfix != null && prop.postfix != null) res = postfix.compareTo(prop.postfix);
		
		if(name.endsWith("id") && !prop.name.endsWith("id")) return -1;
		else if(!name.endsWith("id") && prop.name.endsWith("id")) return 1;
		else if(level < prop.level) return 1;
		else if(level > prop.level) return -1;
		else if(res != 0) return res;
		else {
			return PropertyName.order.get(cleanName) - PropertyName.order.get(prop.cleanName);
		}
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof PropertyName){
			return name.equals(((PropertyName)o).name);
		}
		else return false;
	}

	@Override
	public String toString() {
		return name;
	}
	
	public String getNoPostfixName() {
		return groupingName;
	}
	
	public String getCleanName() {
		return cleanName;
	}
	
	public static String getCleanName(String name) {
		return name.replaceAll("[0-9]+", "").replaceAll("\\.[0-9]+", "");		
	}

	public int getLevel() {
		return level;
	}
	
	private static Map<String, Integer> createOrderMap(){
		Map<String, Integer> map = new TreeMap<>();
		map.put("id", 0);
		map.put("order", 1);
		map.put("source", 2);
		map.put("destination", 3);
		
		map.put("weight", 4);
		
		map.put("text", 5);
		map.put("fontsize", 7);
		
		map.put("sticky-x", 10);
		map.put("sticky-y", 20);

		map.put("distribution-x", 24);
		map.put("delta-x", 25);		
		
		map.put("distribution-y", 26);
		map.put("delta-y", 27);
			
		map.put("reference-x", 40);		
		map.put("reference-y", 50);

		map.put("x", 60);		
		map.put("y", 70);
		
		map.put("min-x", 73);		
		map.put("min-y", 74);
				
		map.put("width", 80);		
		map.put("height", 90);
		map.put("h-w ratio", 92);

		map.put("shape", 94);
		map.put("curve", 96);
		
		map.put("text", 102);
		map.put("fontsize", 104);

		map.put("coloring", 110);		
		
		map.put("fill", 112);		
		map.put("stroke", 114);
		map.put("opacity", 116);

		
		map.put("thickness", 120);		
		map.put("rotation", 130);

		return map;
	}

	public static String getPrefix(int level) {
		return prefixes.charAt(level - 1) + ".";
	}
	
	public String getPostfix() {
		return postfix;
	}
	
	public boolean isX() {
		return cleanName.equals("x");
	}
	
	public boolean isY() {
		return cleanName.equals("y");
	}
	
	public boolean isID() {
		return cleanName.equals("id") || cleanName.equals("source") || cleanName.equals("destination"); 
	}
	
	public boolean isWidth() {
		return cleanName.equals("width");
	}
	
	public boolean isHeight() {
		return cleanName.equals("height");
	}
	
	public boolean isText() {
		return cleanName.equals("text");
	}
	
	public boolean isFontSize() {
		return cleanName.equals("fontsize");
	}

	public boolean isStickyX() {
		return cleanName.equals("sticky-x");
	}
	
	public boolean isStickyY() {
		return cleanName.equals("sticky-y");
	}
	
	public boolean isDistributionX() {
		return cleanName.equals("distribution-x");
	}
	
	public boolean isDistributionY() {
		return cleanName.equals("distribution-y");
	}
	
	public boolean isDeltaX() {
		return cleanName.equals("delta-x");
	}
	
	public boolean isDeltaY() {
		return cleanName.equals("delta-y");
	}
	
	public boolean isReferenceX() {
		return cleanName.equals("reference-x");
	}
	
	public boolean isReferenceY() {
		return cleanName.equals("reference-y");
	}
	
	public boolean isShape() {
		return cleanName.equals("shape");
	}
	
	public boolean isFill() {
		return cleanName.equals("fill");
	}
	
	public boolean isStroke() {
		return cleanName.equals("stroke");
	}
	
	public boolean isThickness() {
		return cleanName.equals("thickness");
	}
	
	public boolean isRotation() {
		return cleanName.equals("rotation");
	}
	
	public boolean isOpacity() {
		return cleanName.equals("opacity");
	}
	
	public boolean isColoring() {
		return cleanName.equals("coloring");
	}
	
	public boolean isCurve() {
		return cleanName.equals("curve");
	}
}
