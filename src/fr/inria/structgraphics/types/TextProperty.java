package fr.inria.structgraphics.types;

import fr.inria.structgraphics.graphics.Mark;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

public class TextProperty extends SimpleStringProperty implements Shareable {
	public TextProperty(java.lang.Object bean, String value) {
		super(bean, "text", value);
	}
	
	@Override
	public String getName() {
		if(getBean() instanceof Mark) {
			return ((Mark)getBean()).getNestingPropertyName(super.getName());
		} else return super.getName();
	}
	
	@Override 
	public void setValue(String value) {
		if(!value.equals(get())) super.setValue(value);
		else fireValueChangedEvent();
	}
	
	private BooleanProperty activeProperty = new SimpleBooleanProperty(true);

	@Override
	public BooleanProperty getActiveProperty() {
		return activeProperty;
	}

	@Override
	public boolean isSimilar(Shareable property) {
		if(property instanceof TextProperty) {
			return getValue().equals(((TextProperty) property).getValue());
		}
		
		return false;
	}
	
	
	
	protected BooleanProperty publicProperty = new SimpleBooleanProperty(true);
	
	@Override
	public BooleanProperty getPublicProperty() {
		return publicProperty;
	}
	
	protected BooleanProperty hiddenProperty = new SimpleBooleanProperty(false);
	
	@Override
	public BooleanProperty getHiddenProperty() {
		return hiddenProperty;
	}
}
