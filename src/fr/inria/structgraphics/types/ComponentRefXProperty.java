package fr.inria.structgraphics.types;

import fr.inria.structgraphics.graphics.ReferenceCoords.RefX;

public class ComponentRefXProperty extends ComponentRefProperty<RefX> {

	public ComponentRefXProperty(Object bean) {
		super(bean, "reference-x", RefX.Left);
	}
	
	public ComponentRefXProperty(Object bean, RefX value) {
		super(bean, "reference-x", value);
	}
	
	
	@Override 
	public void setValue(RefX value) {
		if(!value.equals(get())) super.setValue(value);
		else fireValueChangedEvent();
	}
}
