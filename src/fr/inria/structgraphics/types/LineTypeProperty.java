package fr.inria.structgraphics.types;

import fr.inria.structgraphics.graphics.Mark;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

public class LineTypeProperty extends SimpleObjectProperty<LineTypeProperty.Type> implements Shareable {
	
	public enum Type {
		None, Straight, Bezier, StraightSolid, BezierSolid, Area, TopBottom
	}
	
	public LineTypeProperty(Object bean, Type value) {
		super(bean, "curve", value);
	}
	
	@Override
	public String getName() {
		if(getBean() instanceof Mark) { 
			return ((Mark)getBean()).getNestingPropertyName(super.getName());
		} else return super.getName();
	}
	
	@Override 
	public void setValue(LineTypeProperty.Type value) {
		if(!value.equals(get())) super.setValue(value);
		else fireValueChangedEvent();
	}

	private BooleanProperty activeProperty = new SimpleBooleanProperty(true);

	@Override
	public BooleanProperty getActiveProperty() {
		return activeProperty;
	}
	
	@Override
	public boolean isSimilar(Shareable property) {
		if(property instanceof LineTypeProperty) {
			return getValue().equals(((LineTypeProperty) property).getValue());
		}
		
		return false;
	}
	
	protected BooleanProperty publicProperty = new SimpleBooleanProperty(true);
	
	@Override
	public BooleanProperty getPublicProperty() {
		return publicProperty;
	}
	
	protected BooleanProperty hiddenProperty = new SimpleBooleanProperty(false);
	
	@Override
	public BooleanProperty getHiddenProperty() {
		return hiddenProperty;
	}
	
}
