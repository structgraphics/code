package fr.inria.structgraphics.types;

public class DistanceProperty extends ConstrainedDoubleProperty {

	public DistanceProperty(Object bean, String name, double val) {
		super(bean, name, val);
	}
	
}
