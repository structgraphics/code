package fr.inria.structgraphics.types;

public class ConnectionWeightProperty extends DistanceProperty {

	public ConnectionWeightProperty(java.lang.Object bean) {
		super(bean, "weight", 0);		
	}
	
	public ConnectionWeightProperty(java.lang.Object bean, double value) {
		super(bean, "weight", value);		
	}
	
}
