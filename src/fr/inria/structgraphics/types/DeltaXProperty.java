package fr.inria.structgraphics.types;

public class DeltaXProperty extends DistanceProperty {

	public DeltaXProperty(java.lang.Object bean) {
		super(bean, "delta-x", 0);
	}
	
	public DeltaXProperty(java.lang.Object bean, double value) {
		super(bean, "delta-x", value);
	}
	
}
