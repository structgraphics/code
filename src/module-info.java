module structgraphics {
	exports fr.inria.structgraphics.graphics;
	exports fr.inria.structgraphics.graphics.controls;
	exports fr.inria.structgraphics.ui;
	exports fr.inria.structgraphics.ui.inspector.detailmodel;
	exports fr.inria.structgraphics.types;
	exports fr.inria.structgraphics.persistence;
	exports fr.inria.structgraphics.ui.viscanvas;
	exports fr.inria.structgraphics.ui.viscanvas.groupinteractors;
	exports fr.inria.structgraphics.graphics.splines;
	exports fr.inria.structgraphics.ui.spreadsheet;
	exports fr.inria.structgraphics.ui.viscanvas.groupings;
	exports fr.inria.structgraphics.ui.utils;
	exports fr.inria.structgraphics.ui.tools;
	exports fr.inria.structgraphics.ui.inspector.util;
	exports fr.inria.structgraphics.ui.inspector;
	exports fr.inria.structgraphics.ui.library;

	requires java.desktop;
	requires java.logging;
	requires javafx.base;
	requires javafx.controls;
	requires javafx.graphics;
	requires javafx.media;
	requires javafx.web;
	requires javax.json;
	requires matheclipse.core;
	requires org.controlsfx.controls;
}