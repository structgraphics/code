# StructGraphics
This project contains the Java code of [StructGraphics](https://www.lri.fr/~fanis/StructGraphics/), a visualization design tool.

StructGraphics makes use of the spreadsheet implementation of [ControlsFX ](https://github.com/controlsfx/controlsfx)(SpreadsheetView), originally developed for JDK 1.8. ControlsFX relied on some JavaFX feautures that became unavailable in later versions. Fortunately, the latest versions of JavaFX resovled this problem, and ControlsFX's developers [currently work](https://github.com/controlsfx/controlsfx/issues/1141) to bring SpreadsheetView back. Although an official release is not available, I was able to use a [temporary snapshot](https://github.com/controlsfx/controlsfx/issues/1141#issuecomment-651451806) of the library and migrate StructGraphics' code to the newest Java/JavaFX versions. 

I have tested the new code with JDK 14, JavaFX 14, and Eclipse (4.16). JavaFX, ControlsFX, and StructGraphics are structured as Java modules, and finding a way to compile and run the code can be tricky. I include a .lunch run configuration file which inlcudes the VM arguments needed to run the code:

`--module-path ${project_classpath:structgraphics} --add-modules structgraphics,org.controlsfx.controls,javafx.controls,javafx.media,javafx.web,javafx.base,javafx.graphics --add-exports=javafx.controls/com.sun.javafx.scene.control.behavior=org.controlsfx.controls
--add-exports=javafx.base/com.sun.javafx.event=org.controlsfx.controls
--add-exports=javafx.base/com.sun.javafx.event=structgraphics
--add-exports=structgraphics/fr.inria.structgraphics=javafx.graphics
--add-exports=org.controlsfx.controls/impl.org.controlsfx.i18n=structgraphics
--add-exports=org.controlsfx.controls/impl.org.controlsfx.spreadsheet=structgraphics
--add-opens=javafx.controls/javafx.scene.control.skin=org.controlsfx.controls
--add-opens=javafx.graphics/javafx.scene=org.controlsfx.controls`

The main class of the application is: `fr.inria.structgraphics.VisSketcher` 

To start with the tool, you can open existing vsualization designs found under the gallery folder (.json and .wrk). Files with a .wrk extension are also JSON files that store an entire workspace, including the library, all graphical structures, and the data on the spreedsheet. 